from mongokit import Document
from app import db
from mongokit import ValidationError
from datetime import datetime


class Roles(Document):
    __collection__ = 'roles'

    structure = {
        "name" : unicode,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Roles %r>' % (self.name)

db.register([Roles])

class Module(Document):
    __collection__ = 'module'

    structure = {
        "module_name" :unicode,
        "display_name":unicode ,
        "priority" :int,
        "class1":unicode,
        "class2":unicode,
        "url":unicode,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Module %r>' % (self.module_name)

db.register([Module])

class Permissions(Document):
    __collection__ = 'permissions'

    structure = {
        "role_id" : Roles,
        "module_id" : Module,
        "pr_read" :bool,
        "pr_add" : bool,
        "pr_edit" : bool,
        "pr_delete" : bool,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Permissions %r>' % (self._id)

db.register([Permissions])



class Users(Document):
    __collection__ = 'users'

    structure = {
        "role_id" : Roles,
        "first_name" : unicode,
        "last_name" : unicode,
        "email" : str,
        "user_name" : str,
        "password" : str,
        "phone" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Users %r>' % (self.user_name)

db.register([Users]) 

class Doctor(Document):
    __collection__ = 'doctor'

    structure = {
        "first_name" : unicode,
        "last_name" : unicode,
        "title" : str,
        "email" : str,
        "user_name" : str,
        "mci" : str,
        "password" : str,
        "phone" : str,
        "fax" : str,
        "image" : str,
        "degree_title" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Doctor %r>' % (self.first_name)

db.register([Doctor])       

# class User(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     username = db.Column(db.String(64), index=True, unique=True)
#     email = db.Column(db.String(120), index=True, unique=True)
#     password_hash = db.Column(db.String(128))
#     posts = db.relationship('Post', backref='author', lazy='dynamic')

#     def __repr__(self):
#         return '<User {}>'.format(self.username)


# class Post(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     body = db.Column(db.String(140))
#     timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
#     user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

#     def __repr__(self):
#         return '<Post {}>'.format(self.body)
