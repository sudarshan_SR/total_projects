from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextField
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class DoctorForm(FlaskForm):
	
	first_name = TextField('First Name', validators=[DataRequired()])
	last_name = TextField('Last Name', validators=[DataRequired()])
	email = StringField('Email', validators=[DataRequired()])
	# "user_name" : StringField('Username', validators=[DataRequired()])
	password = StringField('Password', validators=[DataRequired()])
	phone = StringField('Phone Number', validators=[DataRequired()])
	fax = StringField('Fax Number', validators=[DataRequired()])
	degree_title = StringField('Degree Title', validators=[DataRequired()])
	submit = SubmitField('submit')
		