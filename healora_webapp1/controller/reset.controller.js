(function () {
    'use strict';
    angular
        .module('app')
        .controller('ResetController', ResetController);

    ResetController.$inject = ['$cookieStore','UserService','$timeout', '$rootScope','$routeParams','$scope','$http','$location','FlashService'];
    function ResetController($cookieStore,UserService,$timeout, $rootScope,$routeParams, $scope,$http,$location,FlashService) {
        var vm = this;
        var key = "";
        if($routeParams.key){
             key = $routeParams.key;
             key = key.trim();
        }
        $scope.reset = 0;
        initController();
        
        function initController() {
            var param = JSON.stringify({"token":key});
            $http({
                    url: verifyToken,
                    method: "POST",
                    data: param,
                    headers: { 'Content-Type': 'application/json' }
                }).success(function (data, status, headers, config) {
                    if (data['status']!="0"){
                        $scope.reset = 1;
                    }else{
                        FlashService.Error("Your link has expired, please try again");
                        $scope.reset = 0;
                    }
                   // $location.path('/reset/key');
                }).error(function (data, status, headers, config) {
                   FlashService.Error("Something went wrong. Please try again");   
                });
                

        }
        $scope.instant = function() {
            var param = JSON.stringify({"password":vm.password2,"token":key});
            if(vm.password2!=vm.password)
            {
                FlashService.Error("Password didn't matched");
            }
            else
            {
            $http({
                    url: reset,
                    method: "POST",
                    data: param,
                    headers: { 'Content-Type': 'application/json' }
                }).success(function (data, status, headers, config) {
                    if (data['status']!="0"){
                        FlashService.Success("You have successfully changed password. Please Login!!!");
                        $timeout(function () {
                            // $scope.$digest();
                            $location.path('/register');
                        }, 2000);
                        
                        $scope.reset = 1;
                    }else{
                        FlashService.Error("Link TIME-OUT");
                        $scope.reset = 0;
                        $timeout(function () { 
                           // $location.path('/timeout');  
                        }, 2000);
                        
                        

                    }





                }).error(function (data, status, headers, config) {
                   FlashService.Error("Something went wrong. Please try again");   
                });
         //  $location.path('/');
            }
           }


        }
        
    }

)();
