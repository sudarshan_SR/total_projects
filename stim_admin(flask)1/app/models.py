__author__ = 'Sudarshan Dudhe'
# import requirement
# We are creating all tables in single file.
# In this we have structure and validators 

from mongokit import Document
from app import app, db
from mongokit import ValidationError
from datetime import datetime



class Roles(Document):
    __collection__ = 'roles'

    structure = {
        "name" : unicode,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Roles %r>' % (self.name)

db.register([Roles])

class Module(Document):
    __collection__ = 'module'

    structure = {
        "module_name" :unicode,
        "display_name":unicode ,
        "priority" :int,
        "class1":unicode,
        "class2":unicode,
        "url":unicode,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Module %r>' % (self.module_name)

db.register([Module])

class Permissions(Document):
    __collection__ = 'permissions'

    structure = {
        "role_id" : Roles,
        "module_id" : Module,
        "pr_read" :int,
        "pr_add" : int,
        "pr_edit" : int,
        "pr_delete" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Permissions %r>' % (self._id)

db.register([Permissions])


class Admin_users(Document):
    __collection__ = 'admin_user'

    structure = {
        "role_id" : Roles,
        "first_name" : unicode,
        "last_name" : unicode,
        "email" : str,
        "user_name" : str,
        "password" : str,
        "phone" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Admin_users %r>' % (self.first_name)

db.register([Admin_users])

# Classes starts here
# Main User table
class Users(Document):
    __collection__ = 'users'

    structure = {
        "first_name" : unicode,
        "last_name" : unicode,
        "email" : str,
        "user_name" : str,
        "password" : str,
        "phone" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Users %r>' % (self.first_name)

db.register([Users])





# Classes starts here
# Main User table
class UserInsta(Document):
    __collection__ = 'user_insta'

    structure = {
        "main_user_id" : Users,
        "full_name" : str,
        "profile_pic_url" : str,
        "profile_pic_id" : str,
        "following" : int,
        "user_id" : str,
        "user_name" : str,
        "password" : str,
        "email" : str,
        "phone" : str,
        "is_private" : str,
        "is_verified" : str,
        "gender" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<UserInsta %r>' % (self.user_name)

db.register([UserInsta])



# Classes starts here
# Main User table
class Target(Document):
    __collection__ = 'target'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "full_name" : str,
        "profile_pic_url" : str,
        "user_id" : str,
        "user_name" : str,
        "followers" : str,
        "is_private" : str,
        "is_verified" : str,
        "type" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Target %r>' % (self.user_name)

db.register([Target])


# Classes starts here
# Main User table
class MyPost(Document):
    __collection__ = 'my_post'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "media_id" : str,
        "image" : str,
        "comment" : str,
        "like" : str,
        "type" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<MyPost %r>' % (self.image)

db.register([MyPost])




# Classes starts here
# Main User table
class Manage(Document):
    __collection__ = 'manage'

    structure = {
        "name" : str,
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Users %r>' % (self.name)

db.register([Manage])



# Classes starts here
# Main User table
class Followers(Document):
    __collection__ = 'followers'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "full_name" : str,
        "profile_pic_url" : str,
        "user_id" : str,
        "user_name" : str,
        "is_private" : str,
        "is_verified" : str,
        "status" : int,
        "sms" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Followers %r>' % (self.user_name)

db.register([Followers])



# Classes starts here
# Main User table
class Token(Document):
    __collection__ = 'token'

    structure = {
        "user_id" : Users,
        "token" : str,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Token %r>' % (self.token)

db.register([Token])

# Classes starts here
# Main User table
class Actions(Document):
    __collection__ = 'actions'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "target_id":Target,
        "target_type":str,
        "action_type":str,
        "media":str,
        "post_id":str,
        "to_username":str,
        "to_userid":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Actions %r>' % (self.to_username)

db.register([Actions])


# Classes starts here
# Main User table
class Followed(Document):
    __collection__ = 'followed'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "target_id":str,
        "to_userid":str,
        "target_type":str,
        "to_username":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Followed %r>' % (self.to_username)

db.register([Followed])

class Plans(Document):
    __collection__ = 'plans'

    structure = {
        "plan_name":str,
        "plan_detail":str,
        "price":str,
        "status" : int,
        "speed" : str,
        "action_count" : str, 
        "max_follow" : str, 
        "max_like" : str, 
        "engagement" : str,
        "managed" : str,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Plans %r>' % (self.plan_name)

db.register([Plans])

class MyPlan(Document):
    __collection__ = 'my_plan'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "plan_id": Plans,
        "plan_start_date": datetime,
        "pla_end_date": datetime,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<MyPlan %r>' % (self.plan_id)

db.register([MyPlan])


#Growth =>gender values:
#   0=Everyone
#   1=Male
#   2=Female
class Growth(Document):
    __collection__ = 'growth'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "plan_id": Plans,
        "speed":str,
        "max_follow_limit":str,
        "follow_private":str,
        "gender":str,
        "profile_pic":str,
        "business_account":str,
        "min_post":str,
        "max_post":str,
        "min_following":str,
        "max_following":str,
        "min_follower":str,
        "max_follower":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Growth %r>' % (self.speed)

db.register([Growth])

#Unfollow limit values:
#..................
# 0 = whitelist
# 1 = whitelist+100
# 2 = whitelist+200
# and so on...
#...................
# source values:
# 0 = Anyone
# 1 = People Stim followed
#...................
class Unfollow(Document):
    __collection__ = 'unfollow'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "plan_id": Plans,
        "speed":str,
        "limit":str,
        "source":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Unfollow %r>' % (self.plan_id)

db.register([Unfollow])

# Engagement source values:
# 0 = Anyone 
# 1 = People Stim followed
# 2 = People in Whitelist
# 3 = People not in Whitelist
class Engagement(Document):
    __collection__ = 'engagement'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "plan_id": Plans,
        "speed":str,
        "source":str,
        "min_like":str,
        "max_like":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Engagement %r>' % (self.plan_id)

db.register([Engagement])


class Sleep(Document):
    __collection__ = 'sleep'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "plan_id": Plans,
        "permission":str,
        "start_time":str,
        "duration":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Sleep %r>' % (self.plan_name)

db.register([Sleep])



class Liked(Document):
    __collection__ = 'liked'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "media_id": str,
        "media_image": str,
        "media_comments": str,
        "media_likes": str,
        "to_user_id":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Sleep %r>' % (self.media_id)

db.register([Liked])





class Events(Document):
    __collection__ = 'events'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "event_name":str,
        "event_detail":str,
        "event_type":str,
        "icon":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Events %r>' % (self.event_name)

db.register([Events])

class Whitelist(Document):
    __collection__ = 'whitelist'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "full_name" : str,
        "profile_pic_url" : str,
        "user_id" : str,
        "user_name" : str,
        "followers" : str,
        "is_private" : str,
        "is_verified" : str,
        "type" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime    }
    use_dot_notation = True

    def __repr__(self):
        return '<Whitelist %r>' % (self.full_name)

db.register([Whitelist])

class Blacklist(Document):
    __collection__ = 'blacklist'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "keyword" :str,
        "language" :str,
        "full_name" : str,
        "profile_pic_url" : str,
        "user_id" : str,
        "user_name" : str,
        "followers" : str,
        "is_private" : str,
        "is_verified" : str,
        "type" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Blacklist %r>' % (self.full_name)

db.register([Blacklist])

class Orders(Document):
    __collection__ = 'orders'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "plan_id" : Plans,
        "order_id" : str,
        "temp" : str,
        "payment_method" : str,
        "payment_id" : str,
        "token" : str,
        "payer_id" : str,
        "status" : str,
        "total_amount" : str,
        "comments" : str,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Orders %r>' % (self.status)

db.register([Orders])




class Message(Document):
    __collection__ = 'message'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "status" : int,
        "text" : str,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Message %r>' % (self.text)

db.register([Message])


class MessageTime(Document):
    __collection__ = 'message_time'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "time_from" : str,
        "time_to" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Message %r>' % (self.status)

db.register([MessageTime])



class SMS(Document):
    __collection__ = 'sms'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "message_id" : Message,
        "to_userid" : str,
        "message" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<sms %r>' % (self.status)

db.register([SMS])