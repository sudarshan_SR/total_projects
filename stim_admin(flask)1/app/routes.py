from flask import jsonify,g
from flask import render_template, flash,request, session, redirect, url_for,make_response
from app import app, db,dbs,cache, connection
from app import models,model,constants,context_processors
from model import *
from sqlalchemy import text
from bson.objectid import ObjectId
from flask_login import login_user, logout_user, current_user, login_required
from app.forms import LoginForm, DoctorForm
from flask_wtf import Form
from wtforms.ext.appengine.db import model_form
from datetime import datetime
from flask_restful import reqparse, abort, Api, Resource
from functools import wraps
from flask_httpauth import HTTPBasicAuth
# from app.model import model
import hashlib,os,json
from app import msgtext
auth = HTTPBasicAuth()


api = Api(app)

parser = reqparse.RequestParser()

def auth_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        # print session
        if session:
            password = hashlib.md5(str(session['password']).encode())
            getUser=db.Admin_users.find_one({"email":str(session['username']), "password":password.hexdigest()})
            if getUser:
                return f(*args, **kwargs)

        return redirect('/login')

    return decorated

class logout_user(Resource):
    def get(self):
        session['username']=""
        session['password']=""
        print "logout_user"
        session.clear()
        for key in session.keys():
            session.pop(key)
        headers = {'Content-Type': 'text/html'}
        flash('You successfully updated profile.', 'success')
        return make_response(render_template('rest_framework/login.html'),200,headers)

api.add_resource(logout_user, '/logout') 


class Login(Resource):
    def get(self):
        form=LoginForm
        headers = {'Content-Type': 'text/html','form':form}
        return make_response(render_template('rest_framework/login.html'),headers)

    # @api.representation('application/json')

    def post(self):
        # print api.representation('application/json')
        self.parser = reqparse.RequestParser()
        # self.parser.add_argument('first_name', type=str, required=True)
        self.parser.add_argument('password', type=str, required=True)
        self.parser.add_argument('username', type=str, required=True)
        # self.parser.add_argument('last_name', type=str, required=True)
        data =self.parser.parse_args()
        try:
            password = hashlib.md5(str(data['password']).encode())
            getUser=db.Admin_users.find_one({"email":data["username"], "password":password.hexdigest()})
            if getUser:
                session['username']=getUser.email
                session['password']=data['password']
                context_processors.init_menu(getUser.role_id)
                return redirect('/')
            else:
                flash('Invalid Login Credentials', 'error')
                return make_response(render_template('rest_framework/login.html'))
        except Exception as e:
            print e
            flash('Sorry, something went wrong.', 'error')
        # if request.authorization and request.authorization.username == 'username' and request.authorization.password == 'password':
        # return '<h1>You are logged in</h1>'
        return make_response(render_template('rest_framework/login.html'),201)
        

        

api.add_resource(Login, '/login') 

class Index(Resource):
    decorators = [auth_required]
    def get(self):
        # session['menu']=
        menu=session['menu']
        # print "ok"
        # print menu
        # print cache.get('menu')
        # if session:
        #     print "ok"
        # for i in session:
        #     print i
            # print ":"
            # print session[i]
        headers = {'Content-Type': 'text/html'}
        # context_processors.init_menu()
        # print session['menu']
        return make_response(render_template('dashboard/dashboard.html'),headers,menu)

api.add_resource(Index,'/')

# class Registration(Resource):
#     def get(self):
#       getRole = db.Roles.find()
#       acc=''
#       if getRole:
#           for x in getRole:
#               acc=acc+'<option value="'+x.name+'">'+x.name+' </option>'
#           session['acc']=acc
#         headers = {'Content-Type': 'text/html'}
#         return make_response(render_template('register.html'),200,headers)
#     def post(self):
#       self.parser = reqparse.RequestParser()
#         self.parser.add_argument('email', required=True)
#         self.parser.add_argument('fname',type=str, required=True)
#         self.parser.add_argument('role',type=str, required=True)
#         self.parser.add_argument('lname',type=str, required=True)
#         self.parser.add_argument('password', required=True)
#         self.parser.add_argument('phone', required=True)
#         args =self.parser.parse_args()
        
#         getUser = db.Users.find_one({"email" : args["email"]})
#         if getUser:
#           return "email already exist"
#         else:
#           password = hashlib.md5(str(args["password"]).encode())
#           getRole = db.Roles.find_one({"name" : args["role"]})
#           userObj = db.Users()
#           userObj.role_id=ObjectId(getRole._id)
#           userObj.first_name = unicode(args["fname"], "utf-8")
#           userObj.last_name = unicode(args["lname"], "utf-8")
#           userObj.email = str(args["email"])
#           userObj.phone = str(args["phone"])
#           userObj.password = password.hexdigest()
#           userObj.status = 1
#           userObj.user_name=str(userObj.first_name +" "+userObj.last_name)
#           userObj.date_created = constants.FORMATTED_TIME()
#           userObj.date_updated = constants.FORMATTED_TIME()
#           userObj.save()

#           pth=os.path.realpath('')
#           # print pth
#           f=open(pth+"/app/models.py", "a+")
#           f.write("\n\nclass "+ msgtext.success+"(Document):\n\t__collection__ = 'success'\n\tstructure = {\n\t\t'status' : int,\n\t}\n\tuse_dot_notation = True\n\n\tdef __repr__(self):\n\t\treturn '<success %r>' % (self.status)\ndb.register([Success])")
#           f.close()
#           f= open(pth+"/app/models.py","r")
#           contents =f.read()
#           f.close()
#           sql = text('CREATE TABLE success ( staff_number INT NOT NULL AUTO_INCREMENT, fname VARCHAR(20), lname VARCHAR(30), gender CHAR(1), joining DATE, birth_date DATE, PRIMARY KEY (staff_number) )')
#           result = dbs.engine.execute(sql)
#           if 1:
#               f1=open(pth+"/app/model.py", "a+")
#               f1.write("\n\nclass "+ msgtext.success+"(dbs.Model):\n\t__tablename__ = 'success'\n\tid = dbs.Column(dbs.Integer, primary_key = True)\n\tstatus  = dbs.Column(dbs.Integer, nullable=False)\n\n\tdef __init__(self,status):\n\t\tself.status =status\n\n\tdef __repr__(self):\n\t\treturn '<New %r>' % self.status")
#               f1.close()
#               f1= open(pth+"/app/model.py","r")
#               contents =f1.read()
#               f1.close()
#           # userObj = db.Test()
#           # userObj.status = 0
#           # userObj.save()
#           # print userObj
#       return redirect('/access')
# api.add_resource(Registration,'/register')




class My_profile(Resource):
    decorators = [auth_required]
    def get(self):
        getUser=db.Admin_users.find_one({"email":str(session['username'])})
        # print getUser1.password
        if getUser:
            session["id"]=str(getUser._id)
            session["fname"]=getUser.first_name
            session["lname"]=getUser.last_name
            session["email"]=getUser.email
            
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('my_profile/myProfile.html'),200,headers)
    def post(self):
        error = None
        try:
            self.parser = reqparse.RequestParser()
            self.parser.add_argument('email', required=True)
            self.parser.add_argument('fname',type=str, required=True)
            self.parser.add_argument('lname',type=str, required=True)
            self.parser.add_argument('id',type=str, required=True)
            args =self.parser.parse_args()
            print args
            userObj=db.Admin_users.find_one({"_id":ObjectId(args['id'])})
            if userObj:
                updateData1={
                            "email":str(args['email']),
                            "first_name":str(args['fname']),
                            "last_name":str(args['lname']),
                            "date_updated" : constants.FORMATTED_TIME()
                            }

                AdminObj=db.Admin_users.find_and_modify({ "_id":ObjectId(args['id'])},{"$set":updateData1})
            
            if AdminObj:
                flash('You successfully updated profile.', 'success')
            else:
                flash('Invalid data', 'error')    
        except Exception as e:
            flash('Invalid data', 'error')
            print e
        return make_response(redirect('/my_profile'))
api.add_resource(My_profile, '/my_profile')


class Users(Resource):
    decorators = [auth_required]
    def get(self):
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('users/manageusers.html'),200,headers)
    def delete(self):
        data = request.values['_id']
        userObj=db.Users.find_one({"_id":ObjectId(data)})
        userObj.delete()
        return_data={'message':'User sucessfully deleted!'}
        return make_response(json.dumps(return_data))
api.add_resource(Users, '/userlist')

class DataUser(Resource):
    #@auth.login_required
    def get(self):
        json_data = []
        data={}
        qs=db.Users.find()
        for item in qs:
            buttonedit = '<a class="pointer" title="Edit" href="/user_edit/'+ str(item._id) +'"><i class="fa fa-pencil" style="color:#3c8dbc;"></i></a>&nbsp;&nbsp;'
            buttondelete = '&nbsp;&nbsp;<a class="pointer" title="Delete" onclick="deleteUser(\''+ str(item._id) +'\');"><i class="fa fa-trash" style="color:#dd4b39;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;'
            item.buttons=buttonedit+buttondelete
            if 4 <= item.date_updated.date().day <= 20 or 24 <= item.date_updated.date().day <= 30:
                    suffix = "th"
            else:
                suffix = ["st", "nd", "rd"][item.date_updated.date().day % 10 - 1]
            timeslot = datetime.strftime(item.date_updated,"%I:%M %p")
            date_updated = str(item.date_updated.date().day) + suffix + ' ' + str(item.date_updated.date().strftime("%B, %Y")) + " "+str(timeslot)
            json_data.append({
                "fname":str(item.first_name),
                "lname":str(item.last_name),
                "email":str(item.email),
                "phone":str(item.phone),
                "date_updated":date_updated,
                "actions":item.buttons
                })
        data={
        "data":json_data
        }
        return make_response(json.dumps(data))

api.add_resource(DataUser, '/datatable/users/')

class AdminUsers(Resource):
    decorators = [auth_required]
    def get(self):
        
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('admin_users/manageusers.html'),200,headers)
    def delete(self):
        data = request.values['_id']
        userObj=db.Admin_users.find_one({"_id":ObjectId(data)})
        userObj.delete()
        return_data={'message':'Admin User sucessfully deleted!'}
        return make_response(json.dumps(return_data))
api.add_resource(AdminUsers, '/admin_users')

class DataAdminUser(Resource):
    #@auth.login_required
    def get(self):
        json_data = []
        data={}
        qs=db.Admin_users.find()
        for item in qs:
            buttonedit = '<a class="pointer" title="Edit" href="/admin_user_edit/'+ str(item._id) +'"><i class="fa fa-pencil" style="color:#3c8dbc;"></i></a>&nbsp;&nbsp;'
            buttondelete = '&nbsp;&nbsp;<a class="pointer" title="Delete" onclick="deleteUser(\''+ str(item._id) +'\');"><i class="fa fa-trash" style="color:#dd4b39;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;'
            item.buttons=buttonedit+buttondelete
            if 4 <= item.date_updated.date().day <= 20 or 24 <= item.date_updated.date().day <= 30:
                    suffix = "th"
            else:
                suffix = ["st", "nd", "rd"][item.date_updated.date().day % 10 - 1]
            timeslot = datetime.strftime(item.date_updated,"%I:%M %p")
            date_updated = str(item.date_updated.date().day) + suffix + ' ' + str(item.date_updated.date().strftime("%B, %Y")) + " "+str(timeslot)
            json_data.append({
                "fname":str(item.first_name),
                "lname":str(item.last_name),
                "email":str(item.email),
                "phone":str(item.phone),
                "date_updated":date_updated,
                "actions":item.buttons
                })
        data={
        "data":json_data
        }
        return make_response(json.dumps(data))

api.add_resource(DataAdminUser, '/datatable/admin_users/')

class AdminUser_edit(Resource):
    #@auth.login_required
    def get(self,id):
        try:
            data={}
            getUser=db.Admin_users.find_one({"_id":ObjectId(str(id))})
            if getUser:
                data={
                    'id' : getUser._id,
                    'fname' : getUser.first_name,
                    'lname' : getUser.last_name,
                    'email' : getUser.email,
                    'user_name' : getUser.user_name,
                    'phone' : getUser.phone,
                }
            role=db.Roles.find_one({"_id":getUser.role_id})
            getRole = db.Roles.find()
            roles=''
            if role:
                for x in getRole:
                    if role.name==x.name:
                        roles=roles+'<option value="'+str(role.name)+'" selected>'+str(role.name)+' </option>'
                    else:
                        roles=roles+'<option value="'+str(x.name)+'">'+str(x.name)+' </option>'
            else:
                for x in getRole:
                        roles=roles+'<option value="'+str(x.name)+'">'+str(x.name)+' </option>'
        except Exception as e:
            flash('Sorry, Something Went Wrong. Please try again. ', 'error')
            print e
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('admin_users/editUsers.html',data=data,roles=roles),200,headers)
    def post(self,id):
        try:
            self.parser = reqparse.RequestParser()
            self.parser.add_argument('first_name', required=True)
            self.parser.add_argument('last_name',type=str, required=True)
            self.parser.add_argument('email',type=str, required=True)
            self.parser.add_argument('user_name',type=str, required=True)
            self.parser.add_argument('phone',type=str, required=True)
            args =self.parser.parse_args()
            user_exist=db.Admin_users.find_one({"email":str(args["email"])})
            if user_exist:
                updateData={
                            "email":str(args['email']),
                            "first_name":unicode(args["first_name"]),
                            "last_name":unicode(args["last_name"]),
                            "user_name":str(args["user_name"]),
                            "phone":str(args["phone"]),
                            "date_updated" : constants.FORMATTED_TIME()
                            }
                userObj=db.Admin_users.find_and_modify({ "_id":ObjectId(id)},{"$set":updateData})
                print userObj
                return redirect('admin_users')
            else:
                flash('Admin With This Email Already Exist', 'error')
        except Exception as e:
            flash('Sorry, Something Went Wrong ', 'error')
            print e
        return make_response(redirect('/admin_user_edit/'+id))
api.add_resource(AdminUser_edit, '/admin_user_edit/<id>')

class AdminUser_create(Resource):
    #@auth.login_required
    def get(self):
        role=db.Roles.find_one({})
        getRole = db.Roles.find()
        roles=''
        if role:
            for x in getRole:
                if role.name==x.name:
                    roles=roles+'<option value="'+str(role.name)+'" selected>'+str(role.name)+' </option>'
                else:
                    roles=roles+'<option value="'+str(x.name)+'">'+str(x.name)+' </option>'
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('admin_users/addUsers.html',roles=roles),200,headers)
    def post(self):
        args = request.form.to_dict()
        user_exist=db.Admin_users.find_one({"email":str(args["email"])})
        if user_exist:
            flash('Admin User With This Email Already Exist', 'error')
            return redirect('admin_user_create')
        else:
            password = hashlib.md5(str(args['password']).encode())
            roleObj=db.Roles.find_one({"name":unicode(args['role'])})
            userObj = db.Admin_users()
            userObj.role_id = ObjectId(roleObj._id)
            userObj.first_name = unicode(args["first_name"])
            userObj.last_name = unicode(args["last_name"])
            userObj.email = str(args["email"])
            userObj.user_name = str(args["user_name"])
            userObj.phone = str(args["phone"])
            userObj.password=password.hexdigest()
            userObj.status = int(1)
            userObj.date_created = constants.FORMATTED_TIME()
            userObj.date_updated = constants.FORMATTED_TIME()
            userObj.save()
            return redirect('admin_users')
api.add_resource(AdminUser_create, '/admin_user_create')

class Plans(Resource):
    #@auth.login_required
    def get(self):
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('plan/manage.html'),200,headers)
    def delete(self):
        data = request.values['_id']
        planObj=db.Plans.find_one({"_id":ObjectId(data)})
        planObj.delete()
        return_data={'message':'Plan sucessfully deleted!'}
        return make_response(json.dumps(return_data))
api.add_resource(Plans, '/plans')

class DataPlans(Resource):
    #@auth.login_required
    def get(self):
        json_data = []
        qs=db.Plans.find()
        for item in qs:
            buttonedit = '<a class="pointer" title="Edit" href="/plan_edit/'+ str(item._id) +'"><i class="fa fa-pencil" style="color:#3c8dbc;"></i></a>&nbsp;&nbsp;'
            buttondelete = '&nbsp;&nbsp;<a class="pointer" title="Delete" onclick="deletePlan(\''+ str(item._id) +'\');"><i class="fa fa-trash" style="color:#dd4b39;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;'
            item.buttons=buttonedit+buttondelete
            if 4 <= item.date_updated.date().day <= 20 or 24 <= item.date_updated.date().day <= 30:
                    suffix = "th"
            else:
                suffix = ["st", "nd", "rd"][item.date_updated.date().day % 10 - 1]
            timeslot = datetime.strftime(item.date_updated,"%I:%M %p")
            date_updated = str(item.date_updated.date().day) + suffix + ' ' + str(item.date_updated.date().strftime("%B, %Y")) + " "+str(timeslot)
            json_data.append({
                "plan_name":str(item.plan_name),
                "price":str(item.price),
                "date_updated":date_updated,
                "actions":item.buttons
                })
            data={
            "data":json_data
            }

        return make_response(json.dumps(data))

api.add_resource(DataPlans, '/datatable/plans/')

class Plan_create(Resource):
    #@auth.login_required
    def get(self):
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('plan/add.html'),200,headers)
    def post(self):
        args = request.form.to_dict()
        plan_exist=db.Plans.find_one({"plan_name":str(args["plan_name"])})
        if plan_exist:
            flash('Plan With This Name Already Exist', 'error')
        else:
            planObj = db.Plans()
            planObj.plan_name = str(args["plan_name"])
            planObj.price = str(args["price"])
            planObj.plan_detail = str(args["plan_detail"])
            planObj.speed = str(args["speed"])
            planObj.action_count = str(args["action_count"])
            planObj.max_follow = str(args["max_follow"])
            planObj.max_like = str(args["max_like"])
            planObj.engagement = str(args["engagement"])
            planObj.managed = str(args["managed"])
            planObj.status = int(1)
            planObj.date_created = constants.FORMATTED_TIME()
            planObj.date_updated = constants.FORMATTED_TIME()
            planObj.save()
            return redirect('plans')
api.add_resource(Plan_create, '/plan_create')

class Plan_edit(Resource):
    #@auth.login_required
    def get(self,id):
        print id
        getPlan=db.Plans.find_one({"_id":ObjectId(str(id))})
        if getPlan:
            data={
                'id' : getPlan._id,
                'plan_name' : getPlan.plan_name,
                'price' : getPlan.price,
                'plan_detail' : getPlan.plan_detail,
                'speed' : getPlan.speed,
                'action_count' : getPlan.action_count,
                'max_follow' : getPlan.max_follow,
                'max_like' : getPlan.max_like,
                'engagement' : getPlan.engagement,
                'managed' : getPlan.managed
            }
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('plan/edit.html',data=data),200,headers)
    def post(self,id):
        data = request.form.to_dict()
        planObj=db.Plans.find_one({"_id":ObjectId(id)})
        
        if planObj:
            updateData={
                        'plan_name' : data['plan_name'],
                        'price' : data['price'],
                        'plan_detail' : data['plan_detail'],
                        'speed' : data['speed'],
                        'action_count' : data['action_count'],
                        'max_follow' : data['max_follow'],
                        'max_like' : data['max_like'],
                        'engagement' : data['engagement'],
                        'managed' : data['managed'],
                        "date_updated" : constants.FORMATTED_TIME()
                        }
            planUpdateObj=db.Plans.find_and_modify({ "_id":ObjectId(id)},{"$set":updateData})
            return redirect('/plans')
        else:
            flash('Sorry,Something Went Wrong.', 'error')
        return make_response(redirect('/plan_edit/'+id))
        
api.add_resource(Plan_edit, '/plan_edit/<id>')

class User_create(Resource):
    #@auth.login_required
    def get(self):
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('users/addUsers.html'),200,headers)
    def post(self):
        args = request.form.to_dict()
        
        user_exist=db.Users.find_one({"email":str(args["email"])})
        if user_exist:
            flash('User With This Email Already Exist', 'error')
            return redirect('user_create')
        else:
            userObj = db.Users()
            userObj.first_name = unicode(args["first_name"])
            userObj.last_name = unicode(args["last_name"])
            userObj.email = str(args["email"])
            userObj.user_name = str(args["user_name"])
            userObj.phone = str(args["phone"])
            userObj.status = int(1)
            userObj.date_created = constants.FORMATTED_TIME()
            userObj.date_updated = constants.FORMATTED_TIME()
            userObj.save()
            return redirect('userlist')
api.add_resource(User_create, '/user_create')


class User_edit(Resource):
    #@auth.login_required
    def get(self,id):
        print id
        getUser=db.Users.find_one({"_id":ObjectId(str(id))})
        if getUser:
            data={
                'id' : getUser._id,
                'fname' : getUser.first_name,
                'lname' : getUser.last_name,
                'email' : getUser.email,
                'user_name' : getUser.user_name,
                'phone' : getUser.phone,
            }
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('users/editUsers.html',data=data),200,headers)
    def post(self,id):
        try:
            self.parser = reqparse.RequestParser()
            self.parser.add_argument('first_name', required=True)
            self.parser.add_argument('last_name',type=str, required=True)
            self.parser.add_argument('email',type=str, required=True)
            self.parser.add_argument('user_name',type=str, required=True)
            self.parser.add_argument('phone',type=str, required=True)
            args =self.parser.parse_args()
            print args
            user_exist=db.Users.find_one({"email":str(args["email"])})
            if user_exist:
                updateData={
                            "email":str(args['email']),
                            "first_name":unicode(args["first_name"]),
                            "last_name":unicode(args["last_name"]),
                            "user_name":str(args["user_name"]),
                            "phone":str(args["phone"]),
                            "date_updated" : constants.FORMATTED_TIME()
                            }
                userObj=db.Users.find_and_modify({ "_id":ObjectId(id)},{"$set":updateData})
                print userObj
                return redirect('userlist')
            else:
                flash('User With This Email Already Exist', 'error')
            return make_response(redirect('/user_edit/'+id))
        except Exception as e:
            print e
api.add_resource(User_edit, '/user_edit/<id>')

class Orders(Resource):
    #@auth.login_required
    def get(self):
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('orderpage/manage.html'),200,headers)

api.add_resource(Orders, '/orders')

class DataOrders(Resource):
    #@auth.login_required
    def get(self):
        print "her"
        json_data = []
        qs=db.Orders.find()
        for item in qs:
            buttonedit = '<a class="pointer" title="Edit" href="/order_edit/'+ str(item._id) +'"><i class="fa fa-pencil" style="color:#3c8dbc;"></i></a>&nbsp;&nbsp;'
            buttondelete = '&nbsp;&nbsp;<a class="pointer" title="Delete" onclick="deleteOrder(\''+ str(item._id) +'\');"><i class="fa fa-trash" style="color:#dd4b39;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;'
            item.buttons=buttonedit+buttondelete
            if 4 <= item.date_updated.date().day <= 20 or 24 <= item.date_updated.date().day <= 30:
                    suffix = "th"
            else:
                suffix = ["st", "nd", "rd"][item.date_updated.date().day % 10 - 1]
            timeslot = datetime.strftime(item.date_updated,"%I:%M %p")
            date_updated = str(item.date_updated.date().day) + suffix + ' ' + str(item.date_updated.date().strftime("%B, %Y")) + " "+str(timeslot)
            userObj=db.Users.find_one({"_id":ObjectId(item.main_user_id)})
            planObj=db.Plans.find_one({"_id":ObjectId(item.plan_id)})
            
            json_data.append({
                "order_id":str(item.order_id),
                "user_name":str(userObj.user_name),
                "plan_name":str(planObj.plan_name),
                "total_amount":str(item.total_amount),
                "date_updated":date_updated,
                "actions":item.buttons
                })
        data={
        "data":json_data
        }
        # print json_data
        return make_response(json.dumps(data))

api.add_resource(DataOrders, '/datatable/orders/')

class Order_create(Resource):
    #@auth.login_required
    def get(self):
        
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('orderpage/add.html'),200,headers)
    def post(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('first_name', required=True)
        self.parser.add_argument('last_name',type=str, required=True)
        self.parser.add_argument('email',type=str, required=True)
        self.parser.add_argument('user_name',type=str, required=True)
        self.parser.add_argument('phone',type=str, required=True)
        args =self.parser.parse_args()
        print args
        user_exist=db.Users.find_one({"email":str(args["email"])})
        if user_exist:
            flash('User With This Email Already Exist', 'error')
        else:
            userObj = db.Users()
            userObj.first_name = str(args["first_name"])
            userObj.last_name = str(args["last_name"])
            userObj.email = str(args["email"])
            userObj.user_name = str(args["user_name"])
            userObj.phone = str(args["phone"])
            userObj.status = int(1)
            userObj.date_created = constants.FORMATTED_TIME()
            userObj.date_updated = constants.FORMATTED_TIME()
            userObj.save()
            return redirect('roles')
api.add_resource(Order_create, '/orders/create')

class Order_edit(Resource):
    #@auth.login_required
    def get(self,id):
        print id
        getUser=db.Users.find_one({"_id":ObjectId(str(id))})
        if getUser:
            data={
                'id' : getUser._id,
                'fname' : getUser.first_name,
                'lname' : getUser.last_name,
                'email' : getUser.email,
                'user_name' : getUser.user_name,
                'phone' : getUser.phone,
            }
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('orderpage/edit.html'),200,headers)
    def post(self,id):
        try:
            self.parser = reqparse.RequestParser()
            self.parser.add_argument('first_name', required=True)
            self.parser.add_argument('last_name',type=str, required=True)
            self.parser.add_argument('email',type=str, required=True)
            self.parser.add_argument('user_name',type=str, required=True)
            self.parser.add_argument('phone',type=str, required=True)
            args =self.parser.parse_args()
            print args
            user_exist=db.Users.find_one({"email":str(args["email"])})
            if user_exist:
                updateData={
                            "email":str(args['email']),
                            "first_name":unicode(args["first_name"]),
                            "last_name":unicode(args["last_name"]),
                            "user_name":str(args["user_name"]),
                            "date_updated" : constants.FORMATTED_TIME()
                            }
                userObj=db.Users.find_and_modify({ "_id":ObjectId(id)},{"$set":updateData})
                print userObj
                return redirect('userlist')
            else:
                flash('Plan With This Already Exist', 'error')
            return make_response(redirect('/order_edit/'+id))
        except Exception as e:
            print e
api.add_resource(Order_edit, '/order_edit/<id>')

class Roles(Resource):
    #@auth.login_required
    def get(self):
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('roles/list.html'),200,headers)
    def delete(self):
        data = request.values['_id']
        roleObj=db.Roles.find_one({"_id":ObjectId(data)})
        roleObj.delete()

        perm_Obj=db.Permissions.find({"role_id":ObjectId(roleObj._id)})
        for perm in perm_Obj:
            perm.delete()
        return_data={'message':'Role sucessfully deleted'}
        return make_response(json.dumps(return_data))
api.add_resource(Roles, '/roles')

class DataRoles(Resource):
    #@auth.login_required
    def get(self):
        print "her"
        json_data = []
        data={}
        qs=db.Roles.find()
        for item in qs:
            buttonedit = '<a class="pointer" title="Edit" href="/role_edit/'+ str(item._id) +'"><i class="fa fa-pencil" style="color:#3c8dbc;"></i></a>&nbsp;&nbsp;'
            buttondelete = '&nbsp;&nbsp;<a class="pointer" title="Delete" onclick="deleteRoles(\''+ str(item._id) +'\');"><i class="fa fa-trash" style="color:#dd4b39;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;'
            item.buttons=buttonedit+buttondelete
            if 4 <= item.date_updated.date().day <= 20 or 24 <= item.date_updated.date().day <= 30:
                    suffix = "th"
            else:
                suffix = ["st", "nd", "rd"][item.date_updated.date().day % 10 - 1]
            timeslot = datetime.strftime(item.date_updated,"%I:%M %p")
            date_updated = str(item.date_updated.date().day) + suffix + ' ' + str(item.date_updated.date().strftime("%B, %Y")) + " "+str(timeslot)
            json_data.append({
                "name":str(item.name),
                "date_updated":date_updated,
                "actions":item.buttons
                })
        data={
        "data":json_data
        }
        return make_response(json.dumps(data))

api.add_resource(DataRoles, '/datatable/roles/')

class Role_create(Resource):
    #@auth.login_required
    def get(self):
        # MyForm = model_form(Roles, Form)
        moduleDictid={}
        moduleOBj = db.Module.find()
        for mod in moduleOBj:
            moduleDictid[str(mod._id)] = mod.display_name
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('roles/addroles.html',dictmodule= moduleDictid),200,headers)
    def post(self):
        values=[]
        args = request.form.to_dict()
        print args['name']
        role_exist=db.Roles.find_one({"name":unicode(args["name"])})
        if role_exist:
            flash('Role With This Name Already Exist', 'error')
        else:
            roleObj = db.Roles()
            roleObj.name = unicode(args["name"])
            roleObj.status = int(1)
            roleObj.date_created = constants.FORMATTED_TIME()
            roleObj.date_updated = constants.FORMATTED_TIME()
            roleObj.save()
        for key, value in args.iteritems():
            if key != "name" and  key != "submit":
                values.append(key.split('-'))
        print values
        # 1 is for Read
        # 2 is for Add
        # 3 is for Edit
        # 4 is for Delete
        for x in values:
            if int(x[1]) is 2:
                permObj=db.Permissions.find_one({"role_id":ObjectId(roleObj._id),"module_id":ObjectId(x[0])})
                if permObj:
                    updateData={
                                "pr_add":int(1)
                    }
                    permUpdateObj=db.Permissions.find_and_modify({ "_id":ObjectId(permObj._id)},{"$set":updateData})
                else:
                    perm_Obj=db.Permissions()
                    perm_Obj.role_id=ObjectId(roleObj._id)
                    perm_Obj.module_id=ObjectId(x[0])
                    perm_Obj.pr_read=int(0)
                    perm_Obj.pr_add=int(1)
                    perm_Obj.pr_edit=int(0)
                    perm_Obj.pr_delete=int(0)
                    perm_Obj.date_created=constants.FORMATTED_TIME()
                    perm_Obj.date_updated=constants.FORMATTED_TIME()
                    perm_Obj.save()
            if int(x[1]) is 1:
                permObj=db.Permissions.find_one({"role_id":ObjectId(roleObj._id),"module_id":ObjectId(x[0])})
                if permObj:
                    updateData={
                                "pr_read":int(1)
                    }
                    permUpdateObj=db.Permissions.find_and_modify({ "_id":ObjectId(permObj._id)},{"$set":updateData})
                else:
                    perm_Obj=db.Permissions()
                    perm_Obj.role_id=ObjectId(roleObj._id)
                    perm_Obj.module_id=ObjectId(x[0])
                    perm_Obj.pr_read=int(1)
                    perm_Obj.pr_add=int(0)
                    perm_Obj.pr_edit=int(0)
                    perm_Obj.pr_delete=int(0)
                    perm_Obj.date_created=constants.FORMATTED_TIME()
                    perm_Obj.date_updated=constants.FORMATTED_TIME()
                    perm_Obj.save()
            if int(x[1]) is 3:
                permObj=db.Permissions.find_one({"role_id":ObjectId(roleObj._id),"module_id":ObjectId(x[0])})
                if permObj:
                    updateData={
                                "pr_edit":int(1)
                    }
                    permUpdateObj=db.Permissions.find_and_modify({ "_id":ObjectId(permObj._id)},{"$set":updateData})
                else:
                    perm_Obj=db.Permissions()
                    perm_Obj.role_id=ObjectId(roleObj._id)
                    perm_Obj.module_id=ObjectId(x[0])
                    perm_Obj.pr_read=int(0)
                    perm_Obj.pr_add=int(0)
                    perm_Obj.pr_edit=int(1)
                    perm_Obj.pr_delete=int(0)
                    perm_Obj.date_created=constants.FORMATTED_TIME()
                    perm_Obj.date_updated=constants.FORMATTED_TIME()
                    perm_Obj.save()
            if int(x[1]) is 4:
                permObj=db.Permissions.find_one({"role_id":ObjectId(roleObj._id),"module_id":ObjectId(x[0])})
                if permObj:
                    updateData={
                                "pr_delete":int(1)
                    }
                    permUpdateObj=db.Permissions.find_and_modify({ "_id":ObjectId(permObj._id)},{"$set":updateData})
                else:
                    perm_Obj=db.Permissions()
                    perm_Obj.role_id=ObjectId(roleObj._id)
                    perm_Obj.module_id=ObjectId(x[0])
                    perm_Obj.pr_read=int(0)
                    perm_Obj.pr_add=int(0)
                    perm_Obj.pr_edit=int(0)
                    perm_Obj.pr_delete=int(1)
                    perm_Obj.date_created=constants.FORMATTED_TIME()
                    perm_Obj.date_updated=constants.FORMATTED_TIME()
                    perm_Obj.save()
            
        return redirect('roles')
api.add_resource(Role_create, '/role_create')

class Role_edit(Resource):
    #@auth.login_required
    def get(self,id):
        modulelist = []
        dictCombine = {}
        moduleDictid = {}
        data={}

        getRole=db.Roles.find_one({"_id":ObjectId(str(id))})
        if getRole:
            permodule = db.Permissions.find({"role_id":ObjectId(id)})
            
            for x in permodule:
                dictCombine[str(x.module_id)] =     {'read':x.pr_read,'add':x.pr_add,'edit':x.pr_edit,'delete':x.pr_delete}
            
            getmodules = db.Module.find()
            for x in getmodules:
                moduleDictid[str(x._id)] = x.display_name
            # print "available modules:",moduleDictid
            # print id
            data={
                'id' : getRole._id,
                'name' : getRole.name
            }
        else:
            flash('Plan With This Already Exist', 'error')

        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('roles/editroles.html',dictmodule= moduleDictid,listmodule = dictCombine,data=data),200,headers)
    def post(self,id):
        try:
            existperm={}
            permodule = db.Permissions.find({"role_id":ObjectId(id)})
            listperm=[]
            for i in permodule:
                listperm=[]
                if i.pr_read:
                    listperm.append('1')
                if i.pr_add:
                    listperm.append('2')
                if i.pr_edit:
                    listperm.append('3')
                if i.pr_delete:
                    listperm.append('4')
                existperm[str(i.module_id)]=listperm
            print existperm
            # print listperm
            # existperm[]
            values=[]
            data = request.form.to_dict()
            # print data
            for key, value in data.iteritems():
                if key != "name" and  key != "submit":
                    values.append(key.split('-'))
            # print values
            roleObj=db.Roles.find_one({"_id":ObjectId(id)})
            # 1 is for Read
            # 2 is for Add
            # 3 is for Edit
            # 4 is for Delete
            for x in values:
                currlist=[]
                # print x
                if existperm[x[0]]:
                    for k in existperm[x[0]]:
                        currlist= existperm[x[0]]
                        # print currlist
                        if int(k)==int(x[1]):
                           currlist.remove(str(k))
                        existperm[x[0]]=currlist
                             
                if int(x[1]) is 2:
                    permObj=db.Permissions.find_one({"role_id":ObjectId(roleObj._id),"module_id":ObjectId(x[0])})
                    if permObj:
                        updateData={
                                    "pr_add":int(1)
                        }
                        permUpdateObj=db.Permissions.find_and_modify({ "_id":ObjectId(permObj._id)},{"$set":updateData})
                    else:
                        perm_Obj=db.Permissions()
                        perm_Obj.role_id=ObjectId(roleObj._id)
                        perm_Obj.module_id=ObjectId(x[0])
                        perm_Obj.pr_read=int(0)
                        perm_Obj.pr_add=int(1)
                        perm_Obj.pr_edit=int(0)
                        perm_Obj.pr_delete=int(0)
                        perm_Obj.date_created=constants.FORMATTED_TIME()
                        perm_Obj.date_updated=constants.FORMATTED_TIME()
                        perm_Obj.save()
                if int(x[1]) is 1:
                    permObj=db.Permissions.find_one({"role_id":ObjectId(roleObj._id),"module_id":ObjectId(x[0])})
                    if permObj:
                        updateData={
                                    "pr_read":int(1)
                        }
                        permUpdateObj=db.Permissions.find_and_modify({ "_id":ObjectId(permObj._id)},{"$set":updateData})
                    else:
                        perm_Obj=db.Permissions()
                        perm_Obj.role_id=ObjectId(roleObj._id)
                        perm_Obj.module_id=ObjectId(x[0])
                        perm_Obj.pr_read=int(1)
                        perm_Obj.pr_add=int(0)
                        perm_Obj.pr_edit=int(0)
                        perm_Obj.pr_delete=int(0)
                        perm_Obj.date_created=constants.FORMATTED_TIME()
                        perm_Obj.date_updated=constants.FORMATTED_TIME()
                        perm_Obj.save()
                if int(x[1]) is 3:
                    permObj=db.Permissions.find_one({"role_id":ObjectId(roleObj._id),"module_id":ObjectId(x[0])})
                    if permObj:
                        updateData={
                                    "pr_edit":int(1)
                        }
                        permUpdateObj=db.Permissions.find_and_modify({ "_id":ObjectId(permObj._id)},{"$set":updateData})
                    else:
                        perm_Obj=db.Permissions()
                        perm_Obj.role_id=ObjectId(roleObj._id)
                        perm_Obj.module_id=ObjectId(x[0])
                        perm_Obj.pr_read=int(0)
                        perm_Obj.pr_add=int(0)
                        perm_Obj.pr_edit=int(1)
                        perm_Obj.pr_delete=int(0)
                        perm_Obj.date_created=constants.FORMATTED_TIME()
                        perm_Obj.date_updated=constants.FORMATTED_TIME()
                        perm_Obj.save()
                if int(x[1]) is 4:
                    permObj=db.Permissions.find_one({"role_id":ObjectId(roleObj._id),"module_id":ObjectId(x[0])})
                    if permObj:
                        updateData={
                                    "pr_delete":int(1)
                        }
                        permUpdateObj=db.Permissions.find_and_modify({ "_id":ObjectId(permObj._id)},{"$set":updateData})
                    else:
                        perm_Obj=db.Permissions()
                        perm_Obj.role_id=ObjectId(roleObj._id)
                        perm_Obj.module_id=ObjectId(x[0])
                        perm_Obj.pr_read=int(0)
                        perm_Obj.pr_add=int(0)
                        perm_Obj.pr_edit=int(0)
                        perm_Obj.pr_delete=int(1)
                        perm_Obj.date_created=constants.FORMATTED_TIME()
                        perm_Obj.date_updated=constants.FORMATTED_TIME()
                        perm_Obj.save()
            
            for ex in existperm:
                exlist=existperm[ex]
                if exlist:
                    for x in exlist:
                        permObj=db.Permissions.find_one({"role_id":ObjectId(roleObj._id),"module_id":ObjectId(ex)})
                        if permObj:
                            if int(x)==2:
                                updateData={
                                            "pr_add":int(0)
                                }
                                permUpdateObj=db.Permissions.find_and_modify({ "_id":ObjectId(permObj._id)},{"$set":updateData})
                            if int(x)==1:
                                updateData={
                                            "pr_read":int(0)
                                }
                                permUpdateObj=db.Permissions.find_and_modify({ "_id":ObjectId(permObj._id)},{"$set":updateData})
                            if int(x)==3:
                                updateData={
                                            "pr_edit":int(0)
                                }
                                permUpdateObj=db.Permissions.find_and_modify({ "_id":ObjectId(permObj._id)},{"$set":updateData})
                            if int(x)==4:
                                updateData={
                                            "pr_delete":int(0)
                                }
                                permUpdateObj=db.Permissions.find_and_modify({ "_id":ObjectId(permObj._id)},{"$set":updateData})
            return make_response(redirect('/role_edit/'+id))
        except Exception as e:
            print e
            return make_response(redirect('/role_edit/'+id))
api.add_resource(Role_edit, '/role_edit/<id>')

# class EmailUpload(Resource):
#     #@auth.login_required
#     def get(self):
#         headers = {'Content-Type': 'text/html'}
#         return make_response(render_template('add_email.html'),200,headers)

# api.add_resource(EmailUpload, '/add_email')

# class EmailEdit(Resource):
#     #@auth.login_required
#     def get(self):
#         headers = {'Content-Type': 'text/html'}
#         return make_response(render_template('edit_email.html'),200,headers)

# api.add_resource(EmailEdit, '/edit_email')


# class CombineEmail(Resource):
#     #@auth.login_required
#     def get(self):
#         headers = {'Content-Type': 'text/html'}
#         return make_response(render_template('combining.html'),200,headers)

# api.add_resource(CombineEmail, '/combining')

# class Ticket(Resource):
#     #@auth.login_required
#     def get(self):
#         headers = {'Content-Type': 'text/html'}
#         return make_response(render_template('ticket.html'),200,headers)

# api.add_resource(Ticket, '/ticket')

# class Reports(Resource):
#     #@auth.login_required
#     def get(self):
#         headers = {'Content-Type': 'text/html'}
#         return make_response(render_template('charts.html'),200,headers)

# api.add_resource(Reports, '/reports')

# class Access(Resource):
#     #@auth.login_required
#     def get(self):
#         n=1
#         acc=''
#         users=db.Users.find()
#         for u in users:
# 			role=db.Roles.find_one({"_id":ObjectId(u.role_id)})
# 			acc=acc+'<tr><td class="v-align-middle semi-bold">'+str(n)+'</td>'
# 			acc=acc+'<td class="v-align-middle">'+str(u.user_name)+'</td>'
# 			acc=acc+'<td class="v-align-middle semi-bold">'+str(u.date_created)+'</td>'
# 			acc=acc+'<td class="v-align-middle semi-bold"><form id="myform" method="get" action="user_edit">'
# 			acc=acc+'<input type="hidden" name="email" value="'+u.email+'" id="1">'
# 			acc=acc+'<button  value="'+u.user_name+'" name="name" onclick="document.getElementById("myform").submit();" for="1"><i class="fa fa-pencil"'
# 			acc=acc+' aria-hidden="true"></i></button></form>&nbsp;&nbsp;<form id="myform1" method="post" action="user_edit"><input type="hidden" name="method" value="delete" id="d1">'
# 			acc=acc+'<input type="hidden" name="role" value="'+role.name+'" id="d1"><input type="hidden" name="id" value="'+str(u._id)+'" id="d1">'
# 			acc=acc+'<button  onclick="document.getElementById("myform1").submit();" for="d1" ><i class="fa pg-trash" aria-hidden="true"></i></button></form></td></tr>'

# 			# acc=acc+'</tr>'<input type="hidden" name="email" value="'+u.email+'" id="1">
# 			n +=1
#         session['acc']=acc
#         print "acc"
#         # print acc
#         print session['acc']
#         headers = {'Content-Type': 'text/html'}
#         return make_response(render_template('access.html'),200,headers)

# api.add_resource(Access, '/access')

# class Invoice(Resource):
#     #@auth.login_required
#     def get(self):
#         headers = {'Content-Type': 'text/html'}
#         return make_response(render_template('invoice.html'),200,headers)

# api.add_resource(Invoice, '/invoice')

# class Social(Resource):
#     #@auth.login_required
#     def get(self):
#         headers = {'Content-Type': 'text/html'}
#         return make_response(render_template('social.html'),200,headers)

# api.add_resource(Social, '/social')

# class AccessInner(Resource):
#     def get(self):
        
#         headers = {'Content-Type': 'text/html'}
#         return make_response(render_template('access_inner.html'),200,headers)
#     def post(self):
#         return "ok"

# api.add_resource(AccessInner, '/access_inner')

# class Roles(Resource):
#     """docstring for ClassName"""
#     #@auth.login_required
#     def get(self):
#         self.parser = reqparse.RequestParser()
#         # self.parser.add_argument('name', type=str, required=True)
#         print self.parser.parse_args()
#         n=1

#         acc=''
#         roles=db.Roles.find()
#         for u in roles:
#             nn=u.name
#             acc=acc+'<tr><td class="v-align-middle semi-bold">'+str(n)+'</td>'
#             acc=acc+'<td class="v-align-middle">'+str(u.name)+'</td>'
#             acc=acc+'<td class="v-align-middle semi-bold">'+str(u.date_created)+'</td>'
#             acc=acc+'<td class="v-align-middle semi-bold"><form method="get" role="form" action="add_roles"><button type="submit"  value="'+nn+'" name="name"><i class="fa fa-pencil"'
#             acc=acc+' aria-hidden="true" ></i></button></form>&nbsp;&nbsp;'
#             acc=acc+'<form id="myform" method="post" action="roles"><input type="hidden" name="method" value="delete" id="1"><button value="'+nn+'" onclick="document.getElementById("myform").submit();" for="1" name="name" ><i class="fa pg-trash" aria-hidden="true"></i></a></td></tr>'
#             n +=1
#             #             acc=acc+'<td class="v-align-middle semi-bold"><form id="myform" method="post" action="register"><input type="hidden" name="email" value="sdudhe92@gmail.com" id="1"> <button  value="" name="name" onclick="document.getElementById("myform").submit();" for="1"><i class="fa fa-pencil"'
#             # acc=acc+' aria-hidden="true"></i></button></form>&nbsp;&nbsp;<i class="fa pg-trash" aria-hidden="true"></i></td></tr>'

#         session['acc']=acc
#         print "acc"

#         # print acc <a id="mylink" data-id="1" href="add_roles"></a>method="put"<form method="post" action="{{ url_for('remove', id=object.id) }}">Delete</button></form>
#         print session['acc']
#         headers = {'Content-Type': 'text/html'}
#         return make_response(render_template('roles.html'),200,headers)

#     def post(self):
#         self.parser = reqparse.RequestParser()
#         self.parser.add_argument('name', required=True)
#         self.parser.add_argument('method')
#         args =self.parser.parse_args()
#         # return args
#         role=db.Roles.find_one({"name":self.parser.parse_args()["name"]})
#         if role :
# 			if args["method"]=="delete":
				
# 				dd=connection.simplify.roles
# 				dd.remove({"name":self.parser.parse_args()["name"]})
# 				return redirect('/roles')
# 				# dir(dd.remove({"name":self.parser.parse_args()["name"]}))
# 				# dir(connection.simplify.drop_collection(db.Roles.remove()))
# 			if args["method"]=="edit" :
# 				role.name=args["name"]
# 				role.date_updated = constants.FORMATTED_TIME()
# 				role.save()
# 				print "Role Id"
# 				permObj=db.Permissions.find({'role_id':ObjectId(role._id)})
# 				if permObj:
# 					for x in permObj:
# 						pr=ObjectId(x._id)
# 						mod=db.Module.find_one({"_id":ObjectId(x.module_id)})
# 						# print mod.module_name
# 						self.parser.add_argument(mod.module_name,action='append')
# 						a=self.parser.parse_args()[mod.module_name]
# 						updateData1={	"pr_read":int(int(a[0])),
# 										"pr_edit":int(int(a[1])),
# 										"pr_add":int(int(a[2])),
# 										"pr_delete":int(int(a[3])),
# 										"date_updated" : constants.FORMATTED_TIME()
# 										}
# 						db.Permissions.find_and_modify({ "_id":pr},{"$set":updateData1})
#         return redirect('/roles')

#     def delete(self):
#         return "check-success"
# api.add_resource(Roles, '/roles')

# class AddRoles(Resource):
#     """docstring for ClassName"""
#     #@auth.login_required
#     def get(self):
#         n=1
#         acc=''
#         for x in session:
#             if x=="role_name":
#                 print "session"
#                 if session.get(x):
#                     session[x]=''
#                 print session.get(x)
                
#         self.parser = reqparse.RequestParser()
#         if self.parser.add_argument('name'):
#         # self.parser.add_argument("data-name")
#             args = self.parser.parse_args()
#             # print args["name"]
#             rol=db.Roles.find_one({'name':args["name"]})
#             if rol:
#                 print "role"
#                 session['role_name']=rol.name
#                 print session['role_name']
#                 perms=db.Permissions.find({'role_id':ObjectId(rol._id)})
#                 for x in perms:
#                     mod=db.Module.find_one({'_id':x.module_id})
#                     print mod.module_name
#                     acc=acc+'<input type="hidden" name="method" value="edit" >'
#                     acc=acc+'<p>'+str(mod.display_name)+'</p>'
#                     acc=acc+'<div class="checkbox check-success  ">'
#                     # id="'+str(n)+'checkbox">'
#                     if x.pr_read:
#                         acc=acc+'<input type="hidden"  checked="checked"  name="'+mod.module_name+'"" value="1" id="checkbox">'

#                         acc=acc+'<input type="checkbox" checked="checked"  onclick="this.previousSibling.value=1-this.previousSibling.value" id="checkbox'+str(n)+'">'
#                     else:
#                         acc=acc+'<input type="hidden"  checked="unchecked"  name="'+mod.module_name+'"" value="0" id="checkbox">'

#                         acc=acc+'<input type="checkbox"   onclick="this.previousSibling.value=1-this.previousSibling.value" id="checkbox'+str(n)+'">'

#                     acc=acc+'<label for="checkbox'+str(n)+'">Read</label></div>'
#                     n+=1
#                     acc=acc+'<div class="checkbox check-success  ">'
#                     if x.pr_edit:
#                         acc=acc+'<input type="hidden"  checked="checked"  name="'+mod.module_name+'"" value="1" id="checkbox">'
                    
#                         acc=acc+'<input type="checkbox" checked="checked"  onclick="this.previousSibling.value=1-this.previousSibling.value" id="checkbox'+str(n)+'">'
#                     else:
#                         acc=acc+'<input type="hidden"  checked="unchecked"  name="'+mod.module_name+'"" value="0" id="checkbox">'

#                         acc=acc+'<input type="checkbox"  onclick="this.previousSibling.value=1-this.previousSibling.value" id="checkbox'+str(n)+'">'

#                     acc=acc+'<label for="checkbox'+str(n)+'">Edit</label>'
#                     acc=acc+'</div>'
#                     n+=1
#                     acc=acc+'<div class="checkbox check-success  ">'
                    
#                     if x.pr_add:
#                         acc=acc+'<input type="hidden"  checked="checked"  name="'+mod.module_name+'"" value="1" id="checkbox">'

#                         acc=acc+'<input type="checkbox" checked="checked"  onclick="this.previousSibling.value=1-this.previousSibling.value" id="checkbox'+str(n)+'">'
#                     else:
#                         acc=acc+'<input type="hidden"  checked="unchecked"  name="'+mod.module_name+'"" value="0" id="checkbox">'

#                         acc=acc+'<input type="checkbox"   onclick="this.previousSibling.value=1-this.previousSibling.value" id="checkbox'+str(n)+'">'
#                     acc=acc+'<label for="checkbox'+str(n)+'">Add</label>'
#                     acc=acc+'</div>'
#                     n+=1
#                     acc=acc+'<div class="checkbox check-success  ">'
#                     if x.pr_delete:
#                         acc=acc+'<input type="hidden"  checked="checked"  name="'+mod.module_name+'"" value="1" id="checkbox">'

#                         acc=acc+'<input type="checkbox" checked="checked"  onclick="this.previousSibling.value=1-this.previousSibling.value" id="checkbox'+str(n)+'">'
#                     else:
#                         acc=acc+'<input type="hidden"  checked="unchecked"  name="'+mod.module_name+'"" value="0" id="checkbox">'

#                         acc=acc+'<input type="checkbox"  onclick="this.previousSibling.value=1-this.previousSibling.value" id="checkbox'+str(n)+'">'
#                     acc=acc+'<label for="checkbox'+str(n)+'">Delete</label>'
#                     acc=acc+'</div>'
#                     n +=1
#             else:
#                 pass
#                     # print "Permissions"
#         if args["name"] is None:

#             print "gett"
#             module=db.Module.find()
#             if module:
#                 for mod in module:
#                     print mod
#                     # mod=db.Module.find_one({'_id':ObjectId(u.module_id)})
#                     acc=acc+'<p>'+str(mod.display_name)+'</p>'
#                     acc=acc+'<div class="checkbox check-success  ">'
#                     acc=acc+'<input type="hidden"  checked="checked"  name="'+mod.module_name+'"" value="1" id="checkbox">'
#                     # id="'+str(n)+'checkbox">'
#                     acc=acc+'<input type="checkbox" checked="checked"  onclick="this.previousSibling.value=1-this.previousSibling.value" id="checkbox'+str(n)+'">'
#                     acc=acc+'<label for="checkbox'+str(n)+'">Read</label></div>'
#                     n+=1
#                     acc=acc+'<div class="checkbox check-success  ">'
#                     acc=acc+'<input type="hidden"  checked="unchecked"  name="'+mod.module_name+'"" value="1" id="checkbox">'
#                     acc=acc+'<input type="checkbox" checked="checked" onclick="this.previousSibling.value=1-this.previousSibling.value" id="checkbox'+str(n)+'">'
#                     acc=acc+'<label for="checkbox'+str(n)+'">Edit</label>'
#                     acc=acc+'</div>'
#                     n+=1
#                     acc=acc+'<div class="checkbox check-success  ">'
#                     acc=acc+'<input type="hidden"  checked="unchecked"  name="'+mod.module_name+'"" value="1" id="checkbox">'
#                     acc=acc+'<input type="checkbox" checked="checked" onclick="this.previousSibling.value=1-this.previousSibling.value" id="checkbox'+str(n)+'">'
#                     acc=acc+'<label for="checkbox'+str(n)+'">Add</label>'
#                     acc=acc+'</div>'
#                     n+=1
#                     acc=acc+'<div class="checkbox check-success  ">'
#                     acc=acc+'<input type="hidden"  checked="unchecked"  name="'+mod.module_name+'"" value="1" id="checkbox">'
#                     acc=acc+'<input type="checkbox" checked="checked" onclick="this.previousSibling.value=1-this.previousSibling.value" id="checkbox'+str(n)+'">'
#                     acc=acc+'<label for="checkbox'+str(n)+'">Delete</label>'
#                     acc=acc+'</div>'
#                     n +=1
# # <input type="hidden" name="checkboxName" value="0"><input type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value">
#         session['acc']=acc
#         print "acc"
#         # print acc
#         # print session['acc']
#         headers = {'Content-Type': 'text/html'}
#         return make_response(render_template('add_roles.html'),200,headers)

#     def post(self):
#         self.parser = reqparse.RequestParser()
#         self.parser.add_argument('name', required=True)
#         # args = 
#         role=db.Roles.find_one({"name":self.parser.parse_args()["name"]})
#         if role :
#             perms=db.Permissions.find({'role_id':ObjectId(role._id)})
#             for f in perms:
#                 print help(f.delete())
#                 break
#             # db.Permissions.remove({'role_id':ObjectId(role._id)})
#             # db.Roles.remove({'_id':ObjectId(role._id)})

#             return "Roles already exist"
#         elif self.parser.parse_args()["name"] is None:
#             return "name is None"
#             # self.parser = reqparse.RequestParser()
#         elif self.parser.parse_args()["name"] =="":
#             return "name should not be null"
#         else:
#             mod=db.Module.find()
#             for x in mod:
#                 self.parser.add_argument(x.module_name,action='append')
#             args = self.parser.parse_args()
#             roleObj=db.Roles()
#             roleObj.name=args["name"]
#             # unicode(args["name"], "utf-8")
#             roleObj.date_created = constants.FORMATTED_TIME()
#             roleObj.date_updated = constants.FORMATTED_TIME()
#             roleObj.save()
#             # args=__init__(self)
                        
#             for u in args:
#                 if u!="name":
#                     print u
#                     getMod=db.Module.find_one({"module_name":u})
#                     if getMod:
#                         a = args[u]
#                         print a
#                         permObj=db.Permissions()
#                         permObj.role_id = roleObj._id
#                         permObj.module_id = getMod._id
#                         permObj.pr_read=int(int(a[0]))
#                         permObj.pr_add = int(int(a[1]))
#                         permObj.pr_edit = int(int(a[2]))
#                         permObj.pr_delete = int(int(a[3]))
#                         permObj.date_created = constants.FORMATTED_TIME()
#                         permObj.date_updated = constants.FORMATTED_TIME()
#                         permObj.save()

#         values=[]
#         # # print self
#         # self.parser = reqparse.RequestParser()
#         # args =self.parser.parse_args()
#         # print args["name"]
#         headers = {'Content-Type': 'text/html'}
#         return redirect('/roles')


#     def put(self):
#         return make_response(render_template('roles.html'),200,headers)
# api.add_resource(AddRoles, '/add_roles')

#         # module = db.Module()
#         # module.module_name = unicode(args["module_name"], "utf-8")
#         # module.display_name = unicode(args["display_name"], "utf-8")
#         # module.priority = int(args["priority"])
#         # module.date_created = constants.FORMATTED_TIME()
#         # module.date_updated = constants.FORMATTED_TIME()
#         # module.class1 = unicode(args["class1"], "utf-8")
#         # module.class2 = unicode(args["class2"], "utf-8")
#         # module.url = unicode(args["url"], "utf-8")
#         # module.save()
#         # print args["name"]
#         # roleObj = db.Roles()
#         # 
#         # permObj = db.Permissions()
#         # for key, value in args:
#         #     if key=='perms':
#         #         values.append(key.split('-'))
#         # getMod = db.Module.find_one({"module_name" : args["module_name"]})
#         # getRole = db.Roles.find_one({"name" : args["role"]})
            
#         # 
#         # self.parser.add_argument('module_name', type=str, required=True)
#                 # self.parser.add_argument('display_name', type=str, required=True)
#                 # self.parser.add_argument('priority', type=int, required=True)
#                 # self.parser.add_argument('class1', type=str, required=True)
#                 # self.parser.add_argument('class2', type=str, required=True)
#                 # self.parser.add_argument('url', type=str, required=True)
        

#         # permObj.roles= roleObj._id
#         # permObj.module = 
#         # TODOS[todo_id] = {'task': args['task']}