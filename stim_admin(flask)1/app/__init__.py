from flask_cors import CORS
from flask import Flask, render_template
from flask_mongokit import MongoKit, Connection
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_caching import Cache
# from flask_mail import Mail
# from flask_cors import CORS, cross_origin
# from flask import send_from_directory

# from flask_sqlalchemy import SQLAlchemy
# from flask_migrate import Migrate
from config import Config
# from flask_mongokit import Mongokit
app = Flask(__name__)
CORS(app)
app.config.from_object(Config)
# db = SQLAlchemy(app)
connection = Connection(app.config['MONGODB_HOST'], app.config['MONGODB_PORT'])
db = MongoKit(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:Qwerty@123@localhost/healora_admin'
app.config['SECRET_KEY'] = "This is the key"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Define the database object which is imported Here we are connecting to two diffrent databases mongo and mysql
dbs = SQLAlchemy(app)
migrate = Migrate(app, db)
# migrate = Migrate(app, db)
# Check Configuring Flask-Caching section for more details
cache = Cache(app, config={'CACHE_TYPE': 'simple'})
from app import routes, models, model
