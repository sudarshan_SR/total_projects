# coding=utf-8
# future
# standard library
from datetime import date, datetime, timedelta
import re

from validate_email import validate_email

from admin_app import constants


# try/except

def is_date_valid(date_param):
    """
    Check for valid date entered format i.e yyyy-mm-dd and range of date 1900-01-01 to present date
    :param date: string
    :return: boolean, str msg
    """
    max_dob = date.today()
    min_dob = date(year=1900, month=1, day=1)
    date_valid_format = re.compile('(\d{4})[\-](\d{2})[\-](\d{2})$')
    match = date_valid_format.match(date_param)
    if match and (min_dob < datetime.strptime(date_param, "%Y-%m-%d").date() < max_dob):
        return True, constants.VALID_DATE
    else:
        return False, constants.INVALID_DATE


def is_phone_valid(phone):
    """
    Check for valid phone number entered
    :param phone: int
    :return: boolean, str msg
    """
    if phone is None:
        return False, ''
    try:
        phone = int(phone)
    except ValueError as E:
        return False, str(E)
    except TypeError as E:
        return False, str(E)
    if len(str(phone)) != 10:
        return False, constants.INVALID_PHONE_NO
    else:
        return True, constants.VALID_PHONE_NO

def is_date_valid_for_registration(date_param):
    """
    Check for valid date entered format i.e yyyy-mm-dd and range of date 1900-01-01 to present date
    Minimum age is 18 years
    :param date: string
    :return: boolean, str msg
    """
    if date_param is None:
        return False, ''
    max_dob = date.today() - timedelta(days=6570)
    min_dob = date(year=1900, month=1, day=1)
    date_valid_format = re.compile('(\d{4})[\-](\d{2})[\-](\d{2})$')
    match = date_valid_format.match(date_param)
    if match and (min_dob < datetime.strptime(date_param, "%Y-%m-%d").date() < max_dob):
        return True, constants.VALID_DATE
    else:
        return False, constants.INVALID_DATE


def is_name_valid(name_param):
    """
    Validate name
    :param name_param: str
    :return: boolean
    """
    if name_param is None:
        return False
    name_valid_format = re.compile('^[A-Za-z .]+$')
    if name_valid_format.match(name_param):
        return True
    else:
        return False


def is_sex_valid(sex_param):
    """
    Check the param and return formatted sex or None
    :param sex_param:
    :return: return 'male' or 'female' or None
    """
    if sex_param is None:
        return False
    if sex_param.lower() == 'male' or sex_param.lower() == 'm':
        return 'male'
    elif sex_param.lower() == 'female' or sex_param.lower() == 'f':
        return 'female'
    else:
        return None

def is_email_valid(email_param):
    """
    Checks if email is valid
    :param email_param:
    :return: boolean
    """
    if email_param is None:
        return False
    if validate_email(email_param):
        return True
    else:
        return False
def is_domain_valid(domain_param):
    """
    Checks if domain is of the format example.com
    :param domain_param:
    :return:
    """
    domain_valid_format = re.compile('^[a-zA-Z\d-]{1,63}(\.[a-zA-Z\d-]{1,63})*$')
    is_valid = domain_valid_format.match(domain_param)
    if is_valid:
        return True
    else:
        return False
