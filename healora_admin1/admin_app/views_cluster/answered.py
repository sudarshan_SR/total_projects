import json
import sys
import datetime
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.template import RequestContext
from django.utils import timezone
from django.views.generic import View
from django_datatables_view.base_datatable_view import BaseDatatableView

from admin_app import constants
from admin_app.forms import addroles
from admin_app.models import TblAdmin, TblModule, TblModulePermission, TblRoles, TblEmail


class Answered(View):
    try:
        def __init__(self, **kwargs):
            
            self.template_name_dict = {
                                     'list' : 'answered/list.html',
                                     'add' : 'answered/addroles.html',
                                     'edit' : 'answered/addroles.html'
                                    }
        
        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context

        def get_queryset(self, **kwargs):
            return 
        
        def delete(self, request, *args, **kwargs):
            iduser = kwargs['idkey']
            # try:
            roleobject = TblRoles.objects.filter(pk=iduser)
            Obj = TblAdmin.objects.filter(role=roleobject)
            if Obj:
                data={'message':'Role can not be deleted, Its assigned to a User'}
                return HttpResponse(json.dumps(data))
            else:
               
                TblRoles.objects.filter(pk=iduser).delete()
                # TblLogs(module=2, module_id=iduser, action=2, user_type=1, user_id=request.user.pk).save()
                data={'message':'Role sucessfully Deleted'}
                return HttpResponse(json.dumps(data))
            # except Exception as e:
            #     data={'message':'Error deleting the role'}
            #     return HttpResponse(json.dumps(data))
            
        
        def post(self, request, **kwargs):
            context_dict = {}
            page = self.kwargs['page']
            
            values = []
            # print request.POST
            
            if page == 'edit':
                Id = kwargs['idkey']
                tblroleobject = get_object_or_404(TblRoles, pk=int(Id))
                form = addroles(request.POST or None,instance=tblroleobject,idkey = Id)
                if form.is_valid():
                    post = form.save(commit=False)
                    post.date_created =  timezone.now()
                    post.date_updated =  timezone.now()
                    post.save()
                    TblModulePermission.objects.filter(role=Id).delete()
                    # print request.POST
                    for key, value in request.POST.items():
                        if key != "name" and  key != "submit" and  key != "csrfmiddlewaretoken":
                            values.append(key.split('-'))
                    print values
                    for x in values:
                        if int(x[1]) is 2:
                            module = TblModule.objects.get(pk=x[0])
                            person, created = TblModulePermission.objects.update_or_create(
                                 role=tblroleobject,module=x[0],
                                 defaults={'role': tblroleobject,'module':module,'pr_add':1,'date_created' : constants.FORMATTED_TIME() , 'date_modified' : constants.FORMATTED_TIME()},
                            )
                        if int(x[1]) is 1:
                            module = TblModule.objects.get(pk=x[0])
                            person, created = TblModulePermission.objects.update_or_create(
                                 role=tblroleobject,module=x[0],
                                 defaults={'role': tblroleobject,'module':module,'date_created' : constants.FORMATTED_TIME() , 'date_modified': constants.FORMATTED_TIME()},
                            )
                            
                        if int(x[1]) is 3:
                            module = TblModule.objects.get(pk=x[0])
                            person, created = TblModulePermission.objects.update_or_create(
                                 role=tblroleobject,module=x[0],
                                 defaults={'role': tblroleobject,'module':module,'pr_edit':1,'date_created' : constants.FORMATTED_TIME() , 'date_modified' : constants.FORMATTED_TIME()},
                            )
                        if int(x[1]) is 4:
                            module = TblModule.objects.get(pk=x[0])
                            person, created = TblModulePermission.objects.update_or_create(
                                 role=tblroleobject,module=x[0],
                                 defaults={'role': tblroleobject,'module':module,'pr_delete':1,'date_created' : constants.FORMATTED_TIME() , 'date_modified' : constants.FORMATTED_TIME()},
                            )  
                        request.flash['success'] =  "Role and its permission have been successfully added"      
                    return HttpResponseRedirect("/roles/list")
                    
                else:
                    form_errors = form.errors
                    context_dict.update({'form'  :form,'idkey':Id})   
                    return render(request, self.template_name_dict[page], context_dict)
            elif page == 'add':
                print request.POST
                values = []
                formerrors= ''
                form = addroles(request.POST)
                if form.is_valid():
                # if request.POST['role']  != '':
                    
                    tblroleobject = TblRoles.objects.create(name=str(request.POST['name']), date_created = constants.FORMATTED_TIME(), date_updated = constants.FORMATTED_TIME())
                    for key, value in request.POST.items():
                        if key != "name" and  key != "submit" and  key != "csrfmiddlewaretoken":
                            values.append(key.split('-'))
                    for x in values:
                        if int(x[1]) is 2:
                            module = TblModule.objects.get(pk=x[0])
                            person, created = TblModulePermission.objects.update_or_create(
                                 role=tblroleobject,module=x[0],
                                 defaults={'role': tblroleobject,'module':module,'pr_add':1,'date_created' : constants.FORMATTED_TIME() , 'date_modified' : constants.FORMATTED_TIME()},
                            )
                        if int(x[1]) is 1:
                            module = TblModule.objects.get(pk=x[0])
                            person, created = TblModulePermission.objects.update_or_create(
                                 role=tblroleobject,module=x[0],
                                 defaults={'role': tblroleobject,'module':module,'date_created' : constants.FORMATTED_TIME(), 'date_modified': constants.FORMATTED_TIME()},
                            )
                            
                        if int(x[1]) is 3:
                            module = TblModule.objects.get(pk=x[0])
                            person, created = TblModulePermission.objects.update_or_create(
                                 role=tblroleobject,module=x[0],
                                 defaults={'role': tblroleobject,'module':module,'pr_edit':1,'date_created' : constants.FORMATTED_TIME(), 'date_modified' : constants.FORMATTED_TIME()},
                            )
                        if int(x[1]) is 4:
                            module = TblModule.objects.get(pk=x[0])
                            person, created = TblModulePermission.objects.update_or_create(
                                 role=tblroleobject,module=x[0],
                                 defaults={'role': tblroleobject,'module':module,'pr_delete':1,'date_created' : constants.FORMATTED_TIME(), 'date_modified' : constants.FORMATTED_TIME()},
                            )     
                    request.flash['success'] =  "Role and its permissions have been successfully Added"  
                    return HttpResponseRedirect("/roles/list")
                
                else:
                    form_errors = form.errors
                    getmodules = TblModule.objects.all()
                    modulelist = []
                    moduleDictid = {}
                    for x in getmodules:
                        moduleDictid[x.pk] = x.display_name
                        modulelist.append(x.display_name)
                    context_dict.update({'form':form,'dictmodule' : moduleDictid})
                    return render(request, self.template_name_dict[page], context_dict)
                    # form_errors = form.errors
                    # print "egrerg"
                    # print form_errors
                    # context_dict.update({'form'  :form})  
                    # return render(request, self.template_name_dict[page], context_dict)
        
        def get(self, request, **kwargs):


            modulelist = []
            moduleDictid = {}
            permission = request.session['permission']
            context_dict = {'addPermission':permission['role'][0]}
            pagetype = self.kwargs['page']
            return render(request, self.template_name_dict[pagetype], context_dict)

    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno


class OrderListJson(BaseDatatableView):
    print "kunal"   
    # The model we're going to show
    model = TblEmail
  # define the columns that will be returned

    columns = ['id', 'email_from', 'subject','email_date','status','category_id']
    # define the columns that will be returned
   
    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns =  ['id', 'email_from', 'subject','email_date','status','category_id', 'id']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500
    
    def __init__(self):
        pass
           
    def render_column(self, row, column):
        # We want to render user as a custom column
        if column == 'user':
            return '{0} {1}'.format(row.fname, row.lname)
        else:
            return super(OrderListJson, self).render_column(row, column)
#        
    def get_initial_queryset(self):
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        return TblEmail.objects.filter(status=1)
     
    def filter_queryset(self, qs):
        
        search = self.request.GET.get(u'search[value]', None)
        from_date = self.request.GET.get(u'columns[0][search][value]', None)[1:-1]
        to_date = self.request.GET.get(u'columns[1][search][value]', None)[1:-1]
        from_date = from_date.replace('\\', '')
        to_date = to_date.replace('\\', '')
        if search:
            qs_params = None
            q = Q(name__icontains=search)| Q(date_created__icontains=search) 
            qs_params = qs_params | q if qs_params else q
            qs = qs.filter(qs_params)
       
        return qs
    
    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        permission = self.request.session['permission']
        buttonedit = ''
        buttondelete = ''
        json_data = []
        for item in qs:
            # date = str(datetime.datetime.strptime(item.date_created, "%Y-%m-%dT%H:%M:%S"))

            # print "date is:",date
            # if permission['role'][2]==1:
            buttonedit = '<a class="pointer" title="View" href="/unanswered/view/'+ str(item.id) +'"><i class="fa fa-eye" style="color:#3c8dbc;"></i></a>&nbsp;&nbsp;'
            # if permission['role'][1]==1:
            #     buttondelete = '&nbsp;&nbsp;<a class="pointer" title="Delete" onclick="deleteRoles(\''+ str(item.id) +'\');"><i class="fa fa-trash" style="color:#dd4b39;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;'
            item.buttons=buttonedit+buttondelete
            # timecreated = datetime.datetime.strftime(item.date_created,"%I:%M %p")
            # datecreated = str(item.date_created.date().day) + suffix + ' ' + str(item.date_created.date().strftime("%B, %Y")) + " "+str(timecreated)
            
            json_data.append([
                item.email_from,
                item.subject,
                item.email_date,
                item.status,
                item.category_id,
                item.buttons
            ])
        return json_data