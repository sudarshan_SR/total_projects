from django.views.generic import View
from django.template import RequestContext
from django.shortcuts import render
from django.template import RequestContext
from admin_app.forms import Profileforms
from admin_app.forms import PostForm
from django.shortcuts import get_object_or_404
from django.utils import timezone
from admin_app.models import TblAdmin
from django.http import HttpResponseRedirect
import sys

class My_Profile(View):
    try:
        def __init__(self):
            self.template_name = 'my_profile/myProfile.html'

        
        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context

        def get_queryset(self, **kwargs):

            return 

        def post(self, request, **kwargs):
            # form = PostForm(request.POST)

            instance = get_object_or_404(TblAdmin, id=request.user.id)
            form = PostForm(request.POST or None, request.FILES or None, instance=instance)
            if form.is_valid():
                post = form.save(commit=False)
                post.save()
                request.flash['success'] =  'Profile Updated Successfully'
                return HttpResponseRedirect("/my-profile")
            else:
                form_errors = form.errors
                return render(request, self.template_name, {'form': form})
        
        def get(self, request, **kwargs):
            
            post = TblAdmin.objects.get(pk=request.user.id)
            form = PostForm(instance=post)
            return render(request, self.template_name, {'form': form})

    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno
