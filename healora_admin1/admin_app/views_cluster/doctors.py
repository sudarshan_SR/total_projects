import hashlib
import json
import sys,unicodedata,re
import datetime
from django.core.cache import cache
from django.core.mail import EmailMessage
from django.db.models import Q
from django.db import connection
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.template import Context, RequestContext
from django.template.loader import get_template
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.views.generic import View
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.conf import settings
from admin_app.forms import managedoctorform, managedoctorform2,specialityform
from admin_app.models import Doctor, DoctorAddress,DoctorSpeciality,TblSpeciality,DjangoAdminLog

class Doctors(View):
    try:
        def __init__(self, **kwargs):
            # print "fasdfads"
            self.template_name_dict = {
                                     'list' : 'doctor/manageusers.html',
                                     'create' : 'doctor/addUsers.html',
                                     'edit' : 'doctor/addUsers.html'
                                    }
        


        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context

        def get_queryset(self, **kwargs):
            return 
        
        def delete(self, request, *args, **kwargs):
            iduser = kwargs['idkey']
            try:
                # Doctor.objects.filter(pk=iduser).delete()
                Obj = Doctor.objects.get(pk=iduser)
                Obj.status = abs(Obj.status-1)
                Obj.is_active = 0
                Obj.save()
                if Obj.status==1:
                    messg="Doctor Re-Activated"
                    change_message1="Un-Active"
                    change_message2="Active"
                else:
                    messg="Doctor De-Activated"
                    change_message1="Active"
                    change_message2="Un-Active"
                b = DjangoAdminLog(action_time=timezone.now(), object_id=iduser, object_repr=Obj.email, action_flag=1,change_message=messg,pre_change=change_message1,post_change=change_message2,user_id=request.user.id)
                b.save()
                # TblLogs(module=2, module_id=iduser, action=2, user_type=1, user_id=request.user.pk).save()
            except Exception as e:
                print e
            
            data={'message':'Doctor status sucessfully updated!'}
            return HttpResponse(json.dumps(data))
        
        def post(self, request, **kwargs):
            context_dict = {}
            curs=1
            key_type = self.kwargs['page']
            Id = request.POST['userid']
            form = managedoctorform(request.POST or None,request.FILES)
            form1 = managedoctorform2(request.POST or None)
            form2 = specialityform(request.POST or None)
            if key_type == 'edit':
                instance = get_object_or_404(Doctor, pk=int(kwargs['idkey']))
                form = managedoctorform(request.POST or None, request.FILES,instance=instance)
                try:
                    instance1 = get_object_or_404(DoctorAddress, doctor_id=int(Id))
                    form1 = managedoctorform2(request.POST or None,instance=instance1)
                    curs=0
                except Exception, e:
                    print e
                    
                    # post1 = form1.save(commit=False)
                    # post1.doctor_id=int(Id)

                    # post1.save()
                if form.is_valid():
                    post = form.save(commit=False)
                    post1 = form1.save(commit=False)
                    post1.doctor_id=Id
                    post.date_updated =  timezone.now()
                    post1.date_updated =  timezone.now()
                    if curs:
                        post1.date_created =  timezone.now()
                        post1.status = 1
                    post1.save()
                    post.save()
                    obj=get_object_or_404(Doctor, id=post.id)
                    if request.FILES:
                        img= 'http://admin.healora.com/media/'+str(obj.image)
                        cursor = connection.cursor()
                        getMenu=''' UPDATE `doctor` SET `image`=%s WHERE `id`= %s  '''
                        cursor.execute(getMenu,(img,post.id))
                    print "posss"
                    post2=form2.save(commit=False)
                    post1list=post2.speciality
                    post1list=unicodedata.normalize('NFKD', post1list).encode('ascii','ignore')
                    speclist= map(int, re.findall(r'\d+', post1list))
                    print speclist
                    di=[]
                    doc = DoctorSpeciality.objects.filter(doctor_id=Id)
                    for d in doc:
                        ds=d.id
                        di.append(int(d.speciality))
                    for k in speclist:
                        
                        if k in di :
                            # print k
                            di.remove(k)
                            instance= get_object_or_404(DoctorSpeciality, speciality=k,doctor_id=Id)
                            form2 = specialityform(request.POST or None,instance=instance)
                            post2=form2.save(commit=False)
                            post2.speciality=int(k)
                            post2.date_updated =  timezone.now()
                            post2.save()
                        else:
                            form2 = specialityform()
                            post2=form2.save(commit=False)
                            post2.speciality=int(k)
                            post2.doctor_id = int(Id)
                            post2.date_created=timezone.now()
                            post2.date_updated =  timezone.now()
                            sp=get_object_or_404(TblSpeciality,id=k)
                            post2.speciality_name=str(sp.name)
                            post2.save()

                    for n in di:
                        instance= get_object_or_404(DoctorSpeciality, speciality=n,doctor_id=Id)
                        instance.delete()
                    return HttpResponseRedirect("/managedoctor/list")
                else:
                    form_errors = form.errors
                    context_dict.update({'form'  :form, 'form1':form1,'idkey':Id})   
                    return render(request, self.template_name_dict[key_type], context_dict)
            elif key_type == 'create':
                print 'reached here'
                if form.is_valid():
                    post = form.save(commit=False)
                    post1 = form1.save(commit=False)
                    post2 = form2.save(commit=False)
                    # print post
                    readablePassword = post.password
                    post.password = hashlib.md5(readablePassword.encode("utf")).hexdigest()

                    post.is_active =  1
                    post.sex = ""
                    post.date_created =  timezone.now()
                    post.date_updated =  timezone.now()
                    post.status = 1
                    post1.date_created =  timezone.now()
                    post1.date_updated =  timezone.now()
                    post1.status = 1
                    post.save()
                    post1.doctor_id=post.id
                    post1.save()
                    obj=get_object_or_404(Doctor, id=post.id)
                    if request.FILES:
                        img= 'http://admin.healora.com/media/'+str(obj.image)
                        cursor = connection.cursor()
                        getMenu=''' UPDATE `doctor` SET `image`=%s WHERE `id`= %s  '''
                        cursor.execute(getMenu,(img,post.id))
                    intt = post2.speciality
                    post1list=post2.speciality
                    post1list=unicodedata.normalize('NFKD', post1list).encode('ascii','ignore')
                    speclist= map(int, re.findall(r'\d+', post1list))
                    print speclist
                    for k in speclist:
                        form2 = specialityform()
                        post2=form2.save(commit=False)
                        post2.speciality=int(k)
                        post2.doctor_id = post.id
                        post2.date_created=timezone.now()
                        post2.date_updated =  timezone.now()
                        sp=get_object_or_404(TblSpeciality,id=k)
                        post2.speciality_name=str(sp.name)
                        post2.save()
                    # post2.save()
                    # sendRegistrationEmail(request.POST['email'],request.POST['fname'],request.POST['lname'],readablePassword)
                
                    return HttpResponseRedirect("/managedoctor/list")
                else:
                    # print "inside"
                    form_errors = form.errors
                    print form_errors
                    context_dict.update({'form':form,'form1':form1,'idkey':Id})  
                    return render(request, 'doctor/addUsers.html', context_dict)
        
        
        def get(self, request, **kwargs):
            permission = request.session['permission']
            context_dict = {'addPermission':permission['sub_admin'][0]}
            key_type = self.kwargs['page']
            if key_type == 'create':
                form = managedoctorform()
                form1 = managedoctorform2()
                form2 = specialityform()
                sess=TblSpeciality.objects.all()
                
                formsession=''
                formsession=formsession+'<ul id="id_speciality">'
                n=0
                for i in sess:
                    formsession=formsession+'<li><label for="id_speciality_'+str(n)+'">'
                    formsession=formsession+'<input id="id_speciality_'+str(n)+'" maxlength="255"  '
                    formsession=formsession+' name="speciality" type="checkbox" value="'+str(i.id)+'">  '+i.name+'</label></li>'
                    n =n+ 1
                formsession=formsession+'</ul>'
                context_dict.update({'form':form,'form1':form1 ,'form2': formsession,'form3': form2 })
            elif key_type == 'logout':
                # for key in request.session.keys():
                #     del request.session[key]
                menu = cache.delete('menu')
                menu = cache.delete('roleId')
                menu = cache.delete('permission')
                nameurl = "http://admin.doctorinsta.com"
                return HttpResponseRedirect(nameurl+'/auth/logout ')
            elif key_type == 'edit':
                Id = kwargs['idkey']
                print Id
                user = get_object_or_404(Doctor, pk=Id)
                form = managedoctorform(request.GET or None, instance=user)
                spec=DoctorSpeciality.objects.filter(doctor_id=Id)

                sess=TblSpeciality.objects.all()
                
                formsession=''
                formsession=formsession+'<ul id="id_speciality">'
                n=0
                mark=1
                for i in sess:
                    formsession=formsession+'<li><label for="id_speciality_'+str(n)+'">'
                    
                    for j in spec:
                        if j.speciality == i.id :
                            print "done"
                            mark=0
                            formsession=formsession+'<input id="id_speciality_'+str(n)+'" maxlength="255" checked'
                    if mark:
                        print "notdone"
                        formsession=formsession+'<input id="id_speciality_'+str(n)+'" maxlength="255"  '

                    formsession=formsession+' name="speciality" type="checkbox" value="'+str(i.id)+'">  '+i.name+'</label></li>'
                    n =n+ 1
                    mark=1
                formsession=formsession+'</ul>'
                print "type"
                print type(spec)
                # print spec.speciality_namechecked="checked"
                # get_object_or_404(DoctorSpeciality, doctor_id=Id)
                # form2 = specialityform(request.GET or None )
                # for inst in spec:
                sp=get_object_or_404(DoctorSpeciality,id=1)

                form3 = specialityform()

                try:
                    if get_object_or_404(DoctorAddress, doctor_id=Id):
                        userAdd = get_object_or_404(DoctorAddress, doctor_id=Id)
                        print "okokok"
                    if userAdd:
                        form1 = managedoctorform2(request.GET or None, instance=userAdd)
                    context_dict.update({'form'  :form,'form1'  :form1, 'form2':formsession,'form3':form3, 'idkey':Id})
                except Exception, e:
                    print e
                    form1 = managedoctorform2()
                    context_dict.update({'form'  :form,'form1'  :form1,'form2':formsession,'form3':form3, 'idkey':Id})
                
                
                # context_dict.update({'form'  :form,'form1'  :form1,'idkey':Id})   
                
            return render(request, self.template_name_dict[key_type], context_dict)

    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno

def sendRegistrationEmail(email,fname,lname,password):
    if email is not None and email != '':
        pass
            # name = fname + ' ' + lname
            # registraionTemplate = get_template('../templates/registeration.html')
            # c = Context({"passwordvalue": password,"fullname":name})
            # html = registraionTemplate.render(c)
            # subject = "Welcome " + str(fname) + " to Doctor Insta" 
            # email = EmailMessage(subject, html, to=[email],from_email='support@doctorinsta.com')
            # email.content_subtype = "html"
            # email.send()       
class list(View):
    try:
        def __init__(self, **kwargs):
            # print "fasdfasd"
            self.template_name = 'doctor/manageusers.html'
        
        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context
        
        
        def get_queryset(self, **kwargs):

            return 
        
        def delete(self, request, *args, **kwargs):
          
            return 
        
        def post(self, request, **kwargs):

            return
        
        def get(self, request, **kwargs):
#            user_type = self.kwargs['user_type']
            context_dict = {}
            return render(request, self.template_name, context_dict)

    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno

        

class OrderListJson(BaseDatatableView):
   
    # The model we're going to show
    model = Doctor
    # print "testing"
    columns = ['id','first_name', 'last_name', 'email','phone','status','date_created']
    # value like ''
    order_columns = ['id','first_name', 'last_name', 'email','phone','status','status']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500
    
    def __init__(self):
        pass
           
    def render_column(self, row, column):
        # We want to render user as a custom column
        if column == 'user':
            return '{0} {1}'.format(row.fname, row.lname)
        else:
            return super(OrderListJson, self).render_column(row, column)
#        
    def get_initial_queryset(self):
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        return Doctor.objects.all()
     
    def filter_queryset(self, qs):
        # use parameters passed in GET request to filter queryset

        # simple example:
        # search = self.request.GET.get(u'search[value]', None)
        # if search:
        #     #from_date = FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(from_date), '%Y-%m-%d')
        #     qs = qs.filter(Q(fname__istartswith=search)| Q(lname__istartswith=search))

        # # more advanced example using extra parameters
        # filter_customer = self.request.GET.get(u'customer', None)

        # if filter_customer:
        #     customer_parts = filter_customer.split(' ')
        #     qs_params = None
        #     for part in customer_parts:
        #         q = Q(fname_istartswith=part)|Q(lname__istartswith=part)
        #         qs_params = qs_params | q if qs_params else q
        #     qs = qs.filter(qs_params)
        
        # return qs


        search = self.request.GET.get(u'search[value]', None)
        from_date = self.request.GET.get(u'columns[0][search][value]', None)[1:-1]
        to_date = self.request.GET.get(u'columns[1][search][value]', None)[1:-1]
        from_date = from_date.replace('\\', '')
        to_date = to_date.replace('\\', '')
            #from_date = FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(from_date), '%Y-%m-%d')
        if search:
            qs_params = None
            q = Q(first_name__icontains=search)| Q(last_name__icontains=search) | Q(email__icontains=search)|Q(phone__icontains=search)|Q(date_created__icontains=search)
            qs_params = qs_params | q if qs_params else q
            qs = qs.filter(qs_params)
        if from_date:
            qs = qs.filter(Q(date_created__gte=from_date))
        if to_date:
            qs = qs.filter(Q(date_created__lte=to_date))
        return qs
    
    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        # print self.request.session['permission'],"-----------------permission-----------"
        # print "here reached"
        permission = self.request.session['permission']
        buttonedit = ''
        buttondelete = ''
        json_data = []
        for item in qs:
            status="Un-Active"
            if item.status==1:
                status="Active"
            if permission['sub_admin'][2]==1:
                buttonedit = '<a class="pointer" title="Edit" href="/managedoctor/edit/'+ str(item.id) +'"><i class="fa fa-pencil" style="color:#3c8dbc;"></i></a>&nbsp;&nbsp;'
            if permission['sub_admin'][1]==1:
                if item.status==1:
                    buttondelete = '&nbsp;&nbsp;<a class="pointer" title="De-Activate" onclick="deleteDoctor(\''+ str(item.id) +'\');"><i class="fa fa-times" style="color:#dd4b39;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;'
                else:
                    buttondelete = '&nbsp;&nbsp;<a class="pointer" title="Activate" onclick="deleteDoctor(\''+ str(item.id) +'\');"><i class="fa fa-check-square-o" style="color:#dd4b39;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;'
        
            item.buttons=buttonedit+buttondelete
            if 4 <= item.date_created.date().day <= 20 or 24 <= item.date_created.date().day <= 30:
                suffix = "th"
            else:
                suffix = ["st", "nd", "rd"][item.date_created.date().day % 10 - 1]
            timeslot = datetime.datetime.strftime(item.date_created,"%I:%M %p")
            date_created = str(item.date_created.date().day) + suffix + ' ' + str(item.date_created.date().strftime("%B, %Y")) + " "+str(timeslot)
           
            json_data.append([
                item.pk,
                item.first_name,
                item.last_name,
                item.email,
                item.phone,
                status,
                date_created,
                item.buttons
            ])
        return json_data
