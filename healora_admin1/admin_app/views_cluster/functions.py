import csv, traceback
from datetime import datetime
import datetime
import json
from django.db import connection
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.utils.crypto import get_random_string
from django.db.models import Count,Sum,Avg
from rest_framework import status
from django.core.mail import EmailMessage
from admin_app import constants
from admin_app.models import TblAutherization, TblEmail, TblTemplate


@csrf_exempt
def sendEmail(request):
    content = {
                   'status' : 0,
                   'message' : 'No Content',
                   'data' : ''
                   }
    try:
        loadedJsonData = json.loads(request.body)
        toEmail = loadedJsonData.get('sendtoemail')
        content = loadedJsonData.get('content')
        verified = loadedJsonData.get('verified')
        document = loadedJsonData.get('document')
        subject = "Re: "+loadedJsonData.get('sendsubject')
        content=content.replace('"', '')
        content ="<br />".join(content.split("\\n"))
        email = EmailMessage(subject, content, to=[toEmail],from_email='hellosimplifyreality@gmail.com')
        if verified == "1":
            if document!="":
                filepath = settings.MEDIA_ROOT + str(document)
                email.attach_file(filepath)
        email.content_subtype = "html"
        res = email.send()
        if res==1:
            objemail = TblEmail.objects.get(pk=loadedJsonData.get('pk'))
            objemail.status = 1
            objemail.answered_by = request.session['userId']
            objemail.answered_date = constants.FORMATTED_TIME()
            objemail.comments = loadedJsonData.get('comments')
            objemail.response_email = content
            objemail.save()
            content = {
                       'status' : 1,
                       'message' : 'No Content',
                       'data' : ''
                       }
    except Exception as general_exception:
        print str(general_exception)
    return HttpResponse(json.dumps(content))



@csrf_exempt
def processEmail(request):
    cursor = connection.cursor()
    content = {
                   'status' : 0,
                   'message' : 'No Content',
                   'data' : ''
                   }
    try:
      objAllEmail = TblEmail.objects.filter(status=0)
      for x in objAllEmail:
        prepareEmail={}
        sqlTemplate="select a.template from tbl_template as a join tbl_category as b on a.id=b.template_id where b.category_name='"+x.category_id+"'"
        cursor.execute(sqlTemplate)
        getCONTENT = cursor.fetchone()
        responceContent = getCONTENT[0]
        responceContent = responceContent.replace('(user)',"Sir/Mam")
        prepareEmail["to"]=x.email_from
        prepareEmail["subject"] = "Re: "+x.subject
        prepareEmail["content"] = responceContent
        sendRes=sendEmailAuto(prepareEmail)
        if sendRes==1:
            objemail = TblEmail.objects.get(pk=x.pk)
            objemail.status = 1
            objemail.answered_by = "0"
            objemail.answered_date = constants.FORMATTED_TIME()
            objemail.comments = "Auto Generated Reply"
            objemail.response_email = responceContent
            objemail.save()
      content = {
                 'status' : 1,
                 'message' : 'No Content',
                 'data' : ''
                 }
    except Exception as general_exception:
        print str(general_exception)
    return HttpResponse(json.dumps(content))





def sendEmailAuto(data):
    res=False
    content = {
                   'status' : 0,
                   'message' : 'No Content',
                   'data' : ''
                   }
    try:
        toEmail = data["to"]
        content = data["content"]
        subject = data["subject"]
        # content=content.replace('"', '')
        content ="<br />".join(content.split("\\n"))
        email = EmailMessage(subject, content, to=[toEmail],from_email='hellosimplifyreality@gmail.com')
        email.content_subtype = "plain"
        result = email.send()
        if result:
          res = True
    except Exception as general_exception:
        print str(general_exception)
    return res