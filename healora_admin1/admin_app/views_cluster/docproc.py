import hashlib, json, sys, datetime, unicodedata
from django.core.cache import cache
from django.core.mail import EmailMessage
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.template import Context, RequestContext
from django.template.loader import get_template
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.views.generic import View
from django_datatables_view.base_datatable_view import BaseDatatableView

from admin_app.forms import managedocprocform,specialityform1
from admin_app.models import DoctorProcedure, Procedure, Doctor,TblSpeciality


class Docproc(View):
    try:
        def __init__(self, **kwargs):
            procsession=''
            procsession=procsession+'<select class="form-control" id="id_procedure" maxlength="12" name="procedure" required>'
            proc=Procedure.objects.all()
            for p in proc:
                title= p.name
                name=unicodedata.normalize('NFKD', title).encode('ascii','ignore')
                # print type(name)
                procsession=procsession+'<option value="'+str(p.id)+'">'+name+'</option>'
            procsession=procsession+'</select>'
            self.procsession=procsession
            self.template_name_dict = {
                                     'list' : 'docproc/manage.html',
                                     'create' : 'docproc/add.html',
                                     'edit' : 'docproc/add.html',
                                     'data':'docproc/add.html'
                                    }
        

        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context

        def get_queryset(self, **kwargs):
            return 
        
        def delete(self, request, *args, **kwargs):
            iduser = kwargs['idkey']
            try:
                Obj = DoctorProcedure.objects.get(pk=iduser).delete()
                # Obj.is_active = 0
                # Obj.save()
                # TblLogs(module=2, module_id=iduser, action=2, user_type=1, user_id=request.user.pk).save()
            except Exception as e:
                print e
            
            data={'message':'DoctorProcedure sucessfully deleted!'}
            return HttpResponse(json.dumps(data))
        
        def post(self, request, **kwargs):
            context_dict = {}
            key_type = self.kwargs['page']
            Id = request.POST['userid']
            form = managedocprocform(request.POST or None)
            if key_type == 'edit':
                instance = get_object_or_404(DoctorProcedure, pk=int(Id))
                form = managedocprocform(request.POST or None,instance=instance)
                if form.is_valid():
                    post = form.save(commit=False)
                    post.date_updated =  timezone.now()
                    ProObj = Procedure.objects.get(pk=post.procedure)
                    DocObj = Doctor.objects.get(pk=post.doctor)
                    doctorPercent=(float(post.percentage)*float(ProObj.price))/100
                    post.doctor_price=round(float(ProObj.price) + doctorPercent,2)
                    post.total=round((float(post.doctor_price) * 0.18)+post.doctor_price,2)
                    post.procedure_name=ProObj.name
                    post.doctor_name=DocObj.first_name+' '+DocObj.last_name
                    post.save()
                    return HttpResponseRedirect("/docproc/list")
                else:
                    form_errors = form.errors
                    context_dict.update({'form'  :form,'idkey':Id})   
                    return render(request, self.template_name_dict[key_type], context_dict)
            elif key_type == 'create':
                
                if form.is_valid():
                    post = form.save(commit=False)
                    post.date_created =  timezone.now()
                    post.date_updated =  timezone.now()
                    ProObj = Procedure.objects.get(pk=post.procedure)
                    DocObj = Doctor.objects.get(pk=post.doctor)
                    doctorPercent=(float(post.percentage)*float(ProObj.price))/100
                    post.doctor_price=round(float(ProObj.price) + doctorPercent,2)
                    post.total=round((float(post.doctor_price) * 0.18)+post.doctor_price,2)
                    post.procedure_name=ProObj.name
                    post.doctor_name=DocObj.first_name+' '+DocObj.last_name
                    post.status =  1
                    post.save()
                    return HttpResponseRedirect("/docproc/list")
                else:
                    form_errors = form.errors
                    print form_errors
                    context_dict.update({'form'  :form,'idkey':Id})  
                    return render(request, 'docproc/add.html', context_dict)
        
        
        def get(self, request, **kwargs):
            permission = request.session['permission']
            context_dict = {'addPermission':permission['sub_admin'][0]}
            key_type = self.kwargs['page']
           
            if key_type == 'create':
                form = managedocprocform()
                sess=TblSpeciality.objects.all()
                formsession=''
                formsession=formsession+'<div class="form-group"><label class="col-sm-1 control-label" for="speciality">Speciality</label>'
                formsession=formsession+'<div class="col-sm-10">'
                formsession=formsession+'<select class="form-control" id="id_speciality" maxlength="255" name="speciality">'
                for i in sess:
                    formsession=formsession+'<option  value="'+str(i.id)+'">'+i.name+'</option>'
                    
                formsession=formsession+'</select>  </div></div>'
        
                procsession=''
               
                procsession=procsession+'<select class="form-control" id="id_procedure" maxlength="12" name="procedure" required>'
                proc=Procedure.objects.all().order_by('name')
                for p in proc:
                    title= p.name
                    name=unicodedata.normalize('NFKD', title).encode('ascii','ignore')
                    procsession=procsession+'<option value="'+str(p.id)+'">'+name+'</option>'
                procsession=procsession+'</select>'
                print "Doctor"
                # formsession=''
                context_dict.update({'form' :form,'form1':formsession, 'form2': procsession})
                
                # form1 = specialityform1()
            elif key_type == 'logout':
                menu = cache.delete('menu')
                menu = cache.delete('roleId')
                menu = cache.delete('permission')
                nameurl = "http://admin.doctorinsta.com"
                return HttpResponseRedirect(nameurl+'/auth/logout ')
            elif key_type == 'edit':
                procsession=''
                Id = kwargs['idkey']
                user = get_object_or_404(DoctorProcedure, pk=Id)
                form = managedocprocform(request.GET or None, instance=user)
                # form1 = specialityform1()
                print user.procedure
                sess=TblSpeciality.objects.all()
                formsession=''
                formsession=formsession+'<div class="form-group"><label class="col-sm-1 control-label" for="speciality">Speciality</label>'
                formsession=formsession+'<div class="col-sm-10">'
                formsession=formsession+'<select class="form-control" id="id_speciality" maxlength="255" name="speciality">'
                for i in sess:
                    formsession=formsession+'<option value="'+str(i.id)+'">'+i.name+'</option>'
                
                formsession=formsession+'</select>  </div></div>'
                procsession=''
               
                procsession=procsession+'<select class="form-control" id="id_procedure" maxlength="12" name="procedure" required>'
                proc=Procedure.objects.all().order_by('name')
                for p in proc:
                    title= p.name
                    name=unicodedata.normalize('NFKD', title).encode('ascii','ignore')
                    if p.id==user.procedure:
                        procsession=procsession+'<option value="'+str(p.id)+'" selected>'+name+'</option>'
                    else:
                        procsession=procsession+'<option value="'+str(p.id)+'">'+name+'</option>'
                procsession=procsession+'</select>'
                
                
                context_dict.update({'form'  :form,'form1':formsession,'form2':procsession,'idkey':Id})
            elif key_type == 'data':
                Id = kwargs['idkey']
                # print "ID"
                # print Id
                user = get_object_or_404(DoctorProcedure, pk=Id)
                form = managedocprocform(request.GET or None, instance=user)
                self.procsession=''
                # procsession=procsession+'<select class="form-control" id="id_procedure" maxlength="12" name="procedure" required>'
                # proc=Procedure.objects.all()
                # for p in proc:
                #     title= p.name
                #     name=unicodedata.normalize('NFKD', title).encode('ascii','ignore')
                #     procsession=procsession+'<option value="'+str(p.id)+'">'+name+'</option>'
                #     # break
                # procsession=procsession+'</select>'
                # print procsession
                formsession=''
                # context_dict.update({'form' :form,'form1':formsession, 'form2': self.procsession, 'idkey':Id})
                # return render(request, self.template_name_dict[key_type], context_dict)
                

            print context_dict
            return render(request, self.template_name_dict[key_type], context_dict)
    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno

class get_data(View):
    try:
        def get(self,request,**kwargs):
            key_type = self.kwargs['page']
            Id = kwargs['idkey']
            # print "ID"
            # print Id
            user = get_object_or_404(DoctorProcedure, pk=Id)
            form = managedocprocform(request.GET or None, instance=user)
            self.procsession=''
            # procsession=procsession+'<select class="form-control" id="id_procedure" maxlength="12" name="procedure" required>'
            # proc=Procedure.objects.all()
            # for p in proc:
            #     title= p.name
            #     name=unicodedata.normalize('NFKD', title).encode('ascii','ignore')
            #     procsession=procsession+'<option value="'+str(p.id)+'">'+name+'</option>'
            #     # break
            # procsession=procsession+'</select>'
            # print procsession
            formsession=''
            context_dict.update({'form' :form, 'form2': self.procsession, 'idkey':Id})
            # return render(request, self.template_name_dict[key_type], context_dict)
            

            print context_dict
            return render(request, self.template_name_dict[key_type], context_dict)
        pass
    except Exception as e:
        raise e

class list(View):
    try:
        def __init__(self, **kwargs):
            self.template_name = 'docproc/manage.html'
        
        # def get_context_data(self, request, **kwargs):
        #     context = RequestContext(request)
        #     print context
        #     return context
        
        
        # def get_queryset(self, **kwargs):

        #     return 
        
        # def delete(self, request, *args, **kwargs):
          
        #     return 
        
        # def post(self, request, **kwargs):

        #     return
        
        def get(self, request, **kwargs):
#            user_type = self.kwargs['user_type']
            # context_dict.update({'idkey':Id})
            context_dict = {}
            print "here"
            print request
            print kwargs
            return render(request, self.template_name, context_dict)

    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno

        

class OrderListJson(BaseDatatableView):
   
    # The model we're going to show
    model = DoctorProcedure
    columns = ['id','doctor','procedure','doctor_price','percentage','total','date_created']
    # value like ''
    order_columns = ['doctor','id','procedure','doctor_price','percentage','total','status']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500
    
    def __init__(self):
        pass
           
    def render_column(self, row, column):
        # We want to render user as a custom column
        if column == 'user':
            return '{0} {1}'.format(row.fname, row.lname)
        else:
            return super(OrderListJson, self).render_column(row, column)
#        
    def get_initial_queryset(self):
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        return DoctorProcedure.objects.filter(status=1)
     
    def filter_queryset(self, qs):
        # use parameters passed in GET request to filter queryset

        # simple example:
        # search = self.request.GET.get(u'search[value]', None)
        # if search:
        #     #from_date = FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(from_date), '%Y-%m-%d')
        #     qs = qs.filter(Q(fname__istartswith=search)| Q(lname__istartswith=search))

        # # more advanced example using extra parameters
        # filter_customer = self.request.GET.get(u'customer', None)

        # if filter_customer:
        #     customer_parts = filter_customer.split(' ')
        #     qs_params = None
        #     for part in customer_parts:
        #         q = Q(fname_istartswith=part)|Q(lname__istartswith=part)
        #         qs_params = qs_params | q if qs_params else q
        #     qs = qs.filter(qs_params)
        
        # return qs


        search = self.request.GET.get(u'search[value]', None)
        from_date = self.request.GET.get(u'columns[0][search][value]', None)[1:-1]
        to_date = self.request.GET.get(u'columns[1][search][value]', None)[1:-1]
        from_date = from_date.replace('\\', '')
        to_date = to_date.replace('\\', '')
            #from_date = FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(from_date), '%Y-%m-%d')
        if search:
            qs_params = None
            q =Q(id__icontains=search)|Q(doctor_name__icontains=search)|Q(procedure_name__icontains=search)|Q(percentage__icontains=search)|Q(doctor_price__icontains=search)|Q(date_created__icontains=search)
            qs_params = qs_params | q if qs_params else q
            qs = qs.filter(qs_params)
        if from_date:
            qs = qs.filter(Q(date_created__gte=from_date))
        if to_date:
            qs = qs.filter(Q(date_created__lte=to_date))
        return qs
    
    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        # print self.request.session['permission'],"-----------------permission-----------"
        permission = self.request.session['permission']
        buttonedit = ''
        doctorname = ""
        proname = ""
        buttondelete = ''
        json_data = []
        tic=0
        for item in qs:
            if permission['sub_admin'][2]==1:
                buttonedit = '<a class="pointer" title="Edit" href="/docproc/edit/'+ str(item.id) +'"><i class="fa fa-pencil" style="color:#3c8dbc;"></i></a>&nbsp;&nbsp;'
            if permission['sub_admin'][1]==1:
                buttondelete = '&nbsp;&nbsp;<a class="pointer" title="Delete" onclick="deletedocproc(\''+ str(item.id) +'\');"><i class="fa fa-trash" style="color:#dd4b39;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;'
            item.buttons=buttonedit+buttondelete
            if 4 <= item.date_created.date().day <= 20 or 24 <= item.date_created.date().day <= 30:
                suffix = "th"
            else:
                suffix = ["st", "nd", "rd"][item.date_created.date().day % 10 - 1]
            timeslot = datetime.datetime.strftime(item.date_created,"%I:%M %p")
            date_created = str(item.date_created.date().day) + suffix + ' ' + str(item.date_created.date().strftime("%B, %Y")) + " "+str(timeslot)
            try:
                docObj = Doctor.objects.get(pk=item.doctor)
                if tic==int(item.doctor):
                	doctorname = ''
                elif docObj is not None :
                	tic=int(item.doctor)
                	doctorname = docObj.first_name+' '+docObj.last_name
                proObj = Procedure.objects.get(pk=item.procedure)
                if proObj is not None:
                    proname = proObj.name
            except Exception as e:
                pass
            json_data.append([
                item.pk,
                item.doctor_name,
                item.procedure_name,
                item.doctor_price,
                item.percentage,
                item.total,
                date_created,
                item.buttons
            ])
        return json_data
