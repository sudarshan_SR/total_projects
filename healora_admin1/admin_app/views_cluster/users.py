import hashlib
import json
import sys
import datetime
from django.core.cache import cache
from django.core.mail import EmailMessage
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.template import Context, RequestContext
from django.template.loader import get_template
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.views.generic import View
from django_datatables_view.base_datatable_view import BaseDatatableView

from admin_app.forms import manageadminform
from admin_app.models import TblAdmin


class Users(View):
    try:
        def __init__(self, **kwargs):
            self.template_name_dict = {
                                     'list' : 'users/manageusers.html',
                                     'create' : 'users/addUsers.html',
                                     'edit' : 'users/addUsers.html'
                                    }
        


        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context

        def get_queryset(self, **kwargs):
            return 
        
        def delete(self, request, *args, **kwargs):
            iduser = kwargs['idkey']
            try:
                Obj = TblAdmin.objects.get(pk=iduser)
                Obj.is_active = 0
                Obj.save()
                # TblLogs(module=2, module_id=iduser, action=2, user_type=1, user_id=request.user.pk).save()
            except Exception as e:
                print e
            
            data={'message':'User sucessfully deleted!'}
            return HttpResponse(json.dumps(data))
        
        def post(self, request, **kwargs):
            context_dict = {}
            patient_type = self.kwargs['patient_type']
            Id = request.POST['userid']
            form = manageadminform(request.POST or None)
            if patient_type == 'edit':
                instance = get_object_or_404(TblAdmin, pk=int(Id))
                form = manageadminform(request.POST or None,instance=instance,user_id=Id)
                if form.is_valid():
                    post = form.save(commit=False)

                    post.date_updated =  timezone.now()
                    post.save()
                    return HttpResponseRedirect("/manageadmin/list")
                else:
                    form_errors = form.errors
                    context_dict.update({'form'  :form,'idkey':Id})   
                    return render(request, self.template_name_dict[patient_type], context_dict)
            elif patient_type == 'create':
                
                if form.is_valid():
                    post = form.save(commit=False)
                    readablePassword = get_random_string(length=8)
                    post.password = hashlib.md5(readablePassword.encode("utf")).hexdigest()
                    post.created_at =  timezone.now()
                    post.updated_at =  timezone.now()
#                    existingemail = TblRoles.objects.get(id=1,)
                    post.last_login = timezone.now()
                    post.is_active = 1
                    post.save()
                    sendRegistrationEmail(request.POST['email'],request.POST['fname'],request.POST['lname'],readablePassword)
                
                    return HttpResponseRedirect("/manageadmin/list")
                else:
                    form_errors = form.errors
                    print form_errors
                    context_dict.update({'form'  :form,'idkey':Id})  
                    return render(request, 'users/addUsers.html', context_dict)
        
        
        def get(self, request, **kwargs):
            permission = request.session['permission']
            context_dict = {'addPermission':permission['sub_admin'][0]}
            patient_type = self.kwargs['patient_type']
            print patient_type
            if patient_type == 'create':
                form = manageadminform()
                context_dict.update({'form':form})
            elif patient_type == 'logout':
                # for key in request.session.keys():
                #     del request.session[key]
                menu = cache.delete('menu')
                menu = cache.delete('roleId')
                menu = cache.delete('permission')
                nameurl = "http://admin.doctorinsta.com"
                return HttpResponseRedirect(nameurl+'/auth/logout ')
            elif patient_type == 'edit':
                Id = kwargs['idkey']
                user = get_object_or_404(TblAdmin, pk=Id)
                form = manageadminform(request.GET or None, instance=user)
                context_dict.update({'form'  :form,'idkey':Id})   
                
            return render(request, self.template_name_dict[patient_type], context_dict)

    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno

def sendRegistrationEmail(email,fname,lname,password):
    if email is not None and email != '':
        pass
            # name = fname + ' ' + lname
            # registraionTemplate = get_template('../templates/registeration.html')
            # c = Context({"passwordvalue": password,"fullname":name})
            # html = registraionTemplate.render(c)
            # subject = "Welcome " + str(fname) + " to Doctor Insta" 
            # email = EmailMessage(subject, html, to=[email],from_email='support@doctorinsta.com')
            # email.content_subtype = "html"
            # email.send()       
class list(View):
    try:
        def __init__(self, **kwargs):
            
            self.template_name = 'users/manageusers.html'
        
        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context
        
        
        def get_queryset(self, **kwargs):

            return 
        
        def delete(self, request, *args, **kwargs):
          
            return 
        
        def post(self, request, **kwargs):

            return
        
        def get(self, request, **kwargs):
#            user_type = self.kwargs['user_type']
            context_dict = {}
            return render(request, self.template_name, context_dict)

    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno

        

class OrderListJson(BaseDatatableView):
   
    # The model we're going to show
    model = TblAdmin
  # define the columns that will be returned
    columns = ['id','fname', 'lname', 'email','phone','sex']

    # define the columns that will be returned
    columns = ['id','fname', 'lname', 'email','phone','sex']
    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = ['id','fname', 'lname', 'email','phone','sex']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500
    
    def __init__(self):
        pass
           
    def render_column(self, row, column):
        # We want to render user as a custom column
        if column == 'user':
            return '{0} {1}'.format(row.fname, row.lname)
        else:
            return super(OrderListJson, self).render_column(row, column)
#        
    def get_initial_queryset(self):
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        return TblAdmin.objects.filter(is_active=1)
     
    def filter_queryset(self, qs):
        # use parameters passed in GET request to filter queryset

        # simple example:
        # search = self.request.GET.get(u'search[value]', None)
        # if search:
        #     #from_date = FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(from_date), '%Y-%m-%d')
        #     qs = qs.filter(Q(fname__istartswith=search)| Q(lname__istartswith=search))

        # # more advanced example using extra parameters
        # filter_customer = self.request.GET.get(u'customer', None)

        # if filter_customer:
        #     customer_parts = filter_customer.split(' ')
        #     qs_params = None
        #     for part in customer_parts:
        #         q = Q(fname_istartswith=part)|Q(lname__istartswith=part)
        #         qs_params = qs_params | q if qs_params else q
        #     qs = qs.filter(qs_params)
        
        # return qs


        search = self.request.GET.get(u'search[value]', None)
        from_date = self.request.GET.get(u'columns[0][search][value]', None)[1:-1]
        to_date = self.request.GET.get(u'columns[1][search][value]', None)[1:-1]
        from_date = from_date.replace('\\', '')
        to_date = to_date.replace('\\', '')
            #from_date = FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(from_date), '%Y-%m-%d')
        if search:
            qs_params = None
            q = Q(fname__icontains=search)| Q(lname__icontains=search) | Q(email__icontains=search)|Q(sex__icontains=search)|Q(created_at__icontains=search)
            qs_params = qs_params | q if qs_params else q
            qs = qs.filter(qs_params)
        if from_date:
            qs = qs.filter(Q(created_at__gte=from_date))
        if to_date:
            qs = qs.filter(Q(created_at__lte=to_date))
        return qs
    
    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        # print self.request.session['permission'],"-----------------permission-----------"
        permission = self.request.session['permission']
        buttonedit = ''
        buttondelete = ''
        json_data = []
        for item in qs:
            if permission['sub_admin'][2]==1:
                buttonedit = '<a class="pointer" title="Edit" href="/manageadmin/edit/'+ str(item.id) +'"><i class="fa fa-pencil" style="color:#3c8dbc;"></i></a>&nbsp;&nbsp;'
            if permission['sub_admin'][1]==1:
                buttondelete = '&nbsp;&nbsp;<a class="pointer" title="Delete" onclick="deleteFromAdminTable(\''+ str(item.id) +'\');"><i class="fa fa-trash" style="color:#dd4b39;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;'
            item.buttons=buttonedit+buttondelete
            if 4 <= item.created_at.date().day <= 20 or 24 <= item.created_at.date().day <= 30:
                suffix = "th"
            else:
                suffix = ["st", "nd", "rd"][item.created_at.date().day % 10 - 1]
            timeslot = datetime.datetime.strftime(item.created_at,"%I:%M %p")
            created_at = str(item.created_at.date().day) + suffix + ' ' + str(item.created_at.date().strftime("%B, %Y")) + " "+str(timeslot)
           
            json_data.append([
                item.pk,
                item.fname,
                item.lname,
                item.email,
                item.phone,
                item.sex,
                item.buttons
            ])
        return json_data
