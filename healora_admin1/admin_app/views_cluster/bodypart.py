import hashlib
import json
import sys
import datetime
from django.core.cache import cache
from django.core.mail import EmailMessage
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.template import Context, RequestContext
from django.template.loader import get_template
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.views.generic import View
from django_datatables_view.base_datatable_view import BaseDatatableView

from admin_app.forms import managebodyform
from admin_app.models import BodyPart,Procedure,TblCategory


class Bodypart(View):
    try:
        def __init__(self, **kwargs):
            self.template_name_dict = {
                                     'list' : 'bodypart/manage.html',
                                     'create' : 'bodypart/add.html',
                                     'edit' : 'bodypart/add.html'
                                    }
        


        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context

        def get_queryset(self, **kwargs):
            return 
        
        def delete(self, request, *args, **kwargs):
            iduser = kwargs['idkey']
            try:
                Obj = BodyPart.objects.get(pk=iduser).delete()
                # Obj.is_active = 0
                # Obj.save()
                # TblLogs(module=2, module_id=iduser, action=2, user_type=1, user_id=request.user.pk).save()
            except Exception as e:
                print e
            
            data={'message':'Body Part sucessfully deleted!'}
            return HttpResponse(json.dumps(data))
        
        def post(self, request, **kwargs):
            context_dict = {}
            key_type = self.kwargs['page']
            Id = request.POST['userid']
            form = managebodyform(request.POST or None)
            if key_type == 'edit':
                instance = get_object_or_404(BodyPart, pk=int(Id))
                form = managebodyform(request.POST or None,instance=instance)
                if form.is_valid():
                    post = form.save(commit=False)

                    post.date_updated =  timezone.now()
                    post.save()
                    return HttpResponseRedirect("/bodypart/list")
                else:
                    form_errors = form.errors
                    context_dict.update({'form'  :form,'idkey':Id})   
                    return render(request, self.template_name_dict[key_type], context_dict)
            elif key_type == 'create':
                
                if form.is_valid():
                    post = form.save(commit=False)
                    post.date_updated =  timezone.now()
                    post.status =  1
                    post.save()
                    return HttpResponseRedirect("/bodypart/list")
                else:
                    form_errors = form.errors
                    print form_errors
                    context_dict.update({'form'  :form,'idkey':Id})  
                    return render(request, 'bodypart/add.html', context_dict)
        
        
        def get(self, request, **kwargs):
            permission = request.session['permission']
            context_dict = {'addPermission':permission['sub_admin'][0]}
            key_type = self.kwargs['page']
            if key_type == 'create':
                form = managebodyform()
                context_dict.update({'form':form})
            elif key_type == 'logout':
                menu = cache.delete('menu')
                menu = cache.delete('roleId')
                menu = cache.delete('permission')
                nameurl = "http://admin.doctorinsta.com"
                return HttpResponseRedirect(nameurl+'/auth/logout ')
            elif key_type == 'edit':
                Id = kwargs['idkey']
                user = get_object_or_404(BodyPart, pk=Id)
                form = managebodyform(request.GET or None, instance=user)
                context_dict.update({'form'  :form,'idkey':Id})
            return render(request, self.template_name_dict[key_type], context_dict)
    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno

class list(View):
    try:
        def __init__(self, **kwargs):
            self.template_name = 'bodypart/manage.html'
        
        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context
        
        
        def get_queryset(self, **kwargs):

            return 
        
        def delete(self, request, *args, **kwargs):
          
            return 
        
        def post(self, request, **kwargs):

            return
        
        def get(self, request, **kwargs):
#            user_type = self.kwargs['user_type']
            context_dict = {}
            return render(request, self.template_name, context_dict)

    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno

        

class OrderListJson(BaseDatatableView):
   
    # The model we're going to show
    model = BodyPart
    columns = ['id','name','procedurex  ','date_updated']
    # value like ''
    order_columns = ['id','name','status']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500
    
    def __init__(self):
        pass
           
    def render_column(self, row, column):
        # We want to render user as a custom column
        if column == 'user':
            return '{0} {1}'.format(row.fname, row.lname)
        else:
            return super(OrderListJson, self).render_column(row, column)
#        
    def get_initial_queryset(self):
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        return BodyPart.objects.filter(status=1)
     
    def filter_queryset(self, qs):
        # use parameters passed in GET request to filter queryset

        # simple example:
        # search = self.request.GET.get(u'search[value]', None)
        # if search:
        #     #from_date = FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(from_date), '%Y-%m-%d')
        #     qs = qs.filter(Q(fname__istartswith=search)| Q(lname__istartswith=search))

        # # more advanced example using extra parameters
        # filter_customer = self.request.GET.get(u'customer', None)

        # if filter_customer:
        #     customer_parts = filter_customer.split(' ')
        #     qs_params = None
        #     for part in customer_parts:
        #         q = Q(fname_istartswith=part)|Q(lname__istartswith=part)
        #         qs_params = qs_params | q if qs_params else q
        #     qs = qs.filter(qs_params)
        
        # return qs


        search = self.request.GET.get(u'search[value]', None)
        from_date = self.request.GET.get(u'columns[0][search][value]', None)[1:-1]
        to_date = self.request.GET.get(u'columns[1][search][value]', None)[1:-1]
        from_date = from_date.replace('\\', '')
        to_date = to_date.replace('\\', '')
            #from_date = FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(from_date), '%Y-%m-%d')
        if search:
            qs_params = None
            q = Q(name__icontains=search)|Q(date_updated__icontains=search)
            qs_params = qs_params | q if qs_params else q
            qs = qs.filter(qs_params)
        if from_date:
            qs = qs.filter(Q(date_updated__gte=from_date))
        if to_date:
            qs = qs.filter(Q(date_updated__lte=to_date))
        return qs
    
    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        # print self.request.session['permission'],"-----------------permission-----------"
        permission = self.request.session['permission']
        buttonedit = ''
        buttondelete = ''
        json_data = []
        tic=0
        for item in qs:
            if permission['sub_admin'][2]==1:
                buttonedit = '<a class="pointer" title="Edit" href="/bodypart/edit/'+ str(item.id) +'"><i class="fa fa-pencil" style="color:#3c8dbc;"></i></a>&nbsp;&nbsp;'
            if permission['sub_admin'][1]==1:
                buttondelete = '&nbsp;&nbsp;<a class="pointer" title="Delete" onclick="deleteBodypart(\''+ str(item.id) +'\');"><i class="fa fa-trash" style="color:#dd4b39;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;'
            item.buttons=buttonedit+buttondelete
            if 4 <= item.date_updated.date().day <= 20 or 24 <= item.date_updated.date().day <= 30:
                suffix = "th"
            else:
                suffix = ["st", "nd", "rd"][item.date_updated.date().day % 10 - 1]
            timeslot = datetime.datetime.strftime(item.date_updated,"%I:%M %p")
            date_updated = str(item.date_updated.date().day) + suffix + ' ' + str(item.date_updated.date().strftime("%B, %Y")) + " "+str(timeslot)
            # try:
            #     cat=TblCategory.objects.get(body_part=item.pk)

            #     proc = Procedure.objects.filter(category=cat.id)
            #     if not proc:
            #         d= item.pk
            #         nm=item.name
            #         proc=''
            #         json_data.append([
            #         d,
            #         nm,
            #         proc,
            #         date_updated,
            #         item.buttons
            #         ])
            #     else:
            #         for i in proc:
            #             # print item.name
            #             if item.name=="Breast - Plastic Surgery" :
            #                 print i.name
            #             if tic==int(item.pk):
            #                 d=''
            #                 nm=''
            #                 proc=i.name
            #             elif proc is not None :
            #                 d= item.pk
            #                 nm=item.name
            #                 proc=i.name
            #                 tic=int(item.pk)
            #                 # doctorname = docObj.first_name+' '+docObj.last_name
                        
            #             json_data.append([
            #             d,
            #             nm,
            #             proc,
            #             date_updated,
            #             item.buttons
            #             ])
            #         # print i.id
            # # 
            # except Exception as e:
            #     print e
            json_data.append([
                item.pk,
                item.name,
                # proc,
                date_updated,
                item.buttons
            ])
        return json_data
