import sys
from admin_app import constants
from django.db import connection
from django.shortcuts import get_object_or_404, render
from django.template import RequestContext
from django.views.generic import TemplateView
from django_datatables_view.base_datatable_view import BaseDatatableView
from admin_app.models import TblUser
from admin_app.models import TblAdmin,TblSpeciality,Procedure,DoctorProcedure,TblCategory
from admin_app import constants
import datetime
from base_class import Base_Class
from django.http import JsonResponse 
# from django.views.generic import View
from rest_framework.views import APIView
from rest_framework.response import Response
from admin_app.models import Users,Procedure,TblCategory,TblOrder,DoctorProcedure,Doctor


'''
The class Dashboard for all practical purposes only displays content
So it is inherited from TemplateView 

'''
class Orderview(Base_Class, TemplateView):

    try:
        def __init__(self):
            Base_Class.__init__(self)
            self.template_name = 'view/view.html'
            self.name = ''
    

        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context
        def get_queryset(self, **kwargs):

            return 
        def post(self, request, **kwargs):
            
            return
        def get(self, request, **kwargs):
            #print self._menu
            Id = kwargs['idkey']

            instance = get_object_or_404(TblOrder, pk=int(Id))
            user = get_object_or_404(Users, pk=int(instance.user))
            docproc = get_object_or_404(DoctorProcedure, pk=int(instance.doctor_procedure))
            procedure = get_object_or_404(Procedure, pk=int(docproc.procedure))
            print instance.id
            context_dict = {
                            "order_id":instance.order_id,
                            "total_amount":instance.total_amount,
                            "reserve_price":instance.reserve_price,
                            "balance":instance.balance,
                            "order_date":instance.date_created,
                            "comments":instance.comments,
                            "username":user.user_name,
                            "procedure":procedure.name,

                            # "totalorder":"totalorder",
                            # "paidorders":"paidorders"
                            }
            
            return render(request, self.template_name, context_dict)
    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno                   


class OrderListJson(BaseDatatableView):
   
    # The model we're going to show
    model = TblOrder
    # columns = ['order_id','user','doctor_procedure','doctor_procedure','balance','reserve_price','date_created']
    # # value like ''
    # order_columns = ['order_id','user','order_id','order_id','balance','reserve_price','date_created']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500
    
    def __init__(self):
        pass
           
    def render_column(self, row, column):
        # We want to render user as a custom column
        if column == 'user':
            return '{0} {1}'.format(row.fname, row.lname)
        else:
            return super(OrderListJson, self).render_column(row, column)
#        
    def get_initial_queryset(self):
        # idk= self.request.session['payment']
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        return TblOrder.objects.all()
     
    def filter_queryset(self, qs):
        # use parameters passed in GET request to filter queryset

        # simple example:
        # search = self.request.GET.get(u'search[value]', None)
        # if search:
        #     #from_date = FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(from_date), '%Y-%m-%d')
        #     qs = qs.filter(Q(fname__istartswith=search)| Q(lname__istartswith=search))

        # # more advanced example using extra parameters
        # filter_customer = self.request.GET.get(u'customer', None)

        # if filter_customer:
        #     customer_parts = filter_customer.split(' ')
        #     qs_params = None
        #     for part in customer_parts:
        #         q = Q(fname_istartswith=part)|Q(lname__istartswith=part)
        #         qs_params = qs_params | q if qs_params else q
        #     qs = qs.filter(qs_params)
        
        # return qs


        search = self.request.GET.get(u'search[value]', None)
        from_date = self.request.GET.get(u'columns[0][search][value]', None)[1:-1]
        to_date = self.request.GET.get(u'columns[1][search][value]', None)[1:-1]
        from_date = from_date.replace('\\', '')
        to_date = to_date.replace('\\', '')
            #from_date = FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(from_date), '%Y-%m-%d')
        if search:
            qs_params = None
            q = Q(order_id__icontains=search)| Q(total_amount__icontains=search) | Q(reserve_price__icontains=search)|Q(balance__icontains=search)|Q(date_created__icontains=search)
            qs_params = qs_params | q if qs_params else q
            qs = qs.filter(qs_params)
        if from_date:
            qs = qs.filter(Q(date_created__gte=from_date))
        if to_date:
            qs = qs.filter(Q(date_created__lte=to_date))
        return qs
    
    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        # print self.request.session['permission'],"-----------------permission-----------"
        permission = self.request.session['permission']
        buttonedit = ''
        buttondelete = ''
        docprocedure=""
        doctorname = ""
        username = ""
        proname = ""
        json_data = []
        for item in qs:
            if permission['sub_admin'][2]==1:
                buttonedit = '<a class="pointer" title="Edit" href="/payment/edit/'+ str(item.id) +'"><i class="fa fa-pencil" style="color:#3c8dbc;"></i></a>&nbsp;&nbsp;'
            if permission['sub_admin'][1]==1:
                buttondelete = '&nbsp;&nbsp;<a class="pointer" title="Delete" onclick="deleteOrder(\''+ str(item.id) +'\');"><i class="fa fa-trash" style="color:#dd4b39;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;' 
            item.buttons=buttonedit+buttondelete
            if 4 <= item.date_created.date().day <= 20 or 24 <= item.date_created.date().day <= 30:
                suffix = "th"
            else:
                suffix = ["st", "nd", "rd"][item.date_created.date().day % 10 - 1]
            timeslot = datetime.datetime.strftime(item.date_created,"%I:%M %p")
            date_created = str(item.date_created.date().day) + suffix + ' ' + str(item.date_created.date().strftime("%B, %Y")) + " "+str(timeslot)
            try:
                if item.doctor_procedure:
                    docProcObj = DoctorProcedure.objects.get(pk=item.doctor_procedure)
                    docObj = Doctor.objects.get(pk=docProcObj.doctor)
                    if docObj is not None:
                        doctorname = docObj.first_name+' '+docObj.last_name
                    proObj = Procedure.objects.get(pk=docProcObj.procedure)
                    if proObj is not None:
                        proname = proObj.name
                    # print item.user
                    userObj = Users.objects.get(pk=item.user)
                    # print userObj.first_name
                    if userObj is not None:
                        username = userObj.first_name+' '+userObj.last_name
            except Exception as e:
                print e
            # print doctorname
            json_data.append([
                item.order_id,
                username,
                doctorname,
                proname,
                item.total_amount,
                item.reserve_price,
                item.balance,
                date_created,
                item.buttons,
                
            ])
        return json_data
