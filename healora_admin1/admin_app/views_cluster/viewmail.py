import sys

from admin_app import constants
from django.db import connection
from django.shortcuts import render
from django.template import RequestContext
from django.views.generic import TemplateView
from base_class import Base_Class
from admin_app.models import TblEmail, TblCategory, TblTemplate, TblUser
from django.views.decorators.csrf import csrf_exempt
from bs4 import BeautifulSoup
import collections

'''
The class Dashboard for all practical purposes only displays content
So it is inherited from TemplateView 
'''
class Viewmail(Base_Class, TemplateView):
    try:
        def __init__(self):
            Base_Class.__init__(self)
            self.template_name = 'unanswered/viewmail.html'
            self.name = ''

        
        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context

        def get_queryset(self, **kwargs):

            return 

        @csrf_exempt    
        def post(self, request, **kwargs):
#            print "kunal monga++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++==="
            return
        
        def get(self, request, **kwargs):
            responseBody=""
            objemail = TblEmail.objects.get(pk=kwargs["id"])
            if objemail.status==0:
                objCategory = TblCategory.objects.get(category_name=objemail.category_id)
                print objCategory.template_id
                objTemplate = TblTemplate.objects.get(pk=objCategory.template_id)
                userFname= "Sir/Mam"
                try:
                    objUser = TblUser.objects.get(pk=objemail.business_user_id)
                    userFname=objUser.fname
                except TblUser.DoesNotExist:
                    pass
                
                responseBody = objTemplate.template
                responseBody = responseBody.replace('(user)',userFname)
                context_dict = {
                            "pk":objemail.pk,
                            "emailFrom":objemail.email_from,
                            "subject":objemail.subject,
                            "emailDate":objemail.email_date,
                            "emailStatus":objemail.status,
                            "emailCategory":objemail.category_id,
                            "emailBody":objemail.body,
                            "responseBody":responseBody
                                }
            else:
                # objUser = TblUser.objects.get(pk=objemail.business_user_id)
                context_dict = {
                            "pk":objemail.pk,
                            "emailFrom":objemail.email_from,
                            "subject":objemail.subject,
                            "emailDate":objemail.email_date,
                            "emailStatus":objemail.status,
                            "emailCategory":objemail.category_id,
                            "emailBody":objemail.body,
                            "responseBody":objemail.response_email
                                }
            return render(request, self.template_name, context_dict)

    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno
        

