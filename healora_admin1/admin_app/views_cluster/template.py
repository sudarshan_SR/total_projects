import json
import sys
import datetime
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.template import RequestContext
from django.utils import timezone
from django.views.generic import View
from django_datatables_view.base_datatable_view import BaseDatatableView

from admin_app import constants
from admin_app.forms import addroles, addtemplate
from admin_app.models import TblAdmin, TblModule, TblModulePermission, TblRoles, TblTemplate, TblBusiness


class Template(View):
    try:
        def __init__(self, **kwargs):
            
            self.template_name_dict = {
                                     'list' : 'template/list.html',
                                     'add' : 'template/addtemplate.html',
                                     'edit' : 'template/addtemplate.html'
                                    }
        
        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context

        def get_queryset(self, **kwargs):
            return 
        
        def delete(self, request, *args, **kwargs):
            iduser = kwargs['idkey']
            # try:
            roleobject = TblRoles.objects.filter(pk=iduser)
            Obj = TblAdmin.objects.filter(role=roleobject)
            if Obj:
                data={'message':'Role can not be deleted, Its assigned to a User'}
                return HttpResponse(json.dumps(data))
            else:
               
                TblRoles.objects.filter(pk=iduser).delete()
                # TblLogs(module=2, module_id=iduser, action=2, user_type=1, user_id=request.user.pk).save()
                data={'message':'Role sucessfully Deleted'}
                return HttpResponse(json.dumps(data))
            # except Exception as e:
            #     data={'message':'Error deleting the role'}
            #     return HttpResponse(json.dumps(data))
            
        
        def post(self, request, **kwargs):
            context_dict = {}
            page = self.kwargs['page']
            
            values = []
            # print request.POST
            
            if page == 'edit':
                Id = kwargs['idkey']
                instance = get_object_or_404(TblTemplate, pk=int(Id))
                form = addtemplate(request.POST or None,instance=instance,user_id=Id)
                if form.is_valid():
                    post = form.save(commit=False)
                    post.save()
                    return HttpResponseRedirect("/template/list")
                else:
                    form_errors = form.errors
                    context_dict.update({'form'  :form,'idkey':Id})   
                    return render(request, self.template_name_dict[page], context_dict)
            elif page == 'add':
                print request.POST
                values = []
                formerrors= ''
                form = addtemplate(request.POST)
                if form.is_valid():
                # if request.POST['role']  != '':
                    post = form.save(commit=False)
                    post.save()
                    request.flash['success'] =  "Template have been successfully Added"  
                    return HttpResponseRedirect("/template/list")
                
                else:
                    form_errors = form.errors
                    getmodules = TblModule.objects.all()
                    modulelist = []
                    moduleDictid = {}
                    for x in getmodules:
                        moduleDictid[x.pk] = x.display_name
                        modulelist.append(x.display_name)
                    context_dict.update({'form':form,'dictmodule' : moduleDictid})
                    return render(request, self.template_name_dict[page], context_dict)
                    # form_errors = form.errors
                    # print "egrerg"
                    # print form_errors
                    # context_dict.update({'form'  :form})  
                    # return render(request, self.template_name_dict[page], context_dict)
        
        def get(self, request, **kwargs):


            modulelist = []
            moduleDictid = {}
            permission = request.session['permission']
            context_dict = {'addPermission':permission['role'][0]}
            pagetype = self.kwargs['page']
            if pagetype == 'add':
                    # role =  request.session['roleId']
                    # getmodules = TblModulePermission.objects.filter(role=role)
                    # for x in getmodules:
                    #     moduleDictid[x.module.pk] = x.module.display_name
                form = addtemplate()
                context_dict.update({'form':form,'dictmodule' : moduleDictid})
            elif pagetype == 'edit':

                Id = kwargs['idkey']
                curTemplate = get_object_or_404(TblTemplate, pk=Id)
                form = addtemplate(request.GET or None, instance=curTemplate)
                context_dict.update({'form'  :form,'idkey':Id})    
            return render(request, self.template_name_dict[pagetype], context_dict)

    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno



class OrderListJson(BaseDatatableView):
   
    # The model we're going to show
    model = TblTemplate
  # define the columns that will be returned
    columns = ['id', 'name', 'template','business_id']

    # define the columns that will be returned
   
    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns =  ['id', 'name', 'template','business_id']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500
    
    def __init__(self):
        pass
           
    def render_column(self, row, column):
        # We want to render user as a custom column
        if column == 'user':
            return '{0} {1}'.format(row.fname, row.lname)
        else:
            return super(OrderListJson, self).render_column(row, column)
#        
    def get_initial_queryset(self):
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        return TblTemplate.objects.filter()
     
    def filter_queryset(self, qs):
        # use parameters passed in GET request to filter queryset

        # simple example:
        # search = self.request.GET.get(u'search[value]', None)
        # if search:
        #     #from_date = FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(from_date), '%Y-%m-%d')
        #     qs = qs.filter(Q(fname__istartswith=search)| Q(lname__istartswith=search))

        # # more advanced example using extra parameters
        # filter_customer = self.request.GET.get(u'customer', None)

        # if filter_customer:
        #     customer_parts = filter_customer.split(' ')
        #     qs_params = None
        #     for part in customer_parts:
        #         q = Q(fname_istartswith=part)|Q(lname__istartswith=part)
        #         qs_params = qs_params | q if qs_params else q
        #     qs = qs.filter(qs_params)
        
        # return qs


        search = self.request.GET.get(u'search[value]', None)
        from_date = self.request.GET.get(u'columns[0][search][value]', None)[1:-1]
        to_date = self.request.GET.get(u'columns[1][search][value]', None)[1:-1]
        from_date = from_date.replace('\\', '')
        to_date = to_date.replace('\\', '')
            #from_date = FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(from_date), '%Y-%m-%d')
        if search:
            qs_params = None
            q = Q(name__icontains=search)| Q(date_created__icontains=search) 
            qs_params = qs_params | q if qs_params else q
            qs = qs.filter(qs_params)
       
        return qs
    
    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        permission = self.request.session['permission']
        buttonedit = ''
        businessName = ''
        buttondelete = ''
        json_data = []
        for item in qs:
            buttonedit = '<a class="pointer" title="Edit" href="/template/edit/'+ str(item.id) +'"><i class="fa fa-pencil" style="color:#3c8dbc;"></i></a>&nbsp;&nbsp;'
            item.buttons=buttonedit+buttondelete
            try:
                business = TblBusiness.objects.get(pk=item.business_id)
                businessName = business.name
            except TblBusiness.DoesNotExist:
                businessName = ""
            json_data.append([
                item.pk,
                item.name.title(),
                item.template,
                businessName,
                item.buttons
            ])
        return json_data