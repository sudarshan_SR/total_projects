from django.db import connection

# from memcached_stats import MemcachedStats

'''
List of all types of Built In Views : https://docs.djangoproject.com/en/1.9/ref/class-based-views/
'''

class Base_Class(object):
    def __init__(self):
#        answers = TblModulePermission.objects.filter(role=1).select_related(priority=0)
#        for x in answers:
#            print x.module.module_name
        pass
#         if cache.get('menu') is None or cache.get('menu')=='':
#             role = cache.get('roleId')
#             parentId=0
#             menu = self.create_menu(role=role,parentId=parentId)
#             cache.set('menu', menu)
#         self.menu = cache.get('menu')
#        print self.menu
#        cursor = connection.cursor()
#        getChilds=''' select '''
#        cursor.execute(getChilds,(userId,))
        
    def create_menu(self,**kwargs):
        menu='<ul>'
        cursor = connection.cursor()
        getMenu=''' SELECT b.module_name,b.display_name,b.url,b.class,b.has_child,b.parent_id,b.priority,b.id FROM tbl_module_permission AS a JOIN tbl_module AS b ON a.module = b.id where b.parent_id = %s AND a.role =%s ORDER BY b.priority ASC  '''
        cursor.execute(getMenu,(kwargs['parentId'],kwargs['role']))
        for x in cursor:
            print "menu"
            print x[1]
            moduleId = x[7]
            addClass = ''
            if x[4] == 1:
                addClass = 'active has-sub'
            menu=menu+'<li class="'+addClass+'">'
            menu=menu+'<a href="'+x[2]+'"><span><i class="'+x[3]+'"></i> '+x[1]
            menu=menu+'</span></a>'
            if x[4] == 1:
                menu = menu+self.create_menu(role=kwargs['role'],parentId=moduleId)
            menu=menu+'</li>'
        menu=menu+'</ul>'
        return menu