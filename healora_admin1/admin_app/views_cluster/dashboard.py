import sys
from admin_app import constants
from django.db import connection
from django.shortcuts import render
from django.template import RequestContext
from django.views.generic import TemplateView
from admin_app.models import TblUser
from admin_app.models import TblAdmin,TblSpeciality,Procedure,DoctorProcedure,TblCategory
from admin_app import constants
import datetime
from base_class import Base_Class
from django.http import JsonResponse 
# from django.views.generic import View
from rest_framework.views import APIView
from rest_framework.response import Response


'''
The class Dashboard for all practical purposes only displays content
So it is inherited from TemplateView 

'''
class Dashboard(Base_Class, TemplateView):

    try:
        def __init__(self):
            Base_Class.__init__(self)
            self.template_name = 'dashboard/dashboard.html'
            self.name = ''
    

        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context
        def get_queryset(self, **kwargs):

            return 
        def post(self, request, **kwargs):
            
            return
        def get(self, request, **kwargs):
            #print self._menu
            completedCount = 0
            countDoctor=0
            rating=[]
     
            cursor = connection.cursor()
            getdata=''' SELECT COUNT(*) FROM tbl_doctor '''
            print type(cursor.execute(getdata));
            for x in cursor:
                countDoctor = x[0]
                break;
            cursor = connection.cursor()
            getdata=''' SELECT COUNT(*) FROM `order` '''
            print type (cursor.execute(getdata));
            for x in cursor:
                totalorder = x[0]
                break;
            cursor = connection.cursor()
            getdata='''SELECT COUNT(*) FROM users '''
            cursor.execute(getdata);
            for x in cursor:
                userCount = x[0]
                break;
            data=''' SELECT COUNT(temp) FROM `order` WHERE temp=4; '''
            cursor.execute(data);
            for x in cursor:
                paidorders = x[0]
                break;
            
            context_dict = {
                            "totalAppt":"",
                            "completedAppt":"",
                            "totalRevenue":"",
                            "countDoctor":"countDoctor",
                            "upcomingAppt":"",
                            "userCount":"userCount",
                            "totalorder":"totalorder",
                            "paidorders":"paidorders"
                            }
            request.session['countDoctor']=countDoctor
            print request.session['countDoctor']
            request.session['totalorder']=totalorder
            print request.session['totalorder']
            request.session['userCount']=userCount
            print request.session['userCount']
            request.session['paidorders']=paidorders
            print request.session['paidorders']
            return render(request, self.template_name, context_dict)
    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno                   

def get_data(request,*args,**kwargs):
    data = {
        "sales": 100,
        "customers": 10
        }
    return JsonResponse(data)


class ChartData(APIView):
    authentication_classes = []
    permission_classes = []
    
    # user = TblAdmin.get_object_or_404(TblAdmin, pk=Id)

    def get(self, request, format=None):
        # cursor = connection.cursor()
        # getdata=''' SELECT COUNT(*) FROM orders '''
        # print "orders"
        # print type (cursor.execute(getdata));
        # for x in cursor:
        #     totalorder = x[0]
        #     print "orders :"
        #     print totalorder
        #     break;
        
        now=constants.FORMATTED_TIME()
        t=str(constants.FORMATTED_TIME())
        p=str( now.date() - datetime.timedelta(days=1) )
        p=p+" "+"00:00:00"
        
        w=str( now.date() - datetime.timedelta(days=7) )
        w=w+" "+"00:00:00"
        
        m=str( now.date() - datetime.timedelta(days=30) )
        m=m+" "+"00:00:00"
        y=str( now.date() - datetime.timedelta(days=365) )
        y=y+" "+"00:00:00"
        
        cursor = connection.cursor()
        getdata='''SELECT COUNT(*) from `order` where `date_created` between ' '''+p+''' ' and ' '''+t+''' '  '''
        cursor.execute(getdata)
        for x in cursor:
            daily = x[0]

        getdata='''SELECT COUNT(*) from `order` where `date_created` between ' '''+w+''' ' and ' '''+t+''' '  '''
        # print "orders"
        cursor.execute(getdata)
        for x in cursor:
            week = x[0]

        getdata='''SELECT COUNT(*) from `order` where `date_created` between ' '''+m+''' ' and ' '''+t+''' '  '''
        # print "orders"
        cursor.execute(getdata)
        for x in cursor:
            month = x[0]
        getdata='''SELECT COUNT(*) from `order` where `date_created` between ' '''+y+''' ' and ' '''+t+''' '  '''
        # print "orders"
        cursor.execute(getdata)
        for x in cursor:
            year = x[0]
            # print totalorder
    
        labels=["Daily","Weekly","Monthly","Yearly"]
        default_items=[daily,week,month,year]
        data = {
           "labels": labels,
           "defaultd": default_items,
           # "users":User.object.all().count()
        }
        return JsonResponse(data)

class PieChartData(APIView):
    authentication_classes = []
    permission_classes = []
    
    # user = TblAdmin.get_object_or_404(TblAdmin, pk=Id)

    def get(self, request, format=None):
        # cursor = connection.cursor()
        # getdata=''' SELECT COUNT(*) FROM orders '''
        # print "orders"
        # print type (cursor.execute(getdata));
        # for x in cursor:
        #     totalorder = x[0]
        #     print "orders :"
        #     print totalorder
        #     break;
        
        # now=constants.FORMATTED_TIME()
        # t=str(constants.FORMATTED_TIME())
        # print t
        # p=str( now.date() - datetime.timedelta(days=1) )
        # p=p+" "+"00:00:00"
        label1=[]
        default_items=[]
        spec=TblSpeciality.objects.filter(status=1)
        for x in spec:
            c=0
            label1.append(str(x.name))
            cat=TblCategory.objects.filter(speciality=x.id)
            for i in cat:
                proc = Procedure.objects.filter(category=i.id)
                if not proc:
                    print "ddd"
                    
                else:
                    # print "her"
                    for j in proc:
                        doc=DoctorProcedure.objects.filter(procedure=j.id)
                        c=c+doc.count()
            default_items.append(c)
        # print label1
        labels=label1
        # ["Daily","Weekly","Monthly","Yearly"]
        # default_items=[12,5,2,11]
        data = {
           "labels": labels,
           "defaultd": default_items,
           # "users":User.object.all().count()
        }
        return JsonResponse(data)

