from django.views.generic import View
from django.shortcuts import render
import sys
from django.template import RequestContext

class Manage_Users(View):
    try:
        def __init__(self, **kwargs):
            
            self.template_name_dict = {
                                     'patients' : 'users/managePatients.html',
                                     'practioners' : 'users/managePractitioners.html',
                                     'subadmins' : 'users/manageSubAdmins.html'
                                    }
        
        def get_context_data(self, request, **kwargs):
            context = RequestContext(request)
            return context

        def get_queryset(self, **kwargs):

            return 

        def post(self, request, **kwargs):

            return
        
        def get(self, request, **kwargs):
            user_type = self.kwargs['user_type']
            context_dict = {}
            return render(request, self.template_name_dict[user_type], context_dict)

    except Exception as general_exception:
        print general_exception
        print sys.exc_traceback.tb_lineno
