from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django_datatables_view.base_datatable_view import BaseDatatableView
from admin_app.models import TblSchedule
# from memcached_stats import MemcachedStats

'''
List of all types of Built In Views : https://docs.djangoproject.com/en/1.9/ref/class-based-views/
'''

class OrderListJson(BaseDatatableView):
    # The model we're going to show
    model = TblSchedule

    # define the columns that will be returned
    columns = ['timeslot', 'slot_date', 'reschedule_status']

    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = ['timeslot', 'slot_date', 'reschedule_status']

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500
    
    def __init__(self):
        pass
           
    def render_column(self, row, column):
        # We want to render user as a custom column
        if column == 'user':
            return '{0} {1}'.format(row.fname, row.lname)
        else:
            return super(OrderListJson, self).render_column(row, column)
        
#     def get_initial_queryset(self):
#         # return queryset used as base for futher sorting/filtering
#         # these are simply objects displayed in datatable
#         # You should not filter data returned here by any filter values entered by user. This is because
#         # we need some base queryset to count total number of records.
#         return TblSchedule.objects.filter(status=1)
#     
    def filter_queryset(self, qs):
        # use parameters passed in GET request to filter queryset

        # simple example:
        search = self.request.GET.get(u'search[value]', None)
        from_date = self.request.GET.get(u'columns[0][search][value]', None)[1:-1]
        to_date = self.request.GET.get(u'columns[1][search][value]', None)
        from_date = from_date.replace('\\', '')
        if from_date:
            #from_date = FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(from_date), '%Y-%m-%d')
            qs = qs.filter(fname__istartswith=search, date__gte=from_date)

        # more advanced example using extra parameters
        filter_customer = self.request.GET.get(u'customer', None)

        if filter_customer:
            customer_parts = filter_customer.split(' ')
            qs_params = None
            for part in customer_parts:
                q = Q(fname_istartswith=part)|Q(lname__istartswith=part)
                qs_params = qs_params | q if qs_params else q
            qs = qs.filter(qs_params)
        
        return qs
    
    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        json_data = []
        for item in qs:
            json_data.append([
                item.timeslot,
                item.slot_date,
                item.reschedule_status
            ])
        return json_data