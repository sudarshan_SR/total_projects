from django import template
register = template.Library()

@register.filter(name = 'get_item')
def get_item(status):
    if status == 1:
        return 'Active'
    elif status == 0:
        return 'Inactive'
    else:
        return 'Invalid'
    
@register.filter(name = 'prepare_media_path')
def prepare_media_path(url):
    return '/media/' + str(url)


@register.filter(name = 'role_permissions_add')
def role_permissions_add(d, key_name):
	try:
        	value = d[key_name]['add']
		return value
	except KeyError:
		value =''
		return value
role_permissions_add = register.filter('role_permissions_add', role_permissions_add)

@register.filter(name = 'role_permissions_edit')
def role_permissions_edit(d, key_name):
	
	try:

		value = d[key_name]['edit']
		return value
	except KeyError:
		value =''
		return value
role_permissions_edit = register.filter('role_permissions_edit', role_permissions_edit)


@register.filter(name = 'role_permissions_delete')
def role_permissions_delete(d, key_name):
	
	try:

		value = d[key_name]['delete']
		return value
	except KeyError:
		value =''
		return value
role_permissions_delete = register.filter('role_permissions_delete', role_permissions_delete)

@register.filter(name='access')
def access(value, arg):
    return value[arg]

@register.filter(name='split')
def split(string, sep):
    """Return the string split by sep.

    Example usage: {{ value|split:"/" }}
    """
    stringvar = string.split(sep)
    print stringvar
    return stringvar[1]