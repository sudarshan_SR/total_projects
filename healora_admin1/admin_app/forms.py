from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import os, re, numbers, traceback
from itertools import chain
from django import forms
from django.db.models import Q
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from admin_app.models import TblAdmin, TblRoles, TblTemplate, TblBusiness, TblCategory,DoctorSpeciality, Doctor, DoctorAddress, BodyPart, TblSpeciality, Procedure, DoctorProcedure, Blog ,Users,TblOrder
from django.db.models.fields.files import FieldFile
from django.forms.widgets import HiddenInput
from admin_app import constants
import validators
from custom_widgets import ColumnCheckboxSelectMultiple


class Profileforms(forms.Form):
    name = forms.CharField(label='Your name', max_length=100)


class PostForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)

    def clean(self):
        error_messages = []
        cleaned_data = super(PostForm, self).clean()
        email = cleaned_data.get("email")
        fname = cleaned_data.get("fname")
        lname = cleaned_data.get("lname")
        phone = cleaned_data.get("phone")
        sex = cleaned_data.get("sex")
        dob = cleaned_data.get("dob")
        profile_pic = cleaned_data['profile_picture']

        if fname is None or fname == '':
            error_messages.append('First Name can not be Blank')
        if lname is None or lname == '':
            error_messages.append('Last Name can not be Blank')
        else:
            if len(str(phone)) != 10:
                error_messages.append('Phone must be 10 digit number')
        if not re.match(r'^[0-9]+$', str(phone)):
            error_messages.append('Enter a valid Phone number')
        if not re.match(r'^[A-Za-z]+$', fname):
            error_messages.append("Enter a valid First Name")
        if not re.match(r'^[A-Za-z]+$', lname):
            error_messages.append("Enter a valid Last Name")
        existing = TblAdmin.objects.filter(
            phone=phone,
        ).filter(~Q(email=email)).exists()
        if profile_pic:
            ext = os.path.splitext(profile_pic.name)[1]
            if ext != '.jpg' and ext != '.png':
                error_messages.append("Select a valid Image.")

        if len(fname) < 3:
            error_messages.append('First Name must be 3 characters long')
        if len(lname) < 3:
            error_messages.append('Last Name must be 3 characters long')

        if existing:
            error_messages.append('Phone number must be unique')
        if sex is None or sex == '':
            error_messages.append('Sex  can not be Blank')

        if dob is None or dob == '':
            error_messages.append('Date Of Birth  can not be Blank')

        if len(error_messages):
            raise forms.ValidationError(error_messages)
            return self.cleaned_data

    class Meta:
        model = TblAdmin
        fields = ('email', 'fname', 'lname', 'phone', 'sex', 'dob', 'profile_picture')
        SEX = (
            ('', 'Select a value'),
            ('male', 'Male'),
            ('female', 'Female'),
        )
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'fname': forms.TextInput(attrs={'class': 'form-control', 'required': True, 'pattern ': '[A-Za-z]{3,}'}),
            'lname': forms.TextInput(attrs={'class': 'form-control', 'required': True, 'pattern ': '[A-Za-z]{3,}'}),
            'phone': forms.TextInput(attrs={'class': 'form-control', 'maxlength': 10}),
            'sex': forms.Select(choices=SEX, attrs={'class': 'form-control'}),
            'dob': forms.TextInput(attrs={'class': 'form-control', 'id': 'dob', 'readonly': True}),
            'profile_picture': forms.ClearableFileInput(),

        }
        labels = {
            'email': "Email *",
            'fname': "First Name *",
            'lname': "Last Name *",
            'phone': "Phone *",
            'sex': "Sex *",
            'dob': "Dob *",
            'profile_picture': "Profile Picture ",

        }

class manageadminform(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.user_id = kwargs.pop('user_id', None)
        super(manageadminform, self).__init__(*args, **kwargs)
        self.fields['email'].required = False
        self.fields['phone'].required = False

    def clean(self):
        error_messages = []
        cleaned_data = super(manageadminform, self).clean()
        idkey = self.user_id
        email = cleaned_data.get("email")
        phone = cleaned_data.get("phone")
        lname = cleaned_data.get("lname")
        phone = cleaned_data.get("phone")
        sex = cleaned_data.get("sex")
        dob = cleaned_data.get("dob")
        role = cleaned_data.get("role")

        if idkey is not None and idkey != '':

            existingemail = TblAdmin.objects.filter(email=email, ).filter(~Q(id=idkey)).exists()
            existingPhone = TblAdmin.objects.filter(phone=phone, ).filter(~Q(id=idkey)).exists()
        else:

            existingemail = TblAdmin.objects.filter(email=email, ).exists()
            existingPhone = TblAdmin.objects.filter(phone=phone, ).exists()
        # # validate coupon name (check for special characters)
        # if re.match("^[a-zA-Z0-9_]*$", offercode):
        # 	value = True
        # else:
        # 	error_messages.append('Enter a Valid Coupon Name')
        try:
            validate_email(email)
            valid = True
        except ValidationError:
            error_messages.append('Invalid Email Format')

        if type(phone) is None:
            error_messages.append('Please enter a valid number')

        if existingemail:
            error_messages.append('Email Already Exist')
        if existingPhone:
            error_messages.append('Phone Already Exist')
        # if image:
        #     ext = os.path.splitext(image.name)[1]
        #     if ext != '.jpg' and ext != '.png':
        #         error_messages.append("Select a valid Image.")
        # else:
        #     if image is None or image is False:
        #         error_messages.append("Image Must be Selected")
        if len(error_messages):
            raise forms.ValidationError(error_messages)
            return self.cleaned_data

    class Meta:
        model = TblAdmin
        fields = ('email', 'fname', 'lname', 'phone', 'sex', 'dob', 'role')
        exclude = ('created_at',)
        SEX = (
            ('', 'Select a value'),
            ('male', 'Male'),
            ('female', 'Female'),
        )
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'fname': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'lname': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'phone': forms.TextInput(attrs={'class': 'form-control', 'maxlength': 10, 'required': True,}),
            'sex': forms.Select(choices=SEX, attrs={'class': 'form-control', 'required': True,}),
            'dob': forms.TextInput(attrs={'class': 'form-control', 'id': 'dob', 'required': True, }),#'readonly': True,
            'role': forms.Select(attrs={'class': 'form-control', 'required': True,}),

        }


class managedoctorform(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.user_id = kwargs.pop('user_id', None)
        super(managedoctorform, self).__init__(*args, **kwargs)
        self.fields['email'].required = False
        self.fields['phone'].required = False

    def clean(self):
        error_messages = []
        cleaned_data = super(managedoctorform, self).clean()
        idkey = self.user_id
        email = cleaned_data.get("email")
        phone = cleaned_data.get("phone")
        last_name = cleaned_data.get("last_name")
        first_name = cleaned_data.get("first_name")
        image = cleaned_data.get("image")
        degree_title = cleaned_data.get("degree_title")
        # dob = cleaned_data.get("dob")
        fax = cleaned_data.get("fax")
        # password = cleaned_data.get("password")

        if idkey is not None and idkey != '':
            print "testing", idkey
            existingemail = Doctor.objects.filter(email=email, ).filter(~Q(id=idkey)).exists()
            existingPhone = Doctor.objects.filter(phone=phone, ).filter(~Q(id=idkey)).exists()
            print "af"
        else:
            print idkey

            existingemail = Doctor.objects.filter(email=email, ).exists()
            existingPhone = Doctor.objects.filter(phone=phone, ).exists()
        # # validate coupon name (check for special characters)
        # if re.match("^[a-zA-Z0-9_]*$", offercode):
        #   value = True
        # else:
        #   error_messages.append('Enter a Valid Coupon Name')
        try:
            validate_email(email)
            valid = True
        except ValidationError:
            error_messages.append('Invalid Email Format')

        if type(phone) is None:
            error_messages.append('Please enter a valid number')

        # if existingemail:
        #     error_messages.append('Email Already Exist')
        # if existingPhone:
        #     error_messages.append('Phone Already Exist')
        if len(error_messages):
            raise forms.ValidationError(error_messages)
            return self.cleaned_data

    class Meta:
        model = Doctor
        fields = ('email', 'first_name', 'last_name', 'phone', 'degree_title', 'fax','image','password')
        exclude = ('created_at',)
        SEX = (
            ('', 'Select a value'),
            ('male', 'Male'),
            ('female', 'Female'),
        )
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'first_name': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'last_name': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'phone': forms.TextInput(attrs={'class': 'form-control', 'maxlength': 10, 'required': True,}),
            'fax': forms.TextInput(attrs={'class': 'form-control', 'maxlength': 10, 'required': False,}),
            'degree_title': forms.TextInput(attrs={'class': 'form-control', 'maxlength': 10, 'required': True,}),
            'image': forms.ClearableFileInput(),
            'password': forms.TextInput(attrs={'class': 'form-control', 'maxlength': 10, 'required': True,}),
        }


class managedoctorform2(forms.ModelForm):
    class Meta:
        model = DoctorAddress
        fields = ('address_line1','address_line2','appartment','city', 'state', 'zipcode')
        widgets = {
            'address_line1': forms.TextInput(attrs={'class': 'form-control'}),
            'address_line2': forms.TextInput(attrs={'class': 'form-control'}),
            'appartment': forms.TextInput(attrs={'class': 'form-control'} ),
            'city': forms.TextInput(attrs={'class': 'form-control','placeholder': ('More than 1 character'), 'pattern ': '[A-Za-z0-9\s]{2,}'}),
            'state': forms.TextInput(attrs={'class': 'form-control','placeholder': ('More than 1 character'), 'pattern ': '[A-Za-z0-9\s]{2,}'}),
            # 'country': forms.TextInput(attrs={'class': 'form-control', 'pattern ': '[A-Za-z\s]{3,}'}),
            'zipcode': forms.TextInput(attrs={'class': 'form-control','placeholder': ('More than 4 digits'), 'pattern ': '[0-9]{5,7}'}),
        }
        labels = {
            'address_line1': "Address Line 1",
            'address_line2': "Address Line 2",
            'appartment': "Appartment",
            'city': "City",
            'state': "State",
            # 'country': "Country",
            'zipcode': "Zip Code",
        }

class managebodyform(forms.ModelForm):
    class Meta:
        model = BodyPart
        fields = ('name',)
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control','placeholder': ('Only characters allowed'), 'pattern ': '[A-Za-z\s]{3,}'}),
            }
        labels = {
            'name': "Name",
        }

class managespecialityform(forms.ModelForm):
    class Meta:
        model = TblSpeciality
        fields = ('name',)
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control','placeholder': ('Only characters allowed'), 'pattern ': '[A-Za-z\s]{3,}'}),
            }
        labels = {
            'name': "Name",
        }

class managecategotyform(forms.ModelForm):
    class Meta:
        model = TblCategory
        fields = ('name','image','desc','speciality','body_part','short_name')
        allbody = [[x.id, x.name] for x in BodyPart.objects.filter(status=1).order_by('name')]
        allspec = [[x.id, x.name] for x in TblSpeciality.objects.filter(status=1).order_by('name')]
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': ('Example-Example'),'pattern ': '[A-Za-z\s]{3,}'+'-'+'[A-Za-z\s]{3,}'}),
            'short_name': forms.TextInput(attrs={'class': 'form-control','placeholder': ('Example-Example'), 'pattern ': '[A-Za-z\s]{3,}'+'-'+'[A-Za-z\s]{3,}'}),
            'body_part': forms.Select(choices=allbody, attrs={'class': "form-control", 'required': True}),
            'speciality': forms.Select(choices=allspec, attrs={'class': "form-control", 'required': True}),
            'image': forms.TextInput(attrs={'class': 'form-control','placeholder': ('Example.jpg or png...'), 'pattern ': '[A-Za-z\s]{3,}'+'.'+'[A-Za-z\s]{2,}'}),
            'desc': forms.TextInput(attrs={'class': 'form-control','placeholder': ('Example-Example'), 'pattern ': '[A-Za-z\s]{3,}'+'-'+'[A-Za-z\s]{3,}'}   ),
            }
        labels = {
            'name': "Name",
            'short_name': "Short Name",
            'body_part': "Body Part",
            'speciality': "Speciality",
            'image': "Image",
            'desc': "Desc",
        }

class manageblogform(forms.ModelForm):
    class Meta:
        model = Blog
        fields = ('category','created_by','image','name','podcast','desc','tags','url')
        # allbody = [[x.id, x.name] for x in BodyPart.objects.filter(status=1).order_by('name')]
        # allspec = [[x.id, x.name] for x in TblSpeciality.objects.filter(status=1).order_by('name')]
        widgets = {
            'category': forms.TextInput(attrs={'class': 'form-control', 'pattern ': '[A-Za-z\s]{3,}'}),
            'created_by': forms.TextInput(attrs={'class': 'form-control', 'pattern ': '[A-Za-z\s]{3,}'}),
            # 'body_part': forms.Select(choices=allbody, attrs={'class': "form-control", 'required': True}),
            # 'speciality': forms.Select(choices=allspec, attrs={'class': "form-control", 'required': True}),
            'desc': forms.TextInput(attrs={'class': 'form-control'}),
            
            'name': forms.TextInput(attrs={'class': 'form-control', 'pattern ': '[A-Za-z\s]{3,}'}),
            'podcast': forms.NumberInput(attrs={'class': 'form-control'}),
            'short_name': forms.TextInput(attrs={'class': 'form-control', 'pattern ': '[A-Za-z\s]{3,}'}),
            'tags': forms.TextInput(attrs={'class': 'form-control', 'pattern ': '[A-Za-z\s]{3,}'}),
            'image': forms.ClearableFileInput(),
            #'image_detail': forms.TextInput(attrs={'class': 'form-control', 'pattern ': '[A-Za-z\s]{3,}'}),
            'url': forms.TextInput(attrs={'class': 'form-control', 'pattern ': '[A-Za-z\s]{3,}'}),
            }
        labels = {
            'category': "Category",
            'created_by': "Created_by",
            'desc': "Desc",
            'name': "Name",
            'podcast': "Podcast",
            'short_name': "Short_name",
            'tags' : "Tags",
            'image' :"Image",
            #'image_detail':"Image_detail",
            'url':"Url"
        }

class manageprocform(forms.ModelForm):
    class Meta:
        model = Procedure
        fields = ('name','cpt','sku','desc','category','price','short_name')
        allCat = [[x.id, x.name] for x in TblCategory.objects.filter(status=1).order_by('name')]
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'short_name': forms.TextInput(attrs={'class': 'form-control'}),
            'cpt': forms.TextInput(attrs={'class': 'form-control'}),
            'sku': forms.TextInput(attrs={'class': 'form-control'}),
            'price': forms.TextInput(attrs={'class': 'form-control','placeholder': ('Medicare Price (No Dollar Sign Or Any Characters)(Format : 1.1 or 1.0)'), 'pattern ': '[0-9]{1,}'+'.'+'[0-9]{1,}', 'required': True,}),
            'category': forms.Select(choices=allCat, attrs={'class': "form-control", 'required': True}),
            'desc': forms.TextInput(attrs={'class': 'form-control'}),
            }
        labels = {
            'name': "Name",
            'short_name': "Short Name",
            'cpt': "CPT",
            'sku': "SKU",
            'price': "Med Price",
            'category': "Category",
            'desc': "Desc",
        }

class managedocprocform(forms.ModelForm):
    class Meta:
        model = DoctorProcedure
        fields = ('doctor','procedure','percentage')
        allDoc = [[x.id, x.first_name+' '+x.last_name] for x in Doctor.objects.all().order_by('first_name')]
        allProc = [[x.id, x.name] for x in Procedure.objects.filter(status=1).order_by('name')]
        widgets = {
            'doctor': forms.Select(choices=allDoc, attrs={'class': "form-control", 'required': True}),
            'procedure': forms.Select(choices=allProc, attrs={'class': "form-control", 'required': True}),
            'percentage': forms.TextInput(attrs={'class': 'form-control','pattern ': '[0-9]{1,}','required': True}),
            }
        labels = {
            'doctor': "Doctor",
            'procedure': "Procedure",
            'percentage': "Percentage",
        }
class specialityform(forms.ModelForm):
    class Meta:
        model =  DoctorSpeciality
        fields = ('speciality',)
        allspec = [[x.id, x.name] for x in TblSpeciality.objects.filter(status=1).order_by('name')]
        widgets = {
            'speciality': forms.CheckboxSelectMultiple(choices=allspec,attrs={ 'required': False}),
            }

class specialityform1(forms.ModelForm):
    class Meta:
        model =  DoctorSpeciality
        fields = ('speciality',)
        allspec = [[x.id, x.name] for x in TblSpeciality.objects.filter(status=1).order_by('name')]
        widgets = {
            'speciality': forms.Select(choices=allspec,attrs={ 'class': 'form-control','required': False}),
            }
 
           

class manageBlogForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.blog_id = kwargs.pop('blog_id', None)
        super(manageBlogForm, self).__init__(*args, **kwargs)
        self.fields['name'].required = False
    
    def clean(self):
        error_messages = []
        cleaned_data = super(manageBlogForm, self).clean()
        idkey = self.blog_id
        name = cleaned_data.get("name")
        short_name = cleaned_data.get("short_name")
        created_by = cleaned_data.get("created_by")
        podcast = cleaned_data.get("podcast")
        desc = cleaned_data.get("desc")
        # image = cleaned_data.get("image")
        short_text = cleaned_data.get("short_text")
        category = cleaned_data.get("category")
        # dob = cleaned_data.get("dob")
        tags = cleaned_data.get("tags")
        image = cleaned_data.get("image")
        # password = cleaned_data.get("password")

        if idkey is not None and idkey != '':
            print "in"
            existingname = Blog.objects.filter(name=name,).filter(~Q(id=idkey)).exists()
            existingshortname = Blog.objects.filter(short_name=short_name,).filter(~Q(id=idkey)).exists()
        else:
            existingname = Blog.objects.filter(name=name, ).exists()
            existingshortname = Blog.objects.filter(short_name=short_name, ).exists()
        if image:
            ext = os.path.splitext(image.name)[1]
            if ext != '.jpg' and ext != '.png':
                error_messages.append("Select a valid Image.")
        else:
            if image is None or image is False:
                error_messages.append("Image Must be Selected")

        # 
        # # validate coupon name (check for special characters)
        # if re.match("^[a-zA-Z0-9_]*$", offercode):
        #   value = True
        # else:
        #   error_messages.append('Enter a Valid Coupon Name')
        # try:
        #     validate_email(email)
        #     valid = True
        # except ValidationError:
        #     error_messages.append('Invalid Name Format')

        # if type(phone) is None:
        #     error_messages.append('Please enter a valid number')

        if existingname:
            error_messages.append('Name Already Exist')
        if existingshortname:
            error_messages.append('Short Name Already Exist')
        if len(error_messages):
            raise forms.ValidationError(error_messages)
            return self.cleaned_data

    class Meta:
        model = Blog
        fields = ('name', 'short_name', 'category', 'created_by', 'podcast', 'short_text', 'tags', 'image', 'desc')
        # exclude = ('created_at',)
        SEX = (
            ('', 'Select a value'),
            ('male', 'Male'),
            ('female', 'Female'),
        )
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'short_name': forms.TextInput(attrs={'class': 'form-control', 'maxlength': 60, 'required': True,}),
            'category': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'created_by': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'podcast': forms.TextInput(attrs={'class': 'form-control',}),
            'short_text': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'tags': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'image': forms.ClearableFileInput(),
            'desc': forms.Textarea(attrs={'class': 'form-control', 'required': False})
        }
        labels = {
            'short_name': "Slug",
           
        }



class addroles(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.idkey = kwargs.pop('idkey', None)
        super(addroles, self).__init__(*args, **kwargs)

    def clean(self):
        error_messages = []
        cleaned_data = super(addroles, self).clean()
        name = cleaned_data.get("name")
        if self.idkey is not None and self.idkey != '':
            existing = TblRoles.objects.filter(
                name=name,
            ).filter(~Q(pk=self.idkey)).exists()
        else:

            existing = TblRoles.objects.filter(
                name=name,
            ).exists()
        if existing:
            error_messages.append('Role name already exist')

        if len(error_messages):
            raise forms.ValidationError(error_messages)
            return self.cleaned_data

    class Meta:
        model = TblRoles
        fields = ('id', 'name')
        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control", 'required': True, 'pattern ': '[a-zA-Z]{3,}'}),

        }
        labels = {
            'name': 'Role Name *'
        }


class addtemplate(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.user_id = kwargs.pop('user_id', None)
        super(addtemplate, self).__init__(*args, **kwargs)
        
    def clean(self):
        error_messages = []
        cleaned_data = super(addtemplate, self).clean()
        idkey = self.user_id
        name = cleaned_data.get("name")
        business_id = cleaned_data.get("business_id")
        template = cleaned_data.get("template")
        if len(error_messages):
            raise forms.ValidationError(error_messages)
            return self.cleaned_data

    class Meta:
        model = TblTemplate
        fields = ('name', 'business_id', 'template')
        # exclude = ('created_at',)
        allBusiness = [[x.id, x.name] for x in TblBusiness.objects.filter(status=1).order_by('name')]
        # print allBusiness
        
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'business_id': forms.Select(choices=allBusiness, attrs={'class': "form-control", 'required': True}),
            'template': forms.Textarea(attrs={'class': 'form-control', 'required': True,})
        }
class managepatientsform(forms.ModelForm):
    class Meta:
        model = Users
        fields = ('id','first_name','last_name','email','phone')
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'last_name': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            'phone': forms.TextInput(attrs={'class': 'form-control', 'maxlength': 10, 'required': True,}),
            'email': forms.TextInput(attrs={'class': 'form-control', 'required': True,}),
            }
        labels = {
            'first_name': "First_name",
            'last_name': "Last_name",
            'email': "Email",
            'phone': "Phone",
            }
class ordersform(forms.ModelForm):
    class Meta:
        model = TblOrder
        fields = ('order_id','payer_id','balance','reserve_price','total_amount','payment_id','request','responce','temp','payment_method','status','responce_execute')
        TEMP = (
            ('', 'Select a value'),
            ('0', '0'),
            ('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4'),
        )
        STATUS = (
            ('', 'Select a value'),
            ('0', '0'),
            ('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4'),
        )

        widgets = {
            'order_id': forms.TextInput(attrs={'class': 'form-control','required': True,}),
            'total_amount' : forms.TextInput(attrs={'class': 'form-control','required': True,}),
            'reserve_price': forms.TextInput(attrs={'class': "form-control", 'required': True}),
            'balance': forms.TextInput(attrs={'class': "form-control", 'required': True}),
            'payer_id': forms.TextInput(attrs={'class': "form-control", 'required': True}),
            'payment_id': forms.TextInput(attrs={'class': "form-control", 'required': True}),
            'request' : forms.TextInput(attrs={'class': "form-control", 'required': True}),
            'responce': forms.TextInput(attrs={'class': "form-control", 'required': True}),
            'responce_execute': forms.TextInput(attrs={'class': "form-control", 'required': True}),
            'temp': forms.Select(choices=TEMP, attrs={'class': 'form-control', 'required': True,}),
            'payment_method': forms.TextInput(attrs={'class': "form-control", 'required': True}),
            'status':forms.Select(choices=STATUS, attrs={'class': 'form-control', 'required': True,}),

            }
        labels = {
         'total_amount': "Total_Amount",
         'reserve_price': "Reserve_Price",
         'balance' : "Balance",
         'order_id': "Order_id",
         'payer_id':"Payer_id",
         'payment_id': "Payment_id",
         'request':"Request",
         'responce':"Responce",
         'temp':"Temp",
         'payment_method':"Payment_method",
         'responce_execute':"Responce_execute",
         }

