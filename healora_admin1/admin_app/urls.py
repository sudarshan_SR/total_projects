from django.conf.urls import url, patterns
from django.contrib.auth.decorators import login_required
from django.conf import settings
import views_cluster
from admin_app.views_cluster import functions, dashboard
from views_cluster.dashboard import ChartData,PieChartData
from views_cluster.docproc import get_data
import os
from django.views.generic import TemplateView
#Importing all the clustered views automatically
dir_name = 'admin_app.views_cluster.'
file_list = os.listdir(os.path.dirname(__file__) + '/views_cluster')
# Exclude these files from importing by for loop, imported them exclusively
exclude_list = []
for exclude_file in exclude_list:
    try:
        file_list.remove(exclude_file)
    except ValueError:
        pass  # or scream: thing not in some_list!
    except AttributeError:
        pass  # call security, some_list not quacking like a list!

for files in file_list:
   mod_name, file_ext = os.path.splitext(os.path.split(files)[-1])
   if file_ext.lower() == '.py' and  mod_name != '__init__' and mod_name != 'datatables' and mod_name != 'Checksum' and mod_name != 'constants' and mod_name != 'functions':
      exec "from {0} import {1}".format(dir_name + files.split(".")[0], files.split(".")[0].title())

from django.views.generic.base import RedirectView

favicon_view = RedirectView.as_view(url='/static/images/favicon_sr.ico', permanent=True)


urlpatterns = patterns(
    '',
#    ---------------------Dashboard Urls-------------------------------
    url(r'^$', login_required(Dashboard.as_view()), name = 'dashboard'),
    url(r'^orderview/(?P<idkey>\w+)$', login_required(Orderview.as_view()), name = 'orderview'),
    url(r'^datatable/orderview/$', views_cluster.orderView.OrderListJson.as_view(), name='orderview'),
    url(r'^my-profile$', login_required(My_Profile.as_view()), name = 'myprofile'),
    # url(r'^api/data/$', get_data, name='api-data'),
    url(r'^api/chart/data/$', ChartData.as_view()),
    url(r'^api/pie_chart/data/$', PieChartData.as_view()),
    url(r'^docproc/api/(?P<page>\w+)/(?P<idkey>\w+)$',  Docproc.as_view(),name='api'),
    #l(r'^api/linechart/data/$', LineChart.as_view()),

   url(r'^my-profile$', login_required(My_Profile.as_view()), name = 'myprofile'),
    # url(r'^functions/appointmentByCategory$', functions.appointmentByCategory, name = 'functions_childs'),
    # url(r'^functions/appointmentByDoctor$', functions.appointmentByDoctor, name = 'functions_childs'),
    # url(r'^functions/ratingByDoctor$', functions.ratingByDoctor, name = 'functions_childs'),
    # url(r'^functions/apptByCategory$', functions.apptByCategory, name = 'functions_childs'),
    url(r'^functions/sendEmail$', functions.sendEmail, name = 'functions_childs'),
    url(r'^functions/processEmail/$', functions.processEmail, name = 'functions'),
    url(r'^favicon\.ico$', favicon_view),
    
#    ---------------------Dashboard Urls Ends-------------------------------    
    
    #--------------manage user(sub-admins) Url starts---------------#
    # url(r'^manageadmin/user$', login_required(views_cluster.users.list.as_view()), name = 'manage_user'),
    url(r'^manageadmin/(?P<patient_type>\w+)$', login_required(Users.as_view()), name = 'manage_user'),
    url(r'^manageadmin/(?P<patient_type>\w+)/(?P<idkey>\w+)$', login_required(Users.as_view()), name = 'manage_user_edit'),
    url(r'^datatable/users/$', views_cluster.users.OrderListJson.as_view(), name='users'),
    url(r'^manageadmin/delete/(?P<idkey>\w+)$', login_required(Users.as_view()), name = 'manage_user_delete'),
    #--------------manage user(sub-admins) Url ends---------------#


    #--------------manage user(sub-admins) Url starts---------------#
    # url(r'^manageadmin/user$', login_required(views_cluster.users.list.as_view()), name = 'manage_user'),
    url(r'^managepatients/(?P<patient_type>\w+)$', login_required(Patients.as_view()), name = 'manage_patient'),
    url(r'^managepatients/(?P<patient_type>\w+)/(?P<idkey>\w+)$', login_required(Patients.as_view()), name = 'manage_patient_edit'),
    url(r'^datatable/patients/$', views_cluster.patients.OrderListJson.as_view(), name='patients'),
    url(r'^managepatients/delete/(?P<idkey>\w+)$', login_required(Patients.as_view()), name = 'manage_patient_delete'),
    
    url(r'^history/(?P<page>\w+)/(?P<idkey>\w+)$', login_required(History.as_view()), name = 'History'),
    url(r'^datatable/history/$', views_cluster.history.OrderListJson.as_view(), name='history'),
    url(r'^managehistory/history/(?P<idkey>\w+)$', login_required(views_cluster.history.list.as_view()), name = 'manage_history_delete'),
    #--------------manage user(sub-admins) Url ends---------------#
    url(r'^payment/(?P<page>\w+)$', login_required(Payment.as_view()), name = 'payment'),
    url(r'^payment/(?P<page>\w+)/(?P<idkey>\w+)$', login_required(Payment.as_view()), name = 'payment'),
    url(r'^datatable/payment/$', views_cluster.payment.OrderListJson.as_view(), name='payment'),
    url(r'^manage_payment/payment/(?P<idkey>\w+)$', login_required(views_cluster.payment.list.as_view()), name = 'manage_payment_delete'),

     #--------------manage user(sub-admins) Url starts---------------#
    # url(r'^manageadmin/user$', login_required(views_cluster.users.list.as_view()), name = 'manage_user'),
    url(r'^bodypart/(?P<page>\w+)$', login_required(Bodypart.as_view()), name = 'body'),
    url(r'^bodypart/(?P<page>\w+)/(?P<idkey>\w+)$', login_required(Bodypart.as_view()), name = 'body'),
    url(r'^datatable/bodypart/$', views_cluster.bodypart.OrderListJson.as_view(), name='bodypart'),
    url(r'^bodypart/delete/(?P<idkey>\w+)$', login_required(Bodypart.as_view()), name = 'body'),
    #--------------manage user(sub-admins) Url ends---------------#

     #--------------manage user(sub-admins) Url starts---------------#
    # url(r'^manageadmin/user$', login_required(views_cluster.users.list.as_view()), name = 'manage_user'),
    url(r'^category/(?P<page>\w+)$', login_required(Category.as_view()), name = 'body'),
    url(r'^category/(?P<page>\w+)/(?P<idkey>\w+)$', login_required(Category.as_view()), name = 'body'),
    url(r'^datatable/category/$', views_cluster.category.OrderListJson.as_view(), name='bodypart'),
    url(r'^category/delete/(?P<idkey>\w+)$', login_required(Category.as_view()), name = 'body'),
    #--------------manage user(sub-admins) Url ends---------------#



     #--------------manage user(sub-admins) Url starts---------------#
    # url(r'^manageadmin/user$', login_required(views_cluster.users.list.as_view()), name = 'manage_user'),
    url(r'^procedure/(?P<page>\w+)$', login_required(Proc.as_view()), name = 'Proc'),
    url(r'^procedure/(?P<page>\w+)/(?P<idkey>\w+)$', login_required(Proc.as_view()), name = 'Proc'),
    url(r'^datatable/procedure/$', views_cluster.proc.OrderListJson.as_view(), name='Proc'),
    url(r'^procedure/delete/(?P<idkey>\w+)$', login_required(Proc.as_view()), name = 'Proc'),
    #--------------manage user(sub-admins) Url ends---------------#

      #--------------manage user(sub-admins) Url starts---------------#
    # url(r'^manageadmin/user$', login_required(views_cluster.users.list.as_view()), name = 'manage_user'),
    url(r'^docproc/(?P<page>\w+)$', login_required(Docproc.as_view()), name = 'Proc'),
    url(r'^docproc/(?P<page>\w+)/(?P<idkey>\w+)$', login_required(Docproc.as_view()), name = 'Proc'),
    url(r'^datatable/docproc/$', views_cluster.docproc.OrderListJson.as_view(), name='Proc'),
    url(r'^docproc/delete/(?P<idkey>\w+)$', login_required(Docproc.as_view()), name = 'Proc'),
    #--------------manage user(sub-admins) Url ends---------------#

      #--------------manage user(sub-admins) Url starts---------------#
    # url(r'^manageadmin/user$', login_required(views_cluster.users.list.as_view()), name = 'manage_user'),
    url(r'^orders/(?P<page>\w+)$', login_required(Orders.as_view()), name = 'Order'),
    url(r'^orders/(?P<page>\w+)/(?P<idkey>\w+)$', login_required(Orders.as_view()), name = 'Order'),
    url(r'^datatable/orders/$', views_cluster.orders.OrderListJson.as_view(), name='Order'),
    url(r'^orders/delete/(?P<idkey>\w+)$', login_required(Orders.as_view()), name = 'Order'),
    #--------------manage user(sub-admins) Url ends---------------#


      #--------------manage user(sub-admins) Url starts---------------#
    # url(r'^manageadmin/user$', login_required(views_cluster.users.list.as_view()), name = 'manage_user'),
    url(r'^blog/(?P<page>\w+)$', login_required(Blogpost.as_view()), name = 'Proc'),
    url(r'^blog/(?P<page>\w+)/(?P<idkey>\w+)$', login_required(Blogpost.as_view()), name = 'Proc'),
    url(r'^datatable/blog/$', views_cluster.blogpost.OrderListJson.as_view(), name='Proc'),
    url(r'^blog/delete/(?P<idkey>\w+)$', login_required(Blogpost.as_view()), name = 'Proc'),
    #--------------manage user(sub-admins) Url ends---------------#

     #--------------manage user(sub-admins) Url starts---------------#
    # url(r'^manageadmin/user$', login_required(views_cluster.users.list.as_view()), name = 'manage_user'),
    url(r'^speciality/(?P<page>\w+)$', login_required(Speciality.as_view()), name = 'body'),
    url(r'^speciality/(?P<page>\w+)/(?P<idkey>\w+)$', login_required(Speciality.as_view()), name = 'body'),
    url(r'^datatable/speciality/$', views_cluster.speciality.OrderListJson.as_view(), name='bodypart'),
    url(r'^speciality/delete/(?P<idkey>\w+)$', login_required(Speciality.as_view()), name = 'body'),

#--------------manage user(sub-admins) Url starts---------------#
    # url(r'^manageadmin/user$', login_required(views_cluster.users.list.as_view()), name = 'manage_user'),
    url(r'^managedoctor/(?P<page>\w+)$', login_required(Doctors.as_view()), name = 'manage_Doctor'),
    url(r'^managedoctor/(?P<page>\w+)/(?P<idkey>\w+)$', login_required(Doctors.as_view()), name = 'manage_Doctor_edit'),
    url(r'^datatable/doctor/$', views_cluster.doctors.OrderListJson.as_view(), name='Doctor'),
    url(r'^managedoctor/delete/(?P<idkey>\w+)$', login_required(Doctors.as_view()), name = 'manage_Doctor_delete'),
    #--------------manage user(sub-admins) Url ends---------------#

    #---------reminder Url ENDS------------#
    #--------------roles Url starts---------------#
    url(r'^roles/(?P<page>\w+)/(?P<idkey>\w+)/$', login_required(Roles.as_view()), name = 'manage_user'),
    url(r'^roles/(?P<page>\w+)/$', login_required(Roles.as_view()), name = 'manage_roles'),
    url(r'^datatable/roles/$', views_cluster.roles.OrderListJson.as_view(), name='order_list_roles'),
    url(r'^roles/delete/(?P<idkey>\w+)$', login_required(Roles.as_view()), name = 'manage_roles_delete'),


    #---------------------#
    #--------------Template Url starts---------------#
    url(r'^template/(?P<page>\w+)/(?P<idkey>\w+)/$', login_required(Template.as_view()), name = 'manage_user'),
    url(r'^template/(?P<page>\w+)/$', login_required(Template.as_view()), name = 'manage_roles'),
    url(r'^datatable/template/$', views_cluster.template.OrderListJson.as_view(), name='order_list_roles'),
    url(r'^template/delete/(?P<idkey>\w+)$', login_required(Template.as_view()), name = 'manage_roles_delete'),

    #---------------------#
    #--------------Category Url starts---------------#
    url(r'^categories/(?P<page>\w+)/(?P<idkey>\w+)/$', login_required(Category.as_view()), name = 'manage_user'),
    url(r'^categories/(?P<page>\w+)/$', login_required(Category.as_view()), name = 'manage_roles'),
    url(r'^datatable/categories/$', views_cluster.category.OrderListJson.as_view(), name='order_list_roles'),
    url(r'^categories/delete/(?P<idkey>\w+)$', login_required(Category.as_view()), name = 'manage_roles_delete'),
    

    #--------------emails  Url ends---------------#
    url(r'^unanswered/(?P<page>\w+)/$', login_required(Unanswered.as_view()), name = 'manage_email'),
    url(r'^unanswered/delete/(?P<id>\w+)$', login_required(Unanswered.as_view()), name = 'delete_email'),
    url(r'^unanswered/view/(?P<id>\w+)$', login_required(Viewmail.as_view()), name = 'viewEmail'),
    url(r'^datatable/unanswered/$', views_cluster.unanswered.OrderListJson.as_view(), name='unanswered_list'),
    url(r'^unanswered/(?P<page>\w+)/(?P<id>\w+)$', login_required(Unanswered.as_view()), name = 'manage_view_edit_page'),
    # url(r'^unanswered/edit/(?P<id>\w+)$', login_required(Unanswered.as_view()), name = 'manage_patient_edit'),
    #---------patient Url ENDS------------#
    #--------------emails  Url ends---------------#
    url(r'^answered/(?P<page>\w+)/$', login_required(Answered.as_view()), name = 'manage_email'),
    url(r'^answered/delete/(?P<id>\w+)$', login_required(Answered.as_view()), name = 'delete_email'),
    url(r'^answered/view/(?P<id>\w+)$', login_required(Viewmail.as_view()), name = 'viewEmail'),
    url(r'^datatable/answered/$', views_cluster.answered.OrderListJson.as_view(), name='unanswered_list'),
    url(r'^answered/(?P<page>\w+)/(?P<id>\w+)$', login_required(Answered.as_view()), name = 'manage_view_edit_page'),
    # url(r'^unanswered/edit/(?P<id>\w+)$', login_required(Unanswered.as_view()), name = 'manage_patient_edit'),
    #---------patient Url ENDS------------#
    

)

urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}))
