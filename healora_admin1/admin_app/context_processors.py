from django.conf import settings
from django.core.cache import cache
from django.db import connection
from .models import TblAdmin
def init_menu(request):
    username=''
    menu = cache.delete('menu')
    menu = cache.delete('permission')
    newMenuset = ''
    menu = cache.delete('menu')
    menu = cache.delete('roleId')
    menu = cache.delete('permission')
    print request
    url_title_list = request.path.split('/', 1)[1].split('/')
    title = ' | '.join(url_title_list)

    try:
        if request.session['newmenu'] != '':
            # print request.session['newmenu']
            sessionSet = True
        else:
            sessionSet = False

    except Exception, e:
        sessionSet = False
    
    
    menu = cache.get('menu')
    print "cache"
    profile_picture = None
    if request.user.is_authenticated():
        if sessionSet is False:
            menu = create_menu(request,request.user.role_id)
            print menu
            
            request.session['newmenu'] = menu
            request.session['userId'] = request.user.pk
            permission = create_permission(request,request.user.role_id)
            request.session['roleId'] =  request.user.role_id
            request.session['permission'] = permission
            sessionSet = True
        username = request.user.fname
        if request.user is not "anonymous":
            profile_picture = request.user.profile_picture
        else:
            profile_picture = None
    else:
        request.session['newmenu'] = ''
    return { 'menu' : request.session['newmenu'],'profilePicture': profile_picture, 'username':username,'title': title}

def create_menu(request, role=1, parentId=0):
        #roleId is set during login in backend.py
        menu='<ul>'
        cursor = connection.cursor()
        getMenu=''' SELECT b.module_name,b.display_name,b.url,b.class,b.has_child,b.parent_id,b.priority,b.id FROM tbl_module_permission AS a JOIN tbl_module AS b ON a.module = b.id where b.parent_id = %s AND a.role =%s ORDER BY b.priority ASC  '''
        cursor.execute(getMenu,(parentId,role))
        print "cursor_menu"
        print cursor
        for x in cursor:
            print "cursor_menu X"
            print x
            moduleId = x[7]
            addClass = ''
            if x[4] == 1:
                addClass = 'active has-sub'
            menu=menu+'<li class="'+addClass+'">'
            menu=menu+'<a href="'+x[2]+'"><span><i class="'+x[3]+'"></i> '+x[1]
            menu=menu+'</span></a>'
            if x[4] == 1:
                menu = menu + create_menu(request, role, moduleId)
            menu=menu+'</li>'
        menu=menu+'</ul>'
        print "menu"
        print menu
        return menu
    
def create_permission(request, role=1):
    cursor = connection.cursor()
    getdata=''' SELECT b.module_name, a.module, a.pr_add, a.pr_delete, a.pr_edit FROM tbl_module_permission AS a JOIN tbl_module AS b ON a.module = b.id where a.role =%s ORDER BY b.priority ASC  '''
    cursor.execute(getdata,(role,))
    print "sudarshan"
    print cursor
    listing={}
    for x in cursor:
        print x
        # will be (add, delete, edit)
        listing[x[0]] = [x[2],x[3],x[4]]
    print listing
    return listing



def server_urls(request):
    # return the value you want as a dictionnary. you may add multiple values in there.
    return {'SERVER_URL': settings.SERVER_URL, 'USERAPI_URL': settings.USERAPI_URL, 'WEBAPP_URL': settings.WEBAPP_URL}

