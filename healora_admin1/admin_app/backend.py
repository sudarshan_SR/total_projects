from django.conf import settings
from admin_app.models import TblAdmin
from django.core.cache import cache

'''
Used for the purpose of Login.
The following class needs to be mentioned in the settings file
'''
class AuthBackend:
    def authenticate(self, username=None, password=None):
        kwargs = {'email': username}
        try:
            user = TblAdmin.objects.get(**kwargs)
            if user.check_password(password):
                # request.session['userId'] = user.pk
                # request.session['roleId'] = user.role_id
                # request.session['fname'] = user.fname
                # request.session['lname'] = user.lname
                # # cache.set('userId', user.pk, timeout=None)
                # # cache.set('roleId', user.role_id, timeout=None)
                # # cache.set('fname', user.fname, timeout=None)
                # # cache.set('lname', user.lname, timeout=None)
                return user
        except TblAdmin.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return TblAdmin.objects.get(pk=user_id)
        except TblAdmin.DoesNotExist:
            return None