'''
This file will be used across the project for using and defining constants
'''
import datetime
import time

import pytz


def DATE_TIME():
    return datetime.datetime.now(pytz.timezone('Asia/Calcutta'))

def NEXT_DATE_TIME():
    return DATE_TIME() + datetime.timedelta(days=1)


def FORMATTED_TIME():
    return datetime.datetime.strptime(str(DATE_TIME()).split('.')[0], '%Y-%m-%d %H:%M:%S')

def FORMATTED_TIME_PLUS():
    FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(FORMATTED_TIME()), '%Y-%m-%d %H:%M:%S')
    return  FORMATTED_TIME_PLUS + datetime.timedelta(0, 330 * 60)

def TIME():    
    return FORMATTED_TIME().time()

def DATE():
    return FORMATTED_TIME().date()

def getTime(dateTime):
#    return dateTime
    return datetime.datetime.strftime(datetime.datetime.strptime(str(dateTime), '%Y-%m-%d %H:%M:%S'),"%I:%M %p")


def getDateTime(dateTime):
#    return dateTime
    try:
        return datetime.datetime.strftime(datetime.datetime.strptime(str(dateTime), '%Y-%m-%d %H:%M:%S'),"%Y-%m-%d %H:%M:%S")
    except Exception:
        return ""
### ---------------------reduce 7 minutes from time--------------
def LATER():
    return FORMATTED_TIME() - datetime.timedelta(0, 7 * 60)

def LATERTIME():
    return LATER().time()

def LATER_15():
    return FORMATTED_TIME() - datetime.timedelta(0, 15 * 60)
def LATER_15_TIME():
    LATER_15().time()
#### -------------------- add 30 minutes
def AFTER():
    return FORMATTED_TIME() + datetime.timedelta(0, 30 * 60)

def AFTERTIME():
    return AFTER().time()


#####-------After add 120 minutes
def AFTER_TWO():
    return FORMATTED_TIME() + datetime.timedelta(0, 120 * 60)

def AFTER_TWO_TIME():
    return AFTER_TWO().time()

def CURRENT_TIME_PLUS_PARAMS(minutes):
    FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(FORMATTED_TIME()), '%Y-%m-%d %H:%M:%S')
    return  FORMATTED_TIME_PLUS + datetime.timedelta(0, minutes * 60)

def CURRENT_TIME_MINUS_PARAMS(minutes):
    FORMATTED_TIME_PLUS = datetime.datetime.strptime(str(FORMATTED_TIME()), '%Y-%m-%d %H:%M:%S')
    return  FORMATTED_TIME_PLUS - datetime.timedelta(0, minutes * 60)


AUTH_KEY_UNAUTHORISED = 'Authkey authorsation failed'
MISSING_PARAM = 'Missing Parameters'
EMAIL_ALREADY_EXISTS = 'Email ID already exists'
PROFILE_SUCCESSFULLY_UPDATED = 'Profile successfully updated'
PHONE_NUMBER_EXCEPTION = 'Phone number should be of 10 numbers'
GENERAL_EXCEPTION = 'Something went wrong'
#----------EXCEPTIONS RELATED TO DATABASE <BEGIN>--------------#
DATA_EXCEPTION = 'Something went wrong' #for example, numeric value out of range, division by zero, and so on
INTEGRITY_EXCEPTION = 'Something went wrong' #for example a foreign key check fails, duplicate key, and so on
INTERNAL_EXCEPTION = 'Something went wrong' #for example, an invalid cursor, the transaction is out of sync, and so on
NOT_SUPPORTED_EXCEPTION = 'Something went wrong' #for example, requesting a transaction-oriented function when transactions are not available
OPERATIONAL_EXCEPTION = 'Something went wrong' #for example, an unexpected disconnect, the data source name is not found, a transaction could not be processed, a memory allocation error occurrs, and so on
PROGRAMMING_ERROR_EXCEPTION = 'Something went wrong' #for example, a table is not found or already exists, there is a syntax error in the MySQL statement, a wrong number of parameters is specified, and so on
#----------EXCEPTIONS RELATED TO DATABASE <END>--------------#
SUBMIT_RATING_VALUE_EXCEPTION = 'Bad value of schedule ID or some other value error.'
SUBMIT_REVIEW_VALUE_EXCEPTION = 'Bad value of schedule ID or some other value error.'
NO_SUCH_SCHEDULE_EXCEPTION = 'No such schedule found'
TOKEN_MISSING = "Token Missing"
AUTH_KEY_MISSING = "Auth Key Missing"
TOKEN_FAIL = "Token check failed"
VISIT_HISTORY_LOADED = "Visit history successfully loaded"
VISIT_HISTORY_DETAILS_LOADED = "Visit history details successfully loaded"
MY_APPOINTMENTS_FETCHED = "My appointments successfully fetched"
PROFILE_INFO_SENT = "User's profile view successful"
PROFILE_UPDATED = "Profile successfully updated"
PHONE_NUMBER_ALREADY_EXISTS = "Phone number already exists"
STAR_RATING_SUBMITTED = "Star rating successfully submitted"
STAR_RATING_REVIEW = "Review successfully submitted"
CANCEL_SCHEDULE_SUCCESS = "Schedule successfully cancelled"
SEARCH_BY_DOCTOR_SUCCESS = "Doctors listed successfully"
GET_MESSAGES_SUCCESS = "Message fetching successful"
CHILD_EXISTS = "Child already exists"
SLOT_TAKEN = "Slot already taken"
WRONG_REQUEST = "Wrong Request"
INVALID_COUPON="Invalid Coupon Code"
COUPON_USED="Coupon already used"
AMOUNT=400
COUPON_APPLIED="Coupon applied successfully"
INVALID_REQUEST = "Wrong Request"
SUCCESS = "Successfully Listed"
NO_SLOTS = "No slots available"
NO_CONTENT = "No data available"
zoom_api_key ="oHEMFKO9Txmon4NUDIqPZQ"
zoom_api_secret_key="rupq9FAmSi9og0vfdozbgixPnx3qN5aQNH5E"
zoom_id_url = "https://api.zoom.us/v1/user/custcreate"
zoom_token_url = "https://api.zoom.us/v1/user/get"
user_validated = "User Validated Successfully"
does_not_exit = "Data does not exist"
password_updated = "Password Successfully Updated"
created = "successfully Created"
already_exist = "user already exist"
forgot_password = "Email For password reset has been sent"
registration_failed = "Registration failed"
user_email_doesnot_exist = "User with this E-mail does not exist"
SUCCESS_RESCHEDUE="Appointment Rescheduled Successfully"
SUCCESS_QUESTIONS_SUBMITTED = "Successfully Submitted"
CAN_RESCHEDUE="Can Reschedule Appointment"
CANT_RESCHEDUE="Cant Reschedule"
SUCCESS_CANCEL = "Appontment has been Successfully Cancelled"
CANT_CANCEL="Appontment Can not be cancelled"
VALID_DATE = 'Date entered is valid'
INVALID_DATE = 'Date entered is invalid. Enter in the format yyyy-mm-dd'
VALID_PHONE_NO = 'Entered phone number is valid'
INVALID_PHONE_NO = 'Entered phone number is invalid'
USER_ALREADY_EXISTS = "User with this E-mail/phone already exists"
INVALID_EMAIL = "Email entered is invalid."
INVALID_DOMAIN = "Domain name entered is invalid."
INVALID_FILETYPE = 'Unsupported file (CSV only.)'


# Live credenatils
PAYTM_REFUND_URL= 'https://secure.paytm.in/oltp/HANDLER_INTERNAL/REFUND'
PAYTM_MERCHANT_MID = "doctor24747098748480"
PAYTM_MERCHANT_KEY =  "d#BRmcysckE#uWGt"

# Test Paytm Credentails
#PAYTM_REFUND_URL= "https://pguat.paytm.com/oltp/HANDLER_INTERNAL/REFUND"
#PAYTM_MERCHANT_MID = "Brahma36388373304106"
#PAYTM_MERCHANT_KEY =  "rAjAuz5!YgSYTEkz"



MOBIKWIK_MERCHANT_ID = "MBK9171"

SUCCESS_DELETED="Successfully Deleted"


MOBIKWIK_SECRET_KEY = "IqYubK9C5so7eRixVWuUZA1vlaTB"

# MOBIKWIK_MERCHANT_ID = "MBK9002"
# MOBIKWIK_SECRET_KEY = "ju6tygh7u7tdg554k098ujd5468o"
#File Size chart
# 2.5MB - 2621440
# 5MB - 5242880
# 10MB - 10485760
# 20MB - 20971520
# 50MB - 5242880
# 100MB 104857600
# 250MB - 214958080
# 500MB - 429916160

EXCERPT_MAX_LENGTH = 160
EXCERPT_ERROR_MESSAGE = 'Excerpt cannot exceed 160 characters'

TITLE_ERROR_MESSAGE = 'Title should contain only alphanumeric characters'

IMAGE_ALLOWED_CONTENT_TYPES = ['image/jpeg']
IMAGE_MAX_UPLOAD_SIZE = 5242880
IMAGE_TYPE_ERROR_MESSAGE = 'Only JPEG images are allowed'
IMAGE_SIZE_ERROR_MESSAGE = 'Please upload image of less than ' + str(int(IMAGE_MAX_UPLOAD_SIZE) / (1000000)) + 'MB'

CSV_ALLOWED_CONTENT_TYPES = ['csv', 'text/csv', 'application/vnd.ms-excel']
CSV_TYPE_ERROR_MESSAGE = 'Only csv files are allowed'
TEMPLATE_ALLOWED_CONTENT_TYPES = ['html', 'text/html']
TEMPLATE_TYPE_ERROR_MESSAGE = 'Only html files are allowed'

PRESCRIPTION_ALLOWED_CONTENT_TYPES = ['pdf','csv']
PRESCRIPTION_MAX_UPLOAD_SIZE = 5242880
PRESCRIPTION_TYPE_ERROR_MESSAGE = 'Only Pdf and csv files  are allowed'
PRESCRIPTION_SIZE_ERROR_MESSAGE = 'Please upload File  less than ' + str(int(IMAGE_MAX_UPLOAD_SIZE) / (1000000)) + 'MB'


RAZORPAY_KEY_ID = "rzp_live_KGhIaE4n2fMHFM"
RAZORPAY_SECRET_KEY = "pklnShVK46YGwOlDGzB11Srp"


VIDEO_ALLOWED_CONTENT_TYPES = ['video/mp4']
VIDEO_MAX_UPLOAD_SIZE = 20971520
VIDEO_TYPE_ERROR_MESSAGE = 'Only MP4 video format is allowed'
VIDEO_SIZE_ERROR_MESSAGE = 'Please upload video of less than ' + str(int(VIDEO_MAX_UPLOAD_SIZE) / (1000000)) + 'MB'


DOCTOR_INSTA_EMAILS_IDS = {'amit': 'amit@doctorinsta.com',
                           'dhruv': 'dhruv.agarwal@doctorinsta.com',
                           'kunal': 'kunal.monga@doctorinsta.com',
                           'pulkit': 'pulkit.doctorinsta@gmail.com',
                           'mansi': 'mansi.aggarwal@doctorinsta.com',
                           'imran': 'i.husain.doctorinsta@gmail.com',
                           'hema': 'hemlata.doctorinsta@gmail.com'
                           }