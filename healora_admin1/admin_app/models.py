# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.contrib.auth.signals import user_logged_in
import hashlib, os
from django.template.defaultfilters import default
from django.core.signals import setting_changed
from django.core.validators import MinValueValidator
import admin_python
from django.conf import settings
import constants, traceback
from django.contrib.auth.models import BaseUserManager
import string
import random
'''
Manager class for TblUserManager as we are not using using the default User table
'''
class TblAdminManager(models.Manager):
    
    def get_by_natural_key(self, fname):
        '''
        To define a natural key that can be used for lookup purposes
        '''
        return self.get(fname=fname)

def get_admin_image_path(instance, filename):
    image = os.path.join('adminPic', str(instance.email), filename)
    print image
    return image

def get_blog_image_path(instance, filename):
    image = os.path.join('blog', str(instance.name), filename)
    return image

def get_image_path_d(instance, filename):
    image = os.path.join('doctor',str(instance.email), filename)
    return image

class TblAdmin(models.Model):
    email = models.CharField(unique=True, max_length=250)
    fname = models.CharField(max_length=250)
    lname = models.CharField(max_length=256)
    password = models.CharField(max_length=250)
    sex = models.CharField(max_length=50, blank=True, null=True)
    dob = models.DateField(blank=True, null=True)
    remember_token = models.CharField(max_length=500)
    profile_picture = models.FileField(upload_to=get_admin_image_path, blank=True, null=True)
    role = models.ForeignKey('TblRoles', blank=True, null=True)
    updated_at = models.DateTimeField()
    created_at = models.DateTimeField()
    last_login = models.DateTimeField()
    is_active = models.IntegerField()
    phone = models.BigIntegerField()
    emp_id = models.CharField(max_length=256)
    check_password = models.IntegerField()
    
    objects = TblAdminManager()
    REQUIRED_FIELDS = ['password', 'fname']
    USERNAME_FIELD = 'email'
    
    def is_authenticated(self):
        return True
    
    def set_password(self, password):
        hash_object = hashlib.md5(password)
        self.password = hash_object.hexdigest()
        return hash_object.hexdigest()
    
    def check_password(self, password):
        hash_object = hashlib.md5(password)
        return self.password == hash_object.hexdigest()
    
    def has_usable_password(self):
        return True
    
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin
        
    def __unicode__(self):
        return u'%s' % (self.fname)

    class Meta:
        
        db_table = 'tbl_admin'



class TblAutherization(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    doctor_id = models.IntegerField(blank=True, null=True)
    secret_key = models.CharField(max_length=250)
    status = models.IntegerField(blank=True, null=True)
    date_created = models.DateTimeField()
    date_updated = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_autherization'

def get_image_path(instance, filename):
    return os.path.join('adminPic', str(instance.url), filename)

def get_video_path(instance, filename):
    return os.path.join('video', str(instance.slug), filename)

class TblForgotPassword(models.Model):
    tbl_user = models.ForeignKey('TblUser', blank=True, null=True)
    email = models.CharField(max_length=250)
    unique_code = models.CharField(max_length=250)
    authenticated = models.IntegerField()
    date_created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'tbl_forgot_password'


class TblModule(models.Model):
    module_name = models.CharField(max_length=100, blank=True, null=True)
    display_name = models.TextField()
    url = models.CharField(max_length=256)
    class_field = models.TextField(db_column='class')  # Field renamed because it was a Python reserved word.
    has_child = models.IntegerField()
    parent_id = models.IntegerField()
    priority = models.IntegerField()
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_module'


class TblModulePermission(models.Model):
    module = models.ForeignKey(TblModule, db_column='module')
    role = models.ForeignKey('TblRoles', db_column='role')
    pr_add = models.IntegerField()
    pr_delete = models.IntegerField()
    pr_edit = models.IntegerField()
    date_created = models.DateTimeField()
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'tbl_module_permission'


        
class TblPriority(models.Model):
    priority = models.IntegerField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_priority'



class TblRoles(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_roles'

    def __unicode__(self):
        return unicode(self.name)

        

class TblBusiness(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=250)
    phone_number = models.BigIntegerField()
    merchant_id = models.IntegerField()
    image = models.CharField(max_length=250)
    address = models.TextField()
    state = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    status = models.IntegerField()
    is_active = models.IntegerField()
    last_login = models.DateTimeField()
    date = models.DateTimeField()
    admin_name = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'tbl_business'



class TblUser(models.Model):
    business_id = models.IntegerField()
    unique_id = models.IntegerField()
    unique_id_detail = models.CharField(max_length=256)
    email = models.CharField(max_length=200)
    phone = models.BigIntegerField(blank=True, null=True)
    dob = models.CharField(max_length=100)
    sex = models.CharField(max_length=50)
    fname = models.CharField(max_length=100)
    lname = models.CharField(max_length=90)
    date_created = models.DateTimeField()
    
    def __unicode__(self):
        return u'%s' % (''.join([self.fname, ' ', self.lname , '(' , self.email ,')' ]))
    
    class Meta:
        managed = False
        db_table = 'tbl_user'


class TblUserDetails(models.Model):
    user = models.ForeignKey(TblUser, blank=True, null=True)
    city = models.CharField(max_length=25, blank=True, null=True)
    state = models.CharField(max_length=50, blank=True, null=True)
    country = models.CharField(max_length=50, blank=True, null=True)
    zipcode = models.CharField(max_length=50, blank=True, null=True)
    height = models.FloatField(blank=True, null=True)
    is_sports_person = models.IntegerField(default=0)
    daily_activity_type = models.IntegerField(default=1)
    date_created = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_user_details'


class CeleryTaskmeta(models.Model):
    task_id = models.CharField(unique=True, max_length=255)
    status = models.CharField(max_length=50)
    result = models.TextField(blank=True, null=True)
    date_done = models.DateTimeField()
    traceback = models.TextField(blank=True, null=True)
    hidden = models.IntegerField()
    meta = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'celery_taskmeta'


class CeleryTasksetmeta(models.Model):
    taskset_id = models.CharField(unique=True, max_length=255)
    result = models.TextField()
    date_done = models.DateTimeField()
    hidden = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'celery_tasksetmeta'

class TblEmail(models.Model):
    business_id = models.IntegerField()
    business_user_id = models.CharField(max_length=256)
    email_uid = models.IntegerField()
    email_from = models.CharField(max_length=256)
    subject = models.CharField(max_length=256)
    body = models.CharField(max_length=256)
    email_date = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField()
    attachment = models.CharField(max_length=256)
    category_id = models.CharField(max_length=256)
    answered_by = models.IntegerField(blank=True, null=True)
    answered_date = models.DateTimeField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    response_email = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'tbl_email'



class TblPolicyUser(models.Model):
    policy_no = models.IntegerField()
    life_assured = models.CharField(max_length=256)
    policy_holder = models.CharField(max_length=256)
    assignee = models.CharField(max_length=256)
    nomination = models.CharField(max_length=256)
    date_of_proposal = models.DateField()
    policy_issue_date = models.DateField()
    product = models.CharField(max_length=256)
    sum_assured = models.IntegerField()
    dob = models.DateField()
    age = models.IntegerField()
    policy_term = models.CharField(max_length=256)
    validity = models.DateField()
    email = models.CharField(max_length=256)
    height = models.FloatField()
    weight = models.IntegerField()
    sex = models.CharField(max_length=11)
    address = models.CharField(max_length=256)
    city = models.CharField(max_length=256)
    state = models.CharField(max_length=256)
    pincode = models.IntegerField()
    premium_due = models.CharField(max_length=256)
    premimum_due_date = models.DateField()
    premium_last_recieved = models.IntegerField()
    premium_recieved_date = models.DateField()
    policy_status = models.IntegerField()
    death_date = models.DateField()
    surrender_value = models.CharField(max_length=256)

    class Meta:
        managed = False
        db_table = 'tbl_policy_user'



class TblUserFetch(models.Model):
    tbl_name = models.CharField(max_length=256)
    business_id = models.IntegerField()
    unique_field_type = models.IntegerField()
    unique_field_name = models.CharField(max_length=256)
    email_field_name = models.CharField(max_length=256)
    fname_field_name = models.CharField(max_length=256)
    dob_field_name = models.CharField(max_length=256)
    sex_field_name = models.CharField(max_length=256)
    lname_field_name = models.CharField(max_length=256)
    date_created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'tbl_user_fetch'





class TblTemplate(models.Model):
    name = models.CharField(max_length=256)
    business_id = models.IntegerField()
    template = models.TextField()

    class Meta:
        managed = False
        db_table = 'tbl_template'


class Blog(models.Model):
    category = models.TextField(blank=True, null=True)
    created_by = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    image_detail = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    podcast = models.TextField(blank=True, null=True)
    short_name = models.TextField(blank=True, null=True)
    image = models.FileField(upload_to=get_image_path, blank=True, null=True)
    short_text = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    tags = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    user_type = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'blog'


class BodyPart(models.Model):
    date_updated = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'body_part'


class TblCategory(models.Model):
    body_part = models.CharField(max_length=12, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    image = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    short_name = models.TextField(blank=True, null=True)
    speciality = models.CharField(max_length=12, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'category'

class Comment(models.Model):
    blog = models.CharField(max_length=12, blank=True, null=True)
    blog_url = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    user = models.CharField(max_length=12, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'comment'

class Doctor(models.Model):
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    degree_title = models.TextField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    fax = models.TextField(blank=True, null=True)
    first_name = models.TextField(blank=True, null=True)
    last_name = models.TextField(blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    phone = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    user_name = models.TextField(blank=True, null=True)
    is_active = models.IntegerField()
    sex = models.CharField(max_length=50, blank=True, null=True)
    image = models.FileField(upload_to=get_image_path_d, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'doctor'


class DoctorAddress(models.Model):
    address_line1 = models.TextField(blank=True, null=True)
    address_line2 = models.TextField(blank=True, null=True)
    appartment = models.TextField(blank=True, null=True)
    city = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    doctor_id = models.CharField(max_length=12, blank=True, null=True)
    loc_lat = models.TextField(blank=True, null=True)
    loc_lon = models.TextField(blank=True, null=True)
    state = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    zipcode = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'doctor_address'


class DoctorProcedure(models.Model):
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    doctor = models.CharField(max_length=12, blank=True, null=True)
    doctor_price = models.TextField(blank=True, null=True)
    procedure_name = models.TextField(blank=True, null=True)
    doctor_name = models.TextField(blank=True, null=True)
    percentage = models.TextField(blank=True, null=True)
    procedure = models.CharField(max_length=12, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    total = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'doctor_procedure'


class DoctorSpeciality(models.Model):
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    doctor_id = models.CharField(max_length=12, blank=True, null=True)
    speciality = models.CharField(max_length = 255,null = True,blank = True )
    speciality_name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'doctor_speciality'


class TblOrder(models.Model):
    balance = models.TextField(blank=True, null=True)
    comments= models.TextField(blank=True, default= "payment started")
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    doctor_procedure = models.CharField(max_length=12, blank=True, null=True)
    order_id = models.TextField(blank=True, null=True)
    payer_id = models.TextField(blank=True, null=True)
    payment_id = models.TextField(blank=True, null=True)
    payment_method = models.TextField(blank=True, null=True)
    payment_time = models.TextField(blank=True, null=True)
    request = models.TextField(blank=True, null=True)
    reserve_price = models.TextField(blank=True, null=True)
    responce = models.TextField(blank=True, null=True)
    responce_execute = models.TextField(blank=True, null=True)
    status = models.TextField(blank=True, null=True)
    temp = models.TextField(blank=True, null=True)
    token = models.TextField(blank=True, null=True)
    total_amount = models.TextField(blank=True, null=True)
    user = models.CharField(max_length=12, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'order'

class Posts(models.Model):
    user_id = models.IntegerField()
    title = models.CharField(max_length=255)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'posts'


class Procedure(models.Model):
    category = models.CharField(max_length=12, blank=True, null=True)
    cpt = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    detail_0_data = models.TextField(db_column='detail.0.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_1_data = models.TextField(db_column='detail.1.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_2_data = models.TextField(db_column='detail.2.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_3_data = models.TextField(db_column='detail.3.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_4_data = models.TextField(db_column='detail.4.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_5_data = models.TextField(db_column='detail.5.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_6_data = models.TextField(db_column='detail.6.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_7_data = models.TextField(db_column='detail.7.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_8_data = models.TextField(db_column='detail.8.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    insurance_price = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    price = models.TextField(blank=True, null=True)
    short_name = models.TextField(blank=True, null=True)
    sku = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'procedure'


class TblSpeciality(models.Model):
    date_updated = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'speciality'


class Token(models.Model):
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    token = models.TextField(blank=True, null=True)
    user_id = models.CharField(max_length=12, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'token'


class UserAddress(models.Model):
    address_line1 = models.TextField(blank=True, null=True)
    address_line2 = models.TextField(blank=True, null=True)
    appartment = models.TextField(blank=True, null=True)
    city = models.TextField(blank=True, null=True)
    company = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    loc_lat = models.TextField(blank=True, null=True)
    loc_lon = models.TextField(blank=True, null=True)
    state = models.TextField(blank=True, null=True)
    user_id = models.TextField(blank=True, null=True)
    zipcode = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_address'


class Users(models.Model):
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    first_name = models.TextField(blank=True, null=True)
    last_name = models.TextField(blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    phone = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    user_name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'

class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    pre_change=models.CharField(max_length=200)
    post_change=models.CharField(max_length=200)
    user_id = models.IntegerField(null=False)
    user_name = models.CharField(max_length=200)
    
    class Meta:
        managed = False
        db_table = 'django_admin_log'