from django import forms
from admin_app.models import TblAdmin
class Profileforms(forms.Form):
    name = forms.CharField(label='Your name', max_length=100)

class PostForm(forms.ModelForm):

    class Meta:
        model = TblAdmin
        fields = ('fname', 'lname','role_id')