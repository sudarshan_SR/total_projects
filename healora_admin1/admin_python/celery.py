##http://docs.celeryproject.org/en/latest/django/first-steps-with-django.html
#
from __future__ import absolute_import

import os
#
from celery import Celery
# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'admin_python.settings')

from django.conf import settings  # noqa

app = Celery('admin_python')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

# if __name__ == '__main__':
#     app.start()

    
@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))

'''
Author : Abhay Gupta
Date : 26-04-2016

#Related files:
/etc/supervisord/supervisord.conf
/etc/supervisord/conf.d/myapp-celery.conf (probably not used)
/var/log/rabbitmq/rabbit@abhay-dr-insta.log
/var/lib/rabbitmq/mnesia/rabbit@abhay-dr-insta/cluster_nodes.config
/etc/supervisord/conf.d/myapp-celery.conf
 
#Things to do:

- Delete default guest user of RabbitMQ

- Authentication and Authorization of RabbitMQ 
  (http://www.rabbitmq.com/access-control.html , 
  https://github.com/rabbitmq/rabbitmq-auth-backend-http)
  
- Intrusion detection (Low priority)
  http://docs.celeryproject.org/en/latest/userguide/security.html#tripwire
  
- Define Broker failover strategy
  http://docs.celeryproject.org/en/latest/configuration.html#std:setting-BROKER_FAILOVER_STRATEGY
  
- Broker should use SSL
  http://docs.celeryproject.org/en/latest/configuration.html#broker-use-ssl
  
- 

#RabbitMQ monitoring tool

- sudo rabbitmq-plugins enable rabbitmq_management
- sudo service rabbitmq-server restart
- http://localhost:15672/

#Celery Monitoring Tool

-  celery -A admin_python flower
-  http://localhost:5555
'''