# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup)
    permission = models.ForeignKey('AuthPermission')

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group_id', 'permission_id'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType')
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type_id', 'codename'),)


class Blog(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    category = models.TextField(blank=True, null=True)
    created_by = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    image = models.TextField(blank=True, null=True)
    image_detail = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    podcast = models.TextField(blank=True, null=True)
    short_name = models.TextField(blank=True, null=True)
    short_text = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    tags = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    user_type = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'blog'


class BodyPart(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    date_updated = models.DateTimeField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'body_part'


class Category(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    body_part = models.CharField(max_length=12, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    image = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    short_name = models.TextField(blank=True, null=True)
    speciality = models.CharField(max_length=12, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'category'


class CeleryTaskmeta(models.Model):
    task_id = models.CharField(unique=True, max_length=255)
    status = models.CharField(max_length=50)
    result = models.TextField(blank=True, null=True)
    date_done = models.DateTimeField()
    traceback = models.TextField(blank=True, null=True)
    hidden = models.IntegerField()
    meta = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'celery_taskmeta'


class CeleryTasksetmeta(models.Model):
    taskset_id = models.CharField(unique=True, max_length=255)
    result = models.TextField()
    date_done = models.DateTimeField()
    hidden = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'celery_tasksetmeta'


class Comment(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    blog = models.CharField(max_length=12, blank=True, null=True)
    blog_url = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    user = models.CharField(max_length=12, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'comment'


class CorsheadersCorsmodel(models.Model):
    cors = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'corsheaders_corsmodel'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', blank=True, null=True)
    user = models.ForeignKey('TblUser')

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DjceleryCrontabschedule(models.Model):
    minute = models.CharField(max_length=64)
    hour = models.CharField(max_length=64)
    day_of_week = models.CharField(max_length=64)
    day_of_month = models.CharField(max_length=64)
    month_of_year = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'djcelery_crontabschedule'


class DjceleryIntervalschedule(models.Model):
    every = models.IntegerField()
    period = models.CharField(max_length=24)

    class Meta:
        managed = False
        db_table = 'djcelery_intervalschedule'


class DjceleryPeriodictask(models.Model):
    name = models.CharField(unique=True, max_length=200)
    task = models.CharField(max_length=200)
    args = models.TextField()
    kwargs = models.TextField()
    queue = models.CharField(max_length=200, blank=True, null=True)
    exchange = models.CharField(max_length=200, blank=True, null=True)
    routing_key = models.CharField(max_length=200, blank=True, null=True)
    expires = models.DateTimeField(blank=True, null=True)
    enabled = models.IntegerField()
    last_run_at = models.DateTimeField(blank=True, null=True)
    total_run_count = models.IntegerField()
    date_changed = models.DateTimeField()
    description = models.TextField()
    crontab = models.ForeignKey(DjceleryCrontabschedule, blank=True, null=True)
    interval = models.ForeignKey(DjceleryIntervalschedule, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'djcelery_periodictask'


class DjceleryPeriodictasks(models.Model):
    ident = models.SmallIntegerField(primary_key=True)
    last_update = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'djcelery_periodictasks'


class DjceleryTaskstate(models.Model):
    state = models.CharField(max_length=64)
    task_id = models.CharField(unique=True, max_length=36)
    name = models.CharField(max_length=200, blank=True, null=True)
    tstamp = models.DateTimeField()
    args = models.TextField(blank=True, null=True)
    kwargs = models.TextField(blank=True, null=True)
    eta = models.DateTimeField(blank=True, null=True)
    expires = models.DateTimeField(blank=True, null=True)
    result = models.TextField(blank=True, null=True)
    traceback = models.TextField(blank=True, null=True)
    runtime = models.FloatField(blank=True, null=True)
    retries = models.IntegerField()
    hidden = models.IntegerField()
    worker = models.ForeignKey('DjceleryWorkerstate', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'djcelery_taskstate'


class DjceleryWorkerstate(models.Model):
    hostname = models.CharField(unique=True, max_length=255)
    last_heartbeat = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'djcelery_workerstate'


class Doctor(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    degree_title = models.TextField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    fax = models.TextField(blank=True, null=True)
    first_name = models.TextField(blank=True, null=True)
    image = models.TextField(blank=True, null=True)
    last_name = models.TextField(blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    phone = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    user_name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'doctor'


class DoctorAddress(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    address_line1 = models.TextField(blank=True, null=True)
    address_line2 = models.TextField(blank=True, null=True)
    appartment = models.TextField(blank=True, null=True)
    city = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    doctor_id = models.CharField(max_length=12, blank=True, null=True)
    loc_lat = models.TextField(blank=True, null=True)
    loc_lon = models.TextField(blank=True, null=True)
    state = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    zipcode = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'doctor_address'


class DoctorProcedure(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    doctor = models.CharField(max_length=12, blank=True, null=True)
    doctor_price = models.TextField(blank=True, null=True)
    percentage = models.TextField(blank=True, null=True)
    procedure = models.CharField(max_length=12, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    total = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'doctor_procedure'


class DoctorSpeciality(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    doctor_id = models.CharField(max_length=12, blank=True, null=True)
    speciality = models.CharField(max_length=12, blank=True, null=True)
    speciality_name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'doctor_speciality'


class Migrations(models.Model):
    migration = models.CharField(max_length=255)
    batch = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'migrations'


class TblOrder(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    balance = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    doctor_procedure = models.CharField(max_length=12, blank=True, null=True)
    order_id = models.TextField(blank=True, null=True)
    payer_id = models.TextField(blank=True, null=True)
    payment_id = models.TextField(blank=True, null=True)
    payment_method = models.TextField(blank=True, null=True)
    payment_time = models.TextField(blank=True, null=True)
    request = models.TextField(blank=True, null=True)
    reserve_price = models.TextField(blank=True, null=True)
    responce = models.TextField(blank=True, null=True)
    responce_execute = models.TextField(blank=True, null=True)
    status = models.TextField(blank=True, null=True)
    temp = models.TextField(blank=True, null=True)
    token = models.TextField(blank=True, null=True)
    total_amount = models.TextField(blank=True, null=True)
    user = models.CharField(max_length=12, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'order'


class PasswordResets(models.Model):
    email = models.CharField(max_length=255)
    token = models.CharField(max_length=255)
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'password_resets'


class Posts(models.Model):
    user_id = models.IntegerField()
    title = models.CharField(max_length=255)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'posts'


class Procedure(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    category = models.CharField(max_length=12, blank=True, null=True)
    cpt = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    detail_0_data = models.TextField(db_column='detail.0.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_1_data = models.TextField(db_column='detail.1.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_2_data = models.TextField(db_column='detail.2.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_3_data = models.TextField(db_column='detail.3.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_4_data = models.TextField(db_column='detail.4.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_5_data = models.TextField(db_column='detail.5.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_6_data = models.TextField(db_column='detail.6.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_7_data = models.TextField(db_column='detail.7.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    detail_8_data = models.TextField(db_column='detail.8.data', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    insurance_price = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    price = models.TextField(blank=True, null=True)
    short_name = models.TextField(blank=True, null=True)
    sku = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'procedure'


class Speciality(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    date_updated = models.DateTimeField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'speciality'


class TblAdmin(models.Model):
    business_id = models.IntegerField()
    email = models.CharField(unique=True, max_length=250)
    fname = models.CharField(max_length=250)
    lname = models.CharField(max_length=256)
    sex = models.CharField(max_length=11)
    dob = models.DateField()
    password = models.CharField(max_length=250)
    remember_token = models.CharField(max_length=500)
    role = models.ForeignKey('TblRoles')
    updated_at = models.DateTimeField()
    created_at = models.DateTimeField()
    last_login = models.DateTimeField()
    is_active = models.IntegerField()
    phone = models.BigIntegerField()
    emp_id = models.CharField(max_length=256)
    check_password = models.IntegerField()
    profile_picture = models.CharField(max_length=400)

    class Meta:
        managed = False
        db_table = 'tbl_admin'


class TblAutherization(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    doctor_id = models.IntegerField(blank=True, null=True)
    secret_key = models.CharField(max_length=250)
    status = models.IntegerField(blank=True, null=True)
    date_created = models.DateTimeField()
    date_updated = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_autherization'


class TblBusiness(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=250)
    phone_number = models.BigIntegerField()
    merchant_id = models.IntegerField()
    image = models.CharField(max_length=250)
    address = models.TextField()
    state = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    status = models.IntegerField()
    is_active = models.IntegerField()
    last_login = models.DateTimeField()
    date = models.DateTimeField()
    admin_name = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'tbl_business'


class TblCategory(models.Model):
    business_id = models.IntegerField()
    category_name = models.CharField(max_length=256)
    display_name = models.CharField(max_length=256)
    template_id = models.IntegerField()
    document_name = models.CharField(max_length=256)
    date_created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'tbl_category'


class TblDoctor(models.Model):
    email = models.CharField(max_length=100, blank=True, null=True)
    password = models.CharField(max_length=250, blank=True, null=True)
    name_prefix = models.CharField(max_length=10, blank=True, null=True)
    fname = models.CharField(max_length=50, blank=True, null=True)
    lname = models.CharField(max_length=50, blank=True, null=True)
    phone_no = models.BigIntegerField(blank=True, null=True)
    sex = models.CharField(max_length=50, blank=True, null=True)
    dob = models.DateField(blank=True, null=True)
    experience = models.IntegerField(blank=True, null=True)
    mci_number = models.CharField(max_length=15, blank=True, null=True)
    language = models.TextField()
    status = models.IntegerField(blank=True, null=True)
    profile_picture = models.CharField(max_length=250, blank=True, null=True)
    signature = models.CharField(max_length=250, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)
    get_question = models.IntegerField()
    is_active = models.IntegerField()
    last_login = models.DateTimeField()
    for_kiosk = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'tbl_doctor'


class TblEmail(models.Model):
    status = models.IntegerField()
    business_id = models.IntegerField()
    business_user_id = models.CharField(max_length=256)
    email_uid = models.IntegerField()
    email_from = models.CharField(max_length=256)
    subject = models.CharField(max_length=256)
    body = models.TextField()
    email_date = models.DateTimeField(blank=True, null=True)
    attachment = models.CharField(max_length=256)
    category_id = models.CharField(max_length=256)
    answered_by = models.IntegerField(blank=True, null=True)
    answered_date = models.DateTimeField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    response_email = models.TextField()
    date_created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'tbl_email'


class TblEmailData(models.Model):
    email_from = models.CharField(max_length=256)
    subject = models.TextField()
    body = models.TextField()

    class Meta:
        managed = False
        db_table = 'tbl_email_data'


class TblForgotPassword(models.Model):
    tbl_user_id = models.IntegerField(blank=True, null=True)
    email = models.CharField(max_length=250)
    unique_code = models.CharField(max_length=250)
    authenticated = models.IntegerField()
    date_created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'tbl_forgot_password'


class TblModule(models.Model):
    module_name = models.CharField(max_length=100, blank=True, null=True)
    display_name = models.TextField()
    url = models.CharField(max_length=256)
    class_field = models.TextField(db_column='class')  # Field renamed because it was a Python reserved word.
    has_child = models.IntegerField()
    parent_id = models.IntegerField()
    priority = models.IntegerField()
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_module'


class TblModulePermission(models.Model):
    role = models.IntegerField()
    module = models.IntegerField()
    pr_add = models.IntegerField(blank=True, null=True)
    pr_delete = models.IntegerField(blank=True, null=True)
    pr_edit = models.IntegerField(blank=True, null=True)
    date_created = models.DateTimeField()
    date_modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'tbl_module_permission'


class TblPolicyUser(models.Model):
    policy_no = models.IntegerField()
    life_assured = models.CharField(max_length=256)
    policy_holder = models.CharField(max_length=256)
    assignee = models.CharField(max_length=256)
    nomination = models.CharField(max_length=256)
    date_of_proposal = models.DateField()
    policy_issue_date = models.DateField()
    product = models.CharField(max_length=256)
    sum_assured = models.IntegerField()
    dob = models.DateField()
    age = models.IntegerField()
    policy_term = models.CharField(max_length=256)
    validity = models.DateField()
    email = models.CharField(max_length=256)
    height = models.FloatField()
    weight = models.IntegerField()
    sex = models.CharField(max_length=11)
    address = models.CharField(max_length=256)
    city = models.CharField(max_length=256)
    state = models.CharField(max_length=256)
    pincode = models.IntegerField()
    premium_due = models.CharField(max_length=256)
    premimum_due_date = models.DateField()
    premium_last_recieved = models.IntegerField()
    premium_recieved_date = models.DateField()
    policy_status = models.IntegerField()
    death_date = models.DateField()
    surrender_value = models.CharField(max_length=256)

    class Meta:
        managed = False
        db_table = 'tbl_policy_user'


class TblRoles(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_roles'


class TblTemplate(models.Model):
    name = models.CharField(max_length=256)
    business_id = models.IntegerField()
    template = models.TextField()

    class Meta:
        managed = False
        db_table = 'tbl_template'


class TblTemplateSms(models.Model):
    name = models.CharField(max_length=256)
    business_id = models.IntegerField()
    template = models.TextField()

    class Meta:
        managed = False
        db_table = 'tbl_template_sms'


class TblUser(models.Model):
    business_id = models.IntegerField()
    unique_id = models.IntegerField()
    unique_id_detail = models.CharField(max_length=256)
    email = models.CharField(max_length=200)
    phone = models.BigIntegerField(blank=True, null=True)
    dob = models.CharField(max_length=100)
    sex = models.CharField(max_length=50)
    fname = models.CharField(max_length=100)
    lname = models.CharField(max_length=90)
    date_created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'tbl_user'


class TblUserFetch(models.Model):
    tbl_name = models.CharField(max_length=256)
    business_id = models.IntegerField()
    unique_field_type = models.IntegerField()
    unique_field_name = models.CharField(max_length=256)
    email_field_name = models.CharField(max_length=256)
    fname_field_name = models.CharField(max_length=256)
    dob_field_name = models.CharField(max_length=256)
    sex_field_name = models.CharField(max_length=256)
    lname_field_name = models.CharField(max_length=256)
    date_created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'tbl_user_fetch'


class Token(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    token = models.TextField(blank=True, null=True)
    user_id = models.CharField(max_length=12, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'token'


class UserAddress(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    address_line1 = models.TextField(blank=True, null=True)
    address_line2 = models.TextField(blank=True, null=True)
    appartment = models.TextField(blank=True, null=True)
    city = models.TextField(blank=True, null=True)
    company = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    loc_lat = models.TextField(blank=True, null=True)
    loc_lon = models.TextField(blank=True, null=True)
    state = models.TextField(blank=True, null=True)
    user_id = models.TextField(blank=True, null=True)
    zipcode = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_address'


class Users(models.Model):
    field_id = models.CharField(db_column='_id', max_length=12)  # Field renamed because it started with '_'.
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    first_name = models.TextField(blank=True, null=True)
    last_name = models.TextField(blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    phone = models.TextField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    user_name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'
