__author__ = 'Kunal Monga'
'''
This file will be used across the project for calling functions that are used at various places
'''
from flask import Blueprint, request, jsonify


# Import the database object from the main app module
from app import db, constants, dbsql, mail
import json, nexmo, bandwidth
from app.model import model
# from flask_mail import Message
# from flask.ext.mail import Message
from bson.objectid import ObjectId
from watson_developer_cloud import NaturalLanguageClassifierV1



def findAndCallWatson(data):
	try:
		print "kunal in watson"
		cursor = dbsql.connection.cursor()
		sqlquery = "SELECT tbl_name,email_field_name,fname_field_name,sex_field_name from tbl_user_fetch where business_id="+str(constants.getBusinessId())+" order BY id desc limit 0,1"
		cursor.execute(sqlquery)
		getFields = cursor.fetchone()
		if getFields is not None:
			sql="select "+getFields[1]+", "+getFields[2]+", "+getFields[3]+",id from "+getFields[0]+" where email='"+str(data["mailFrom"])+"'"
			cursor.execute(sql)
			getdetail = cursor.fetchone()
			# if getdetail is not None:
			readWatson = callWatson(data)
			sex=0
			# if getFields[3]=="Male":
			# 	sex=1
			# sql1="INSERT INTO `tbl_user` (`business_id`, `unique_id`, `unique_id_detail`, `email`, `phone`, `dob`, `sex`, `fname`, `lname`) VALUES ('"+str(constants.getBusinessId())+"','1','email','"+str(getdetail[0])+"','0','','"+str(sex)+"','"+str(getdetail[1])+"','')"''
			# cursor.execute(sql1)
			# dbsql.connection.commit()
			sql2="INSERT INTO `tbl_email` (`business_id`, `business_user_id`, `email_uid`, `email_from`, `subject`, `body`, `email_date`, `status`, `attachment`, `category_id`,`response_email`) VALUES ('"+str(constants.getBusinessId())+"','49',"+str(data["mailUID"])+",'"+str(data["mailFrom"])+"','"+str(data["mailSubject"])+"','"+str(data["mailBody"])+"','"+str(data["mailDate"])+"','0','','"+str(readWatson)+"','')"
			cursor.execute(sql2)
			dbsql.connection.commit()
	except Exception as e:
		print str(e)
	return True 



def findAndCallWatsonSMS(data):
	try:
		readWatson = callWatson(data)
		print readWatson
		# readWatson = 'product-price'
		cursor = dbsql.connection.cursor()
		sqlTemplate="select a.template,b.category_name from tbl_template_healora as a join tbl_category_healora as b on a.id=b.template_id where b.category_name='"+readWatson+"'"
		print sqlTemplate
		cursor.execute(sqlTemplate)
		getCONTENT = cursor.fetchone()
		responceContent = getCONTENT
	except Exception as e:
		print str(e)
	return responceContent 


# def sendEmail():
# 	# msg = Message()
# 	# msg.recipients = "k.m.developer0078@gmail.com"
# 	# msg.subject = "test"
# 	# msg.body = "test"
# 	# mail.send(Message("Hello",
#  #                  sender="from@example.com",
#  #                  recipients=["to@example.com"]))
#  	text="this is the text"
# 	# msg = Message(str(text),sender="hellosimplifyreality@gmail.com",recipients=["k.m.developer0078@gmail.com"])
# 	msg = Message()
# 	msg.body="this is testing message"
# 	msg.sender="hellosimplifyreality@gmail.com"
# 	msg.recipients = ["hellosimplifyreality@gmail.com"]
# 	# print type(msg),len(msg)
# 	print "kunu"
# 	mail.send(msg)
# 	print "fas"
# 	return "test"


def callWatson(data):
	print "kunal"
	claimName = ""
	content = data["mailBody"]
	# natural_language_classifier = NaturalLanguageClassifierV1(username='8a3c1058-e7e9-4c75-9a88-055ac4b7d9a5',password='n2rFYkXtnclr')
	# classes = natural_language_classifier.classify('8fc4f0x298-nlc-3364', content)
	natural_language_classifier = NaturalLanguageClassifierV1(username='c6562efb-0f08-4214-9f5e-e4db266c1732',password='KRaMRi1YOSkt')
	classes = natural_language_classifier.classify('f8fb0cx535-nlc-108', content)
	classes = classes.get_result()
	classes['top_class']
	classes['claim_name'] = classes['top_class']
	return classes['top_class']



def sendSMS(data):
	print "d"
	client = nexmo.Client(key='001cf7fd', secret='EzzEHsvXaf8ti7Js')
	res = client.send_message({'from': '447418342220','to': data['to'],'text': data['text']})
	print res
	return res

def sendSMSBand(data):
	messaging_api = bandwidth.client('messaging', 'u-ggbvdtrc2ry2mocczh6m43a', 't-kwd2epfbnglph3skbbqtm7i', 'nshyw57wcui4jwxcetu45qugm3vlx6oq7hqfbua')
	message_id = messaging_api.send_message(from_ = '+18443847811',to = data['to'],text = data['text'])
	my_message = messaging_api.get_message(message_id)
	return my_message


# curl -X POST  https://rest.nexmo.com/sms/json -d api_key=001cf7fd -d api_secret=EzzEHsvXaf8ti7Js -d to=13102799918 -d from=18884400078 -d text="Hello from Nexmo"