'''
This file will be used across the project for using and defining constants, getting time and some validation and conversion functions
'''
import datetime
import time
import base64
import pytz


unaware = datetime.datetime(2011, 8, 15, 8, 15, 12, 0)
aware = datetime.datetime(2011, 8, 15, 8, 15, 12, 0, pytz.UTC)
now_aware = pytz.utc.localize(unaware)
assert aware == now_aware


def geoFenceArea():
    return 600

def getBusinessId():
    return 1
    
def DATE_TIME():
    return datetime.datetime.now(pytz.timezone('Asia/Calcutta'))

def NEXT_DATE_TIME():
    return DATE_TIME() + datetime.timedelta(days=1)

def formatEmailTime(dateTime):
    print "dateTime"
#    return dateTime
    try:
        return datetime.datetime.strftime(datetime.datetime.strptime(str(dateTime), '%a, %d %b %Y %H:%M:%S'),"%Y-%m-%d %H:%M:%S")
    except Exception:
        try:
            splitDate = dateTime.split()
            dateTime = splitDate[0] + ' ' + splitDate[1] + ' ' + splitDate[2] + ' ' + splitDate[3]
            return datetime.datetime.strftime(datetime.datetime.strptime(str(dateTime), '%d %b %Y %H:%M:%S'),"%Y-%m-%d %H:%M:%S")
        except Exception:
            return FORMATTED_TIME()

def FORMATTED_TIME():
    return datetime.datetime.strptime(str(DATE_TIME()).split('.')[0], '%Y-%m-%d %H:%M:%S')

def FORMATTED_TIME_CREATE(getDate):
    return datetime.datetime.strptime(str(getDate), '%Y-%m-%d %H:%M:%S')


def TIME():    
    return FORMATTED_TIME().time()

def DATE():
    return FORMATTED_TIME().date()

def getTime(dateTime):
#    return dateTime
    return datetime.datetime.strftime(datetime.datetime.strptime(str(dateTime), '%Y-%m-%d %H:%M:%S'),"%I:%M %p")

def getDateTime(dateTime):
#    return dateTime
    try:
        return datetime.datetime.strftime(datetime.datetime.strptime(str(dateTime), '%Y-%m-%d %H:%M:%S'),"%Y-%m-%d %H:%M:%S")
    except Exception:
        return ""

def getTimeStampFromData(getdate):
#    return dateTime
    return str(int(time.mktime(FORMATTED_TIME_CREATE(getdate).timetuple())))


def getTimeStamp():
#    return dateTime
    return str(int(time.mktime(FORMATTED_TIME().timetuple())))


def getTimeStampBeforeOneHour():
#   print FORMATTED_TIME()
    return time.mktime(LATER_ONE_HOUR().timetuple())

def convertTimestamp(timestamp):
    try:
        datetime.datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
    except Exception, e:
        print e
        return False

def LATER_ONE_HOUR():
    return FORMATTED_TIME() - datetime.timedelta(0, 60 * 60)

def LATER_HOURS(hours):
    # print hours
    retddata = FORMATTED_TIME() - datetime.timedelta(0, int(hours) * 60 * 60)
    return retddata

def LATERTIME():
    return LATER().time()

#### -------------------- add 30 minutes
def AFTER():
    return FORMATTED_TIME() + datetime.timedelta(0, 30 * 60)

def AFTERTIME():
    return AFTER().time()


def diffInDates(dateFrom):
    try:
        date1 = datetime.datetime.strptime(str(dateFrom), '%Y-%m-%d %H:%M:%S')
        date2 = datetime.datetime.strptime(str(FORMATTED_TIME()), '%Y-%m-%d %H:%M:%S')
        diff = date2 - date1
        return diff.seconds
    except Exception as e:
        print e
        return 'exiting'

def diffInDynamicsDates(dateFrom,dateTo):
    try:
        date1 = datetime.datetime.strptime(str(dateFrom), '%Y-%m-%d %H:%M:%S')
        date2 = datetime.datetime.strptime(str(dateTo), '%Y-%m-%d %H:%M:%S')
        diff = date2 - date1
        return diff.seconds
    except Exception as e:
        print e
        return 'exiting'
    

def checkInt(to_check):
    try:
        return int(to_check)
    except ValueError:
        return None
    except TypeError:
        return None
    except Exception as generalException:
        return None


def base32encode(string):
    try:
        return base64.b32encode(string)
    except Exception as generalException:
        return None

def base32decode(string):
    try:
        return base64.b32decode(string)
    except Exception as generalException:
        return None