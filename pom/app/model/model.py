__author__ = 'Kunal Monga'

# import requirement
# We are creating all tables in single file.
# In this we have structure and validators 

from mongokit import Document
from app import app, db
from mongokit import ValidationError
from datetime import datetime

# Classes starts here
class Places(Document):
    __collection__ = 'places'

    structure = {
        "name" : unicode,
        "user_id" : int,
        "address" : unicode,
        "loc_lat" : unicode,
        "loc_lon" : unicode,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    # validators = {
    #     "group_name" : validators.max_length(50),
    #     "user_id" : validators.max_length(50),
    #     "updated_by" : validators.max_length(50),
    #     "date_created" : validators.max_length(50),
    #     "date_updated" : validators.max_length(50),
    #     "timestamp" : validators.max_length(50),
    #     "color" : validators.max_length(50),
    #     "image" : validators.max_length(50)
    # }
    use_dot_notation = True

    def __repr__(self):
        return '<Place %r>' % (self.name)

db.register([Places])


class Group(Document):
    __collection__ = 'group'

    structure = {
        "group_name" : unicode,
        "user_id" : int,
        "updated_by" : int,
        "date_created" : datetime,
        "date_updated" : datetime,
        "timestamp" : int,
        "theme_color" : unicode,
        "picture" : str,
        "places": [{
                "user_id" : int,
                "place_id" : Places,
                "date_created" : datetime,
                "restrict" : int
            }]
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Group %r>' % (self.group_name)

db.register([Group])


class MuteGroup(Document):
    __collection__ = 'mutegroup'

    structure = {
        "group_id" : Group,
        "user_id" : int,
        "mute_hours" : str,
        "date_created" : datetime,
    }
    use_dot_notation = True

    def __repr__(self):
        return '<MuteGroup %r>' % (self._id)

db.register([MuteGroup])


class UserGroup(Document):
    __database__ = 'pom'
    __collection__ = 'usergroup'

    structure = {
        "group_id" : Group,
        "user_id" : int,
        "loc_sharing" : int,
        "date_created" : datetime,
    }
    use_dot_notation = True

    def __repr__(self):
        return '<GroupMember %r>' % (self.user_id)

db.register([UserGroup])



class PlaceRestrict(Document):
    __collection__ = 'placerestrict'

    structure = {
        "group_id" : Group,
        "user_id" : int,
        "place_id" : Places,
        "entry_sharing" : int,
        "exit_sharing" : int,
        "date_created" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<PlaceMember %r>' % (self.user_id)

db.register([PlaceRestrict])


class CheckIn(Document):
    __collection__ = 'checkin'

    structure = {
        "name" : unicode,
        "user_id" : int,
        "address" : unicode,
        "loc_lat" : unicode,
        "loc_lon" : unicode,
        "group_id": Group,
        "date_created" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Place %r>' % (self.name)

db.register([CheckIn])

class EntryExitTracker(Document):
    __collection__ = 'entryexittracker'

    structure = {
        "user_id" : int,
        "place_id" : Places,
        "entry" : int,
        "exit" : int,
        "timestamp" : int
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Entry %r>' % (self.user_id)

db.register([EntryExitTracker])

class Location(Document):
    __collection__ = 'location'

    structure = {
        "name" : str,
        "user_id" : int,
        "address" : str,
        "loc_lat" : unicode,
        "loc_lon" : unicode,
        "timestamp" : int,
        "activity" : unicode,
        "altitude" : unicode,
        "speed" : unicode,
        "accuracy" : unicode,
        "battery_status" : unicode,
        "mos" : int,
        "date_created" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Place %r>' % (self.name)

db.register([Location])


class LastLocation(Document):
    __collection__ = 'lastlocation'

    structure = {
        "user_id" : int,
        "loc_lat" : unicode,
        "loc_lon" : unicode,
        "timestamp" : int,
        "mos" : int,
        "date_created" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<LastLocation %r>' % (self.user_id)

db.register([LastLocation])




class Sos(Document):
    __collection__ = 'sos'

    structure = {
        "user_id" : int,
        "date_created" : datetime,
        "date_updated" : datetime,
        "contacts": [{
                "name" : unicode,
                "mobile" : unicode
            }]
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Sos %r>' % (self.group_name)

db.register([Sos])




class Notification(Document):
    __collection__ = 'notification'

    structure = {
        "notif_type" : unicode,
        "user_id" : int,
        "title" : str,
        "message" : str,
        "date_created" : datetime, 
        "detail" :  [{
                "place_id" : str,
                "group_id" : str,
                "timestamp" : int,
                "tag" : str,
                "speed" : str,
                "is_charging" : str,
                "charging_mode" : str,
                "battery_status" : str,
                "loc_lat" : str,
                "loc_lon" : str,
                "accuracy" : str,
                "from_user_id" : int
            }]
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Notification %r>' % (self.notif_type)

db.register([Notification])
