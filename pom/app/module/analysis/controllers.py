__author__ = 'Kunal Monga'
# Import flask dependencies
from flask import Blueprint, request, jsonify


# Import the database object from the main app module test
from app import db, constants, dbsql, functions, mail, functionshealora, functionAus
import json, urllib, datetime, email, re, bandwidth
from bs4 import BeautifulSoup
from app.model import model
from bson.objectid import ObjectId
from app.module.validation import validate
from watson_developer_cloud import NaturalLanguageClassifierV1
from twilio.twiml.messaging_response import MessagingResponse


# Define the blueprint: 'auth', set its url prefix: app.url/auth
analysis_link = Blueprint('analysis', __name__, url_prefix='/analysis')

# Check In  API starts
# Takes location details as input and returns 
# Set the route and accepted methods
@analysis_link.route('/readEmail/', methods=['GET'])
def readEmail():
	mail.select()
	emailUid=[]
	fetchUID=""
	uidList=[]
	lastUID=0
	print "here"
	cursor = dbsql.connection.cursor()
	sqlquery="SELECT email_uid from tbl_email where business_id="+str(constants.getBusinessId())+" order BY email_uid desc limit 0,1"
	cursor.execute(sqlquery)
	getUID = cursor.fetchone()
	print "fa"
	if getUID is not None:
		lastUID=getUID[0]
	print datetime.date.today() - datetime.timedelta(days=1)
	# date = (datetime.date.today()).strftime("%d-%b-%Y")
	date = (datetime.date.today() - datetime.timedelta(days=1)).strftime("%d-%b-%Y")
	result, data = mail.uid('search', None, '(SENTSINCE {date})'.format(date=date))
	data=data[0].split()
	
	if data:
		emailUid = data
		for x in emailUid:
			if int(x) > int(lastUID):
				uidList.append(x)
				if fetchUID != "":
					fetchUID = fetchUID+","+str(x)
				else:
					fetchUID = str(x)
	if fetchUID != "":
		res, emailData = mail.uid('fetch', fetchUID, '(RFC822)')
		print len(emailData)
		count=0
		for emailContent in emailData:
			try:
				body = ""
				mailbody = ""
				extratdata = ""
				raw_email = emailContent[1]
				email_message = email.message_from_string(raw_email)
				maintype = email_message.get_content_maintype()
				emailFrom=email.utils.parseaddr(email_message['From'])[1]
				if maintype == 'multipart':
					# print "fs"
					for part in email_message.get_payload():
						print part
						if part.get_content_maintype() == 'text':
							body= part.get_payload(decode=True)
							print body
				elif maintype == 'text':
					body= email_message.get_payload(decode=True)
					# print "fsd"
				splitDate=email_message['Date'].split()
				emailDate=constants.formatEmailTime(splitDate[0]+' '+splitDate[1]+' '+splitDate[2]+' '+splitDate[3]+' '+splitDate[4])
				body = body.replace("&nbsp", " ")
				body = body.replace(";", "")
				body_new = re.sub('<br\s*?/*>', '\n', body)
				soup = BeautifulSoup(body_new)
				soup_body = soup.text
				# print soup_body
				mailbody = soup_body
				mailbody = mailbody.replace("=C2=A0", "\t\r\n")
				mailbody = mailbody.replace("=", " ")
				mailbody = re.sub('<!--[^>]+-->','', mailbody)
				mailbody = re.sub("'","", mailbody)
				print len(mailbody)
				mailbody = (mailbody[:1024] + '') if len(mailbody) > 1024 else mailbody
				# print "reached"
				extratdata={"mailUID":uidList[count],"mailFrom":emailFrom,"mailDate":emailDate,"mailBody":mailbody,"mailSubject":email_message['Subject']}
				print mailbody
				finRes = functions.findAndCallWatson(extratdata)
				count = count+1
			except Exception as e:
				print str(e)
	content = {
               'status' : 1,
               'message' : 'No Content',
               'data' : data
               }

	return jsonify(content)



# Test Watson
@analysis_link.route('/analyseContent/', methods=['POST'])
def analyseContent():
	claimName = ""
	data = request.json
	content = data["text"]
	natural_language_classifier = NaturalLanguageClassifierV1(username='31c27a64-0966-4a3a-b7f9-fc64cfedb13a',password='WyhldcEXFP7G')
	# classifiers = natural_language_classifier.list_classifiers().get_result()
	# print(json.dumps(classifiers, indent=2))
	classes = natural_language_classifier.classify('6b568ax396-nlc-789', content)
	classes['claim_name'] = classes['top_class']
	return jsonify(classes)




# Test Watson
@analysis_link.route('/messageRead', methods=['POST'])
def messageRead():
	data = request.json
	analyse={"mailBody":data["text"]}
	watsonRes = functions.findAndCallWatsonSMS(analyse)
	smsdata = {"to":data["msisdn"], "text":watsonRes[0], "category":watsonRes[1],"input_text":data["text"]}
	print "ding"
	resu = functions.sendSMS(smsdata)
	content = {
               'status' : 0,
               'message' : 'No Content',
               'data' : 'classes'
               }

	return jsonify(content)


# Test Watson
@analysis_link.route('/messageReadClearBank', methods=['POST'])
def messageReadClearBank():
	data = request.json
	analyse={"mailBody":data["text"]}
	watsonRes = functions.findAndCallWatsonClbanksms(analyse,"tbl_template_cb_sms")
	smsdata = {"to":data["msisdn"], "text":watsonRes[0], "category":watsonRes[1],"input_text":data["text"]}
	smsdata["msg_id"]="id"
	functions.addMessage(smsdata)
	resu = functions.sendSMSbank(smsdata)
	# resu = functions.sendSMSBand(smsdata)
	content = {
               'status' : 0,
               'message' : 'No Content',
               'data' : resu
               }

	return jsonify(content)

# Test Watson
@analysis_link.route('/messageReadSgBank', methods=['POST'])
def messageReadSgBank():
	data = request.json
	analyse={"mailBody":data["text"]}
	watsonRes = functions.findAndCallWatsonClbanksms(analyse,"tbl_template_sg_sms")
	smsdata = {"to":data["msisdn"], "text":watsonRes[0], "category":watsonRes[1],"input_text":data["text"]}
	resu = functions.sendSMSbank(smsdata)
	smsdata["msg_id"]="id"
	functions.addMessage(smsdata)
	# resu = functions.sendSMSBand(smsdata)
	content = {
               'status' : 0,
               'message' : 'No Content',
               'data' : 'classes'
               }

	return jsonify(content)



# Test Watson
@analysis_link.route('/messageReadBand', methods=['POST'])
def messageReadBand():
	data = request.json
	analyse={"mailBody":data["text"]}
	watsonRes = functions.findAndCallWatsonSMS(analyse)
	smsdata = {"to":data["msisdn"], "text":watsonRes[0], "category":watsonRes[1],"input_text":data["text"]}
	resu = functions.sendSMSBand(smsdata)
	smsdata["msg_id"]=resu["id"]
	functions.addMessage(smsdata)
	content = {
               'status' : 0,
               'message' : 'No Content',
               'data' : resu,
               'smsdata':smsdata
               }

	return jsonify(content)

# Test Watson
@analysis_link.route('/messageReadBandAus', methods=['POST'])
def messageReadBandAus():
	data = request.json
	analyse={"mailBody":data["text"]}
	watsonRes = functions.findAndCallWatsonSMS(analyse)
	smsdata = {"to":data["msisdn"], "text":watsonRes[0], "category":watsonRes[1],"input_text":data["text"]}
	resu = functionAus.sendSMS(smsdata)
	content = {
               'status' : 0,
               'message' : 'No Content',
               'data' : resu,
               'smsdata':smsdata
               }

	return jsonify(content)

# Test Watson
@analysis_link.route('/sms', methods=['GET','POST'])
def sms():
	data={}
	data["msisdn"] = request.form['From']
	data["text"] = request.form['Body']
	analyse={"mailBody":data["text"]}
	watsonRes = functionAus.findAndCallWatsonSMS(analyse)
	print "kununu", watsonRes
	smsdata = {"to":data["msisdn"], "text":watsonRes[0], "category":watsonRes[1],"input_text":data["text"]}
	resu = functionAus.sendSMS(smsdata)
	content = {
               'status' : 0,
               'message' : 'No Content',
               'data' : resu,
               'smsdata':smsdata
               }

	# print content
	return jsonify(content)



# Test Watson
@analysis_link.route('/incomingUnanswered', methods=['POST'])
def incomingUnanswered():
	data = request.json
	message= "Hello, Please reply with your question if you want to switch your Simplify Reality call to text."
	smsdata = {"to":data["msisdn"], "text":message, "category":"0","input_text":"Missed Call"}
	resu = functions.sendSMSBand(smsdata)
	smsdata["msg_id"]=resu["id"]
	functions.addMessage(smsdata)
	content = {
               'status' : 0,
               'message' : 'No Content',
               'data' : resu
               }

	return jsonify(content)



# Test Watson
@analysis_link.route('/messageReadBandHealora', methods=['POST'])
def messageReadBandHealora():
	data = request.json
	analyse={"mailBody":data["text"]}
	watsonRes = functionshealora.findAndCallWatsonSMS(analyse)
	smsdata = {"to":data["msisdn"], "text":watsonRes[0], "category":watsonRes[1],"input_text":data["text"]}
	resu = functionshealora.sendSMSBand(smsdata)
	smsdata["msg_id"]=resu["id"]
	functions.addMessage(smsdata)
	content = {
               'status' : 0,
               'message' : 'No Content',
               'data' : resu
               }

	return jsonify(content)




# Test Watson
@analysis_link.route('/incomingUnansweredHealora', methods=['POST'])
def incomingUnansweredHealora():
	data = request.json
	message= "Hello, Please reply with your question if you want to switch your Healora call to text."
	smsdata = {"to":data["msisdn"], "text":message, "category":"0","input_text":"Missed Call"}
	resu = functionshealora.sendSMSBand(smsdata)
	smsdata["msg_id"]=resu["id"]
	functions.addMessage(smsdata)
	content = {
               'status' : 0,
               'message' : 'No Content',
               'data' : resu
               }

	return jsonify(content)