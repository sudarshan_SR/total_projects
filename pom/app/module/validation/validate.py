__author__ = 'Kunal Monga'
# Import flask dependencies
from flask import Blueprint, request, jsonify


# Import the database object from the main app module
from app import db, constants, dbsql, functions
import json, urllib
from app.model import model
from app.module.validation import datalist, valid
from bson.objectid import ObjectId

# Here we will create all validation for the complete structure.

def validate(data,func,action):
	try:
		content={
					"valid":1,
					"validdata":{}
				}
		if 'action_type' in data and action!="": 
			isFor = func+"_"+action
		else:
			isFor = func
		for key, value in datalist.checkList[isFor].iteritems():
			if value["required"]==1:
				if key in data:
					for fun in value["function"]:
						method_to_call = getattr(valid, fun)
						if fun!='notEmpty':
							result = method_to_call(data[key])
						else:
							result = method_to_call(data[key],key)
						if result['status']==0:
							content['valid'] =0
							content['validdata'] = result
							raise ValueError(result['message'])
				else:
					content['valid'] = 0
					content['validdata'] = {
										"status":0,
										"message":str(key)+" does not exist.",
										"debug":"",
										"value":""
										}
					raise ValueError(result['message'])
			elif value["required"]==2:
				if key in data:
					for fun in value["function"]:
						method_to_call = getattr(valid, fun)
						if fun!='notEmpty':
							result = method_to_call(data[key])
						else:
							result = method_to_call(data[key],key)
						if result['status']==0:
							content['valid'] = 0
							content['validdata'] = result
							raise ValueError(result['message'])
			elif value["required"]==3:
				if value["parent"] in data:
					if key in data:
						for fun in value["function"]:
							method_to_call = getattr(valid, fun)
							if fun!='notEmpty':
								result = method_to_call(data[key])
							else:
								result = method_to_call(data[key],key)
							if result['status']==0:
								content['valid'] = 0
								content['validdata'] = result
								raise ValueError(result['message'])
					else:
						print "FAsdasfd"
						content['valid'] = 0
						content['validdata'] = {
										"status":0,
										"message":str(key)+" does not exist.",
										"debug":"",
										"value":""
										}
						raise ValueError(result['message'])

		return content
	except ValueError, e:
		print str(e)
		return content
	except Exception, e:
		print str(e)
		return content





