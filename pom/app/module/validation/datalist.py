__author__ = 'Kunal Monga'

checkList={}
# Required params:
# 1- its required and mandatory
# 2- Its not mandatory
# 3- Required if parent exist
# 
# 
checkList['manage_group_create']={
						"user_id":{ 
								"required":1,
								"parent":"",
								"function":['notEmpty', 'checkInt', 'checkUserExist']
							},
						"name":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"theme_color":{ 
								"required":1,
								"parent":"",
								"function":["colorCode"]
							},
						"picture":{ 
								"required":2,
								"parent":"",
								"function":["notEmpty"]
							},
						"picture_mime_type":{ 
								"required":3,
								"parent":"picture",
								"function":["notEmpty", "checkImgType"]
							},
						"action_type":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							}
					}

checkList['manage_group_update']={
						"user_id":{ 
								"required":1,
								"parent":"",
								"function":['notEmpty', 'checkInt', 'checkUserExist']
							},
						"name":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"theme_color":{ 
								"required":1,
								"parent":"",
								"function":["colorCode"]
							},
						"picture":{ 
								"required":2,
								"parent":"",
								"function":["notEmpty"]
							},
						"picture_mime_type":{ 
								"required":3,
								"parent":"picture",
								"function":["notEmpty", "checkImgType"]
							},
						"action_type":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"group_id":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty",'checkGroupExist']
							}
					}


checkList['get_all_groups']={
						"user_id":{ 
								"required":1,
								"parent":"",
								"function":['notEmpty', 'checkInt', 'checkUserExist']
							},
						"regid":{ 
								"required":2,
								"parent":"",
								"function":["notEmpty"]
							},
						"mos":{ 
								"required":3,
								"parent":"regid",
								"function":['notEmpty', 'checkMos']
							}
					}

checkList['get_a_group']={
						"user_id":{ 
								"required":1,
								"parent":"",
								"function":['notEmpty', 'checkInt', 'checkUserExist']
							},
						"group_id":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty",'checkGroupExist']
							}
					}


checkList['get_invitation_token']={
						"user_id":{ 
								"required":1,
								"parent":"",
								"function":['notEmpty', 'checkInt', 'checkUserExist']
							},
						"group_id":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty",'checkGroupExist']
							}
					}

checkList['configure_place_setting_both']={
						"user_id":{ 
								"required":1,
								"parent":"",
								"function":['notEmpty', 'checkInt', 'checkUserExist']
							},
						"group_id":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty",'checkGroupExist']
							},
						"place_id":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty",'checkPlaceExist']
							},
						"type":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							}
					}


checkList['update_emergency_contacts']={
						"user_id":{ 
								"required":1,
								"parent":"",
								"function":['notEmpty', 'checkInt', 'checkUserExist']
							},
						"contacts":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty",'checkValidSos']
							}
					}

checkList['create_notification_place_entry']={
						"user_id":{ 
								"required":1,
								"parent":"",
								"function":['notEmpty', 'checkInt', 'checkUserExist']
							},
						"notif_type":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"send_push_notification":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"timestamp":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"timestamp":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							}
					}

checkList['create_notification_place_exit']={
						"user_id":{ 
								"required":1,
								"parent":"",
								"function":['notEmpty', 'checkInt', 'checkUserExist']
							},
						"notif_type":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"send_push_notification":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"timestamp":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							},
						"timestamp":{ 
								"required":1,
								"parent":"",
								"function":["notEmpty"]
							}
					}