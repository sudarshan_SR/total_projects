__author__ = 'Kunal Monga'


# Import the database object from the main app module
from app import db, constants, dbsql, functions
import json, urllib, re
from app.model import model
from app.module.validation import datalist, msgtext
from bson.objectid import ObjectId


def checkInt(to_check):
	try:
		content={
				"status":0,
				"message":"Something went wrong.",
				"debug":"",
				"value":""
				}
		value = int(to_check)
		content['message'] = msgtext.intValid
		content['value'] = value
		content['status'] = 1
	except ValueError, e:
		print str(e)
		content['message'] = msgtext.intInValid
		content['debug'] = "Debug"+str(e)
	except TypeError, e:
		print str(e)
		content['message'] = msgtext.intInValid
		content['debug'] = "Debug"+str(e)
	except Exception, e:
		print str(e)
		content['message'] = msgtext.intInValid
		content['debug'] = "Debug"+str(e)
	return content

def notEmpty(data,key):
	content={
			"status":0,
			"message":"Something went wrong",
			"debug":"",
			"value":""
			}
	if data.strip() != "" and data is not None:
		content['message'] = msgtext.success
		content['value'] = data
		content['status'] = 1
	else:
		content['message'] = key+msgtext.isEmpty
		content['debug'] = "Debug"
	return content

def checkUserExist(userId):
	try:
		content={
				"status":0,
				"message":"Something went wrong",
				"debug":"",
				"value":""
				}
		cursor = dbsql.connection.cursor()
		cursor.execute("SELECT a.user_fname,a.user_lname from users as a where a.user_id="+str(userId))
		memProfile = cursor.fetchone()
		if memProfile is not None:
			content['message'] = msgtext.success
			content['value'] = memProfile[0]+' '+memProfile[1]
			content['status'] = 1
		else:
			content['message'] = msgtext.userDoesNtExist
	except Exception, e:
		print str(e)
		content['message'] = msgtext.userDoesNtExist
		content['debug'] = "Debug"+str(e)
	return content
	

def checkPlaceExist(placeId):
	try:
		content={
				"status":0,
				"message":"Something went wrong",
				"debug":"",
				"value":""
				}
		plObj = db.Places.find_one({"_id": ObjectId(placeId)})
		if plObj is not None:
			content['message'] = msgtext.success
			content['value'] = plObj.name
			content['status'] = 1
		else:
			content['message'] = msgtext.placeDoesNtExist
			content['status'] = 0
	except Exception, e:
		print str(e)
		content['message'] = msgtext.placeDoesNtExist
		content['debug'] = "Debug"+str(e)
	return content

def checkGroupExist(groupId):
	try:
		content={
				"status":0,
				"message":"Something went wrong",
				"debug":"",
				"value":""
				}
		gpObj = db.Group.find_one({"_id": ObjectId(groupId)})
		if gpObj is not None:
			content['message'] = msgtext.success
			content['value'] = gpObj.group_name
			content['status'] = 1
		else:
			content['message'] = msgtext.groupDoesNtExist
			content['status'] = 0
	except Exception, e:
		print str(e)
		content['message'] = msgtext.groupDoesNtExist
		content['debug'] = "Debug"+str(e)
	return content


def checkImgType(imageType):
	content={
			"status":0,
			"message":"Something went wrong",
			"debug":"",
			"value":""
			}
	validimage=["png","PNG","jpg","jpeg","JPG","JPEG"]
	if imageType in validimage:
		content['message'] = msgtext.success
		content['value'] = imageType
		content['status'] = 1
	else:
		content['message'] = msgtext.imageTypeDoesNtExist
		content['status'] = 0
	return content

def colorCode(data):
	content={
			"status":0,
			"message":"Something went wrong",
			"debug":"",
			"value":""
			}
	if re.search(r'^#(?:[0-9a-fA-F]{3}){1,2}$', data):
		content['message'] = msgtext.success
		content['value'] = data
		content['status'] = 1
	else:
		content['message'] = msgtext.colorCodeInvalid
		content['status'] = 0
	return content



def checkMos(mostype):
	content={
			"status":0,
			"message":"Something went wrong",
			"debug":"",
			"value":""
			}
	validmos=["android","iOS"]
	if mostype in validmos:
		content['message'] = msgtext.success
		content['value'] = mostype
		content['status'] = 1
	else:
		content['message'] = msgtext.mosDoesNtExist
		content['status'] = 0
	return content





def checkValidSos(sos):
	try:
		listUsercount=[]
		count=0
		content={
				"status":0,
				"message":"Something went wrong",
				"debug":"",
				"value":""
				}
		contactList = json.loads(urllib.unquote(urllib.unquote(sos)))
		pushMobile=[]
		for x in contactList:
			if x['mobile'] in listUsercount:
				content['message'] = msgtext.sosDuplicate
				content['status'] = 0
				return content
			listUsercount.append(x['mobile'])
		count = len(list(set(listUsercount)))

		if count <= 3 :
			content['message'] = msgtext.success
			content['status'] = 1
		else:
			content['message'] = msgtext.sosInvalid
			content['status'] = 0
	except Exception, e:
		print str(e)
		content['message'] = msgtext.groupDoesNtExist
		content['debug'] = "Debug"+str(e)
	return content