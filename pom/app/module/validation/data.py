__author__ = 'Kunal Monga'


# Required params:
# 1- its required and mandatory
# 2- Its not mandatory
# 3- Required if parent exist
# 
# 
manage_group_create ={
						"user_id":{ 
								"required":1,
								"parent":"",
								"function":"notEmpty, checkInt"
							},
						"name":{ 
								"required":1,
								"parent":"",
								"function":"notEmpty"
							},
						"theme_color":{ 
								"required":1,
								"parent":"",
								"function":"colorCode"
							},
						"picture":{ 
								"required":2,
								"parent":"",
								"function":"notEmpty, checkMime"
							},
						"picture_mime_type":{ 
								"required":3,
								"parent":"picture",
								"function":"notEmpty, checkImgType"
							},
						"action_type":{ 
								"required":1,
								"parent":"",
								"function":"notEmpty"
							}
					}

typing=[
						{
							"key":"user_id", 
							"required":1,
							"parent":""
						},

					]