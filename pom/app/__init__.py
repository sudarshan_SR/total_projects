__author__ = 'Kunal Monga'
# Import flask and template operators
from flask import Flask, render_template
from flask_mongokit import MongoKit
from flask_mysqldb import MySQL
from flask_mail import Mail
from flask import send_from_directory
import settings, cred
import imaplib
# Define the WSGI application from_object
app = Flask(__name__)


# Configurations
app.config.from_object(settings)

# Define the database object which is imported Here we are connecting to two diffrent databases mongo and mysql
db = MongoKit(app)
dbsql = MySQL(app)
mail = Mail(app)
# Import email from server
mail = imaplib.IMAP4_SSL(cred.emailserver())
mail.login(cred.emailuser(), cred.emailpass())
mail.list()
# send file path 
@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory('uploads',
                               filename)

# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return 'Not Found'

# Import a module / component using its blueprint handler variable (mod_auth)

from app.module.analysis.controllers import analysis_link as analysis_module
# Register blueprint(s)

app.register_blueprint(analysis_module)
# ..

