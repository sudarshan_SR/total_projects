from flask import Flask
from mongokit import Connection, Document

# configuration
MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017

# create the little application object
app = Flask(__name__)
app.config.from_object(__name__)

# connect to the database
connection = Connection(app.config['MONGODB_HOST'],
                        app.config['MONGODB_PORT'])


class Group(Document):
    __collection__ = 'group'

    structure = {
        "group_name" : unicode,
        "user_id" : unicode,
        "updated_by" : unicode,
        "date_created" : unicode,
        "date_updated" : unicode,
        "timestamp" : unicode,
        "color" : unicode,
        "image" : unicode
    }
    # validators = {
    #     "group_name" : validators.max_length(50),
    #     "user_id" : validators.max_length(50),
    #     "updated_by" : validators.max_length(50),
    #     "date_created" : validators.max_length(50),
    #     "date_updated" : validators.max_length(50),
    #     "timestamp" : validators.max_length(50),
    #     "color" : validators.max_length(50),
    #     "image" : validators.max_length(50)
    # }
    use_dot_notation = True

    def __repr__(self):
        return '<Group %r %r>' % (self.first_name, self.last_name)

connection.register([Group])


collection = connection['pom'].group

print collection.Group.find_one({'name': u'admin'})
