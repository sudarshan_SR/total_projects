__author__ = 'Kunal Monga'

# import requirement
# We are creating all collections in single file.
# In this we have structure and validators 

from mongokit import Document
from app import app, db
from mongokit import ValidationError
from datetime import datetime

# Affiliates collection
class Affiliates(Document):
    __collection__ = 'affiliates'

    structure = {
        "first_name" : unicode,
        "last_name" : unicode,
        "email" : str,
        "user_name" : str,
        "password" : str,
        "phone" : str,
        "tax_id" : str,
        "paypal_account" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Affiliates %r>' % (self.first_name)

db.register([Affiliates])

# Classes starts here
# Main User collection
class Users(Document):
    __collection__ = 'users'

    structure = {
        "affiliate_id" : Affiliates,
        "affiliate_status" : int,
        "affiliates_token" : str,
        "first_name" : unicode,
        "last_name" : unicode,
        "insta_user_name" : unicode,
        "email" : str,
        "user_name" : str,
        "password" : str,
        "phone" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Users %r>' % (self.first_name)

db.register([Users])


# UserInsta collection
#status:0 >>stop
#status:1 >>start
#status:2 >>Temporary follow stop(1 to 3 days)
#status:3 >>Reconnect
#status:4 >>Temporary stop(1 to 3 days)
##########$############
#date_performed:time of performing an cycle
class UserInsta(Document):
    __collection__ = 'user_insta'

    structure = {
        "main_user_id" : Users,
        "full_name" : str,
        "profile_pic_url" : str,
        "profile_pic_id" : str,
        "following" : int,
        "follower_count" : int,
        "posts" : int,
        "user_id" : str,
        "user_name" : str,
        "password" : str,
        "email" : str,
        "proxy":unicode,
        "phone" : str,
        "is_private" : str,
        "is_verified" : str,
        "gender" : str,
        "status" : int,
        "date_performed" : datetime,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<UserInsta %r>' % (self.user_name)

db.register([UserInsta])


# Proxies collection
#status:0 >>runnable
#status:1 >>Blocked
#allotted_to >> username who is using it
#in_process_status >> currently getting allotted
class Proxies(Document):
    __collection__ = 'proxies'

    structure = {
        "proxy" : str,
        "status" : int,
        "in_process_status" : int,
        "allotted_to" : str,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Proxies %r>' % (self.proxy)

db.register([Proxies])

# Target collection
class Target(Document):
    __collection__ = 'target'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "full_name" : str,
        "profile_pic_url" : str,
        "user_id" : str,
        "user_name" : str,
        "followers" : str,
        "is_private" : str,
        "is_verified" : str,
        "type" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Target %r>' % (self.user_name)

db.register([Target])


# MyPost collection
class MyPost(Document):
    __collection__ = 'my_post'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "media_id" : str,
        "image" : str,
        "comment" : str,
        "like" : str,
        "type" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<MyPost %r>' % (self.image)

db.register([MyPost])




# Manage collection
class Manage(Document):
    __collection__ = 'manage'

    structure = {
        "name" : str,
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Users %r>' % (self.name)

db.register([Manage])



# Followers collection
class Followers(Document):
    __collection__ = 'followers'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "full_name" : str,
        "profile_pic_url" : str,
        "user_id" : str,
        "user_name" : str,
        "is_private" : str,
        "is_verified" : str,
        "status" : int,
        "sms" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Followers %r>' % (self.user_name)

db.register([Followers])


# Token collection
class Token(Document):
    __collection__ = 'token'

    structure = {
        "user_id" : Users,
        "token" : str,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Token %r>' % (self.token)

db.register([Token])

# Actions collection
class Actions(Document):
    __collection__ = 'actions'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "target_id":Target,
        "target_type":str,
        "action_type":str,
        "media":str,
        "post_id":str,
        "to_username":str,
        "to_userid":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Actions %r>' % (self.to_username)

db.register([Actions])

# Currently collection
# Currently =>status:
#   0=unfollowing
#   1=following
class Currently(Document):
    __collection__ = 'currently'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Currently %r>' % (self.status)

db.register([Currently])

# Limit Count collection
# Currently =>status:
#   0=active
#   1=un active
#   limit=this denotes how many followed
class Limit(Document):
    __collection__ = 'limit'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "status" : int,
        "count" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Currently %r>' % (self.status)

db.register([Currently])


# Followed collection
class Followed(Document):
    __collection__ = 'followed'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "target_id":str,
        "to_userid":str,
        "target_type":str,
        "to_username":str,
        "is_mute":int,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    default_values={'is_mute':1}

    use_dot_notation = True

    def __repr__(self):
        return '<Followed %r>' % (self.to_username)

db.register([Followed])

# Plans collection
class Plans(Document):
    __collection__ = 'plans'

    structure = {
        "plan_name":str,
        "plan_detail":str,
        "price":str,
        "status" : int,
        "speed" : str,
        "action_count" : str, 
        "max_follow" : str, 
        "max_like" : str, 
        "engagement" : str,
        "managed" : str,
        "plan_mute_allowed":int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Plans %r>' % (self.plan_name)

db.register([Plans])

# MyPlan collection
class MyPlan(Document):
    __collection__ = 'my_plan'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "plan_id": Plans,
        "plan_start_date": datetime,
        "pla_end_date": datetime,
        "status" : int,
        "email_status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<MyPlan %r>' % (self.plan_id)

db.register([MyPlan])


# Growth collection
# Growth =>gender values:
#   0=Everyone
#   1=Male
#   2=Female
class Growth(Document):
    __collection__ = 'growth'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "plan_id": Plans,
        "speed":str,
        "max_follow_limit":str,
        "follow_private":str,
        "gender":str,
        "profile_pic":str,
        "business_account":str,
        "min_post":str,
        "max_post":str,
        "min_following":str,
        "max_following":str,
        "min_follower":str,
        "max_follower":str,
        "status" : int,
        "mute_status":int,
        "date_created" : datetime,
        "date_updated" : datetime
    }

    default_values={'mute_status':1}

    use_dot_notation = True

    def __repr__(self):
        return '<Growth %r>' % (self.speed)

db.register([Growth])

# Unfollow collection
# Unfollow limit values:
#..................
# 0 = whitelist
# 1 = whitelist+100
# 2 = whitelist+200
# and so on...
#...................
# source values:
# 0 = Anyone
# 1 = People Stim followed
#...................
class Unfollow(Document):
    __collection__ = 'unfollow'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "plan_id": Plans,
        "speed":str,
        "limit":str,
        "source":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Unfollow %r>' % (self.plan_id)

db.register([Unfollow])

# Engagement collection
# Engagement source values:
# 0 = Anyone 
# 1 = People Stim followed
# 2 = People in Whitelist
# 3 = People not in Whitelist
class Engagement(Document):
    __collection__ = 'engagement'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "plan_id": Plans,
        "speed":str,
        "source":str,
        "min_like":str,
        "max_like":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Engagement %r>' % (self.plan_id)

db.register([Engagement])


# Sleep collection
class Sleep(Document):
    __collection__ = 'sleep'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "plan_id": Plans,
        "permission":str,
        "start_time":str,
        "duration":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Sleep %r>' % (self.plan_id)

db.register([Sleep])



# Liked collection
class Liked(Document):
    __collection__ = 'liked'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "media_id": str,
        "media_image": str,
        "media_comments": str,
        "media_likes": str,
        "to_user_id":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Sleep %r>' % (self.media_id)

db.register([Liked])



# Events collection
## event_type values:
#..................
# 0 => Instagram Account Added
# 1 => Account started Following
# 2 => Account started Un Following
# 3 => Account Started
# 4 => Account Stopped
# 5 => Account started Liking
# 6 => Your plan has expired
# 7 => temporarily blocked
# 8 => account needs to be Reconnected
#..................
class Events(Document):
    __collection__ = 'events'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "event_name":str,
        "event_detail":str,
        "event_type":str,
        "icon":str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Events %r>' % (self.event_name)

db.register([Events])

# Whitelist collection
class Whitelist(Document):
    __collection__ = 'whitelist'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "full_name" : str,
        "profile_pic_url" : str,
        "user_id" : str,
        "user_name" : str,
        "followers" : str,
        "is_private" : str,
        "is_verified" : str,
        "type" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime    }
    use_dot_notation = True

    def __repr__(self):
        return '<Whitelist %r>' % (self.full_name)

db.register([Whitelist])

# Blacklist collection
class Blacklist(Document):
    __collection__ = 'blacklist'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "keyword" :str,
        "language" :str,
        "full_name" : str,
        "profile_pic_url" : str,
        "user_id" : str,
        "user_name" : str,
        "followers" : str,
        "is_private" : str,
        "is_verified" : str,
        "type" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Blacklist %r>' % (self.full_name)

db.register([Blacklist])

# Orders collection
class Orders(Document):
    __collection__ = 'orders'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "plan_id" : Plans,
        "order_id" : str,
        "temp" : str,
        "payment_method" : str,
        "payment_id" : str,
        "token" : str,
        "payer_id" : str,
        "status" : str,
        "total_amount" : str,
        "comments" : str,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Orders %r>' % (self.status)

db.register([Orders])


# Message collection
class Message(Document):
    __collection__ = 'message'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "status" : int,
        "text" : str,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Message %r>' % (self.text)

db.register([Message])


# MessageTime collection
class MessageTime(Document):
    __collection__ = 'message_time'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "time_from" : str,
        "time_to" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Message %r>' % (self.status)

db.register([MessageTime])


# SMS collection
class SMS(Document):
    __collection__ = 'sms'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "message_id" : Message,
        "to_userid" : str,
        "message" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<sms %r>' % (self.status)

db.register([SMS])


# TickerCount collection
class TickerCount(Document):
    __collection__ = 'ticker_count'

    structure = {
        "count" : int,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<ticker_count %r>' % (self.count)

db.register([TickerCount])

# AccountEmails collection
class AccountEmails(Document):
    __collection__ = 'account_emails'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "soon_expire_mail_status" : int,
        "last_send_date":datetime,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<account_emails %r>' % (self.count)

db.register([AccountEmails])




# AffiliatesToken collection
class AffiliatesToken(Document):
    __collection__ = 'affiliates_token'

    structure = {
        "affiliate_id" : Affiliates,
        "token" : str,
        "clicks":int,
        "percentage":str,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<AffiliatesToken %r>' % (self.token)

db.register([AffiliatesToken])


# AffiliatesAddress collection
class AffiliatesAddress(Document):
    __collection__ = 'affiliates_address'

    structure = {
        "affiliate_id" : Affiliates,
        "address_line1" : str,
        "address_line2" : str,
        "city" : unicode,
        "state" : unicode,
        "country" : str,
        "loc_lat" : unicode,
        "loc_lon" : unicode,
        "zipcode" : int,
        "company" : str,
        "website" : str,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<AffiliatesAddress %r>' % (self.affiliates_id)

db.register([AffiliatesAddress])


# Coupon collection
class Coupon(Document):
    __collection__ = 'coupon'

    structure = {
        "code" : str,
        "percentage" : str,
        "status" : int,
        "description" : str,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<Coupon %r>' % (self.code)

db.register([Coupon])

# UserLog collection
class UserLog(Document):
    __collection__ = 'user_log'

    structure = {
        "main_user_id" : Users,
        "insta_user_id" : UserInsta,
        "action" : str,
        "description" : str,
        "response" : str,
        "status" : int,
        "date_created" : datetime,
        "date_updated" : datetime
    }
    use_dot_notation = True

    def __repr__(self):
        return '<UserLog %r>' % (self.first_name)

db.register([UserLog])