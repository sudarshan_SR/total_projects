__author__ = 'Kunal Monga'
# Import flask and template operators
from flask import Flask, render_template
from flask_mongokit import MongoKit
from flask_mail import Mail
from flask_cors import CORS, cross_origin
from flask import send_from_directory
import settings, os
# import imaplib
# Define the WSGI application from_object
app = Flask(__name__,static_url_path = "/file", static_folder = "app")
CORS(app)


app.config['MAIL_SERVER']=settings.MAIL_SERVER
app.config['MAIL_PORT'] = settings.MAIL_PORT
app.config['MAIL_USERNAME'] = settings.MAIL_USERNAME
app.config['MAIL_PASSWORD'] = settings.MAIL_PASSWORD
app.config['MAIL_USE_TLS'] = settings.MAIL_USE_TLS
# app.config['MAIL_USE_SSL'] = True

mail = Mail(app)
APP_ROOT = os.path.dirname(os.path.abspath(__file__))   # refers to application_top
APP_STATIC = os.path.join(APP_ROOT, 'mailer')
APP_STATIC = os.path.join(APP_ROOT, 'mailer')
APP_INSTA = os.path.join(APP_ROOT, 'insta/api/')
# cors = CORS(app, resources={r"/user/*": {"origins": "*"}})
# cors = CORS(app, resources={r"/product/*": {"origins": "*"}})

# Configurations
app.config.from_object(settings)

# Define the database object which is imported Here we are connecting to two diffrent databases mongo and mysql
db = MongoKit(app)
# dbsql = MySQL(app)
# Import email from server
# send file path 
# @app.route('/uploads/<filename>')
# def uploaded_file(filename):
#     return send_from_directory('uploads',
#                                filename)

# Sample HTTP error handling
# @app.afterrequest()
# def after_request(response):
#   response.headers.add('Access-Control-Allow-Origin', '*')
#   response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
#   response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
#   return response


@app.errorhandler(404)
def not_found(error):
    return 'Not Found'


@app.route("/")
def helloWorld():
  return "Hello, cross-origin-world!"
  
# Import a module / component using its blueprint handler variable (mod_auth)

from app.module.user.controllers import user_link as user_module
from app.module.product.controllers import product_link as product_module
from app.module.event.controllers import event_link as event_module
# Register blueprint(s)

app.register_blueprint(product_module)
app.register_blueprint(user_module)
app.register_blueprint(event_module)
# ..

