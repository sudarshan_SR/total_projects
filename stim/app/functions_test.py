__author__ = 'Kunal Monga'
'''
This file will be used across the project for calling functions that are used at various places
'''
from flask import Blueprint, request, jsonify


# Import the database object from the main app module
from app import db, constants, mail, APP_ROOT,APP_STATIC,sendMail, fetchDirect, fetchInsta
import json, os, random, time, csv,urllib,string
from random import randint,uniform
from app.model import model
from flask_mail import Message
from multiprocessing import Process
from InstagramAPI import InstagramAPI
import stripe
stripe.api_key = "sk_live_MXmGA5iYvgLiHc06U1pWmRG5"
from bson.objectid import ObjectId

def test():
	getUser = db.UserInsta.find_one({"_id":ObjectId('5bb12a050ea4a01d1cb22d58')})
	data = {"username": getUser.user_name ,"password": getUser.password}
	resultFol = fetchInsta.getFollowers(json.dumps(data))
	fetchFollowers = resultFol['search_result']
	return fetchFollowers

def follow(main_user_id, insta_user_id,data,followEach,getFinal,target,target_type):
	result={"status":0,
			"message":"Something Went Wrong"}
	i=1
	while i <= followEach:
		if not len(getFinal):
			if target_type=="hashtag":
				getFinal=HashtagFeed(data,target)
			if target_type=="location":
				getFinal=LocationFeed(data,target)
		followTo = random.choice(getFinal)
		checkToFollow = checkCanFollow(main_user_id, insta_user_id, data, followTo)
		getFinal.remove(followTo)
		print len(getFinal)
		if checkToFollow["canFollow"]:
			data["user_id"] = followTo['pk']
			print main_user_id, insta_user_id
			res = fetchInsta.follow((data))
			followReturn = res["status"]
			if followReturn:
				# Add followed
				resAddFoll = addFollowed(main_user_id,insta_user_id,followTo['from_target'],followTo['pk'],followTo['username'],"3")
				# Add Action for this
				resAddAct = addAction(main_user_id,insta_user_id,followTo['pk'],"follow",followTo['username'],"","",followTo['from_target'],"location")
				i=i+1
				result={"status":1,
				"message":"Success"}
			elif res["message"]=="FeedbackRequired":
				data["main_user_id"]=str(main_user_id)
				data["insta_user_id"]=str(insta_user_id)
				data["status"]=2
				p = Process(target=functionChangeInstaStatus, args=(data,))
				p.start()
				result={"status":0,
				"message":"FeedbackRequired"}
				break
	return result

def HashtagFeed(data,target):
	count = 0
	getFinalTaggers=[]
	for x in target["hashtag"]:
		count = count+1
		data["hashtag"]=x['user_name']
		res = fetchInsta.getHashtagFeed((data))
		if res['search_result']:
			for k in res['search_result']:
   				k['from_target'] = x['target_id']
		getTaggers = res['search_result']
		getFinalTaggers = getFinalTaggers + getTaggers
	return getFinalTaggers

def LocationFeed(data,target):
	count = 0
	getFinalTaggers=[]
	for x in target["location"]:
		count = count+1
		data["location"]=x['target_id']
		res = fetchInsta.getLocationFeed((data))
		if res['search_result']:
			for k in res['search_result']:
   				k['from_target'] = x['target_id']
		getLocData = res['search_result']
		getFinalLoc = getFinalLoc + getLocData
	return getFinalLoc

def startFollowing(main_user_id, insta_user_id, my_user_id, target, countFinal, max_follow ):
	content = {}
   	followEach = int(max_follow)/countFinal
   	getUser = db.UserInsta.find_one({"_id":insta_user_id})
   	data = {"username": getUser.user_name ,"password": getUser.password}
   	feedback=1
   	if len(target["account"]) > 0 and feedback:
   		count = 0
   		gotFollowers = []
   		followTo = ""
   		followed = 0
   		for x in target["account"]:
   			count = count+1
   			data["user_id"] = x['target_id']
   			resultFol = fetchInsta.getOtherFollowers((data))
   			# print resultFol
   			if resultFol['search_result']:
	   			for k in resultFol['search_result']:
	   				k['from_target'] = x['target_id']
			getAnyFollowers = resultFol['search_result']
			gotFollowers = gotFollowers + getAnyFollowers
   		i = 1
		content['account']=gotFollowers
		res=follow(main_user_id, insta_user_id,data,followEach,gotFollowers)
		if not res["status"] and res["message"]=='FeedbackRequired':
			feedback=0
		# while i <= followEach:
		# 	followTo = random.choice(gotFollowers)
		# 	checkToFollow = checkCanFollow(main_user_id, insta_user_id, data, followTo)
		# 	if checkToFollow["canFollow"]:
		# 		data["user_id"] = followTo['pk']
		# 		print main_user_id, insta_user_id
		# 		res = fetchInsta.follow((data))
		# 		followReturn = res["status"]
		# 		if followReturn:
		# 			# Add followed
		# 			resAddFoll = addFollowed(main_user_id,insta_user_id,followTo['from_target'],followTo['pk'],followTo['username'],'1')
		# 			# Add Action for this
		# 			resAddAct = addAction(main_user_id,insta_user_id,followTo['pk'],"follow",followTo['username'],"","",followTo['from_target'],"user")
		# 			i=i+1

	if len(target["hashtag"]) > 0 and feedback:
   		count = 0
   		getFinalTaggers = []
   		followTo = ""
   		followed = 0
   		getFinalTaggers=HashtagFeed(data,target)
   # 		for x in target["hashtag"]:
   # 			count = count+1
   # 			data["hashtag"]=x['user_name']
			# res = fetchInsta.getHashtagFeed((data))
			# if res['search_result']:
			# 	for k in res['search_result']:
	  #  				k['from_target'] = x['target_id']
   # 			getTaggers = res['search_result']
   # 			getFinalTaggers = getFinalTaggers + getTaggers
   		i = 1
		content['hashtag']=getFinalTaggers
		target_type="hashtag"
		res=follow(main_user_id, insta_user_id,data,followEach,getFinalTaggers,target,target_type)
		if not res["status"] and res["message"]=='FeedbackRequired':
			feedback=0
   # 		while i <= followEach:
			# followTo = random.choice(getFinalTaggers)
			# checkToFollow = checkCanFollow(main_user_id, insta_user_id, data, followTo)
			# if checkToFollow["canFollow"]:
			# 	data["user_id"] = followTo['pk']
			# 	print main_user_id, insta_user_id
			# 	res = fetchInsta.follow((data))
			# 	followReturn = res["status"]
			# 	if followReturn:
			# 		# Add followed
			# 		resAddFoll = addFollowed(main_user_id,insta_user_id,followTo['from_target'],followTo['pk'],followTo['username'],"2")
			# 		# Add Action for this
			# 		resAddAct = addAction(main_user_id,insta_user_id,followTo['pk'],"follow",followTo['username'],"","",followTo['from_target'],"tag")
			# 		i=i+1

	if len(target["location"]) > 0 and feedback:
   		count = 0
   		getFinalLoc = []
   		followTo = ""
   		followed = 0
   		getFinalLoc=LocationFeed(data,target)
   # 		for x in target["location"]:
   # 			count = count+1
   # 			data["location"]=x['target_id']
			# res = fetchInsta.getLocationFeed((data))
			# if res['search_result']:
			# 	for k in res['search_result']:
	  #  				k['from_target'] = x['target_id']
			# # return res
   # 			getLocData = res['search_result']
   # 			getFinalLoc = getFinalLoc + getLocData
   		i = 1
		content['location']=getFinalLoc
		target_type="location"
		res=follow(main_user_id, insta_user_id,data,followEach,getFinalLoc)
		# while i <= followEach:
		# 	followTo = random.choice(getFinalLoc)
		# 	checkToFollow = checkCanFollow(main_user_id, insta_user_id, data, followTo)
		# 	if checkToFollow["canFollow"]:
		# 		data["user_id"] = followTo['pk']
		# 		print main_user_id, insta_user_id
		# 		res = fetchInsta.follow((data))
		# 		followReturn = res["status"]
		# 		if followReturn:
		# 			# Add followed
		# 			resAddFoll = addFollowed(main_user_id,insta_user_id,followTo['from_target'],followTo['pk'],followTo['username'],"3")
		# 			# Add Action for this
		# 			resAddAct = addAction(main_user_id,insta_user_id,followTo['pk'],"follow",followTo['username'],"","",followTo['from_target'],"location")
		# 			i=i+1

   	return content 


# def checkSleeping(main_user_id, insta_user_id):
# 	sleeping = False
# 	fromOne = str(constants.DATE())+" "+"00:00:00"
# 	double = False
# 	checkIfSleep = db.Sleep.find_one({"main_user_id":main_user_id,"insta_user_id":insta_user_id})
# 	if checkIfSleep:
# 		duration = checkIfSleep.duration
# 		durationDiff = 24 - int(duration)

# 		fromOne = str(constants.DATE())+" "+str(checkIfSleep.start_time)+":00:00"
# 		fromTwo = str(constants.DATE())+" "+str(checkIfSleep.start_time)+":00:00"
# 		if (24 - int(checkIfSleep.start_time)) < int(duration):
# 			durationDateOne = 24 - int(checkIfSleep.start_time)
# 		else:
# 			durationDateOne = int(duration)

# 		if int(checkIfSleep.start_time) > durationDiff:
# 			print "in"
# 			double = True
# 			durationDateTwo = int(duration) - durationDateOne
# 			fromTwo = str(constants.DATE())+" "+str(checkIfSleep.start_time)+":00:00"
		
# 		# if checkIfSleep:
# 	 #   		fromOne = str(constants.DATE())+" "+str(checkIfSleep.start_time)+":00:00"
#    	else:
#    		fromOne = str(constants.DATE())+" "+"00:00:00"
#    		durationDateOne = 8
   	
#    	if constants.FORMATTED_TIME_CREATE(fromOne) < constants.FORMATTED_TIME() < constants.ADD_HOURS(fromOne,durationDateOne):
#    		sleeping = True
#    	if double == True and sleeping == False:
#    		if constants.FORMATTED_TIME_CREATE(fromTwo) < constants.FORMATTED_TIME() < constants.ADD_HOURS(fromTwo,durationDateTwo):
# 	   		sleeping = True
# 	return sleeping



def checkSleeping(main_user_id, insta_user_id):
	checkIfSleep = db.Sleep.find_one({"main_user_id":main_user_id,"insta_user_id":insta_user_id})
	curr=constants.FORMATTED_TIME().hour
	if checkIfSleep:
		if int(checkIfSleep.start_time)>15:
			diff=24-int(checkIfSleep.start_time)
			nextdayhour=int(checkIfSleep.duration)-diff
			endhour=nextdayhour
			if 15<curr<=23:
				maxm=24
				end=int(checkIfSleep.start_time)+int(checkIfSleep.duration)
				if end>=maxm: 
					if int(checkIfSleep.start_time)<=curr<maxm:
						return True
				else:
					if int(checkIfSleep.start_time)<=curr<end:
						return True
			else:
				if 0<=curr<endhour:
					return True
		else:
			endhour=int(checkIfSleep.start_time)+int(checkIfSleep.duration)
			if int(checkIfSleep.start_time)<=curr<endhour:
				return True
		# return int(checkIfSleep.start_time)<=curr<endhour
		return False
	else:
		if 0<=curr<=7 or curr==23:
			return True            
		return False

def checkCanFollow(main_user_id, insta_user_id, data, followTo):
	content = {
	"canFollow":True,
	"message":""
	}
	checkIfFollowed = db.Followed.find_one({"to_userid":str(followTo['pk']),"insta_user_id":ObjectId(insta_user_id)})
   	if checkIfFollowed:
   		content["canFollow"] = False
   		content["message"] = "Already Following"

   	if content["canFollow"] == True:
   		checkIfBlack = db.Blacklist.find_one({"user_id":str(followTo['pk']),"insta_user_id":ObjectId(insta_user_id),"status":1})
   		if checkIfBlack:
   			content["canFollow"] = False
   			content["message"] = "Black List"
	checkGrowth = db.Growth.find_one({"insta_user_id":ObjectId(insta_user_id),"main_user_id":ObjectId(main_user_id)})
	data['user_id']=followTo['pk']
	resultMedia = fetchInsta.getOtherUserInfo((data))
	time.sleep(uniform(1,2))
	if resultMedia['status']:
		if content["canFollow"] == True:
			if checkGrowth.follow_private=="0":
				if resultMedia['is_private']:
		   			content["canFollow"] = False
		   			content["message"] = "is_private"
		if content["canFollow"] == True:
			if checkGrowth.profile_pic=="1":
		   		if resultMedia['HasAnonymousProfilePicture']:
		   			content["canFollow"] = False
		   			content["message"] = "profile_pic"
		if content["canFollow"] == True:
			if checkGrowth.business_account=="1":
		   		if resultMedia['is_business']:
		   			content["canFollow"] = False
		   			content["message"] = "is_business"
		########## BELOW CODE IS WORKING  BUT RESTICTVE AS MIN MAX VALUE CAN BE A PROBLEM ####### 
		if content["canFollow"] == True:
			if int(checkGrowth.min_post):
				print resultMedia['media_count']
				if int(checkGrowth.min_post)>=resultMedia['media_count']:
					content["canFollow"] = False
					content["message"] = "media_count"
		if content["canFollow"] == True:
			if int(checkGrowth.max_post):
				if int(checkGrowth.max_post)<=resultMedia['media_count']:
					content["canFollow"] = False
					content["message"] = "media_count"
		if content["canFollow"] == True:
			if int(checkGrowth.min_following):
				print resultMedia['following_count']
				if int(checkGrowth.min_following)>=resultMedia['following_count']:
					content["canFollow"] = False
					content["message"] = "following_count"
		if content["canFollow"] == True:
			if int(checkGrowth.max_following):
				if int(checkGrowth.max_following)<=resultMedia['following_count']:
					content["canFollow"] = False
					content["message"] = "following_count"
		if content["canFollow"] == True:
			if int(checkGrowth.min_follower):
				print resultMedia['follower_count']
				if int(checkGrowth.min_follower)>=resultMedia['follower_count']:
					content["canFollow"] = False
					content["message"] = "follower_count"
		if content["canFollow"] == True:
			if int(checkGrowth.max_follower):
				if int(checkGrowth.max_follower)<=resultMedia['follower_count']:
					content["canFollow"] = False
					content["message"] = "follower_count"
	else:
		content["canFollow"] = False
		content["message"] = "Throttled or Something Went Wrong"
		time.sleep(uniform(5,6))
		########## =========================== ####### 

		########## BELOW CODE OLD WORK SAME AS ABOVE BUT NEW ONE IS BETTER ####### 
	# if content["canFollow"] == True:
	# 	if checkGrowth.follow_private=="0":
	# 		if followTo['is_private']:
	#    			content["canFollow"] = False
	#    			content["message"] = "is_private"
 #   	if content["canFollow"] == True:
	# 	if checkGrowth.profile_pic=="1":
	#    		if followTo['profile_pic_id'] is None:
	#    			content["canFollow"] = False
	#    			content["message"] = "profile_pic"

   	return content

def startUnfollowing(main_user_id, insta_user_id, my_user_id, max_count):
	content = {}
   	getUser = db.UserInsta.find_one({"_id":insta_user_id})
   	try:
   		loginData = {"username": getUser.user_name ,"password": getUser.password}
		data=[]
		check_max_count = max_count
	   	getWhomToUnfollow = db.Unfollow.find_one({"main_user_id":main_user_id,"insta_user_id":insta_user_id})
	   	if getWhomToUnfollow and int(getWhomToUnfollow.source):
		   	fetchFollowed = list(db.Followed.find({"main_user_id":main_user_id,"insta_user_id":insta_user_id,"status":1}))
		   	data=[{"to_username":x['to_username'],"to_userid":x['to_userid']} for x in fetchFollowed]
		   	count=len(fetchFollowed)
	   	elif getWhomToUnfollow and not int(getWhomToUnfollow.source):
	   		resultfetch=fetchInsta.getFollowings(loginData)
	   		resFollow=resultfetch['search_result']
	   		data=[{"to_username":x['username'],"to_userid":x['pk']} for x in resFollow]
	   		count=resultfetch['count']
	   	if count < check_max_count:
	   		max_count=count
   		if max_count:
	   		i = 1
   		else:
   			i = 0
   		fetchFollowed = data
	   	while i <= max_count:
	   		if len(fetchFollowed):
		   		randUser = random.choice(fetchFollowed)
		   		checkIfWhite = db.Whitelist.find_one({"user_id":str(randUser["to_userid"]),"main_user_id":main_user_id,"insta_user_id":insta_user_id,"status":1})
		   		if checkIfWhite:
		   			fetchFollowed.remove(randUser)
		   		else:
		   			checkstillfollows = db.Followed.find_one({"to_userid":str(randUser["to_userid"]),"main_user_id":main_user_id,"insta_user_id":insta_user_id,"status":1})
			   		if checkstillfollows or not int(getWhomToUnfollow.source):	
			   			loginData["user_id"]=str(randUser["to_userid"])
						res = fetchInsta.unfollow((loginData))
						resmsg = res["message"]
						res = res["status"]
			   			if res:
							# Add unfollowed
							resAddFoll = updateFollowed(main_user_id,insta_user_id,str(randUser["to_userid"]))
							# Add Action for this
							resAddAct = addAction(main_user_id,insta_user_id,str(randUser["to_userid"]),"unfollow",randUser["to_username"],"","","","")
							i=i+1
						elif res==0 and resmsg =="Requested resource does not exist.":
							resAddFoll = updateFollowed(main_user_id,insta_user_id,str(randUser["to_userid"]))
	   		else:
	   			resCurChange = changeCurrently(main_user_id,insta_user_id,1)
	   			break
	except Exception as e:
   		print e
   	return content 




def startLiking(main_user_id, insta_user_id, totalHits, minLike, maxLike, source ,sourceData):
	content = {}
	randMedia={}
   	getUser = db.UserInsta.find_one({"_id":insta_user_id})
   	data = {"username": getUser.user_name ,"password": getUser.password}
   	i=1
   	try:
   		checker=list(db.Liked.find({"insta_user_id":insta_user_id}))
   		
   		while i <= totalHits:
	   		randUser = random.choice(sourceData)
	   		getFeed = []
	   		data["user_id"] = randUser["userid"]
	   		res = fetchInsta.getOtherMedia((data))
	   		getFeed=res["search_result"]
	   		existFeed=True
	   		enterCount = len(getFeed)
	   		start=1
	   		while existFeed and start <= enterCount:
	   			start=start+1
	   			randMedia = random.choice(getFeed)
	   			# check=db.Liked.find_one({"media_id":str(randMedia['pk']),"insta_user_id":insta_user_id})
				check=[x  for x in checker if str(x['media_id'])==str(randMedia['pk'])]
				if len(check):
					pass
				else:
					if int(randMedia['like_count']) > int(minLike):
						existFeed=False
						data["media_id"]=str(randMedia['pk'])
						res = fetchInsta.like((data))
						try:
							resAddAct = addAction(main_user_id,insta_user_id,randUser["userid"],"like",randUser["username"],str(randMedia['image_versions2']['candidates'][1]['url']),str(randMedia['pk']),"","")
							resAddLike=addLike(main_user_id, insta_user_id,str(randUser["userid"]),str(randMedia['pk']),str(randMedia['like_count']),str(randMedia['comment_count']),str(randMedia['image_versions2']['candidates'][1]['url']))
		   				except Exception as e:
							print e
		   				i=i+1
   	except Exception as e:
   		print e
   	return randMedia 



def addFollowed(main_user_id,insta_user_id,from_target,to_userid,To_username,target_type):
	res = True
	try:
		addFollowed = db.Followed()
		addFollowed.main_user_id = main_user_id
		addFollowed.insta_user_id = insta_user_id
		addFollowed.target_id = str(from_target)
		addFollowed.to_userid = str(to_userid)
		addFollowed.target_type = str(target_type)
		addFollowed.to_username = str(To_username)
		addFollowed.status = 1
		addFollowed.date_created = constants.FORMATTED_TIME()
		addFollowed.date_updated = constants.FORMATTED_TIME()
		addFollowed.save()
	except Exception as e:
		res=False
	
	return res

def updateFollowed(main_user_id,insta_user_id,to_userid):
	res = True
	try:
		fetch = db.Followed.find_one({"insta_user_id":ObjectId(insta_user_id),"to_userid":str(to_userid),"status":1})
		if fetch:
			updateData={
						"status":int(0)
				}
			updateGrowth=db.Followed.find_and_modify({"_id":ObjectId(fetch._id)},
													{
														"$set":updateData
														})
		else:
			pass
	except Exception as e:
		res=False
	
	return res

def addLike(main_user_id,insta_user_id,to_userid,media_id,like,comment,image):
	res = True
	try:
		adding = db.Liked()
		adding.main_user_id = main_user_id
		adding.insta_user_id = insta_user_id
		adding.to_user_id = to_userid
		adding.media_id = str(media_id)
		adding.media_image = str(image)
		adding.media_comments = str(comment)
		adding.media_likes = str(like)
		adding.status = 1
		adding.date_created = constants.FORMATTED_TIME()
		adding.date_updated = constants.FORMATTED_TIME()
		adding.save()
	except Exception as e:
		print e
		res=False
	
	return res

def addAction(main_user_id,insta_user_id,to_userid,action_type,to_username,media,post_id,from_target="",from_target_type=""):
	content={
				"status":1,
				"message":"Success"
				}
	try:
		time.sleep(randint(3,5))
		checkuser=db.Actions.find_one({"insta_user_id":insta_user_id,"action_type":action_type,"to_userid":to_userid})
		if checkuser:
			resAddFoll = updateFollowed(main_user_id,insta_user_id,str(to_userid))
			content={
				"status":1,
				"message":"Action Already Exist."
				}
		else:
			actObj = db.Actions()
			actObj.main_user_id = main_user_id
			actObj.insta_user_id = insta_user_id
			actObj.target_id = str(from_target)
			actObj.target_type = str(from_target_type)
			actObj.action_type = action_type
			actObj.media = media
			actObj.post_id = post_id
			actObj.to_username = str(to_username)
			actObj.to_userid = str(to_userid)
			actObj.status = 1
			actObj.date_created = constants.FORMATTED_TIME()
			actObj.date_updated = constants.FORMATTED_TIME()
			actObj.save()
			content={
			"status":1,
			"message":"Success"
			}
	except Exception as e:
		print e
		content={
		"status":0,
		"message":str(e)
		}
	
	return content



def createCharge(token,amount,order_id,email):
	content={
				"status":1,
				"message":"Success"
				}
	try:
		charge = stripe.Charge.create(
		    amount=amount,
		    currency='usd',
		    description='Payment For Plan',
		    source=token,
		    statement_descriptor='Monthly Charge',
		    metadata={'order_id': order_id},
		    receipt_email=email,
		)
	except Exception as e:
		content={
				"status":0,
				"message":"Failed"
				}
		charge = str(e)
	content["charge"]=charge
	print charge
	return content




def addMyEvent(main_user_id,insta_user_id,event_type,event_name,event_detail,event_icon):
	res=True
	content={
				"status":1,
				"message":""
				}
	try:
		checkuser=db.Events.find({"insta_user_id":ObjectId(str(insta_user_id)),"main_user_id":ObjectId(str(main_user_id))}).sort("date_created",-1)
		if checkuser.count()>=1 and checkuser[0].event_type == str(event_type):
			content={
				"status":1,
				"message":"Event Already Exist."
				}
		else:
			eventObj = db.Events()
			eventObj.main_user_id = ObjectId(str(main_user_id))
			eventObj.insta_user_id = ObjectId(str(insta_user_id))
			eventObj.event_name = str(event_name)
			eventObj.event_detail = str(event_detail)
			eventObj.icon = str(event_icon)
			eventObj.event_type = str(event_type)
			eventObj.status = 1
			eventObj.date_created = constants.FORMATTED_TIME()
			eventObj.date_updated = constants.FORMATTED_TIME()
			eventObj.save()
	except Exception as e:
		print content
		res=False
	return res


def changeCurrently(main_user_id,insta_user_id,status):
	res=True
	content={
				"status":1,
				"message":""
				}
	try:
		checkuser = db.Currently.find_one({"insta_user_id":ObjectId(str(insta_user_id)),"main_user_id":ObjectId(str(main_user_id))})
		if checkuser:
			updateData={
					"status":int(status)
			}
			updateNow=db.Currently.find_and_modify({"_id":checkuser._id},
												{
													"$set":updateData
													})
			print "here"
			content={
				"status":1,
				"message":"Updated."
				}
		else:
			addCurr = db.Currently()
			addCurr.main_user_id = ObjectId(str(main_user_id))
			addCurr.insta_user_id = ObjectId(str(insta_user_id))
			addCurr.status = int(1)
			addCurr.date_created = constants.FORMATTED_TIME()
			addCurr.date_updated = constants.FORMATTED_TIME()
			addCurr.save()
			content={
				"status":1,
				"message":"Created."
				}
	except Exception as e:
		print content
		res=False
	return res




def addSMS(main_user_id,insta_user_id,message_id,to_userid,message):
	res=True
	content={
				"status":1,
				"message":""
				}
	try:
		eventObj = db.SMS()
		eventObj.main_user_id = ObjectId(str(main_user_id))
		eventObj.insta_user_id = ObjectId(str(insta_user_id))
		eventObj.message_id =  ObjectId(str(message_id))
		eventObj.to_userid = str(to_userid)
		eventObj.message = str(message)
		eventObj.status = 1
		eventObj.date_created = constants.FORMATTED_TIME()
		eventObj.date_updated = constants.FORMATTED_TIME()
		eventObj.save()
	except Exception as e:
		print content
		res=False
	return res



def getFollowings(insta_id):
	data = {}
	data["insta_user_id"] = insta_id
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getUser = db.UserInsta.find_one({"_id":ObjectId(data['insta_user_id'])})
	if getUser:
		data["username"]=getUser.user_name
		data["password"]=getUser.password
		result = fetchInsta.countUserFollowings((data))
	   	resultfetch = result['search_result']
		updateData={
					"following":result['count']
			}
		updateGrowth=db.UserInsta.find_and_modify({"_id":ObjectId(str(data["insta_user_id"]))},
												{
													"$set":updateData
													})
	content['result']=result
	return content



def checkPlan(insta_id):
	data = {}
	data["insta_user_id"] = insta_id
	result = []
	planStatus = "Expired"
	planName = "Expired"
	planEnd = ""
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	try:
		myPlan = db.MyPlan.find({
						"insta_user_id":ObjectId(data["insta_user_id"]),
						# "status":1,
						})
		if myPlan.count() > 0:
			planEnd = str(constants.getDATE(myPlan[0].pla_end_date))
			if myPlan[0].pla_end_date > constants.FORMATTED_TIME():
				planStatus = "Active"
		plan = db.Plans.find_one({
						"_id":ObjectId(str(myPlan[0].plan_id)),
						"status":1,
						})
		planName = plan.plan_name
		if planName!="Free Trial":
			planName=planName+""
		if myPlan[0].status==0:
			planName = "Expired"
	except Exception as e:
		print e
		raise
	
	content['result']=result
	content['planStatus']=planStatus
	content['planName']=planName
	content['planEnd']=planEnd
	return content




def instaUser(data):
	data["insta_user_id"] = insta_id
	result = []
	planStatus = "Expired"
	planName = "Expired"
	planEnd = ""
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	try:
		if data["Iid"] == "" :
			getdata =db.UserInsta.find({"main_user_id":ObjectId(str(data['_id']))})
			if getdata:
				for x in getdata:
					dictn["name"]=x.user_name
					dictn["pic"]=x.profile_pic_url
					dictn["date"]=str(x.date_created.date())
					dictn["_id"]=str(x._id)
					dictn["status"]=str(x.status)
					dictn["index"]=str(n)
					n+=1
					checkPlan=checkPlan(x._id)
					dictn["pla_end_date"]=str(checkPlan["planEnd"])
					dictn['plan_name']=str(checkPlan["planName"])
					result.append(dictn.copy())
			content['result'] = result
		else:
			getdata =db.UserInsta.find_one({"_id":ObjectId(str(data["Iid"]))})
			if getdata:
				dictn["name"]=getdata.user_name
				dictn["pic"]=getdata.profile_pic_url
				dictn["date"]=str(getdata.date_created.date())
				checkPlan = checkPlan(x._id)
				dictn["pla_end_date"]=str(checkPlan["planEnd"])
				dictn['plan_name']=str(checkPlan["planName"])
			content['result'] = dictn
	except Exception as e:
		print e
		raise
	content['result']=result
	content['planStatus']=planStatus
	content['planName']=planName
	content['planEnd']=planEnd
	return content

# pass data in the form of dictionaries in list or in dictionary
# note:the keys() of dictionaries should be the same as the field names 
def createCsvFile(fieldnames,data,mycsv):
	try:
		with open('media/'+mycsv, 'w') as f:
		    # fieldnames = ['colomn1','colomn2','colomn3']
		    thewriter = csv.DictWriter(f, fieldnames=fieldnames)
		    thewriter.writeheader()
		    for d in data:
			    dictn=d
			    thewriter.writerow(dictn)
	except Exception as e:
		print e
		return False
	return True

def removeFromDictionary(dct,entriesToRemove):
    for k in entriesToRemove:
        if k in dct:
            # print k
            dct.pop(k, None)
    return dict(dct)

def functionChangeInstaStatus(data):
	# data = request.json
	content = {"status":1,
				"message":"Success"}
	try:
		instaObj =db.UserInsta.find_one({
										"main_user_id":ObjectId(str(data['main_user_id'])),
										"_id":ObjectId(str(data['insta_user_id']))
										})
		if instaObj:
			updateData ={
							"status":int(data["status"]),
							"date_updated":constants.FORMATTED_TIME()
							}
			UserInstaObj=db.UserInsta.find_and_modify({"main_user_id":ObjectId(str(data["main_user_id"])),"_id":ObjectId(str(data["insta_user_id"]))},
													{
													"$set":updateData
													})
			user=db.Users.find_one({"_id":ObjectId(str(data["main_user_id"]))})
			if int(data["status"])==2:
				#add mail
				subject = "Kibi - Support"
				sock = urllib.urlopen(os.path.join(APP_STATIC, 'account_wait.html'))
				html = sock.read()
				sock.close()
				html = string.replace(html, "[USERNAME]", str(str(user.first_name)+" "+str(user.last_name)).strip())
				html = string.replace(html, "[INSTANAME]", str(instaObj.user_name).strip())
				html = string.replace(html, "[DAYS]", str("1-3").strip())
				res = sendMail.sendEmail("support@kibisocial.com",str(user.email),subject,html,"")
				#sent mail			
				# Add Event Here
				resEvent=addMyEvent(user._id,data['insta_user_id'],"2","Account started Un-Following","Your account has been temporarily blocked by Instagram. This could be caused by hashtag spam or by performing too many manual actions. Wait 1-3 days then restart your account.","fal fa-pause fa-started custom-fa fa-pause")
				# Event Added
				resCurCurrently = changeCurrently(str(data['main_user_id']),str(data['insta_user_id']),0)
			elif int(data["status"])==3:
				if "proxy" in instaObj:
					proxy=instaObj.proxy
					updateData1 ={
							"status":int(0),
							"date_updated":constants.FORMATTED_TIME()
							}
					UserInstaObj=db.Proxies.find_and_modify({"proxy":proxy},
														{
														"$set":updateData1
														})
				#add mail
				sock = urllib.urlopen(os.path.join(APP_STATIC, 'account_reconnect.html'))
				html = sock.read()
				sock.close()
				html = string.replace(html, "[USERNAME]", str(str(user.first_name)+" "+str(user.last_name)).strip())
				html = string.replace(html, "[INSTANAME]", str(instaObj.user_name).strip())
				subject = "Kibi - Support"
				# html= "Your account needs to be Reconnected. Please reconnect your account from dashboard now, so your account doesn't stop running."
				res = sendMail.sendEmail("support@kibisocial.com",str(user.email),subject,html,"")
				#sent mail			
				# Add Event Here
				resEvent=addMyEvent(user._id,data['insta_user_id'],"8","Account Stopped","Your account needs to be Reconnected. Please reconnect your account from dashboard now, so your account doesn't stop running.","fal fa-pause fa-started custom-fa fa-pause")
				# Event Added
			elif int(data["status"])==4:
				#add mail
				sock = urllib.urlopen(os.path.join(APP_STATIC, 'account_follow_wait.html'))
				html = sock.read()
				sock.close()
				html = string.replace(html, "[USERNAME]", str(str(user.first_name)+" "+str(user.last_name)).strip())
				html = string.replace(html, "[INSTANAME]", str(instaObj.user_name).strip())
				subject = "Kibi - Support"
				# html= "Your account needs to be Reconnected. Please reconnect your account from dashboard now, so your account doesn't stop running."
				res = sendMail.sendEmail("support@kibisocial.com",str(user.email),subject,html,"")
				#sent mail			
				# Add Event Here
				resEvent=addMyEvent(user._id,data['insta_user_id'],"7","Account Stopped","Your account will not follow for some time this could be due to too many manual actions.","fas fa-arrow-down arrowup")
				# Event Added

		else:
			content = {"status":0,
						"message":"Invalid data"}
	except Exception as e:
		print e
		content = {"status":0,
					"message":"Something Went Wrong, Please Try after Some Time"}
	return content