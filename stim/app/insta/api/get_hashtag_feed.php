<?php
include 'includeExtended.php';
/////// CONFIG ///////
$server_array = $_SERVER['argv'][1];
$data  = (array) json_decode($server_array);
$username = $data['username'];
$password = $data['password'];
$hashtag = $data['hashtag'];
// $username = 'Kibisocial';
// $password = 'Peru2011##';
$verification_method = 0; 
$result=[
		"status"=>0,
		"message"=>"Something went wrong"
	];

function readln( $prompt ) {
	if ( PHP_OS === 'WINNT' ) {
		echo "$prompt ";

		return trim( (string) stream_get_line( STDIN, 6, "\n" ) );
	}

	return trim( (string) readline( "$prompt " ) );
}

try {
	$loginResponse = $ig->changeUser($username,$password);
	$count=1;
	$searchArr = array();
	$count=0;
	$maxId = null;
    $searchRes = $ig->hashtag->getFeed($hashtag,$ig->uuid,$maxId);
    foreach ($searchRes->getItems() as $item) {
    	$tagUser = $item->getUser();
    	$count=$count+1;
		$pushArr= array('pk' => $tagUser->getPk(),'username' => $tagUser->getUsername(),'is_private'=>$tagUser->isIsPrivate(),'profile_pic_url'=>$tagUser->getProfilePicUrl(),'profile_pic_id'=>$tagUser->getProfilePicId());
		array_push($searchArr, $pushArr);
	}
	$maxId = $searchRes->getNextMaxId();
	sleep(3);
	$result['next_max_id'] = $searchRes->getNextMaxId();
	$result['search_result'] = $searchArr;
	$result['count'] = $count;
	$result['status'] = 1;
	$result['message'] = "Success";
	echo json_encode($result);
} catch ( Exception $exception ) {
	$result["message"] = $exception->getMessage();
	$result["status"] = 0;
	echo json_encode($result);
}