<?php
set_time_limit(0);
date_default_timezone_set('UTC');
require __DIR__.'/../vendor/autoload.php';
use InstagramAPI\Instagram;
use InstagramAPI\Response\LoginResponse;
/////// CONFIG ///////
$debug = true;
$truncatedDebug = false;
//////////////////////

// \InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
$ig = new \InstagramAPI\Instagram(true, true, [
            'storage'    => 'mysql',
            'dbhost'     => 'localhost',
            'dbname'     => 'stim',
            'dbusername' => 'root',
            'dbpassword' => 'root',
        ]);
