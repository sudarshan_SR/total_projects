<?php
include 'includeExtended.php';
/////// CONFIG ///////
$server_array = $_SERVER['argv'][1];
$data  = (array) json_decode($server_array);
$username = $data['username'];
$password = $data['password'];
$userId = $data['userId'];
// $username = "sudarshan_dudhe";
// $password = "12010220";
// $userId = "1429711316";
$verification_method = 0; 
$result=[
		"status"=>0,
		"message"=>"Something went wrong"
	];

function readln( $prompt ) {
	if ( PHP_OS === 'WINNT' ) {
		echo "$prompt ";

		return trim( (string) stream_get_line( STDIN, 6, "\n" ) );
	}

	return trim( (string) readline( "$prompt " ) );
}

try {
	$loginResponse = $ig->changeUser($username,$password );
	$fetchDetails = $ig->people->getInfoById($userId);
			// print_r($fetchDetails);
			
	if($fetchDetails instanceof InstagramAPI\Response\UserInfoResponse){
		$currentUser=$fetchDetails->getUser();
		$result["status"] = 1;
		$result["message"] = "Success";
		$result["pk"] = $currentUser->getPk();
		$result["email"] = $currentUser->getEmail();
		$result["user_name"] = $currentUser->getUsername();
		$result["full_name"] = $currentUser->getFullName();
		$result["media_count"] = $currentUser->getMediaCount();
		$result["follower_count"] = $currentUser->getFollowerCount();
		$result["following_count"] = $currentUser->getFollowingCount();
		$result["phone_number"] = $currentUser->getPhoneNumber();
		$result["profile_pic_url"] = $currentUser->getProfilePicUrl();
		$result["profile_pic_id"] = $currentUser->getProfilePicId();
		$result["is_private"] = $currentUser->isIsPrivate();
		$result["is_verified"] = $currentUser->isIsVerified();
	}
	echo json_encode($result);
} catch ( Exception $exception ) {
	$result["message"] = $exception->getMessage();
	$result["status"] = 0;
	if($exception instanceof InstagramAPI\Exception\LoginRequiredException){
		$result["status"] = 3;
		$result["user_name"] = $username;
		$result["message"] = "LoginRequired";
	}
	if ($exception instanceof InstagramAPI\Exception\ChallengeRequiredException) {
		$result["status"] = 3;
		$result["user_name"] = $username;
		$result["message"] = "ChallengeRequired";
	}

	echo json_encode($result);
}