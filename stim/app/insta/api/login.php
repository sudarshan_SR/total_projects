<?php
include 'includeExtended.php';
include 'connect.php';
/////// CONFIG ///////
$server_array = $_SERVER['argv'][1];
$data  = (array) json_decode($server_array);
$username = $data['username'];
$password = $data['password'];
$delete = $data['delete'];
$verification_method = 0; 

if ($delete=="1"){
	$sql = "DELETE FROM user_sessions WHERE username ='".$username."'";
	$res = $conn->query($sql);
}
$result=[
		"status"=>0,
		"message"=>"Something went wrong"
	];

function readln( $prompt ) {
	if ( PHP_OS === 'WINNT' ) {
		echo "$prompt ";

		return trim( (string) stream_get_line( STDIN, 6, "\n" ) );
	}

	return trim( (string) readline( "$prompt " ) );
}

try {
	$loginResponse = $ig->login( $username, $password );
	sleep(2);
	if (!is_null($loginResponse) && $loginResponse->isTwoFactorRequired()) {
		$result["status"] = 2;
		$result["message"] = "twofactor_required";
		$result["identifier"] = $loginResponse->getTwoFactorInfo()->getTwoFactorIdentifier();
		$result["contact_point"] = $loginResponse->getTwoFactorInfo()->getObfuscatedPhoneNumber();
		echo json_encode($result);

	}else{
		$fetchDetails = $ig->people->getSelfInfo();
			// print_r($fetchDetails);
			
		if($fetchDetails instanceof InstagramAPI\Response\UserInfoResponse){
			$currentUser=$fetchDetails->getUser();
			$result["status"] = 1;
			$result["message"] = "login_success";
			$result["user_id"] = $currentUser->getPk();
			echo json_encode($result);
		}
	}
	
} catch ( Exception $exception ) {
	$response = $exception->getResponse();
	if ($exception instanceof InstagramAPI\Exception\ChallengeRequiredException) {
		sleep(3);
		$checkApiPath = substr( $response->getChallenge()->getApiPath(), 1);
		$customResponse = $ig->request($checkApiPath)
									->setNeedsAuth(false)
									->addPost('choice', $verification_method)
									->addPost('_uuid', $ig->uuid)
									->addPost('guid', $ig->uuid)
									->addPost('device_id', $ig->device_id)
									->addPost('_uid', $ig->account_id)
									->addPost('_csrftoken', $ig->client->getToken())
									->getDecodedResponse();
		if (is_array($customResponse)) {
           if ($customResponse["status"]=='fail'){
				$result["contact_point"]='fail';
				$result["status"] = 4;
			}else{
				$result["contact_point"]=$customResponse["step_data"]["contact_point"];
				$result["status"] = 3;
			}
        }
		$result["message"] = 'challenge_required';
		$result["url"] = $checkApiPath;
		
		echo json_encode($result);
	}else{
		$result["message"] = $exception->getMessage();
		$result["status"] = 0;
		if ($exception instanceof InstagramAPI\Exception\NetworkException) {
			$result["status"] = 0;
			$result["message"] = "NetworkException";
		}
		echo json_encode($result);
	}
}