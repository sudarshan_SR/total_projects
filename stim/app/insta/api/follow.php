<?php
include 'includeExtended.php';
/////// CONFIG ///////
$server_array = $_SERVER['argv'][1];
$data  = (array) json_decode($server_array);
$username = $data['username'];
$password = $data['password'];
$ifMute=$data['mute'];
$user_id = $data['user_id'];
// $username = 'Kibisocial';
// $password = 'Peru2011##';
$verification_method = 0; 
$result=[
		"status"=>0,
		"message"=>"Something went wrong"
	];

function readln( $prompt ) {
	if ( PHP_OS === 'WINNT' ) {
		echo "$prompt ";

		return trim( (string) stream_get_line( STDIN, 6, "\n" ) );
	}

	return trim( (string) readline( "$prompt " ) );
}

try {
	$loginResponse = $ig->changeUser($username,$password);
	$actRes = $ig->people->follow($user_id);
	
	sleep(2);
	$getRes = $actRes->getFriendshipStatus();
	if($getRes->getOutgoingRequest()==1 or $getRes->getFollowing()==1){
		sleep(1);
		if($ifMute){
			$muteRes = $ig->people->muteUserMedia($user_id,'all');
		}
		$result["status"]=1;
		$result["message"]="success";
	}else{
		$result["status"]=0;
		$result["message"]="Already Following";
	}
	echo json_encode($result);
} catch ( Exception $exception ) {
	if ($exception instanceof InstagramAPI\Exception\FeedbackRequiredException) {
		$result["message"] = "FeedbackRequired";
		$result["status"] = 0;
		echo json_encode($result);
	}
	else{
		$result["message"] = $exception->getMessage();
		$result["status"] = 0;
		echo json_encode($result);
	}
}