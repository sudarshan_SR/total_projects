<?php
include 'includeExtended.php';
/////// CONFIG ///////
$server_array = $_SERVER['argv'][1];
$data  = (array) json_decode($server_array);
$username = $data['username'];
$password = $data['password'];
$text = $data['text'];
$type = $data['type'];
// $username = "kunal@simplifyreality.com";
// $password = "kunalmonga478";
// $main_user_id = $data['main_user_id'];
$verification_method = 0; 
$result=[
		"status"=>0,
		"message"=>"Something went wrong"
	];

function readln( $prompt ) {
	if ( PHP_OS === 'WINNT' ) {
		echo "$prompt ";

		return trim( (string) stream_get_line( STDIN, 6, "\n" ) );
	}

	return trim( (string) readline( "$prompt " ) );
}

try {
	$loginResponse = $ig->changeUser($username,$password);
	if($type=="user"){
		$searchRes = $ig->people->search($text);
	}else if($type=="tags"){
		$searchRes = $ig->hashtag->search($text);
	}else if($type=="location"){
		$searchRes = $ig->location->findPlaces($text);
	}else{
		$searchRes = $ig->people->search($text);
	}

	$searchArr = array();
	if($searchRes instanceof InstagramAPI\Response\SearchUserResponse){
		foreach ($searchRes->getUsers() as $item) {
			$pushArr= array('is_private' => $item->getIsPrivate(),'is_verified' => $item->getIsVerified(),'username' => $item->getUsername(),'pk' => $item->getPk(),'profile_pic_url' => $item->getProfilePicUrl(),'follower_count' => $item->getFollowerCount(),'full_name' => $item->getFullName() );
			array_push($searchArr, $pushArr);
			 
		}
		$result["status"] = 1;
		$result["message"] = "Success";

	}
	if($searchRes instanceof InstagramAPI\Response\SearchTagResponse){
		foreach ($searchRes->getResults() as $item) {
			$pushArr= array('id' => $item->getId(),'name' => $item->getName(),'media_count' => $item->getMediaCount());
			array_push($searchArr, $pushArr);
			 
		}
		$result["status"] = 1;
		$result["message"] = "Success";

	}
	if($searchRes instanceof InstagramAPI\Response\FBLocationResponse){
		foreach ($searchRes->getItems() as $item) {
			$pushArr= array('location' => $item->getLocation(),'title'=>$item->getTitle(),'subtitle'=>$item->getSubtitle());
			array_push($searchArr, $pushArr);
			 
		}
		$result["status"] = 1;
		$result["message"] = "Success";

	}
	$result['search_result'] = $searchArr;
	echo json_encode($result);
} catch ( Exception $exception ) {
	$result["message"] = $exception->getMessage();
	$result["status"] = 0;
	echo json_encode($result);
}