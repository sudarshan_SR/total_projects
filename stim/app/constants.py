'''
This file will be used across the project for using and defining constants, getting time and some validation and conversion functions
'''
import datetime
import calendar
import time
import base64
import pytz


unaware = datetime.datetime(2011, 8, 15, 8, 15, 12, 0)
aware = datetime.datetime(2011, 8, 15, 8, 15, 12, 0, pytz.UTC)
now_aware = pytz.utc.localize(unaware)
assert aware == now_aware


def geoFenceArea():
    return 600

def getBusinessId():
    return 1
    
def DATE_TIME():
    return datetime.datetime.now(pytz.timezone('America/Chicago'))

def NEXT_DATE_TIME():
    return DATE_TIME() + datetime.timedelta(days=1)

def formatEmailTime(dateTime):
    # print "dateTime"
#    return dateTime
    try:
        return datetime.datetime.strftime(datetime.datetime.strptime(str(dateTime), '%a, %d %b %Y %H:%M:%S'),"%Y-%m-%d %H:%M:%S")
    except Exception:
        try:
            splitDate = dateTime.split()
            dateTime = splitDate[0] + ' ' + splitDate[1] + ' ' + splitDate[2] + ' ' + splitDate[3]
            return datetime.datetime.strftime(datetime.datetime.strptime(str(dateTime), '%d %b %Y %H:%M:%S'),"%Y-%m-%d %H:%M:%S")
        except Exception:
            return FORMATTED_TIME()

def FORMATTED_TIME():
    return datetime.datetime.strptime(str(DATE_TIME()).split('.')[0], '%Y-%m-%d %H:%M:%S')

def FORMATTED_TIME_CREATE(getDate):
    return datetime.datetime.strptime(str(getDate), '%Y-%m-%d %H:%M:%S')

def FORMATTED_DATE_ONLY(getDate):
    return datetime.datetime.strptime(str(getDate), '%Y-%m-%d %H:%M:%S')


def TIME():    
    return FORMATTED_TIME().time()

def getHour():    
    return FORMATTED_TIME().hour

def DATE():
    return FORMATTED_TIME().date()

def getDATE(mydate):
    return mydate.date()

def getTime(dateTime):
#    return dateTime
    return datetime.datetime.strftime(datetime.datetime.strptime(str(dateTime), '%Y-%m-%d %H:%M:%S'),"%I:%M %p")

def getDateTime(dateTime):
#    return dateTime
    try:
        return datetime.datetime.strftime(datetime.datetime.strptime(str(dateTime), '%Y-%m-%d %H:%M:%S'),"%Y-%m-%d %H:%M:%S")
    except Exception:
        return ""

def getTimeStampFromData(getdate):
#    return dateTime
    return str(int(time.mktime(FORMATTED_TIME_CREATE(getdate).timetuple())))


def getTimeStamp():
#    return dateTime
    return str(int(time.mktime(FORMATTED_TIME().timetuple())))


def getTimeStampBeforeOneHour():
#   print FORMATTED_TIME()
    return time.mktime(LATER_ONE_HOUR().timetuple())

def convertTimestamp(timestamp):
    try:
        datetime.datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
    except Exception, e:
        print e
        return False

def LATER_ONE_HOUR():
    return FORMATTED_TIME() - datetime.timedelta(0, 60 * 60)

def ADD_HOURS(getDateTime,addNum):
    mydatetime = datetime.datetime.strftime(datetime.datetime.strptime(str(getDateTime), '%Y-%m-%d %H:%M:%S'),"%Y-%m-%d %H:%M:%S")
    return FORMATTED_TIME_CREATE(mydatetime) + datetime.timedelta(0, int(addNum)*60 * 60)

def LATER_half_HOUR():
    return FORMATTED_TIME() - datetime.timedelta(0, 30 * 60)

def LATER_MIN(value):
    return FORMATTED_TIME() - datetime.timedelta(0, int(value) * 60)

def LATER_HOURS(hours):
    # print hours
    retddata = FORMATTED_TIME() - datetime.timedelta(0, int(hours) * 60 * 60)
    return retddata

def BEFORE_DAYS(days):
    # print hours
    if days == 0:
        retddata = FORMATTED_TIME() - datetime.timedelta(int(days), 0)
    else:
        retddata = FORMATTED_TIME() - datetime.timedelta(int(days-1), 86400)
    return retddata

def LATERTIME():
    return LATER().time()

#### -------------------- add 30 minutes
def AFTER():
    return FORMATTED_TIME() + datetime.timedelta(0, 30 * 60)

def AFTERTIME():
    return AFTER().time()

def AFTER_DAYS(days):
    # print hours
    retddata = FORMATTED_TIME() + datetime.timedelta(int(days), 60 * 60)
    return retddata

def diffInDates(dateFrom):
    try:
        date1 = datetime.datetime.strptime(str(dateFrom), '%Y-%m-%d %H:%M:%S')
        date2 = datetime.datetime.strptime(str(FORMATTED_TIME()), '%Y-%m-%d %H:%M:%S')
        diff = date2 - date1
        return diff.seconds
    except Exception as e:
        print e
        return 'exiting'

def diffInDynamicsDates(dateFrom,dateTo):
    try:
        date1 = datetime.datetime.strptime(str(dateFrom), '%Y-%m-%d %H:%M:%S')
        date2 = datetime.datetime.strptime(str(dateTo), '%Y-%m-%d %H:%M:%S')
        diff = date2 - date1
        return diff.seconds
    except Exception as e:
        print e
        return 'exiting'
    

def checkInt(to_check):
    try:
        return int(to_check)
    except ValueError:
        return None
    except TypeError:
        return None
    except Exception as generalException:
        return None


def base32encode(string):
    try:
        return base64.b32encode(string)
    except Exception as generalException:
        return None

def base32decode(string):
    try:
        return base64.b32decode(string)
    except Exception as generalException:
        return None

def timeDiffInWord(inputTime):
    try:
        m=0
        time=""
        m=abs(FORMATTED_TIME()-inputTime)   #This is the difference in input time and current time
        t=m.seconds
        if m.days:
            if m.days>=30:
                time=str(m.days/30)+" months"
            else:
                time=str(m.days)+" days"
        else:
            if t>=3600:
                time=str(t/3600)+" hours"
            elif t>=60:
                time=str(t/60)+" minutes"
            else:
                time=str(t)+" seconds"
        return time
    except Exception as e:
        print e
        return None

def BEFORE_DAYS_OF_DATE(date,days):
    return date - datetime.timedelta(int(days), 60 * 60)

def Week_Start_Date(date):
    day=date.weekday()
    if day==0:
        return date
    else:
        return BEFORE_DAYS_OF_DATE(date,day)

#Last week dates will be return in the order:[start date, end date]
def Last_Week():
    date=FORMATTED_TIME()
    day=date.weekday()
    return (BEFORE_DAYS_OF_DATE(date,day+7),BEFORE_DAYS_OF_DATE(date,day+1))

#Last month dates will be return in the order:[start date, end date]
def LastMonth():
    month= FORMATTED_TIME().month
    year= FORMATTED_TIME().year
    month-=1
    month_range= calendar.monthrange(year,month)[1]
    first= FORMATTED_TIME().strftime("%Y-"+str(month)+"-01 %H:%M:%S")
    first=FORMATTED_TIME_CREATE(first)
    last= FORMATTED_TIME().strftime("%Y-"+str(month)+"-"+str(month_range)+" %H:%M:%S")
    last=FORMATTED_TIME_CREATE(last)
    return (first,last)

def ThisMonthLastDate():
    month= FORMATTED_TIME().month
    year= FORMATTED_TIME().year
    month_range= calendar.monthrange(year,month)[1]
    last_date= FORMATTED_TIME().strftime("%Y-"+str(month)+"-"+str(month_range)+" %H:%M:%S")
    last_date=FORMATTED_TIME_CREATE(last_date)
    return last_date

def LastYear():
    year= FORMATTED_TIME().year
    y= year-1
    first_date= FORMATTED_TIME().strftime(str(y)+"-01-01 00:00:00")
    first_date=FORMATTED_TIME_CREATE(first_date)
    last_date= FORMATTED_TIME().strftime(str(y)+"-12-31 00:00:00")
    last_date=FORMATTED_TIME_CREATE(last_date)
    return (first_date,last_date)

def compareListDiff(list1,list2,key1,key2):
    for l in list1:
        for s in list2:
            if l[key1]==s[key2]:
                # print s
                list2.remove(s)
    return list2
