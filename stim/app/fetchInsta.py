__author__ = 'Kunal Monga'
client_secret = "feb6d643d6724711957580c1c706947a"
'''
This file will be used across the project for calling functions that are used at various places
'''
from flask import Blueprint, request, jsonify


# Import the database object from the main app module
from app import db, constants, mail, APP_ROOT, APP_INSTA
from subprocess import Popen, PIPE
import json, os
from app.model import model
from flask_mail import Message
import imageio
import random
# imageio.plugins.ffmpeg.download()
from moviepy.editor import *
from InstagramAPI import InstagramAPI
from bson.objectid import ObjectId
base = APP_INSTA

def unique_proxy():
	getdata=list(db.Proxies.find({"status":1}).sort("date_created",1))
	if getdata:
		found=1
		while found:
			randomProxy=random.choice(getdata)
			getdata.remove(randomProxy)
			checkuser=db.UserInsta.find_one({"proxy":randomProxy.proxy})
			if checkuser:
				pass
			else:
				found=0
				return randomProxy.proxy
		return None
	else:
		return None

def take_proxy(data):
	checkuser=db.UserInsta.find_one({"user_name":str(data['username'])})
	if "proxy" not in data:
		data["proxy"]=""
	else:
		if data["proxy"] is not None:
			return data
	if checkuser:
		if 'proxy' in checkuser and checkuser.proxy is not None:
			data["proxy"]=checkuser.proxy
			return data
		else:
			proxy=unique_proxy()
			updateData ={
						"proxy" : proxy,
						"date_updated":constants.FORMATTED_TIME()
						}
			InstaObjUpdate=db.UserInsta.find_and_modify({"_id":checkuser._id},
														{
															"$set":updateData
															})
			data["proxy"]=proxy
			return data
	return data

def loginUser(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"login.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	if content["status"]==4:
		print "i am in"
		content = loginUserEmail(data)
	print "i am here "
	return content

def loginUserEmail(data):
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"login_email.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	print "testing"
	print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	print content
	return content

def challengeVerify(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"challenge_verify.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content

def twoFactor(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"two_factor.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content


def getUInstaDetail(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"user_info.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	# print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content


def search(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"search.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	# print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content


def countUserFollowings(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"get_followings.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	# print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content


def getMedia(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"get_media.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content

def getOtherMedia(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"get_media_others.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	# print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content


def getFollowers(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"get_followers.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	# print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content



def getFollowings(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"get_followings.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	# print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content



def getOtherFollowers(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"get_other_followers.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content



def getOtherFollowings(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"get_other_followings.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content

def getOtherUserInfo(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"get_other_user_info.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content

def follow(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"follow.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content


def unfollow(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"unfollow.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content


def like(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"like.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content

def getHashtagFeed(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"get_hashtag_feed.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content


def getLocationFeed(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"get_location_feed.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content


def sendMessage(data):
	data=take_proxy(data)
	data=json.dumps(data)
	content = {
               'status' : 0,
               'message' : 'Something Went Wrong, Please Try Later',
               }
	process = Popen(["php", base+"send_message.php",data], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	print stdout
	mylist = stdout.splitlines()
	content = json.loads(mylist[-1])
	return content

