__author__ = 'Kunal Monga'
'''
This file will be used across the project for calling functions that are used at various places
'''

# Import the database object from the main app module
from app import db, constants, mail, APP_ROOT
import json, os

from flask_mail import Message
from bson.objectid import ObjectId



def sendEmail(sender,recipient,subject,content,attachment):
	msg = Message(subject, sender='support@kibisocial.com', recipients=[recipient])
	if attachment!="":
		with open(os.path.join(APP_ROOT	, attachment)) as fp:
			pass
			msg.attach('app/'+attachment, "application/pdf", fp.read())
	msg.html = content
	mail.send(msg)