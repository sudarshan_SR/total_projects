f__author__ = 'Kunal Monga'
# Import flask dependencies
from flask import Blueprint, request, jsonify, Response


# Import the database object from the main app module
from app import db, constants, functions, fetch, fetchDirect, APP_STATIC, fetchInsta,sendMail
import json, urllib, datetime, email, re, hashlib, random, string, urllib, os, pdfkit
from subprocess import Popen, PIPE
from bs4 import BeautifulSoup
from app.model import model
from bson.objectid import ObjectId
from app.module.validation import validate, msgtext
from operator import itemgetter

# Define the blueprint: 'auth', set its url prefix: app.url/auth
product_link = Blueprint('product', __name__, url_prefix='/product')
base = '/var/www/html/apps/stim/stim/app/insta/api/'


# Login In  API starts
@product_link.route('/instaLogin/', methods=['POST'])
def instaLogin():
	data = request.json
	resultReturn = {}
	result = []
	instaid = ""
	content = {"status":1,
				"message":"Success"}
	# data="test"
	# Custom Validation starts #
	try:
		description='User tried to reconnect with username: '+data['username']+' and password: '+data['password']
		username=data['username']
		if not username.islower():
			data['username']=username.lower()
		checkuser=db.UserInsta.find_one({"user_name":str(data['username'])})
		if checkuser and checkuser.status==1:
			content["data"]={
				"status":0,
				"result":result,
				"insta_user_id": str(checkuser._id),
				"message":"User Already Exist."
				}
			return jsonify(content)
		if checkuser and not str(checkuser.main_user_id)==str(data['main_user_id']):
			content["data"] = {
				"status":0,
				"result":result,
				"insta_user_id": str(checkuser._id),
				"message":"User Already Registered With Other Email Address"
				}
			return jsonify(content)
		else:
			searchContinue=True
			while searchContinue:
				ProxyExist=db.Proxies.find_one({"allotted_to":str(data['username']),"status":1,"in_process_status":1})
				if ProxyExist:
					proxy=ProxyExist.proxy
				else:
					proxy=fetchInsta.unique_proxy()
				data['proxy']=proxy
				result=fetchInsta.loginUser((data))
				if result['message']=="NetworkException":
					searchContinue=True
					updateData={
							"status":2
					}
					updateProxies=db.Proxies.find_and_modify({"proxy":proxy},
															{
															"$set":updateData
															})
				else:
					searchContinue=False
					db.Proxies.find_and_modify({"proxy":proxy},
															{
															"$set":{
																		"allotted_to":str(data['username']),
																		"in_process_status":1
																	}
															})
			if result['status']==1:
				checkuser_id=db.UserInsta.find_one({"user_id":str(result['user_id'])})
				if checkuser_id and not str(checkuser_id.main_user_id)==str(data['main_user_id']):
					content["data"] = {
						"status":0,
						"result":result,
						"message":"User Already Registered With Other Email Address"
						}
					return jsonify(content)
				detailRes=fetchInsta.getUInstaDetail((data))
				if detailRes["status"]==1:
					if checkuser or checkuser_id:
						updateData ={
						"email" : str(detailRes["email"]),
						"user_name" : str(data['username']),
						"full_name" : str(data['username']),
						"password" : str(data['password']),
						"following":int(detailRes["following_count"]),
						"follower_count":int(detailRes["follower_count"]),
						"posts":int(detailRes["media_count"]),
						"profile_pic_url" : str(detailRes["profile_pic_url"]),
						"is_private" : str(detailRes["is_private"]),
						"is_verified" :str(detailRes["is_verified"]),
						"proxy" : proxy,
						"status":int(1),
						"date_updated":constants.FORMATTED_TIME()
						}
						content["data"] = {
						"status":1,
						"result":result,
						"insta_user_id": str(checkuser_id._id),
						"message":"Success"
						}
						InstaObjUpdate=db.UserInsta.find_and_modify({"_id":checkuser_id._id},
														{
															"$set":updateData
															})
						ProxyExist=db.Proxies.find_one({"allotted_to":str(data['username']),"status":1,"in_process_status":1})
						db.Proxies.find_and_modify({"_id":ProxyExist._id},
															{
															"$set":{
																		"in_process_status":0
																	}
															})
						resEvent=functions.addMyEvent(str(checkuser_id.main_user_id),str(checkuser_id._id),"3","Account Started","Your account started running again.","fas fa-play custom-fa fa-play")
						return jsonify(content)
					else:
						description='User tried to login with username: '+data['username']+' and password: '+data['password']
						userObj = db.UserInsta()
						userObj.main_user_id = ObjectId(str(data['main_user_id']))
						userObj.email = str(detailRes["email"])
						userObj.user_name = str(data['username'])
						userObj.full_name = str(data['username'])
						userObj.password = str(data['password'])
						userObj.phone = str(detailRes["phone_number"])
						userObj.profile_pic_url = str(detailRes["profile_pic_url"])
						userObj.user_id = str(detailRes["pk"])
						userObj.is_private = str(detailRes["is_private"])
						userObj.is_verified = str(detailRes["is_verified"])
						userObj.proxy = proxy
						userObj.status = 1
						userObj.date_performed = constants.FORMATTED_TIME()
						userObj.date_created = constants.FORMATTED_TIME()
						userObj.date_updated = constants.FORMATTED_TIME()
						userObj.save()
						instaid=str(userObj._id)
						content["data"] = {
						"status":1,
						"result":result,
						"insta_user_id": str(instaid),
						"message":"Success"
						}
						ProxyExist=db.Proxies.find_one({"allotted_to":str(data['username']),"status":1,"in_process_status":1})
						db.Proxies.find_and_modify({"_id":ProxyExist._id},
															{
															"$set":{
																		"in_process_status":0
																	}
															})
						resEvent=functions.addMyEvent(str(data['main_user_id']),str(userObj._id),"0","Instagram Account Added","Every account has a beginning.","custom-fa fas fa-user custom-fa user-insta")
						resCurCurrently = functions.changeCurrently(userObj.main_user_id,userObj._id,1)
						return jsonify(content)
			elif result['status']==3:
				resultReturn={
					"status":3,
					"result":result,
					"message":"Challange Required"
					}
			elif result['status']==2:
				resultReturn={
					"status":2,
					"result":result,
					"message":"Two Factor Login"
					}
			else:
				resultReturn={
				"status":0,
				"result":result,
				"message":"Username or Password Incorrect."
				}
	except Exception as e:
		print e
		resultReturn={
		"status":0,
		"result":result,
		"message":str(e)
		}
	functions.manageLog(data['main_user_id'],functions.check_key("insta_user_id",resultReturn,''),'loginAPI',description,functions.check_key("message",resultReturn,'')+' '+str(functions.check_key('result',resultReturn,'')),functions.check_key("status",resultReturn))
	content["data"] = resultReturn
	return jsonify(content)


# Login In  API starts
# Takes username and password  
# Set the route and accepted methods
@product_link.route('/challengeVerify/', methods=['POST'])
def challengeVerify():
	data = request.json
	responseData = {}
	ProxyExist=db.Proxies.find_one({"allotted_to":str(data['username']),"status":1,"in_process_status":1})
	if ProxyExist:
		data['proxy']=ProxyExist.proxy
	result = fetchInsta.challengeVerify((data))
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	content["data"]=result
	description='User tried to challengeVerify with username: '+data['username']+' and password: '+data['password']
	functions.manageLog(functions.log_id(result['user_id'],1),functions.log_id(result['user_id'],2),'challengeVerify',description,functions.check_key("message",result,''),functions.check_key("status",result))
	return jsonify(content)



# Login In  API starts
# Takes username and password  
# Set the route and accepted methods
@product_link.route('/twoFactor/', methods=['POST'])
def twoFactor():
	data = request.json
	responseData = {}
	ProxyExist=db.Proxies.find_one({"allotted_to":str(data['username']),"status":1,"in_process_status":1})
	if ProxyExist:
		data['proxy']=ProxyExist.proxy
	result = fetchInsta.twoFactor((data))
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	content["data"]=result
	return jsonify(content)



# Login In  API starts
# Takes username and password  
# Set the route and accepted methods
@product_link.route('/twofactorLogin/', methods=['POST'])
def twofactorLogin():
	data = request.json
	result=[]
	responseData = {}
	
	process = Popen(["php", base+"two_factor_login.php",json.dumps(data)], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	mylist = stdout.splitlines()
	print stdout
	# print mylist
	# result = json.loads(mylist[-1])
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	content["res"]=result
	return jsonify(content)




# Login In  API starts
@product_link.route('/addTarget/', methods=['POST'])
def addTarget():
	data = request.json
	result = {"status":1,
				"message":"Success"}
	content = {"status":1,
				"message":"Success"}
	# data="test"
	# Custom Validation starts #
	try:		
		checkuser=db.Target.find_one({"user_id":str(data["pk"]),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data['insta_user_id']))})
		if checkuser:
			updateData1 ={
							"status":int(1),
							"date_updated":constants.FORMATTED_TIME()
							}
			Targetdata =db.Target.find_and_modify({"type":str(data["type"]),"_id":ObjectId(str(checkuser._id)),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

														{
														"$set":updateData1
														})
			result={
				"_id":str(checkuser._id),
				"status":1,
				"message":"Target Already Exist."
				}
		else:
			userObj = db.Target()
			userObj.main_user_id = ObjectId(str(data['main_user_id']))
			userObj.insta_user_id = ObjectId(str(data['insta_user_id']))
			userObj.email = str(data['main_user_id'])
			userObj.user_name = str(data['username'].encode('utf8'))
			userObj.full_name = str(data['full_name'].encode('utf8'))
			userObj.profile_pic_url = str(data['profile_pic_url'])
			userObj.user_id = str(data['pk'])
			userObj.followers=str(data['follower_count'])
			userObj.is_private = str(data['is_private'])
			userObj.is_verified = str(data['is_verified'])
			userObj.type = str(data['type'])
			userObj.status = 1
			userObj.date_created = constants.FORMATTED_TIME()
			userObj.date_updated = constants.FORMATTED_TIME()
			userObj.save()
			result={
			"_id":str(userObj._id)
			}
	except Exception as e:
		print e
		result={
		"status":0,
		"message":str(e)
		}
	content["data"] = result
	return jsonify(content)



# Login In  API starts
@product_link.route('/search/', methods=['POST'])
def search():
	data = request.json
	result = {}
	content = {"status":1,
				"message":"Success"}
	getdata =db.UserInsta.find_one({"main_user_id":ObjectId(str(data['main_user_id'])),"_id":ObjectId(str(data['insta_user_id']))})
	if getdata:
		if db.MyPlan.find_one({"insta_user_id":ObjectId(data["insta_user_id"]),"status":1}):
			data["username"]=getdata.user_name
			data["password"]=getdata.password
			result = fetchInsta.search((data))
		else:
			content = {"status":0,
				"message":"Expired"}
		content["data"] = result
	# print result["data"]
	return jsonify(content)



# Login In  API starts
@product_link.route('/getUserTag/', methods=['POST'])
def getUserTag():
	data = request.json
	result = {}
	content = {"status":1,
				"message":"Success"}
	result = fetchDirect.getTag(data['username'],data['password'],data['user_id'])
	content["data"] = result
	return jsonify(content)




# Set the route and accepted methods
@product_link.route('/getInstaUser/', methods=['POST'])
def getInstaUser():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getdata = fetch.getUserDetail('2985975523.425bd29.62c21c3ff5b04444a29ec77f046844ba','2985975523')
	# print data
	content['name'] = getdata
	return jsonify(content) 

# Login In  API Ends
# Return json 


# Set the route and accepted methods
# status 1 is new follower and 2 is old follower and 0 is unfollowed
@product_link.route('/getFollowerUser/', methods=['POST'])
def getFollowerUser():
	data = request.json
	result = {}
	finalFollower = ""
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	
	# try:
	getdata = fetchDirect.getMyFollowers(data['username'],data['password'])
	getExistingFollowers=db.Followers.find({"insta_user_id":str(data["insta_user_id"])})
	count = getExistingFollowers.count()
	# status 1 is new follower and 2 is old follower
	status = 1
	if count==0:
		status = 2
		finalFollower = getdata
	else:
		pass

	if finalFollower!="":
		for x in finalFollower:
			print x['username']
			userObj = db.Followers()
			userObj.main_user_id = ObjectId(str(data['main_user_id']))
			userObj.insta_user_id = ObjectId(str(data['insta_user_id']))
			userObj.user_name = str(x['username'])
			userObj.full_name = str(x['username'])
			userObj.profile_pic_url = str(x['profile_pic_url'])
			userObj.user_id = str(x['pk'])
			userObj.is_private = str(x['is_private'])
			userObj.is_verified = str(x['is_verified'])
			userObj.status = status
			userObj.date_created = constants.FORMATTED_TIME()
			userObj.date_updated = constants.FORMATTED_TIME()
			userObj.save()
	# except Exception as e:
	# 	result={
	# 	"status":0,
	# 	"message":str(e)
	# 	}
	content["data"] = result
	return jsonify(content)
	# Return json 


# Set the route and accepted methods
@product_link.route('/getFollowerAny/', methods=['POST'])
def getFollowerAny():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getdata = fetchDirect.getAnyFollowers('Adventure_Republic','Peru2011##','2159312872')
	# print data
	content['name'] = getdata
	return jsonify(content) 

# Return json 

@product_link.route('/getTarget/', methods=['POST'])
def getTarget():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	try:
		getdata =db.Target.find({"type":str(data["type"]),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data['insta_user_id'])),"status":1}).sort("date_created",-1)
		if getdata:
			for x in getdata:
				# print x.date_created.date()
				# print x.user_name
				dictn["name"]=x.user_name
				dictn["pic"]=x.profile_pic_url
				dictn["date"]=str(x.date_created.strftime("%A %d %B %Y"))
				dictn["_id"]=str(x._id)
				dictn["pk"]=str(x.user_id)
				if x.full_name!="":
					dictn["full_name"]=((x.full_name[:30] + '...') if len(x.full_name) > 9 else x.full_name)
				else:
					dictn["full_name"]=x.user_name			
				dictn["followers"]=x.followers
				dictn["status"]=str(0)
				result.append(dictn.copy())
		content['result'] = result
	except Exception as e:
		print e
	return jsonify(content) 

# Return json

@product_link.route('/searchTarget/', methods=['POST'])
def searchTarget():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	try:
		if data['user_name'].strip() != "":
			regex = ".*" + data['user_name'].strip() + ".*";
			getdata = db.Target.find({"user_name": re.compile(regex, re.IGNORECASE),"type":data['type'],"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data['insta_user_id'])),"status":int(1)})
			R=re.compile(regex, re.IGNORECASE)
			if getdata.count() != 0:
				for x in getdata:
					result.append({
					"_id":str(x._id),
					"name":str(x.user_name),
					"full_name":str(x.full_name),
					"pic":str(x.profile_pic_url),
					"user_id":str(x.user_id),
					"followers":str(x.followers),
					"is_private":str(x.is_private),
					"is_verified":str(x.is_verified),
					"date":str(x.date_created.strftime("%A %d %B %Y")),					
					})
			else:
				content = {
			               'status' : 0,
			               'message' : 'Not found',
			               }
		content['result'] = result
	except Exception as e:
		print e
		content = {
			               'status' : 0,
			               'message' : str(e),
			               }
	return jsonify(content)

@product_link.route('/removeTarget/', methods=['POST'])
def removeTarget():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	try:
		if data["t_id"]:
			updateData1 ={
							"status":int(0),
							"date_updated":constants.FORMATTED_TIME()
							}
			Targetdata =db.Target.find_and_modify({"type":str(data["type"]),"_id":ObjectId(str(data["t_id"])),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

														{
														"$set":updateData1
														})
			
		else:
			getdata =db.Target.find({"type":str(data["type"]),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))})
			for x in getdata:
				updateData1 ={
							"status":int(0),
							"date_updated":constants.FORMATTED_TIME()
							}
				Targetdata =db.Target.find_and_modify({"type":str(data["type"]),"_id":ObjectId(str(x._id)),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

														{
														"$set":updateData1
														})
	except Exception as e:
		print e
	return jsonify(content) 

# Return json

@product_link.route('/getUserInsta/', methods=['POST'])
def getUserInsta():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	sortedRes=[]
	dictn={}
	dc={}
	n=0
	try:
		if data["Iid"] == "" :
		# getdata =db.Actions.find({"main_user_id":ObjectId(str(data['main_user_id'])),"insta_user_id":ObjectId(str(data['insta_user_id']))}).sort("date_created",1)

			getdata =db.UserInsta.find({"main_user_id":ObjectId(str(data['_id'])),"status":{"$ne":-1}}).sort("date_created",-1)
			if getdata.count():
				for x in getdata:
					dictn["name"]=x.user_name
					# dictn["name"]=(x.user_name[:22]) if len(x.user_name) >=22 else x.user_name
					# dictn["remaining_name"]=(x.user_name[22:44]) if len(x.user_name) >=22 else ''
					dictn["pic"]=x.profile_pic_url
					dictn["date"]=str(x.date_created.date())
					dictn["_id"]=str(x._id)
					dictn["status"]=x.status
					dictn["index"]=str(n)
					n+=1
					checkPlan=functions.checkPlan(x._id)
					# print checkPlan
					dictn["pla_end_date"]=str(checkPlan["planEnd"])
					dictn['plan_name']=str(checkPlan["planName"])
					if str(checkPlan["planName"])=="Expired":
						dictn["status"]=5
					result.append(dictn.copy())

				sortedRes=sorted(result, key=itemgetter('name'))
			# print result
			content['result'] = sortedRes

		else:
			getdata =db.UserInsta.find_one({"_id":ObjectId(str(data["Iid"]))})
			if getdata:

				dictn["name"]=getdata.user_name
				dictn["pic"]=getdata.profile_pic_url
				dictn["date"]=str(getdata.date_created.date())
				checkPlan=functions.checkPlan(getdata._id)
				dictn["pla_end_date"]=str(checkPlan["planEnd"])
				dictn['plan_name']=str(checkPlan["planName"])
				# result.append(dictn.copy())
			content['result'] = dictn

	except Exception as e:
		print e
	return jsonify(content) 


@product_link.route('/addAction/', methods=['POST'])
def addAction():
	data = request.json
	content = {"status":1,
				"message":"Success"}
	# data="test"
	# Custom Validation starts #
	try:
		checkuser=db.Actions.find_one({"action_type":str(data['action_type']),"to_username":str(data["to_username"])})
		if checkuser:
			content={
				"status":1,
				"message":"Action Already Exist."
				}
		else:
			actObj = db.Actions()
			actObj.main_user_id = ObjectId(str(data['main_user_id']))
			actObj.insta_user_id = ObjectId(str(data['insta_user_id']))
			actObj.target_id = str(data['target_id'])
			actObj.target_type = str(data['target_type'])
			actObj.action_type = str(data['action_type'])
			actObj.media = str(data['media'])
			actObj.post_id = str(data['post_id'])
			actObj.to_username = str(data['to_username'])
			actObj.to_userid = str(data['to_userid'])
			actObj.status = 1
			actObj.date_created = constants.FORMATTED_TIME()
			actObj.date_updated = constants.FORMATTED_TIME()
			actObj.save()
			content={
			"status":1,
			"message":"Success"
			}
			
	except Exception as e:
		content={
		"status":0,
		"message":str(e)
		}
	# content["act_id"] = actObj._id
	return jsonify(content)


@product_link.route('/getAction/', methods=['POST'])
def getAction():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	try:
		getdata =db.Actions.find({"main_user_id":ObjectId(str(data['main_user_id'])),"insta_user_id":ObjectId(str(data['insta_user_id']))}).sort("date_created",-1)
		if getdata:
			userIObj=db.UserInsta.find_one({"_id":ObjectId(str(data['insta_user_id'])),"main_user_id":ObjectId(str(data['main_user_id']))})
			targetObj=list(db.Target.find({"main_user_id":ObjectId(str(data['main_user_id'])),"insta_user_id":ObjectId(str(data['insta_user_id']))}))
			for x in getdata:
				time=""
				time= constants.timeDiffInWord(x.date_created)
				result.append({
				"_id":str(x._id),
				"media":str(x.media),
				"to_username":str((x.to_username[:16]) if len(x.to_username) >=16 else x.to_username),
				"remaining_to_username":str((x.to_username[16:32]) if len(x.to_username) >=16 else ''),
				"insta_user_name":str( (userIObj.user_name[:16]) if len(userIObj.user_name) >=16 else userIObj.user_name),
				"remaining_insta_user_name":str( (userIObj.user_name[16:32]) if len(userIObj.user_name) >=16 else ''),
				"target_type":str(x.target_type),
				"action_type":str(x.action_type),
				"post_id":str(x.post_id),
				"to_userid":str(x.to_userid),
				"date_created":str(x.date_created),
				"time":str(time),
				"target_id":str(x.target_id),
				"from_target":""
				})
		updateresult=[y.update(from_target=x['user_name'],target_type='location') if y['target_id']==x['user_id'] and x['type']=='3' 
						else y.update(from_target=x['user_name'],target_type='tag') if y['target_id']==x['user_id'] and x['type']=='2' 
						else y.update(from_target=x['user_name']) if y['target_id']==x['user_id'] else{}  for y in result for x in targetObj]
		content["result"] = result
	except Exception as e:
		print e
	return jsonify(content)


@product_link.route('/searchAction/', methods=['POST'])
def searchAction():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	try:
		if data['post_username'].strip() != "":
			regex = ".*" + data['post_username'].strip() + ".*";
			getdata = db.Actions.find({"main_user_id":ObjectId(str(data['main_user_id'])),"insta_user_id":ObjectId(str(data['insta_user_id'])),"to_username": re.compile(regex, re.IGNORECASE)})
			R=re.compile(regex, re.IGNORECASE)
			# print R 
			userIObj=db.UserInsta.find_one({"_id":ObjectId(data['insta_user_id']),"main_user_id":ObjectId(data['main_user_id'])})
			targetObj=list(db.Target.find({"main_user_id":ObjectId(str(data['main_user_id'])),"insta_user_id":ObjectId(str(data['insta_user_id']))}))
			if getdata.count() != 0:
				for x in getdata:
					time=""
					time= constants.timeDiffInWord(x.date_created)
					
					# print time
					result.append({
					"_id":str(x._id),
					"media":str(x.media),
					"to_username":str((x.to_username[:16]) if len(x.to_username) >=16 else x.to_username),
					"remaining_to_username":str((x.to_username[16:32]) if len(x.to_username) >=16 else ''),
					"insta_user_name":str( (userIObj.user_name[:16]) if len(userIObj.user_name) >=16 else userIObj.user_name),
					"remaining_insta_user_name":str( (userIObj.user_name[16:32]) if len(userIObj.user_name) >=16 else ''),
					"target_type":str(x.target_type),
					"action_type":str(x.action_type),
					"post_id":str(x.post_id),
					"to_userid":str(x.to_userid),
					"date_created":str(x.date_created),
					"time":str(time),
					"target_id":str(x.target_id),
					"from_target":""
					})
					updateresult=[y.update(from_target=x['user_name'],target_type='location') if y['target_id']==x['user_id'] and x['type']=='3' 
						else y.update(from_target=x['user_name'],target_type='tag') if y['target_id']==x['user_id'] and x['type']=='2' 
						else y.update(from_target=x['user_name']) if y['target_id']==x['user_id'] else{}  for y in result for x in targetObj]
			else:
				content = {
			               'status' : 0,
			               'message' : 'Not found',
			               }
		content['result'] = result
		# print content			
	except Exception as e:
		print e
		content = {
			               'status' : 0,
			               'message' : str(e),
			               }
	return jsonify(content)




@product_link.route('/addPlan/', methods=['POST'])
def addPlan():
	data = request.json
	content = {"status":1,
				"message":"Success"}
	# data="test"
	# Custom Validation starts #
	try:
		check=db.Plans.find_one({"plan_name":str(data['plan_name'])})

		if check:
			content={
				"status":1,
				"message":"Plan Already Exist."
				}
		else:
			planObj = db.Plans()
			planObj.plan_name = str(data['plan_name'])
			planObj.plan_detail = str(data['plan_detail'])
			planObj.price = str(data['price'])
			planObj.status = 1
			planObj.date_created = constants.FORMATTED_TIME()
			planObj.date_updated = constants.FORMATTED_TIME()
			planObj.save()
			content={
			"status":1,
			"message":"Success"
			}
	except Exception as e:
		content={
		"status":0,
		"message":str(e)
		}
	# content["act_id"] = actObj._id
	return jsonify(content)


@product_link.route('/getPlan/', methods=['POST'])
def getPlan():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	try:
		getdata =db.Plans.find({"status":int("1")}).sort([('price',-1)])
		if getdata:
			for x in getdata:
				if x.price != "0.0":
					# print x.date_created
					# print x.plan_name
					dictn["plan_name"]=x.plan_name
					dictn["plan_detail"]=x.plan_detail
					dictn["price"]=str(x.price)
					dictn["plan_id"]=str(x._id)
					result.append(dictn.copy())
		content['result'] = result			
		content['prorate'] = float(9.935)
	except Exception as e:
		print e
	return jsonify(content) 


def getRand():
	rand = ''
	rand = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(9)])
	checkOid=db.Orders.find_one({"order_id":str(rand)})
	if checkOid:
		getRand()
	else:
		return rand
# Login In  API Ends
# Return json 

@product_link.route('/myOrder/', methods=['POST'])
def myOrder():
	data = request.json
	createPay = ""
	content = {"status":1,
			"message":"Success"}
	
	try:
		rand = getRand()
		odrObj = db.Orders()
		odrObj.main_user_id = ObjectId(data['main_user_id'])
		odrObj.insta_user_id = ObjectId(data['insta_user_id'])
		odrObj.plan_id = ObjectId(data['plan_id'])
		odrObj.order_id = str(rand)
		odrObj.temp = "1"
		odrObj.payment_method = "stripe"
		odrObj.payment_id = ""
		odrObj.token = str(data['token'])
		odrObj.payer_id = str(data['token_arr']["card"]["id"])
		odrObj.status = "2"
		odrObj.comments = "payment started "
		odrObj.total_amount = str(data["amount"])
		odrObj.date_created = constants.FORMATTED_TIME()
		odrObj.date_updated = constants.FORMATTED_TIME()
		odrObj.save()
		getEmail=db.Users.find_one({"_id":ObjectId(data['main_user_id'])})
		chargeAmount = int(100 * float(data["amount"]))
		createPay = functions.createCharge(str(data['token']),chargeAmount,rand,str(getEmail.email))
		if createPay["charge"]["status"]=="succeeded":
			updateData ={
								"status":"1",
								"temp" : "1"
								}
			objUpdate=db.Orders.find_and_modify({"_id":odrObj._id},
												{
													"$set":updateData
													})
			if objUpdate:
				planUpdate=db.MyPlan.find_one({"main_user_id":ObjectId(objUpdate.main_user_id),"insta_user_id":ObjectId(objUpdate.insta_user_id)})
				if planUpdate:
					updateData ={
						"status":int(1),
						"plan_id" : ObjectId(data['plan_id']),
						"plan_start_date": constants.FORMATTED_TIME(),
				        "pla_end_date":constants.FORMATTED_TIME()+datetime.timedelta(days=30),
						"date_updated":constants.FORMATTED_TIME()
						}
					PlObjUpdate=db.MyPlan.find_and_modify({"_id":planUpdate._id},
														{
															"$set":updateData
															})
					proxy=fetchInsta.unique_proxy()
					updateData={
						"proxy":str(proxy)
						}
					updateUserInsta=db.UserInsta.find_and_modify({"_id":ObjectId(objUpdate.insta_user_id)},
													{
														"$set":updateData
														})
					subject = "Kibi - Support"
					sock = urllib.urlopen(os.path.join(APP_STATIC, 'invoice.html'))
					html = sock.read()
					sock.close()
					#added mail
					Instauser=db.UserInsta.find_one({"_id":ObjectId(objUpdate.insta_user_id)})
					Plan=db.Plans.find_one({"_id":ObjectId(data['plan_id'])})
					if "[ORDERID]" in html:
						html = string.replace(html, "[ORDERID]", str(rand).strip())
						html = string.replace(html, "[USERNAME]", str((getEmail.first_name).capitalize()+" "+(getEmail.last_name).capitalize()).strip())
						html = string.replace(html, "[INSTANAME]", str(Instauser.user_name).strip())
						html = string.replace(html, "[E-mail]", str(getEmail.email).strip())
						html = string.replace(html, "[PHONE]", str(getEmail.phone).strip())
						html = string.replace(html, "[PLAN]", str(Plan.plan_name).strip())
						html = string.replace(html, "[PRICE]", str(Plan.price).strip())
						html = string.replace(html, "[AMOUNT]", str(data["amount"]).strip())
						html = string.replace(html, "[DATE]", str(constants.getDATE(constants.FORMATTED_TIME())).strip())
						res = sendMail.sendEmail("support@kibisocial.com",getEmail.email,subject,html,"")
				else:
					content['status']=0
					content['message']="Plan Update Failed"		
		else:
			content['status']=0
			content['message']="Failed"
			updateData ={
								"status":"0",
								"temp" : "0"
								}
			objUpdate=db.Orders.find_and_modify({"_id":odrObj._id},
												{
													"$set":updateData
													})
	except Exception as e:
		print e
		content['status']=0
		content['message']=str(e)
	
	content['createPay']=createPay
	return jsonify(content)

@product_link.route('/addMyPlan/', methods=['POST'])
def addMyPlan():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	dictn={}
	try:
		planClose=db.MyPlan.find_one({"main_user_id":ObjectId(data['main_user_id']),"insta_user_id":ObjectId(data['insta_user_id'])})
		getdata =db.Plans.find_one({"plan_name":str("Free Trial")})
		if planClose:
			content = {
               'status' : 1,
               'message' : 'MyPlan Already Exist',
               }
		else:
			planObj = db.MyPlan()
			planObj.main_user_id = ObjectId(data['main_user_id'])
			planObj.insta_user_id = ObjectId(data['insta_user_id'])
			planObj.plan_id = ObjectId(getdata._id)
			planObj.plan_start_date= constants.FORMATTED_TIME()
	        planObj.pla_end_date= constants.FORMATTED_TIME()+datetime.timedelta(days=7)
	        planObj.status = int(1)
	        planObj.email_status = int(0)
	        planObj.date_created = constants.FORMATTED_TIME()
	        planObj.date_updated = constants.FORMATTED_TIME()
	        planObj.save()
	        content = {
	               'status' : 1,
	               'message' : 'Plan added',
	               }			
			
	except Exception as e:
		print e
	return jsonify(content)

@product_link.route('/getMyPlan/', methods=['POST'])
def getMyPlan():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	dictn={}
	try:
		getdata =db.MyPlan.find_one({"main_user_id":ObjectId(str(data['main_user_id'])),"insta_user_id":ObjectId(str(data['insta_user_id'])),"status":int(1)})
		if getdata:
			namedata =db.Plans.find_one({"_id":ObjectId(getdata.plan_id)})
			if namedata:
				dictn["plan_name"]=str(namedata.plan_name)
				dictn["price"]=str(namedata.price)
				dictn["pla_end_date"]=str(getdata.pla_end_date.date())
				dictn["plan_id"]=str(namedata._id)
			content['data'] = dictn
		else:
			content = {
	               'status' : 0,
	               'message' : 'No plan',
	               }			
			
	except Exception as e:
		print e
	return jsonify(content)

@product_link.route('/getMyOrder/', methods=['POST'])
def getMyOrder():
  data = request.json
  content = {
               'status' : 1,
               'message' : 'Success',
               }
  result=[]
  dictn={}
  n=1
  try:
    getdata =db.Orders.find({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data['insta_user_id'])),"status":str(1)})
    if getdata:
      for x in getdata:
        UIObj=db.UserInsta.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"_id":ObjectId(str(data['insta_user_id']))})
        dictn["Acc_name"]=UIObj.user_name
        dictn["total"]=x.total_amount
        dictn["date"]=str(x.date_created.strftime("%d/%m/%Y"))
        dictn["order_id"]=str(x.order_id)
        dictn["status"]=str('Complete')
        dictn["style"]=str(n%2)
        n+=1
        result.append(dictn.copy())
    content['result'] = result      
  except Exception as e:
    print e
  return jsonify(content)

@product_link.route('/getOrderDetails/', methods=['POST'])
def getOrderDetails():
  data = request.json
  content = {
               'status' : 1,
               'message' : 'Success',
               }
  result=[]
  dictn={}
  try:
    ordrdata =db.Orders.find_one({"order_id":str(data["order_id"]),"status":str(1)})
    if ordrdata:
		UIObj=db.UserInsta.find_one({"main_user_id":ObjectId(str(ordrdata.main_user_id)),"_id":ObjectId(str(ordrdata.insta_user_id))})
		planObj=db.Plans.find_one({"_id":ordrdata.plan_id})
		dictn["Acc_name"]=UIObj.user_name
		dictn["total"]=ordrdata.total_amount
		dictn["date"]=str(ordrdata.date_created.strftime("%d/%m/%Y"))
		dictn["order_id"]=str(ordrdata.order_id)
		dictn["status"]=str('Complete')
		dictn["plan_name"]=str(planObj.plan_name)
		dictn["plan_price"]=str(planObj.price)

    content['result'] = dictn      
  except Exception as e:
    print e
  return jsonify(content)


@product_link.route('/addEvent/', methods=['POST'])
def addEvent():
	data = request.json
	content = {"status":1,
				"message":"Success"}
	# data="test"
	# Custom Validation starts #
	try:
		# Add Event Here
		resEvent = functions.addMyEvent(str(data['main_user_id']),str(data['insta_user_id']),"1","Account started Following","Your account started following users based on the targets and settings you selected.","fas fa-arrow-up arrowup")

		content={
		"status":1,
		"message":"Success"
		}
	except Exception as e:
		content={
		"status":0,
		"message":str(e)
		}
	# content["act_id"] = actObj._id
	return jsonify(content)

@product_link.route('/getEvent/', methods=['POST'])
def getEvent():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	try:
		getdata =db.Events.find({"main_user_id":ObjectId(str(data['main_user_id'])),"insta_user_id":ObjectId(str(data['insta_user_id']))}).sort("date_created",-1)
		for x in getdata:
			time=""
			time= constants.timeDiffInWord(x.date_created)
			dictn["event_name"]=str(x.event_name)
			dictn["event_detail"]=str(x.event_detail)
			dictn["type"]=str(x.event_type)
			dictn["icon"]=str(x.icon)
			dictn["event_id"]=str(x._id)
			dictn["time"]=str(time)
			result.append(dictn.copy())
		content['result'] = result			
	except Exception as e:
		print e
	return jsonify(content)


@product_link.route('/addWhitelist/', methods=['POST'])
def addWhitelist():
	data = request.json
	result = {"status":1,
				"message":"Success"}
	content = {"status":1,
				"message":"Success"}
	# data="test"
	# Custom Validation starts #
	try:
		checkuser=db.Whitelist.find_one({"user_id":str(data["pk"]),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data['insta_user_id']))})
		if checkuser:
			result={
				"_id":str(checkuser._id),
				"status":1,
				"message":"Whitelist Already Exist."
				}
			updateData1 ={
							"status":int(1),
							"date_updated":constants.FORMATTED_TIME()
							}
			Whitelistdata =db.Whitelist.find_and_modify({"_id":ObjectId(str(checkuser._id)),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

														{
														"$set":updateData1
														})
		else:
			WhitelObj = db.Whitelist()
			WhitelObj.main_user_id = ObjectId(str(data['main_user_id']))
			WhitelObj.insta_user_id = ObjectId(str(data['insta_user_id']))
			WhitelObj.user_name = str(data['username'].encode('utf8'))
			WhitelObj.full_name = str(data['full_name'].encode('utf8'))
			WhitelObj.profile_pic_url = str(data['profile_pic_url'])
			WhitelObj.user_id = str(data['pk'])
			WhitelObj.followers=str(data['follower_count'])
			WhitelObj.is_private = str(data['is_private'])
			WhitelObj.is_verified = str(data['is_verified'])
			WhitelObj.type = str(data['type'])
			WhitelObj.status = 1
			WhitelObj.date_created = constants.FORMATTED_TIME()
			WhitelObj.date_updated = constants.FORMATTED_TIME()
			WhitelObj.save()
			content={
			"status":1,
			"message":"Success"
			}
			result={
			"_id":str(WhitelObj._id),
			"status":1,
			"message":"Success"
			}
	except Exception as e:
		print e
		content={
		"status":0,
		"message":str(e)
		}
	content["data"] = result
	return jsonify(content)

@product_link.route('/getWhitelist/', methods=['POST'])
def getWhitelist():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	try:
		getdata =db.Whitelist.find({"status":int("1"),"main_user_id":ObjectId(str(data['main_user_id'])),"insta_user_id":ObjectId(str(data['insta_user_id']))}).sort("date_created",-1)
		if getdata:
			for x in getdata:
				dictn["name"]=x.user_name
				dictn["pic"]=x.profile_pic_url
				dictn["date"]=str(x.date_created.strftime("%A %d %B %Y"))
				dictn["_id"]=str(x._id)
				dictn["pk"]=str(x.user_id)
				result.append(dictn.copy())
		content['result'] = result			
	except Exception as e:
		print e
	return jsonify(content)

@product_link.route('/removeWhitelist/', methods=['POST'])
def removeWhitelist():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	try:
		if data["t_id"]:
			# getdata =db.Whitelist.find({"type":str(data["type"]),"_id":ObjectId(str(data["t_id"])),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))})
			
			updateData1 ={
						"status":int(0),
						"date_updated":constants.FORMATTED_TIME()
						}
			Whitelistdata =db.Whitelist.find_and_modify({"type":str(data["type"]),"_id":ObjectId(str(data["t_id"])),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

														{
														"$set":updateData1
														})
				
		else:
			getdata =db.Whitelist.find({"type":str(data["type"]),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))})
			for x in getdata:
				updateData1 ={
							"status":int(0),
							"date_updated":constants.FORMATTED_TIME()
							}
				Whitelistdata =db.Whitelist.find_and_modify({"type":str(data["type"]),"_id":ObjectId(str(x._id)),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

														{
														"$set":updateData1
														})
	except Exception as e:
		print e
	return jsonify(content) 

@product_link.route('/addBlacklist/', methods=['POST'])
def addBlacklist():
	data = request.json
	result = {"status":1,
				"message":"Success"}
	content = {"status":1,
				"message":"Success"}
	# data="test"
	# Custom Validation starts #
	try:
		if data["keyword"]!= "":
			word = hashlib.md5(str(data["keyword"]).encode())
			data["pk"]=word.hexdigest()
		if data["language"]!= "":
			word = hashlib.md5(str(data["language"]).encode())
			data["pk"]=word.hexdigest()
			
		
		checkuser=db.Blacklist.find_one({"user_id":str(data["pk"]),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data['insta_user_id']))})
		if checkuser:
			
			result={
				"_id":str(checkuser._id),
				"status":1,
				"message":"Blacklist Already Exist."
				}
			updateData1 ={
							"status":int(1),
							"date_updated":constants.FORMATTED_TIME()
							}
			Blacklistdata =db.Blacklist.find_and_modify({"type":str(data["type"]),"_id":ObjectId(str(checkuser._id)),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

														{
														"$set":updateData1
														})
		else:
			BlacklObj = db.Blacklist()
			BlacklObj.main_user_id = ObjectId(str(data['main_user_id']))
			BlacklObj.insta_user_id = ObjectId(str(data['insta_user_id']))
			BlacklObj.keyword = str(data['keyword'])
			BlacklObj.language = str(data['language'])
			BlacklObj.user_name = str(data['username'].encode('utf8'))
			BlacklObj.full_name = str(data['full_name'].encode('utf8'))
			BlacklObj.profile_pic_url = str(data['profile_pic_url'])
			BlacklObj.user_id = str(data['pk'])
			BlacklObj.followers=str(data['follower_count'])
			BlacklObj.is_private = str(data['is_private'])
			BlacklObj.is_verified = str(data['is_verified'])
			BlacklObj.type = str(data['type'])
			BlacklObj.status = 1
			BlacklObj.date_created = constants.FORMATTED_TIME()
			BlacklObj.date_updated = constants.FORMATTED_TIME()
			BlacklObj.save()
			content={
			"status":1,
			"message":"Success"
			}
			result={
				"_id":str(BlacklObj._id),
				"status":1,
				"message":"Success"
				}
	except Exception as e:
		print e
		content={
		"status":0,
		"message":str(e)
		}
	content["data"] = result
	return jsonify(content)

@product_link.route('/getBlacklist/', methods=['POST'])
def getBlacklist():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	try:
		getdata =db.Blacklist.find({"status":int("1"),"main_user_id":ObjectId(str(data['main_user_id'])),"insta_user_id":ObjectId(str(data['insta_user_id']))}).sort("date_created",-1)
		if getdata:
			for x in getdata:
				dictn["name"]=x.user_name
				dictn["pic"]=x.profile_pic_url
				dictn["date"]=str(x.date_created.strftime("%A %d %B %Y"))
				dictn["_id"]=str(x._id)
				dictn["type"]=str(x.type)
				dictn["keyword"]=str(x.keyword)
				dictn["pk"]=str(x.user_id)
				result.append(dictn.copy())
		content['result'] = result			
	except Exception as e:
		print e
	return jsonify(content)

@product_link.route('/removeBlacklist/', methods=['POST'])
def removeBlacklist():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	try:
		if data["t_id"]:
			# getdata =db.Blacklist.find({"type":str(data["type"]),"_id":ObjectId(str(data["t_id"])),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))})
			updateData1 ={
						"status":int(0),
						"date_updated":constants.FORMATTED_TIME()
						}
			Blacklisttdata =db.Blacklist.find_and_modify({"type":str(data["type"]),"_id":ObjectId(str(data["t_id"])),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

														{
														"$set":updateData1
														})
				
		else:
			getdata =db.Blacklist.find({"type":str(data["type"]),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))})
			for x in getdata:
				updateData1 ={
							"status":int(0),
							"date_updated":constants.FORMATTED_TIME()
							}
				Blacklistdata =db.Blacklist.find_and_modify({"type":str(data["type"]),"_id":ObjectId(str(x._id)),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

														{
														"$set":updateData1
														})
	except Exception as e:
		print e
	return jsonify(content) 

@product_link.route('/searchWhitelist/', methods=['POST'])
def searchWhitelist():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	try:
		if data['user_name'].strip() != "":
			regex = ".*" + data['user_name'].strip() + ".*";
			getdata = db.Whitelist.find({"user_name": re.compile(regex, re.IGNORECASE),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data['insta_user_id'])),"status":int(1)})
			R=re.compile(regex, re.IGNORECASE)
			try:
				if getdata.count() != 0:
					for x in getdata:
						result.append({
						"_id":str(x._id),
						"name":x.user_name,
						"full_name":x.full_name,
						"pic":str(x.profile_pic_url),
						"user_id":str(x.user_id),
						"followers":str(x.followers),
						"is_private":str(x.is_private),
						"is_verified":str(x.is_verified),
						"type":str(x.type),
						"date":str(x.date_created.strftime("%A %d %B %Y")),					
						})
				else:
					content = {
				               'status' : 0,
				               'message' : 'Not found',
				               }
				
			except Exception as e:
				print e
		content['result'] = result
		# print content			
	except Exception as e:
		print e
		content = {
			               'status' : 0,
			               'message' : str(e),
			               }
	return jsonify(content)

@product_link.route('/addGrowth/', methods=['POST'])
def addGrowth():
	data = request.json
	content = {"status":1,
				"message":"Success"}
	try:
		MyplanObj=db.MyPlan.find_one({"main_user_id":ObjectId(data['main_user_id']),"insta_user_id":ObjectId(data['insta_user_id'])})
		# planObj =db.Plans.find_one({"_id":ObjectId(MyplanObj.plan_id),"status":int("1")})
		checkuser=db.Growth.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))})
		if not checkuser:
			checkuser=[]
		mute_status=0
		if 'mute_status' in checkuser:
			mute_status=checkuser.mute_status
		if 'mute_status' in data:
			mute_status=data["mute_status"]
		if checkuser:
			updateData1 ={
							"speed":str(data["speed"]),
							"max_follow_limit":str(data["max_follow_limit"]),
					        "follow_private":str(data["follow_private"]),
					        "gender":str(data["gender"]),
					        "profile_pic":str(data["profile_pic"]),
					        "business_account":str(data["business_account"]),
					        "min_post":str(data["min_post"]),
					        "max_post":str(data["max_post"]),
					        "min_following":str(data["min_following"]),
					        "max_following":str(data["max_following"]),
					        "min_follower":str(data["min_follower"]),
					        "max_follower":str(data["max_follower"]),
					        "gender":str(data["gender"]),
					        "plan_id":ObjectId(str(MyplanObj.plan_id)),
					        "mute_status":int(mute_status),
							"date_updated":constants.FORMATTED_TIME()
							}
			GrowthObj=db.Growth.find_and_modify({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

													{
													"$set":updateData1
													})
			
		else:
			GrowthObj = db.Growth()
			GrowthObj.main_user_id = ObjectId(str(data['main_user_id']))
			GrowthObj.insta_user_id = ObjectId(str(data['insta_user_id']))
			GrowthObj.plan_id = ObjectId(str(MyplanObj.plan_id))
			GrowthObj.speed = str(data['speed'])
			GrowthObj.max_follow_limit = str(data['max_follow_limit'])
			GrowthObj.follow_private = str(data['follow_private'])
			GrowthObj.gender = str(data['gender'])
			GrowthObj.profile_pic = str(data['profile_pic'])
			GrowthObj.business_account = str(data['business_account'])
			GrowthObj.min_post=str(data['min_post'])
			GrowthObj.max_post = str(data['max_post'])
			GrowthObj.min_following = str(data['min_following'])
			GrowthObj.max_following = str(data['max_following'])
			GrowthObj.min_follower = str(data['min_follower'])
			GrowthObj.max_follower = str(data['max_follower'])
			GrowthObj.gender = str(data['gender'])
			GrowthObj.status = 1
			GrowthObj.mute_status=1
			GrowthObj.date_created = constants.FORMATTED_TIME()
			GrowthObj.date_updated = constants.FORMATTED_TIME()
			GrowthObj.save()
			
			content={
			"status":1,
			"message":"Success"
			}
	except Exception as e:
		print e
		content={
		"status":0,
		"message":str(e)
		}
	# content["act_id"] = actObj._id
	return jsonify(content)


@product_link.route('/getGrowth/', methods=['POST'])
def getGrowth():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	dictn={}
	try:
		GrowthObj =db.Growth.find_one({"status":int("1"),
										"main_user_id":ObjectId(str(data['main_user_id'])),
										"insta_user_id":ObjectId(str(data['insta_user_id']))
										})
		dictn["mute_available"]=0
		if db.MyPlan.find_one({"status":int(1),"main_user_id":ObjectId(str(data['main_user_id'])),"insta_user_id":ObjectId(str(data['insta_user_id'])),"plan_id":ObjectId('5b5cf9c054fcd327f9050781')}):
			dictn["mute_available"]=1
		if GrowthObj:
			dictn["speed"]=str(GrowthObj.speed)
			dictn["max_follow_limit"]=str(GrowthObj.max_follow_limit)
			dictn["follow_private"]=str(GrowthObj.follow_private)
			dictn["gender"]=str(GrowthObj.gender)
			dictn["profile_pic"]=str(GrowthObj.profile_pic)
			dictn["business_account"]=str(GrowthObj.business_account)
			dictn["min_post"]=str(GrowthObj.min_post)
			dictn["max_post"]=str(GrowthObj.max_post)
			dictn["min_following"]=str(GrowthObj.min_following)
			dictn["max_following"]=str(GrowthObj.max_following)
			dictn["min_follower"]=str(GrowthObj.min_follower)
			dictn["max_follower"]=str(GrowthObj.max_follower)
			dictn["mute_status"]=str(GrowthObj.mute_status)
			content['success']=1
		else:
			content['success']=0

		content['data'] = dictn			
	except Exception as e:
		print e
	return jsonify(content)


# API starts #
@product_link.route('/addUnfollow/', methods=['POST'])
def addUnfollow():
	data = request.json
	content = {"status":1,
				"message":"Success"}
	try:
		# planObj =db.Plans.find_one({"status":int("1")})
		MyplanObj=db.MyPlan.find_one({"main_user_id":ObjectId(data['main_user_id']),"insta_user_id":ObjectId(data['insta_user_id'])})
		checkuser=db.Unfollow.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))})
		if checkuser:
			updateData1 ={
							"speed":str(data["speed"]),
					        "limit" : str(data['limit']),
					        "source": str(data['source']),
					        "plan_id": ObjectId(str(MyplanObj.plan_id)),
							"date_updated":constants.FORMATTED_TIME()
							}
			UnfollowObj=db.Unfollow.find_and_modify({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

													{
													"$set":updateData1
													})
			
		else:
			UnfollowObj = db.Unfollow()
			UnfollowObj.main_user_id = ObjectId(str(data['main_user_id']))
			UnfollowObj.insta_user_id = ObjectId(str(data['insta_user_id']))
			UnfollowObj.plan_id = ObjectId(str(MyplanObj.plan_id))
			UnfollowObj.speed = str(data['speed'])
			UnfollowObj.limit = str(data['limit'])
			UnfollowObj.source = str(data['source'])
			UnfollowObj.status = 1
			UnfollowObj.date_created = constants.FORMATTED_TIME()
			UnfollowObj.date_updated = constants.FORMATTED_TIME()
			UnfollowObj.save()

			content={
			"status":1,
			"message":"Success"
			}
	except Exception as e:
		print e
		content={
		"status":0,
		"message":str(e)
		}
	# content["act_id"] = actObj._id
	return jsonify(content)


@product_link.route('/getUnfollow/', methods=['POST'])
def getUnfollow():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	dictn={}
	try:
		UnfollowObj =db.Unfollow.find_one({"status":int("1"),
										"main_user_id":ObjectId(str(data['main_user_id'])),
										"insta_user_id":ObjectId(str(data['insta_user_id']))
										})
		if UnfollowObj:
			dictn["speed"]=str(UnfollowObj.speed)
			dictn["limit"]=str(UnfollowObj.limit)
			dictn["source"]=str(UnfollowObj.source)
			content['success']=1
		else:
			content['success']=0
		
		content['data'] = dictn			
	except Exception as e:
		print e
	return jsonify(content)

# API starts #
@product_link.route('/addEngagement/', methods=['POST'])
def addEngagement():
	data = request.json
	content = {"status":1,
				"message":"Success"}
	try:
		# planObj =db.Plans.find_one({"status":int("1")})
		MyplanObj=db.MyPlan.find_one({"main_user_id":ObjectId(data['main_user_id']),"insta_user_id":ObjectId(data['insta_user_id'])})
		checkuser=db.Engagement.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))})
		if checkuser:
			updateData1 ={
							"speed":str(data["speed"]),
					        "source": str(data['source']),
					        "min_like" : str(data['min_like']),
					        "max_like" : str(data['max_like']),
					        "plan_id":ObjectId(str(MyplanObj.plan_id)),
							"date_updated":constants.FORMATTED_TIME()
							}
			EngagementObj=db.Engagement.find_and_modify({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

													{
													"$set":updateData1
													})
			
		else:
			EngagementObj = db.Engagement()
			EngagementObj.main_user_id = ObjectId(str(data['main_user_id']))
			EngagementObj.insta_user_id = ObjectId(str(data['insta_user_id']))
			EngagementObj.plan_id = ObjectId(str(MyplanObj.plan_id))
			EngagementObj.speed = str(data['speed'])
			EngagementObj.source = str(data['source'])
			EngagementObj.min_like = str(data['min_like'])
			EngagementObj.max_like = str(data['max_like'])
			EngagementObj.status = 1
			EngagementObj.date_created = constants.FORMATTED_TIME()
			EngagementObj.date_updated = constants.FORMATTED_TIME()
			EngagementObj.save()
			content={
			"status":1,
			"message":"Success"
			}
	except Exception as e:
		print e
		content={
		"status":0,
		"message":str(e)
		}
	return jsonify(content)


@product_link.route('/getEngagement/', methods=['POST'])
def getEngagement():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	dictn={}
	try:
		EngagementObj =db.Engagement.find_one({"status":int("1"),
										"main_user_id":ObjectId(str(data['main_user_id'])),
										"insta_user_id":ObjectId(str(data['insta_user_id']))
										})
		if EngagementObj:
			dictn["speed"]=str(EngagementObj.speed)
			dictn["source"]=str(EngagementObj.source)
			dictn["min_like"]=str(EngagementObj.min_like)
			dictn["max_like"]=str(EngagementObj.max_like)
			content['success']=1
		else:
			content['success']=0
		content['data'] = dictn			
	except Exception as e:
		print e
	return jsonify(content)

# API starts #
@product_link.route('/addSleep/', methods=['POST'])
def addSleep():
	data = request.json
	content = {"status":1,
				"message":"Success"}
	try:
		MyplanObj=db.MyPlan.find_one({"main_user_id":ObjectId(data['main_user_id']),"insta_user_id":ObjectId(data['insta_user_id'])})
		# planObj =db.Plans.find_one({"status":int("1")})

		checkuser=db.Sleep.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))})
		if checkuser:
			updateData1 ={
							"permission":str(data["permission"]),
					        "start_time" : str(data['start_time']),
					        "duration": str(data['duration']),
					        "plan_id":ObjectId(str(MyplanObj.plan_id)),
							"date_updated":constants.FORMATTED_TIME()
							}
			SleepObj=db.Sleep.find_and_modify({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

													{
													"$set":updateData1
													})
			
		else:
			SleepObj = db.Sleep()
			SleepObj.main_user_id = ObjectId(str(data['main_user_id']))
			SleepObj.insta_user_id = ObjectId(str(data['insta_user_id']))
			SleepObj.plan_id = ObjectId(str(MyplanObj.plan_id))
			SleepObj.permission = str(data['permission'])
			SleepObj.start_time = str(data['start_time'])
			SleepObj.duration = str(data['duration'])
			SleepObj.status = 1
			SleepObj.date_created = constants.FORMATTED_TIME()
			SleepObj.date_updated = constants.FORMATTED_TIME()
			SleepObj.save()
			content={
			"status":1,
			"message":"Success"
			}
	except Exception as e:
		print e
		content={
		"status":0,
		"message":str(e)
		}
	return jsonify(content)

@product_link.route('/getSleep/', methods=['POST'])
def getSleep():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	dictn={}
	try:
		SleepObj =db.Sleep.find_one({"status":int("1"),
										"main_user_id":ObjectId(str(data['main_user_id'])),
										"insta_user_id":ObjectId(str(data['insta_user_id']))
										})
		if SleepObj:
			dictn["permission"]=str(SleepObj.permission)
			dictn["start_time"]=str(SleepObj.start_time)
			dictn["duration"]=str(SleepObj.duration)
			content['success']=1
		else:
			content['success']=0
		content['data'] = dictn			
	except Exception as e:
		print e
	return jsonify(content)

@product_link.route('/userStatus/', methods=['POST'])
def userStatus():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               'followed_count' : '0',
               'unfollowed_count' : '0',
               'like_count':'0',
               'CurrentlyAction':1,
               'insta_user_id':data['insta_user_id']
               }
	content["current"]="Started Following"
	countFollowed = []
	countUnFollowed=[]
	countLike=0
	try:
		
		if data['insta_user_id']=="":
			getdata =db.UserInsta.find({"main_user_id":ObjectId(str(data['main_user_id']))}).sort("date_created",-1)
			if getdata.count():
				data['insta_user_id']=str(getdata[0]._id)
				content['insta_user_id']=data['insta_user_id']
		Count=list(db.Followed.find({"main_user_id":ObjectId(str(data['main_user_id'])),"insta_user_id":ObjectId(str(data['insta_user_id']))}))
		LikedCount=db.Liked.find({"main_user_id":ObjectId(str(data['main_user_id'])),"insta_user_id":ObjectId(str(data['insta_user_id']))}).count()
		countFollowed=[x for x in Count if x["status"]==1]
		countUnFollowed=[y for y in Count if y["status"]==0]
		countLike=LikedCount	
		if len(countFollowed):
			content["followed_count"] = len(countFollowed)  
		if len(countUnFollowed):
			content["unfollowed_count"] = len(countUnFollowed)
		if countLike:
			content["like_count"] = countLike
		instaObj =db.UserInsta.find_one({
										"main_user_id":ObjectId(str(data['main_user_id'])),
										"_id":ObjectId(str(data['insta_user_id']))
										})
		if instaObj:
			if data["status"]!="":
				if instaObj.status==0 or instaObj.status==1:
					updateData1 ={
									"status":int(data["status"]),
									"date_updated":constants.FORMATTED_TIME()
									}
					UserInstaObj=db.UserInsta.find_and_modify({"main_user_id":ObjectId(str(data["main_user_id"])),"_id":ObjectId(str(data["insta_user_id"]))},
															{
															"$set":updateData1
															})
					if UserInstaObj.status:
						resEvent=functions.addMyEvent(data['main_user_id'],str(UserInstaObj._id),"4","Account Stopped","You manually stopped your account. No actions will be performed until you restart your account.","fal fa-pause fa-started custom-fa fa-pause")
					else:
						resEvent=functions.addMyEvent(data['main_user_id'],str(UserInstaObj._id),"3","Account Started","Your account started running again.","fas fa-play custom-fa fa-play")

				content['active'] = data["status"]	
			else:
				content['active'] = instaObj.status
		# if data["status"]!="" and int(data["status"])==1:
		FinalObj =db.UserInsta.find_one({
										"main_user_id":ObjectId(str(data['main_user_id'])),
										"_id":ObjectId(str(data['insta_user_id']))
										})
		
		MyPlanObj=db.MyPlan.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data['insta_user_id']))})
		checkCurr = db.Currently.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data['insta_user_id']))})
		if checkCurr:
			content['CurrentlyAction']=checkCurr.status
		if MyPlanObj.status:
			if checkCurr.status==0:
				content['current'] = 'Un-Following'
			if checkCurr.status==1:
				content["current"] = "Following"
			
			if FinalObj.status==0:
				content['current'] = 'Stopped'
			if FinalObj.status==2 or FinalObj.status==4:
				content['current'] = 'Stopped(Temporarily)'
			if FinalObj.status==3:
				content['current'] = 'Stopped'
		else:
			if FinalObj.status!=3:
				content['active'] = 5
			content['current'] = 'Expired'
		content['insta_user_id']=data['insta_user_id']
	except Exception as e:
		print e
		content['active'] = ""
	# print content
	return jsonify(content)

@product_link.route('/AccountGrowth/', methods=['POST'])
def AccountGrowth():
	data = request.json
	dictn={}
	totalF = 0
	startGraph=[]
	plan="No Active Plan"
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	try:
		FollowersList=list(db.Followers.find({
												"main_user_id":ObjectId(str(data['main_user_id'])),
												"insta_user_id":ObjectId(str(data['insta_user_id']))
											}))
		BeforeJoinType=[x for x in FollowersList if x['status']==3]
		BeforeJoin=len(BeforeJoinType)
		UserInstaObj=db.UserInsta.find_one({"main_user_id":ObjectId(str(data['main_user_id'])),"_id":ObjectId(str(data['insta_user_id']))})
		if 'follower_count' in  UserInstaObj and UserInstaObj.follower_count is not None and UserInstaObj.follower_count:
			AfterJoin=int(UserInstaObj.follower_count)-int(BeforeJoin)
		else:
			AfterJoinType=[y for y in FollowersList if y['status']==1]
			AfterJoin=len(AfterJoinType)
			
		Before_30_daysType=[y for y in FollowersList if y['status']==1 and y['date_created']>=constants.LATER_MIN(43200)]
		Before_30_days=len(Before_30_daysType)
		countFollowed=db.Followed.find({
										"main_user_id":ObjectId(str(data['main_user_id'])),
										"insta_user_id":ObjectId(str(data['insta_user_id'])),
										"status":1,}).count()
		myplan=db.MyPlan.find_one({
								"main_user_id":ObjectId(str(data['main_user_id'])),
								"insta_user_id":ObjectId(str(data['insta_user_id'])),
								"status":1,})
		if myplan:
			planName=db.Plans.find_one({"_id":myplan.plan_id})
			if planName:
				plan=planName.plan_name
		if countFollowed!=0:
			percentFollowBack = round((float(AfterJoin)/float(countFollowed))*100,2)
		else:
			percentFollowBack=0
		if percentFollowBack == 0:
			percentFollowBack = 1
		totalF = AfterJoin + BeforeJoin
		value = totalF
		finalCount = totalF
		for x in range(1,5):
			value = value + int((float(percentFollowBack)/float(100))*value)
			startGraph.append(value)
			finalCount=value
		dictn['AfterJoin']=AfterJoin
		dictn['BeforeJoin']=BeforeJoin
		dictn['date_created']=UserInstaObj.date_created.strftime("%d/%m/%Y")
		dictn['Before_30_days']=Before_30_days
		dictn['percentFollowBack']=percentFollowBack
		dictn['plan']=plan
		dictn['startGraph']=startGraph
		dictn['finalCount']=finalCount
		dictn['following']=UserInstaObj.following
	except Exception as e:
		print e
	content['data'] = dictn
	return jsonify(content)

@product_link.route('/My_post/', methods=['POST'])
def My_post():
	data = request.json
	dictn={}
	result=[]
	count=0
	content = {
               'status' : 1,
               'message' : 'Success',
               'count':0
               }
	try:
		MyPostObj =db.MyPost.find({
										"main_user_id":ObjectId(str(data['main_user_id'])),
										"insta_user_id":ObjectId(str(data['insta_user_id']))
										}).limit(10).sort([('date_updated',-1)])
		for x in MyPostObj:
			if x.image is not None and x.image!="":		
				if count<=5:
					dictn['media_id']=x.media_id
					dictn['image']=x.image
					dictn['like']=x.like
					dictn['type']=x.type
					dictn['comment']=x.comment
					dictn['date']=x.date_created
					result.append(dictn.copy())
				count+=1
			else:
				x.delete()
		UserInstaObj=db.UserInsta.find_one({"main_user_id":ObjectId(str(data['main_user_id'])),"_id":ObjectId(str(data['insta_user_id']))})
	except Exception as e:
		print e
	content['data'] = result
	if UserInstaObj.posts:
		content['count']=UserInstaObj.posts
	else:
		content['count']=count

	return jsonify(content)

@product_link.route('/TargetCounting/', methods=['POST'])
def TargetCounting():
	data = request.json
	dictn={'Accounts':0,
		'Hashtag':0,
		'Location':0,
		'Whitelist':0,
		'Blacklist':0}
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	
	try:
		UserInstaObj=db.UserInsta.find_one({"main_user_id":ObjectId(str(data['main_user_id'])),"_id":ObjectId(str(data['insta_user_id']))})
		if UserInstaObj:
			Target =list(db.Target.find({
													"main_user_id":ObjectId(str(data['main_user_id'])),
													"insta_user_id":ObjectId(str(data['insta_user_id'])),
													"status":1
													}))	
			Whitelist =db.Whitelist.find({
											"main_user_id":ObjectId(str(data['main_user_id'])),
											"insta_user_id":ObjectId(str(data['insta_user_id'])),
											"status":1
											})
			Blacklist =db.Blacklist.find({
											"main_user_id":ObjectId(str(data['main_user_id'])),
											"insta_user_id":ObjectId(str(data['insta_user_id'])),
											"status":1
											})
			type1=[x for x in Target if x['type']=="1"]
			type2=[y for y in Target if y['type']=="2"]
			type3=[z for z in Target if z['type']=="3"]
			dictn['Accounts']=len(type1)
			dictn['Hashtag']=len(type2)
			dictn['Location']=len(type3)
			dictn['Whitelist']=Whitelist.count()
			dictn['Blacklist']=Blacklist.count()

	except Exception as e:
		print e
	content['data'] = dictn
	return jsonify(content)

@product_link.route('/followbacks/', methods=['POST'])
def followbacks():
	data = request.json
	dictn={}
	result=[]
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	try:
		FollowObj =db.Followers.find({
										"main_user_id":ObjectId(str(data['main_user_id'])),
										"insta_user_id":ObjectId(str(data['insta_user_id'])),
										"status":1
										}).sort("date_created",-1)
		for x in FollowObj:
				time=""
				time= constants.timeDiffInWord(x.date_created)
				
				dictn['_id']=str(x._id)
				dictn['full_name']=x.full_name
				dictn['profile_pic_url']=x.profile_pic_url
				dictn['user_id']=x.user_id
				dictn['user_name']=x.user_name
				dictn['status']=x.status				
				dictn['date_created']=x.date_created
				dictn['formatted_time']=time
				result.append(dictn.copy())

	except Exception as e:
		print e
	content['data'] = result
	return jsonify(content)

@product_link.route('/searchFollowbacks/', methods=['POST'])
def searchFollowbacks():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	try:
		if data['post_username'].strip() != "":
			regex = ".*" + data['post_username'].strip() + ".*";
			getdata = db.Followers.find({"main_user_id":ObjectId(str(data['main_user_id'])),"insta_user_id":ObjectId(str(data['insta_user_id'])),"user_name": re.compile(regex, re.IGNORECASE)})

			R=re.compile(regex, re.IGNORECASE)
			# print R 
			if getdata.count() != 0:
				for x in getdata:
					time=""
					time= constants.timeDiffInWord(x.date_created)

					dictn['_id']=str(x._id)
					dictn['full_name']=x.full_name
					dictn['profile_pic_url']=x.profile_pic_url
					dictn['user_id']=x.user_id
					dictn['user_name']=x.user_name
					dictn['status']=x.status				
					dictn['date_created']=x.date_created
					dictn['formatted_time']=time
					result.append(dictn.copy())
			else:
				content = {
			               'status' : 0,
			               'message' : 'Not found',
			               }
	except Exception as e:
		print e
		content = {
			               'status' : 0,
			               'message' : str(e),
			               }
	content['data'] = result
	return jsonify(content)



@product_link.route('/TargetingPerformance/', methods=['POST'])
def TargetingPerformance():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	result=[]
	dictn={}
	best={}
	worst={}
	total=0
	folBckTotal=0	
	try:
		if 'type' in data:
			TargetObj =list(db.Target.find({	
									"main_user_id":ObjectId(str(data['main_user_id'])),
									"insta_user_id":ObjectId(str(data['insta_user_id'])),
									"type":str(data["type"]),
									"date_created": {"$gte": constants.BEFORE_DAYS(int(data["days"]))}
									}).sort('date_created',1))
		else:
			TargetObj =list(db.Target.find({	
									"main_user_id":ObjectId(str(data['main_user_id'])),
									"insta_user_id":ObjectId(str(data['insta_user_id'])),
									"date_created": {"$gte": constants.BEFORE_DAYS(int(data["days"]))}
									}).sort('date_created',1))
		FollowedObj=list(db.Followed.find({
										"main_user_id":ObjectId(str(data['main_user_id'])),
										"insta_user_id":ObjectId(str(data['insta_user_id'])),
										# "status":1
										# "target_id":str(x.user_id)
										"date_created": {"$gte": constants.BEFORE_DAYS(int(data["days"]))}
										}))		
		FollowersObj=list(db.Followers.find({"main_user_id":ObjectId(str(data['main_user_id'])),
											"insta_user_id":ObjectId(str(data['insta_user_id'])),
											# "user_id":str(y.to_userid)
											"date_created": {"$gte": constants.BEFORE_DAYS(int(data["days"]))},
											"status":1
											}))
		for x in TargetObj:
			followbacks=0
			actions=0
			actionslis=[]
			actionslis=[y for y in FollowedObj if y['target_id']==x['user_id']]
			actions=len(actionslis)
			dictn['_id']=str(x['_id'])
			dictn['full_name']=x['full_name']
			dictn['user_id']=x['user_id']
			dictn['profile_pic_url']=x['profile_pic_url']
			dictn['is_private']=x['is_private']
			dictn['pk']=x['user_id']
			dictn['is_verified']=x['is_verified']
			dictn['user_name']=x['user_name']
			dictn['status']=x['status']
			dictn['type']=x['type']
			dictn['date_created']=x['date_created']
			dictn['actions']=actions
			total=total+actions
			for z in FollowedObj:
				followback=0
				followback=[y for y in FollowersObj if str(y['user_id'])==str(z['to_userid']) and z['target_id']==x['user_id']]
				if followback:
					followbacks+=1
					folBckTotal+=1
			dictn['followbacks']=followbacks
			result.append(dictn.copy())
		if len(result):
			for r in range(len(result)):
				perc= float(result[r]['actions'])
				if total!=0:
					result[r]["AccPerc"]=round(float(perc/total)*100,2)
				else:
					result[r]["AccPerc"]=0
				perc1=float(result[r]['followbacks'])
				if perc1!=0:
					result[r]["folBackPerc"]=round(float(perc1/perc)*100,2)
				else:
					result[r]["folBackPerc"]=0
			newResult=[x for x in result if x['type']=='1']
			if len(newResult):
				sortedBestRes=sorted(newResult, key=itemgetter('followbacks','actions'),reverse=1)
				best=sortedBestRes[0]
				# sortedWorstRes=sorted(sortedBestRes, key=itemgetter('followbacks','actions'),reverse=0)
				if len(sortedBestRes)>1 and best['actions']!=sortedBestRes[-1]['actions'] or best['followbacks']!=sortedBestRes[-1]['followbacks']:
					worst=sortedBestRes[-1]
				if not best['actions']:
					best={}
	except Exception as e:
		print e
		content = {
	               'status' : 0,
	               'message' : str(e),
	               }
	content['best']=best
	content['worst']=worst
	content['data'] = result
	content['total']=total
	content['folBckTotal']=folBckTotal
	if total!=0:
		content['folBackrate']=round((float(folBckTotal)/float(total))*100,2)
		# print float((folBckTotal/total)*100)
	else:
		content['folBackrate']=0
	return jsonify(content)


@product_link.route('/DirectMsgtiming/', methods=['POST'])
def DirectMsgtiming():
	data = request.json
	content = {"status":1,
				"message":"Success"}
	# data="test"
	# Custom Validation starts #
	try:
		check=db.MessageTime.find_one({"main_user_id":ObjectId(str(data['main_user_id'])),
										"insta_user_id":ObjectId(str(data['insta_user_id']))})
		if check:
			if data['state']==1:
				updateData1 ={
									"time_from":str(data["from"]),
									"date_updated":constants.FORMATTED_TIME()
									}
				MessageTimeObj=db.MessageTime.find_and_modify({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

														{
														"$set":updateData1
														})
			if data['state']==2:
				updateData1 ={
									"time_to":str(data["to"]),
									"date_updated":constants.FORMATTED_TIME()
									}
				MessageTimeObj=db.MessageTime.find_and_modify({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},

														{
														"$set":updateData1
														})
	except Exception as e:
		content={
		"status":0,
		"message":str(e)
		}
	return jsonify(content)

@product_link.route('/getDMtiming/', methods=['POST'])
def getDMtiming():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	dictn={}
	try:
		MsgTimeObj =db.MessageTime.find_one({"status":int("1"),
										"main_user_id":ObjectId(str(data['main_user_id'])),
										"insta_user_id":ObjectId(str(data['insta_user_id']))
										})
		if MsgTimeObj:
			dictn["time_to"]=str(MsgTimeObj.time_to)
			dictn["time_from"]=str(MsgTimeObj.time_from)
		else:
			msgtmObj = db.MessageTime()
			msgtmObj.time_to = str('23')
			msgtmObj.time_from = str('00')
			msgtmObj.status = 1
			msgtmObj.main_user_id = ObjectId(str(data['main_user_id']))
			msgtmObj.insta_user_id = ObjectId(str(data['insta_user_id']))
			msgtmObj.date_created = constants.FORMATTED_TIME()
			msgtmObj.date_updated = constants.FORMATTED_TIME()
			msgtmObj.save()
			dictn["time_to"]=str(msgtmObj.time_to)
			dictn["time_from"]=str(msgtmObj.time_from)
			content={
			"status":1,
			"message":"Success"
			}
		content['data'] = dictn			
	except Exception as e:
		print e
		content['success']=0
	return jsonify(content)

@product_link.route('/importWhitelist/', methods=['POST'])
def ImportWhitelist():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	dictn={}
	importList=[]
	try:
		getdata =db.UserInsta.find_one({"main_user_id":ObjectId(str(data['main_user_id'])),"_id":ObjectId(str(data['insta_user_id']))})
		if getdata:
			data["username"]=getdata.user_name
			data["password"]=getdata.password
			result = fetchInsta.getFollowings((data))
			res=result['search_result']
			count=0
			for x in range(0,len(res)):
				count+=1
				dictn=result['search_result'][x].copy()
				importList.append(dictn)
				wl_status1Obj=db.Whitelist.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"])),"user_id":str(dictn['pk']),"$or":[{"status":1},{"status":0}]})
				if wl_status1Obj:
					if wl_status1Obj.status:
						pass
					else:
						updateData1 ={
						"user_name":str(dictn['username'].encode('utf8')),
						"full_name":str(dictn['full_name'].encode('utf8')),
						"profile_pic_url":str(dictn['profile_pic_url'].encode('utf8')),
						"status":int(1),
						"date_updated":constants.FORMATTED_TIME()
						}
						Whitelistdata =db.Whitelist.find_and_modify({"_id":ObjectId(str(wl_status1Obj._id)),"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},
							{
							"$set":updateData1
							})
				else:
					WhitelObj = db.Whitelist()
					WhitelObj.main_user_id = ObjectId(str(data['main_user_id']))
					WhitelObj.insta_user_id = ObjectId(str(data['insta_user_id']))
					WhitelObj.user_name = str(dictn['username'].encode('utf8'))
					WhitelObj.full_name = str(dictn['full_name'].encode('utf8'))
					WhitelObj.profile_pic_url = str(dictn['profile_pic_url'])
					WhitelObj.user_id = str(dictn['pk'])
					WhitelObj.followers=str(0)
					WhitelObj.is_private = str(dictn['is_private'])
					WhitelObj.is_verified = str(dictn['is_verified'])
					WhitelObj.type = str(1)
					WhitelObj.status = 1
					WhitelObj.date_created = constants.FORMATTED_TIME()
					WhitelObj.date_updated = constants.FORMATTED_TIME()
					WhitelObj.save()
				if count>=int(data['range']):
					break										
		content['success'] = 1			
	except Exception as e:
		print e
		content['success']=0
		content['message']=str("Something Went Wrong, Please Try after Some Time")
	return jsonify(content)


@product_link.route('/cancleAccount/', methods=['POST'])
def CancleAccount():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	try:
		checkuser=db.UserInsta.find_one({"_id":ObjectId(data['insta_user_id'])})
		if checkuser:
			db.UserInsta.find_and_modify({"_id":ObjectId(str(checkuser._id)),"main_user_id":ObjectId(str(checkuser.main_user_id))},

														{
														"$set":{
																"status":int(-1),
																"date_updated":constants.FORMATTED_TIME()
																}
														})
	except Exception as e:
		print e
		content={
		"status":1,
		"message":str(e)
		}
	return jsonify(content)

@product_link.route('/CouponApply/', methods=['POST'])
def CouponApply():
	data = request.json
	result={}
	discount=0.0
	content = {
               'status' : 0,
               'message' : 'Invalid Code',
               }
	try:
		Plan_data =db.Plans.find_one({"_id":ObjectId(data['plan_id'])})
		amount=float(Plan_data.price)
		checkCoupon=db.Coupon.find_one({"code":str(data['code'])})
		if checkCoupon:
			if checkCoupon.code=="20OFFEXPIRED":
				checkOrders=db.Orders.find({"insta_user_id":ObjectId(str(data['insta_user_id'])),"status":"1"})
				if checkOrders.count()>=1:
					pass
				else:
					discount=float(float(amount/100)*float(checkCoupon.percentage))
					amount=amount-discount
					content = {
		               'status' : 1,
		               'message' : 'success',
		               }
		result["final_amount"]=round(amount,2)
		result["plan_price"]=Plan_data.price
		result['coupon']=round(discount,2)
		result['code']=str(data['code'])
	except Exception as e:
		print e
		content={
		"status":0,
		"message":str("Something Went Wrong, Please Try Again Later.")
		}
	content['data']=result
	return jsonify(content)
