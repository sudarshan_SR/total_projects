f__author__ = 'Kunal Monga'
# Import flask dependencies
from flask import Blueprint, request, jsonify, Response


# Import the database object from the main app module
from app import db, constants, functions, fetch, fetchDirect, APP_STATIC
import json, urllib, datetime, email, re, hashlib, random, string, urllib, os, pdfkit
from bs4 import BeautifulSoup
from random import randint
from app.model import model
from bson.objectid import ObjectId
from InstagramAPI import InstagramAPI
from app.module.validation import validate, msgtext

# Define the blueprint: 'auth', set its url prefix: app.url/auth
event_link = Blueprint('event', __name__, url_prefix='/event')



@event_link.route('/testevent/', methods=['GET'])
def testevent():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	api = InstagramAPI('Adventure_Republic', 'Peru2011##')
	api.login()
	getAnyFollowers=fetchDirect.getLocationFeeds("498870164",api)
	print "here"
	print getAnyFollowers
	return jsonify(getAnyFollowers) 

# Set the route and accepted methods
@event_link.route('/selectEvent/', methods=['GET'])
def selectEvent():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getUser = db.UserInsta.find({"status":int(1)})
	target={}
	result={}
	for user in getUser:
		getPlan = db.MyPlan.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id,"plan_end_date": {"$gte": constants.FORMATTED_TIME()}})
		if getPlan:
			goIn = True
			checkLastType = db.Actions.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"$or":[{"action_type":"follow"},{"action_type":"unfollow"}]}).limit(1).sort("date_created",-1)
			getLimit = db.Growth.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id})
			getUnfollowLimit = db.Unfollow.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id})
			if checkLastType[0].action_type=="unfollow":
				getWhiteList = db.Whitelist.find({"main_user_id":user.main_user_id,"insta_user_id":user._id}).count()
				unFLimit = getWhiteList+int(getUnfollowLimit.limit)
				getUserFollowed=db.UserInsta.find_one({"_id":user._id})
				if int(getUserFollowed.following)<=unFLimit:
					pass
				else:
					goIn = False
			if goIn:		
				# print checkLastInstance[0].action_type
				checkLastInstance = db.Actions.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"date_created": {"$gt": constants.LATER_MIN(randint(18, 25))}})
				if checkLastInstance.count() == 0:
					if int(getLimit.max_follow_limit) > 0:
						checkLimit = db.Actions.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"action_type":"follow"}).count()
						if int(getLimit.max_follow_limit) <= checkLimit:
							content["message"]="Can not take action at this time"
							return jsonify(content)
						# Add Event Here
						resEvent=functions.addMyEvent(user.main_user_id,user._id,"1","Account started Following","Your account started following users based on the targets and settings you selected.","fas fa-arrow-up custom-fa arrowup")
						# Event Added
						getPlanDetail = db.Plans.find_one({"_id":getPlan.plan_id})
						counting=["1","2","3"]
						Account=[]
						Hashtag=[]
						Location=[]
						for x in range(1, 4):
							getData=db.Target.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"type":str(x)})
							if getData.count() > 0:
								countFinal=3
								for getTargets in getData:
									createDump = {"target_id":getTargets.user_id,"my_user_id":user.user_id,"user_name":getTargets.user_name}
									if getTargets.type=="1":
										Account.append(createDump)
										if "1" in counting: counting.remove("1")
									if getTargets.type=="2":
										Hashtag.append(createDump)
										if "2" in counting: counting.remove("2")
									if getTargets.type=="3":
										Location.append(createDump)
										if "3" in counting: counting.remove("3")
								countFinal = countFinal - len(counting)
								target["data"]={
												"account":Account,
												"hashtag":Hashtag,
												"location":Location
												}
						result=functions.startFollowing(user.main_user_id,user._id,user.user_id, target['data'], countFinal, getPlanDetail.max_follow)

				else:
					content["message"]="Can not take action at this time"
			else:
				content["message"]="Its in Unfollow Mode."		
	content['result']=result
	return jsonify(content) 





# Set the route and accepted methods
@event_link.route('/selectEventUnfollow/', methods=['GET'])
def selectEventUnfollow():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getUser = db.UserInsta.find({"status":int(1)})
	target={}
	result={}
	for user in getUser:
		getPlan = db.MyPlan.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id,"plan_end_date": {"$gte": constants.FORMATTED_TIME()}})
		if getPlan:
			goIn = True
			checkLastType = db.Actions.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"$or":[{"action_type":"follow"},{"action_type":"unfollow"}]}).limit(1).sort("date_created",-1)
			getLimit = db.Growth.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id})
			getUnfollowLimit = db.Unfollow.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id})
			if checkLastType[0].action_type=="follow":
				unFLimit = getWhiteList+int(getUnfollowLimit.limit)
				getUserFollowed=db.UserInsta.find_one({"_id":user._id})
				if int(getUserFollowed.following)<=unFLimit:
					pass
				else:
					goIn = False
			if goIn:		
				# print checkLastInstance[0].action_type
				checkLastInstance = db.Actions.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"date_created": {"$gt": constants.LATER_MIN(randint(18, 25))}})
				if checkLastInstance.count() == 0:
					if int(getLimit.max_follow_limit) > 0:
						checkLimit = db.Actions.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"action_type":"follow"}).count()
						if int(getLimit.max_follow_limit) <= checkLimit:
							content["message"]="Can not take action at this time"
							return jsonify(content)
						# Add Event Here
						resEvent=functions.addMyEvent(user.main_user_id,user._id,"1","Account started Following","Your account started following users based on the targets and settings you selected.","fas fa-arrow-up custom-fa arrowup")
						# Event Added
						getPlanDetail = db.Plans.find_one({"_id":getPlan.plan_id})
						counting=["1","2","3"]
						Account=[]
						Hashtag=[]
						Location=[]
						for x in range(1, 4):
							getData=db.Target.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"type":str(x)})
							if getData.count() > 0:
								countFinal=3
								for getTargets in getData:
									createDump = {"target_id":getTargets.user_id,"my_user_id":user.user_id,"user_name":getTargets.user_name}
									if getTargets.type=="1":
										Account.append(createDump)
										if "1" in counting: counting.remove("1")
									if getTargets.type=="2":
										Hashtag.append(createDump)
										if "2" in counting: counting.remove("2")
									if getTargets.type=="3":
										Location.append(createDump)
										if "3" in counting: counting.remove("3")
								countFinal = countFinal - len(counting)
								target["data"]={
												"account":Account,
												"hashtag":Hashtag,
												"location":Location
												}
						result=functions.startFollowing(user.main_user_id,user._id,user.user_id, target['data'], countFinal, getPlanDetail.max_follow)

				else:
					content["message"]="Can not take action at this time"
			else:
				content["message"]="Its in Unfollow Mode."		
	content['result']=result
	return jsonify(content) 




# Set the route and accepted methods
@event_link.route('/likeEvent/', methods=['GET'])
def likeEvent():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getUser = db.UserInsta.find({"status":int(1)})
	target={}
	result={}
	planUse=[]
	for user in getUser:
		forPlan=db.Plans.find({"max_like":{"$ne":"0"}})
		for x in forPlan:
			planUse.append(x._id)
		getPlan = db.MyPlan.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id,"plan_id":{"$in":planUse},"plan_end_date": {"$gte": constants.FORMATTED_TIME()}})
		if getPlan:
			checkLastInstance = db.Actions.find({"action_type":"like","main_user_id":user.main_user_id,"insta_user_id":user._id,"date_created": {"$gt": constants.LATER_MIN(randint(400, 600))}})
			if checkLastInstance.count() == 0:
				sourceGenerate = []
				getRules = db.Engagement.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id})
				getPlanDetail = db.Plans.find_one({"_id":getPlan.plan_id})
				totalHits = (int(getPlanDetail.max_like)*int(getRules.speed))/2
				# Add Event Here
				resEvent=functions.addMyEvent(user.main_user_id,user._id,"2","Account started Liking","You started liking posts.","fas fa-arrow-up custom-fa arrowup")
				# Event Added
				if getRules.source=="0":
					getFollowed=db.Followed.find({"status":1,"main_user_id":user.main_user_id,"insta_user_id":user._id})
					for datafol in getFollowed:
						sourceGenerate.append({"userid":datafol.to_userid,"username":datafol.to_username})
				elif getRules.source=="1":
					getFollowed=db.Followed.find({"status":1,"main_user_id":user.main_user_id,"insta_user_id":user._id})
					for datafol in getFollowed:
						sourceGenerate.append({"userid":datafol.to_userid,"username":datafol.to_username})
				elif getRules.source=="2":
					getWhitelist=db.Whitelist.find({"main_user_id":user.main_user_id,"insta_user_id":user._id})
					for datawhite in getWhitelist:
						sourceGenerate.append({"userid":datawhite.user_id,"username":datafol.to_username})
				elif getRules.source=="3":
					getFollowed=db.Followed.find({"status":1,"main_user_id":user.main_user_id,"insta_user_id":user._id})
					for datafol in getFollowed:
						sourceGenerate.append({"userid":datafol.to_userid,"username":datafol.to_username})
					# # list1 = [10, 15, 20, 25, 30, 35, 40]
					# # list2 = [25, 40, 35]
					# # print(Diff(list1, list2))
					# sourceGenerateTemp1=[]
					# sourceGenerateTemp2=[]
					# getFollowed=db.Followed.find({"status":1,"main_user_id":user.main_user_id,"insta_user_id":user._id})
					# for datafol in getFollowed:
					# 	sourceGenerateTemp1.append(1)
					# getWhitelist=db.Whitelist.find({"main_user_id":user.main_user_id,"insta_user_id":user._id})
					# for datawhite in getWhitelist:
					# 	sourceGenerateTemp2.append(1)
					# sourceGenerate = sourceGenerateTemp1 - sourceGenerateTemp2
				result=functions.startLiking(user.main_user_id,user._id,totalHits,getRules.min_like,getRules.max_like,getRules.source,sourceGenerate)

	content['result']=result
	return jsonify(content) 




# Login In  API Ends
# Return json 


# Set the route and accepted methods
@event_link.route('/followEvent/', methods=['POST'])
def followEvent():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getdata = fetch.getUserFollows('2985975523.425bd29.62c21c3ff5b04444a29ec77f046844ba','2985975523')
	# print data
	content['name'] = getdata
	return jsonify(content) 

# Return json 



# Login In  API Ends
# Return json 


# Set the route and accepted methods
@event_link.route('/addManage/', methods=['POST'])
def addManage():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	try:
		checkPost=db.Manage.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))})
		if checkPost:
			content = {
               'status' : 0,
               'message' : 'Already Exist',
               }
		else:
			userObj = db.Manage()
			userObj.main_user_id = ObjectId(str(data['main_user_id']))
			userObj.insta_user_id = ObjectId(str(data['insta_user_id']))
			userObj.name = str(data['name'])
			userObj.date_created = constants.FORMATTED_TIME()
			userObj.date_updated = constants.FORMATTED_TIME()
			userObj.save()
	except Exception as e:
		print e
	# print data
	return jsonify(content) 

# Return json 


# Set the route and accepted methods
@event_link.route('/getManage/', methods=['POST'])
def getManage():
	data = request.json
	manageArr = []
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	try:
		getManage = db.Manage.find({"main_user_id":ObjectId(data["main_user_id"])})
		for x in getManage:
			manageArr.append({"main_user_id":str(x.main_user_id),"insta_user_id":str(x.insta_user_id),"name":str(x.name)})

	except Exception as e:
		print e
	# print data
	content["manageArr"] = manageArr
	return jsonify(content) 

# Return json 


# Set the route and accepted methods
@event_link.route('/deleteManage/', methods=['POST'])
def deleteManage():
	data = request.json
	manageArr = []
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	try:
		deleteM = db.Manage.find_one({"main_user_id":ObjectId(data["main_user_id"]),"insta_user_id":ObjectId(data["insta_user_id"])})
		if deleteM:
			deleteM.delete()
		else:
			content = {
               'status' : 1,
               'message' : 'Does Not Exist/Something Went Wrong',
               }
		getManage = db.Manage.find({"main_user_id":ObjectId(data["main_user_id"])})
		for x in getManage:
			manageArr.append({"main_user_id":str(x.main_user_id),"insta_user_id":str(x.insta_user_id),"name":str(x.name)})
	except Exception as e:
		print e
		content = {
               'status' : 1,
               'message' : 'Does Not Exist/Something Went Wrong',
               }
	# print data
	content["manageArr"] = manageArr
	return jsonify(content) 

# Return json


# Set the route and accepted methods
@event_link.route('/importManage/', methods=['POST'])
def importManage():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Update Success',
               }
	try:
		getGrowth=db.Growth.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["from_insta_user_id"]))})
		if getGrowth:
			updateData={
					"max_follow_limit":getGrowth.max_follow_limit,
			        "follow_private":getGrowth.follow_private,
			        "gender":getGrowth.gender,
			        "profile_pic":getGrowth.profile_pic,
			        "business_account":getGrowth.business_account,
			        "min_post":getGrowth.min_post,
			        "max_post":getGrowth.max_post,
			        "min_following":getGrowth.min_following,
			        "max_following":getGrowth.max_following,
			        "min_follower":getGrowth.min_follower,
			        "max_follower":getGrowth.max_follower
			}
			updateGrowth=db.Orders.find_and_modify({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},
												{
													"$set":updateData
													})
		getEngage=db.Engagement.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["from_insta_user_id"]))})
		if getEngage:
			updateData1={
					"min_like":getEngage.min_like,
			        "max_like":getEngage.max_like
			}
			updateEng=db.Engagement.find_and_modify({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},
												{
													"$set":updateData1
													})
	except Exception as e:
		content = {
               'status' : 0,
               'message' : 'Something Went Wrong',
               'message2': str(e)
               }
	# print data
	return jsonify(content) 

# Return json 





# Login In  API starts
# This will take input insta user id
@event_link.route('/fetchUserMedia/', methods=['POST'])
def fetchUserMedia():
	data = request.json
	result = {}
	instaid = ""
	totalMedia = 0
	content = {"status":1,
				"message":"Success"}
	# data="test"
	# Custom Validation starts #
	try:
		checkuser=db.UserInsta.find_one({"_id":ObjectId(data["insta_user_id"])})
		api = InstagramAPI(checkuser.user_name,checkuser.password)
		api.login()
		getMedia=fetchDirect.getMedia(api)
		for x in getMedia:
			checkPost=db.MyPost.find_one({"media_id":str(x['pk'])})
			if checkPost:
				pass
			else:
				userObj = db.MyPost()
				userObj.main_user_id = ObjectId(str(data['main_user_id']))
				userObj.insta_user_id = ObjectId(str(data['insta_user_id']))
				userObj.media_id = str(x['pk'])
				userObj.image = str(x['image_versions2']['candidates'][1]["url"])
				userObj.comment = str(x['comment_count'])
				userObj.like = str(x['like_count'])
				userObj.type = str(x['media_type'])
				userObj.status = 1
				userObj.date_created = constants.FORMATTED_TIME()
				userObj.date_updated = constants.FORMATTED_TIME()
				userObj.save()
		
		fetchFollowers=fetchDirect.getFollowers(checkuser.user_id,api)
		for y in fetchFollowers:
			check=db.Followers.find_one({"insta_user_id":ObjectId(data["insta_user_id"]),"user_id":str(x['pk'])})
			if check:
				pass
			else:
				userObj = db.Followers()
				userObj.main_user_id = ObjectId(str(data['main_user_id']))
				userObj.insta_user_id = ObjectId(str(data['insta_user_id']))
				userObj.full_name = str(y['username'])
				userObj.profile_pic_url = str(y['profile_pic_url'])
				userObj.user_id = str(y['pk'])
				userObj.user_name = str(y['username'])
				userObj.is_private = str(y['is_private'])
				userObj.is_verified = str(y['is_verified'])
				userObj.status = 3
				userObj.date_created = constants.FORMATTED_TIME()
				userObj.date_updated = constants.FORMATTED_TIME()
				userObj.save()
	
	except Exception as e:
		pass
		result={
		"status":0,
		"message":str(e)
		}
	content["data"] = result
	return jsonify(content)




# Login In  API starts
# This will take input insta user id
@event_link.route('/checkFollowback/', methods=['POST'])
def checkFollowback():
	data = request.json
	result = {}
	instaid = ""
	totalMedia = 0
	content = {"status":1,
				"message":"Success"}
	# data="test"
	# Custom Validation starts #
	try:
		fetchFollowers=fetchDirect.getFollowers(checkuser.user_id,api)
		for y in fetchFollowers:
			check=db.Followers.find_one({"insta_user_id":ObjectId(data["insta_user_id"]),"user_id":str(x['pk'])})
			if check:
				pass
			else:
				userObj = db.Followers()
				userObj.main_user_id = ObjectId(str(data['main_user_id']))
				userObj.insta_user_id = ObjectId(str(data['insta_user_id']))
				userObj.full_name = str(y['username'])
				userObj.profile_pic_url = str(y['profile_pic_url'])
				userObj.user_id = str(y['pk'])
				userObj.user_name = str(y['username'])
				userObj.is_private = str(y['is_private'])
				userObj.is_verified = str(y['is_verified'])
				userObj.status = 1
				userObj.date_created = constants.FORMATTED_TIME()
				userObj.date_updated = constants.FORMATTED_TIME()
				userObj.save()
		
	
	except Exception as e:
		pass
		result={
		"status":0,
		"message":str(e)
		}
	content["data"] = result
	return jsonify(content)



