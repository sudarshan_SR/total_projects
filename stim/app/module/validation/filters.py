__author__ = 'Kunal Monga'
# Import flask dependencies
from flask import Blueprint, request, jsonify


# Import the database object from the main app module
from app import db, constants, dbsql, functions
import json, urllib
from app.model import model
from app.module.validation import datalist, valid
from bson.objectid import ObjectId

# Here we will create all validation for the complete structure.

# def entryExitFilter(data):
# 	try:
# 		content={
# 					"pass":1
# 				}
# 		userId = constants.checkInt(data['user_id']);
# 		if data['notif_type'] == 'place_entry':
# 			getTracker = db.EntryExitTracker.find({"user_id": userId}).sort([("_id", -1)]).limit(1)
# 			try:
# 				# this throws error if not put a else condition
# 				if getTracker.count() >= 1 and getTracker[0].entry==1 and getTracker[0].exit==0 and str(getTracker[0].place_id)==str(data['place_id']):
# 					updateTracker = db.EntryExitTracker.find_and_modify({"_id":ObjectId(getTracker[0]._id)},
# 												{"$set":{
# 														"timestamp":constants.getTimeStamp()
# 														}
# 												})
# 					content={
# 							"pass":0
# 						}
# 				else:
# 					createTrack=db.EntryExitTracker()
# 					createTrack.user_id = userId
# 					createTrack.place_id = ObjectId(data['place_id'])
# 					createTrack.entry = 1
# 					createTrack.exit = 0
# 					createTrack.timestamp = int(constants.getTimeStamp())
# 					createTrack.save()
# 			except Exception as e:
# 				print str(e)
# 				pass

# 		elif data['notif_type'] == 'place_exit':
# 			getTracker = db.EntryExitTracker.find({"user_id": userId}).sort([("_id", -1)]).limit(1)
# 			try:
# 				# this throws error if not put a else condition
# 				if getTracker.count() >= 1 and getTracker[0].entry==1 and getTracker[0].exit==0 and str(getTracker[0].place_id)!=str(data['place_id']):
# 					objLocation=db.Places.find_one({"_id":ObjectId(data["place_id"])})
# 					location={"latPlace":objLocation.loc_lat,"lonPlace":objLocation.loc_lon,"latLocation":data["loc_lat"],"lonLocation":data["loc_lon"]}
# 					# resultDis=functions.checkLocation(location)
# 					# if int(resultDis) > 600: 
# 					updateTracker = db.EntryExitTracker.find_and_modify({"_id":ObjectId(getTracker[0]._id)},
# 										{"$set":{
# 												"exit":1,
# 												"timestamp":constants.getTimeStamp()
# 												}
# 										})					
# 				elif getTracker.count() >= 1 and getTracker[0].entry==1 and getTracker[0].exit==1 and str(getTracker[0].place_id)==str(data['place_id']):
# 					content={
# 							"pass":0
# 						}
# 				else:
# 					createTrack=db.EntryExitTracker()
# 					createTrack.user_id = userId
# 					createTrack.place_id = ObjectId(data['place_id'])
# 					createTrack.entry = 1
# 					createTrack.exit = 1
# 					createTrack.timestamp = int(constants.getTimeStamp())
# 					createTrack.save()

# 			except Exception as e:
# 				print str(e)



# 		return content
	

# 	except ValueError, e:
# 		content={
# 					"pass":0
# 				}
# 		print str(e)
# 		return content
# 	except Exception, e:
# 		content={
# 					"pass":0
# 				}
# 		print str(e)
# 		return content


