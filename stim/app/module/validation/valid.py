__author__ = 'Kunal Monga'


# Import the database object from the main app module
from app import db, constants, functions
import json, urllib, re
from app.model import model
from app.module.validation import datalist, msgtext
from bson.objectid import ObjectId


def checkInt(to_check):
	try:
		content={
				"status":0,
				"message":"Something went wrong.",
				"debug":"",
				"value":""
				}
		value = int(to_check)
		content['message'] = msgtext.intValid
		content['value'] = value
		content['status'] = 1
	except ValueError, e:
		print str(e)
		content['message'] = msgtext.intInValid
		content['debug'] = "Debug "+str(e)
	except TypeError, e:
		print str(e)
		content['message'] = msgtext.intInValid
		content['debug'] = "Debug "+str(e)
	except Exception, e:
		print str(e)
		content['message'] = msgtext.intInValid
		content['debug'] = "Debug "+str(e)
	return content

def checkIntIfNotNull(to_check):
	if to_check.strip() != "":
		try:
			content={
					"status":0,
					"message":"Something went wrong.",
					"debug":"",
					"value":""
					}
			value = int(to_check)
			content['message'] = msgtext.intValid
			content['value'] = value
			content['status'] = 1
		except ValueError, e:
			print str(e)
			content['message'] = msgtext.intInValid
			content['debug'] = "Debug "+str(e)
		except TypeError, e:
			print str(e)
			content['message'] = msgtext.intInValid
			content['debug'] = "Debug "+str(e)
		except Exception, e:
			print str(e)
			content['message'] = msgtext.intInValid
			content['debug'] = "Debug "+str(e)
	else:
		content={
					"status":1,
					"message":"Empty Success",
					"debug":"",
					"value":""
					}
	return content

def notEmpty(data,key):
	content={
			"status":0,
			"message":"Something went wrong",
			"debug":"",
			"value":""
			}
	if data.strip() != "" and data is not None:
		content['message'] = msgtext.success
		content['value'] = data
		content['status'] = 1
	else:
		content['message'] = key.title()+msgtext.isEmpty
		content['debug'] = "Debug"
	return content

def checkEmailExist(email):
	try:
		content={
				"status":0,
				"message":"Something went wrong",
				"debug":"",
				"value":""
				}
		gpObj = db.Users.find_one({"email": str(email)})
		if gpObj is not None:
			content['message'] = msgtext.success
			content['value'] = gpObj.email
			content['status'] = 1
		else:
			content['message'] = msgtext.emailDoesNtExist
			content['status'] = 0
	except Exception, e:
		print str(e)
		content['message'] = msgtext.emailDoesNtExist
		content['debug'] = "Debug"+str(e)
	return content

def checkEmailUnique(email):
	try:
		content={
				"status":0,
				"message":"Something went wrong",
				"debug":"",
				"value":""
				}
		gpObj = db.Users.find_one({"email": str(email)})
		if gpObj is not None:
			content['message'] = msgtext.emailAlreadyExist
			content['status'] = 0
		else:
			content['message'] = msgtext.emailAvailable
			content['status'] = 1
	except Exception, e:
		content['message'] = msgtext.emailAlreadyExist
		content['debug'] = "Debug"+str(e)
	return content

def checkPhoneUnique(phone):
	if phone.strip() != "":
		try:
			content={
					"status":0,
					"message":"Something went wrong",
					"debug":"",
					"value":""
					}
			gpObj = db.Users.find_one({"phone": str(phone)})
			if gpObj is not None:
				content['message'] = msgtext.phoneAlreadyExist
				content['status'] = 0
			else:
				content['message'] = msgtext.phoneAvailable
				content['status'] = 1
		except Exception, e:
			content['message'] = msgtext.phoneAlreadyExist
			content['debug'] = "Debug"+str(e)
	else:
		content={
					"status":1,
					"message":"Phone is null",
					"debug":"",
					"value":""
					}
	return content

def checkPhoneExist(phone):
	try:
		content={
				"status":0,
				"message":"Something went wrong",
				"debug":"",
				"value":""
				}
		gpObj = db.Users.find_one({"phone": str(phone)})
		if gpObj is not None:
			content['message'] = msgtext.success
			content['value'] = gpObj.name
			content['status'] = 1
		else:
			content['message'] = msgtext.phoneDoesNtExist
			content['status'] = 0
	except Exception, e:
		print str(e)
		content['message'] = msgtext.phoneDoesNtExist
		content['debug'] = "Debug"+str(e)
	return content

def checkAffilEmailUnique(email):
	try:
		content={
				"status":0,
				"message":"Something went wrong",
				"debug":"",
				"value":""
				}
		gpObj = db.Affiliates.find_one({"email": str(email)})
		if gpObj is not None:
			content['message'] = msgtext.emailAlreadyExist
			content['status'] = 0
		else:
			content['message'] = msgtext.emailAvailable
			content['status'] = 1
	except Exception, e:
		content['message'] = msgtext.emailAlreadyExist
		content['debug'] = "Debug"+str(e)
	return content

def checkAffilPhoneUnique(phone):
	if phone.strip() != "":
		try:
			content={
					"status":0,
					"message":"Something went wrong",
					"debug":"",
					"value":""
					}
			gpObj = db.Affiliates.find_one({"phone": str(phone)})
			if gpObj is not None:
				content['message'] = msgtext.phoneAlreadyExist
				content['status'] = 0
			else:
				content['message'] = msgtext.phoneAvailable
				content['status'] = 1
		except Exception, e:
			content['message'] = msgtext.phoneAlreadyExist
			content['debug'] = "Debug"+str(e)
	else:
		content={
					"status":1,
					"message":"Phone is null",
					"debug":"",
					"value":""
					}
	return content