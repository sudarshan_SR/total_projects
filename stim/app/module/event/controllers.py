f__author__ = 'Kunal Monga'
# Import flask dependencies
from flask import Blueprint, request, jsonify, Response

# Import the database object from the main app module
from app import db, constants, functions, fetch, fetchDirect, APP_STATIC, fetchInsta, sendMail
import json, urllib, datetime, email, re, hashlib, random, string, urllib, os, pdfkit, time
from bs4 import BeautifulSoup
from multiprocessing import Process
from random import randint,uniform
from app.model import model
from bson.objectid import ObjectId
from InstagramAPI import InstagramAPI
from app.module.validation import validate, msgtext

# Define the blueprint: 'auth', set its url prefix: app.url/auth
event_link = Blueprint('event', __name__, url_prefix='/event')



@event_link.route('/testevent/', methods=['POST'])
def testevent():
	data = request.json
	content = {
               'status' : 1,
               'message' : data,
               }
	sock = urllib.urlopen(os.path.join(APP_STATIC, 'trial_expire_Soon.html'))
	htmlSource = sock.read()
	sock.close()
	subject = "Kibi- Account Plan expiry"
	res = sendMail.sendEmail("support@kibisocial.com",str("anand.simplifyreality@gmail.com"),subject,"THis is test message","")
	content["res"]=res
	# resultLogin = api.LastJson
	return jsonify(content)

@event_link.route('/testGetUser/', methods=['GET'])
def testGetUser():
	content = {
               'status' : 1,
               'message' : 'Success',
               }
	
	# resultLogin = api.LastJson
	EmailList=[]
	getUser = db.Users.find({})
	for x in getUser:
		getUserInsta = db.UserInsta.find({"main_user_id":x._id})
		print getUserInsta.count()
		if getUserInsta.count() <= 0:
			EmailList.append(x.email)
	content['EmailList']=EmailList
	return jsonify(content) 

@event_link.route('/testevent1/', methods=['GET'])
def testevent1():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	api = InstagramAPI('Adventure_Republic', 'Peru2011#')
	return jsonify(content) 

# @event_link.route('/mt', methods=['POST'])
# def muteTest():
# 	print request.json
# 	oj=db.UserInsta.find_one({"_id":ObjectId(request.json['insta_user_id'])})
# 	for i in oj:
# 		print i , oj[i]
# 	ojs=db.Growth.find_one({"insta_user_id":oj._id})
# 	for i in ojs:
# 		print i , ojs[i]
# 	data={}
# 	data['mute']='1'
# 	# fol=db.Followed.find_one({'to_username':'claudiaf0885'})
# 	# for i in fol:
# 	# 	print i , fol[i]
# 	data['user_id']='8131406191'
# 	data['username']='bharatbgarg4'
# 	data['password']='insta1234'
# 	# fetchInsta.follow(data)
# 	print 'fetchInsta run'
# 	print fetchInsta.unfollow(data)
# 	return jsonify(1)

@event_link.route('/muteEvent/<status>', methods=['POST'])
def muteEvent(status):
	oj=db.UserInsta.find_one({"_id":ObjectId(request.json['insta_user_id'])})
	db.Growth.find_and_modify({"insta_user_id":oj._id},{"$set":{'mute_status':int(status)}})
	return jsonify({'Success':1})

# Set the route and accepted methods
@event_link.route('/selectEvent/', methods=['GET'])
def selectEvent():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	# getUser = db.UserInsta.find({"_id":ObjectId("5c0383ed44be8811cbcec43e")})
	# getUser = db.UserInsta.find({"date_performed":{"$lte": constants.LATER_MIN(60)},"status":int(1)}).limit(100)
	getUser = db.UserInsta.find({"status":int(1)})

	target={
	'data':{}
	} 
	user_list=[]
	result={}
	print getUser.count()
	for user in getUser:
		print user._id
		updateUserInsta=db.UserInsta.find_and_modify({"_id":ObjectId(str(user._id))},
											{
												"$set":{'date_performed':constants.FORMATTED_TIME()}
												})
		user_list.append(user.user_name)
		checkCurr = db.Currently.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id,'status':1})
		if checkCurr:
			getPlan = db.MyPlan.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id,"pla_end_date": {"$gte": constants.FORMATTED_TIME()}})
			if getPlan:
				sleeping = functions.checkSleeping(user.main_user_id,user._id)
				if not sleeping:
					checkLastInstance = db.Actions.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"date_created": {"$gt": constants.LATER_MIN(randint(18, 24))}})
					if checkLastInstance.count() == 0:
						getLimit = db.Growth.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id})
						if getLimit and int(getLimit.max_follow_limit) > 0:
							checkLimit = db.Followed.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,'status':1}).count()
							if int(getLimit.max_follow_limit) <= checkLimit:
								resCurChange = functions.changeCurrently(user.main_user_id,user._id,0)
							else:
								# Add Event Here
								resEvent=functions.addMyEvent(user.main_user_id,user._id,"1","Account started Following","Your account started following users based on the targets and settings you selected.","fas fa-arrow-up arrowup")
								# Event Added
								getPlanDetail = db.Plans.find_one({"_id":getPlan.plan_id})
								counting=["1","2","3"]
								Account=[]
								Hashtag=[]
								Location=[]
								for x in range(1, 4):
									getData=db.Target.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"type":str(x),'status':1})
									if getData.count() > 0:
										countFinal=3
										for getTargets in getData:
											createDump = {"target_id":getTargets.user_id,"my_user_id":user.user_id,"user_name":getTargets.user_name}
											if getTargets.type=="1":
												Account.append(createDump)
												if "1" in counting: counting.remove("1")
											if getTargets.type=="2":
												Hashtag.append(createDump)
												if "2" in counting: counting.remove("2")
											if getTargets.type=="3":
												Location.append(createDump)
												if "3" in counting: counting.remove("3")
										countFinal = countFinal - len(counting)
										target["data"]={
														"account":Account,
														"hashtag":Hashtag,
														"location":Location
														}
								if target["data"]:
									myProcess = Process(target=functions.startFollowing, args=(user.main_user_id,user._id,user.user_id, target['data'], countFinal, getPlanDetail.max_follow, getPlanDetail.plan_mute_allowed, getLimit))
									result = myProcess.start()
									# result=functions.startFollowing(user.main_user_id,user._id,user.user_id, target['data'], countFinal, getPlanDetail.max_follow,getLimit)

					else:
						content["message"]="Can not take action at this time"	
				else:
						content["message"]="Sleeping Now"
		else:
			content["message"]="Currently Un Following"			
	result={'data':user_list}
	content['result']=result
	return jsonify(content) 


# Set the route and accepted methods
@event_link.route('/selectEventTest/', methods=['POST'])
def selectEventTest():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getUser = db.UserInsta.find({"user_name":data['user_name'],"status":int(1)})
	# getUser = db.UserInsta.find({"date_performed":{"$lte": constants.LATER_MIN(60)},"status":int(1)}).limit(100)
	target={
	'data':{}
	} 
	user_list=[]
	result={}
	for user in getUser:
		updateUserInsta=db.UserInsta.find_and_modify({"_id":ObjectId(str(user._id))},
											{
												"$set":{'date_performed':constants.FORMATTED_TIME()}
												})
		user_list.append(user.user_name)
		checkCurr = db.Currently.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id,'status':1})
		if checkCurr:
			getPlan = db.MyPlan.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id,"pla_end_date": {"$gte": constants.FORMATTED_TIME()}})
			if getPlan:
				sleeping = functions.checkSleeping(user.main_user_id,user._id)
				if 1:
					checkLastInstance = db.Actions.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"date_created": {"$gt": constants.LATER_MIN(randint(1, 2))}})
					print 'checkLastInstance.count()'
					print checkLastInstance.count()
					if checkLastInstance.count() == 0:
						getLimit = db.Growth.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id})
						print 'getLimit'
						print getLimit
						if getLimit and int(getLimit.max_follow_limit) > 0:
							checkLimit = db.Followed.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,'status':1}).count()
							if int(getLimit.max_follow_limit) <= checkLimit:
								resCurChange = functions.changeCurrently(user.main_user_id,user._id,0)
							else:
								# Add Event Here
								resEvent=functions.addMyEvent(user.main_user_id,user._id,"1","Account started Following","Your account started following users based on the targets and settings you selected.","fas fa-arrow-up arrowup")
								# Event Added
								getPlanDetail = db.Plans.find_one({"_id":getPlan.plan_id})
								counting=["1","2","3"]
								Account=[]
								Hashtag=[]
								Location=[]
								for x in range(1, 4):
									print x
									getData=db.Target.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"type":str(x),'status':1})
									print getData
									if getData.count() > 0:
										countFinal=3
										for getTargets in getData:
											createDump = {"target_id":getTargets.user_id,"my_user_id":user.user_id,"user_name":getTargets.user_name}
											if getTargets.type=="1":
												Account.append(createDump)
												if "1" in counting: counting.remove("1")
											if getTargets.type=="2":
												Hashtag.append(createDump)
												if "2" in counting: counting.remove("2")
											if getTargets.type=="3":
												Location.append(createDump)
												if "3" in counting: counting.remove("3")
										countFinal = countFinal - len(counting)
										target["data"]={
														"account":Account,
														"hashtag":Hashtag,
														"location":Location
														}
								print "target['data']"
								print target['data']
								if target["data"]:
									print 'starting follow'
									myProcess = Process(target=functions.startFollowing, args=(user.main_user_id,user._id,user.user_id, target['data'], countFinal, getPlanDetail.max_follow, getPlanDetail.plan_mute_allowed, getLimit))
									result = myProcess.start()
									# result=functions.startFollowing(user.main_user_id,user._id,user.user_id, target['data'], countFinal, getPlanDetail.max_follow,getLimit)

					else:
						content["message"]="Can not take action at this time"	
				else:
						content["message"]="Sleeping Now"
		else:
			content["message"]="Currently Un Following"			
	result={'data':user_list}
	content['result']=result
	return jsonify(content) 



# Set the route and accepted methods
@event_link.route('/selectEventUnfollow/', methods=['GET'])
def selectEventUnfollow():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getUser = db.UserInsta.find({"$or":[{"status":1},{"status":2}]})
	target={}
	result={}
	for user in getUser:
		updateUserInsta=db.UserInsta.find_and_modify({"_id":ObjectId(str(user._id))},
											{
												"$set":{'date_performed':constants.FORMATTED_TIME()}
												})
		checkCurr = db.Currently.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id,'status':0})
		if checkCurr:
			getPlan = db.MyPlan.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id,"pla_end_date": {"$gte": constants.FORMATTED_TIME()}})
			if getPlan:
				sleeping = functions.checkSleeping(user.main_user_id,user._id)
				if not sleeping:
					checkLastInstance = db.Actions.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"date_created": {"$gt": constants.LATER_MIN(randint(18, 25))}})
					if checkLastInstance.count() == 0:
						getLimit = db.Unfollow.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id})
						getWhitelist = db.Whitelist.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,'status':1}).count()
						totalLimit = getWhitelist+int(getLimit.limit)
						data = {"username": user.user_name ,"password": user.password}
						resultDetail = fetchInsta.getUInstaDetail((data))
						if resultDetail['status']==1 and int(totalLimit) >= 0:
							# checkLimit = db.UserInsta.find_one({"main_user_id":user.main_user_id,"_id":user._id,"status":1})
							checkLimit=int(resultDetail['following_count'])
							getPlanDetail = db.Plans.find_one({"_id":getPlan.plan_id})
							if totalLimit >= int(int(checkLimit)-int(getPlanDetail.max_follow)):
								resCurChange = functions.changeCurrently(user.main_user_id,user._id,1)
							else:	
								# Add Event Here
								resEvent=functions.addMyEvent(user.main_user_id,user._id,"2","Account started Un Following","Your account started Un following users based on the targets and settings you selected.","fas fa-arrow-down arrowup")
								# Event Added

								# print getPlanDetail.max_follow
								myProcess = Process(target=functions.startUnfollowing, args=(user.main_user_id,user._id,user.user_id, int(getPlanDetail.max_follow)))
								result = myProcess.start()
								# result=functions.startUnfollowing(user.main_user_id,user._id,user.user_id, int(getPlanDetail.max_follow))

					else:
						content["message"]="Can not take action at this time"
		else:
			content["message"]="Currently Following"		
	content['result']=result
	return jsonify(content) 



# Set the route and accepted methods
@event_link.route('/getFollowings/', methods=['POST'])
def getFollowings():
	data = request.json
	result = {}
	resultfetch = []
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getUser = db.UserInsta.find_one({"_id":ObjectId(data['insta_user_id'])})
	if getUser:
		data["username"]=getUser.user_name
		data["password"]=getUser.password
		result = fetchInsta.countUserFollowings((data))
	   	resultfetch = result['search_result']
		updateData={
					"following":result['count']
			}
		updateGrowth=db.UserInsta.find_and_modify({"_id":ObjectId(str(data["insta_user_id"]))},
												{
													"$set":updateData
													})
	content['result']=resultfetch
	return jsonify(content) 



# Set the route and accepted methods
@event_link.route('/likeEvent/', methods=['GET'])
def likeEvent():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getUser = db.UserInsta.find({"$or":[{"status":1},{"status":2}]})
	target={}
	result={}
	planUse=[]
	forPlan=db.Plans.find({"max_like":{"$ne":"0"}})
	for x in forPlan:
		planUse.append(x._id)
	for user in getUser:
		getPlan = db.MyPlan.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id,"plan_id":{"$in":planUse},"plan_end_date": {"$gte": constants.FORMATTED_TIME()}})
		if getPlan:
			getPlanDetail = db.Plans.find_one({"_id":getPlan.plan_id})
			if getPlanDetail.engagement=="1":
				checkLastInstance = db.Actions.find({"action_type":"like","main_user_id":user.main_user_id,"insta_user_id":user._id,"date_created": {"$gt": constants.LATER_MIN(randint(400, 600))}})
				if checkLastInstance.count() == 0:
					sourceGenerate = []
					getRules = db.Engagement.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id})
					# getPlanDetail = db.Plans.find_one({"_id":getPlan.plan_id})
					totalHits = (int(getPlanDetail.max_like)*int(getRules.speed))/2
					# Add Event Here
					resEvent=functions.addMyEvent(user.main_user_id,user._id,"5","Account started Liking","You started liking posts.","fas fa-arrow-up arrowup")
					# Event Added
					if getRules.source=="0":
						getFollowed=db.Followed.find({"status":1,"main_user_id":user.main_user_id,"insta_user_id":user._id})
						for datafol in getFollowed:
							sourceGenerate.append({"userid":datafol.to_userid,"username":datafol.to_username})
					elif getRules.source=="1":
						getFollowed=db.Followed.find({"status":1,"main_user_id":user.main_user_id,"insta_user_id":user._id})
						for datafol in getFollowed:
							sourceGenerate.append({"userid":datafol.to_userid,"username":datafol.to_username})
					elif getRules.source=="2":
						getWhitelist=db.Whitelist.find({"main_user_id":user.main_user_id,"insta_user_id":user._id})
						for datawhite in getWhitelist:
							sourceGenerate.append({"userid":datawhite.user_id,"username":datafol.to_username})
					elif getRules.source=="3":
						getFollowed=db.Followed.find({"status":1,"main_user_id":user.main_user_id,"insta_user_id":user._id})
						for datafol in getFollowed:
							sourceGenerate.append({"userid":datafol.to_userid,"username":datafol.to_username})
						# # list1 = [10, 15, 20, 25, 30, 35, 40]
						# # list2 = [25, 40, 35]
						# # print(Diff(list1, list2))
						# sourceGenerateTemp1=[]
						# sourceGenerateTemp2=[]
						# getFollowed=db.Followed.find({"status":1,"main_user_id":user.main_user_id,"insta_user_id":user._id})
						# for datafol in getFollowed:
						# 	sourceGenerateTemp1.append(1)
						# getWhitelist=db.Whitelist.find({"main_user_id":user.main_user_id,"insta_user_id":user._id})
						# for datawhite in getWhitelist:
						# 	sourceGenerateTemp2.append(1)
						# sourceGenerate = sourceGenerateTemp1 - sourceGenerateTemp2
					if sourceGenerate:
						# myProcess = Process(target=functions.startLiking, args=(user.main_user_id,user._id,totalHits,getRules.min_like,getRules.max_like,getRules.source,sourceGenerate))
						# result = myProcess.start()
						result=functions.startLiking(user.main_user_id,user._id,totalHits,getRules.min_like,getRules.max_like,getRules.source,sourceGenerate)

	content['result']=result
	return jsonify(content) 




# Login In  API Ends
# Return json 


# Set the route and accepted methods
@event_link.route('/followEvent/', methods=['POST'])
def followEvent():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getdata = fetch.getUserFollows('2985975523.425bd29.62c21c3ff5b04444a29ec77f046844ba','2985975523')
	# print data
	content['name'] = getdata
	return jsonify(content) 

# Return json 



# Login In  API Ends
# Return json 


# Set the route and accepted methods
@event_link.route('/addManage/', methods=['POST'])
def addManage():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	try:
		checkPost=db.Manage.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))})
		if checkPost:
			content = {
               'status' : 0,
               'message' : 'Already Exist',
               }
		else:
			userObj = db.Manage()
			userObj.main_user_id = ObjectId(str(data['main_user_id']))
			userObj.insta_user_id = ObjectId(str(data['insta_user_id']))
			userObj.name = str(data['name'])
			userObj.date_created = constants.FORMATTED_TIME()
			userObj.date_updated = constants.FORMATTED_TIME()
			userObj.save()
	except Exception as e:
		print e
	# print data
	return jsonify(content) 

# Return json 


# Set the route and accepted methods
@event_link.route('/getManage/', methods=['POST'])
def getManage():
	data = request.json
	manageArr = []
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	try:
		getManage = db.Manage.find({"main_user_id":ObjectId(data["main_user_id"])})
		for x in getManage:
			checkuser=db.UserInsta.find_one({"_id":ObjectId(x.insta_user_id)})
			if checkuser:
				manageArr.append({"main_user_id":str(x.main_user_id),"insta_user_id":str(x.insta_user_id),"name":str(x.name)})

	except Exception as e:
		print e
	# print data
	content["manageArr"] = manageArr
	return jsonify(content) 

# Return json 


# Set the route and accepted methods
@event_link.route('/deleteManage/', methods=['POST'])
def deleteManage():
	data = request.json
	manageArr = []
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	try:
		deleteM = db.Manage.find_one({"main_user_id":ObjectId(data["main_user_id"]),"insta_user_id":ObjectId(data["insta_user_id"])})
		if deleteM:
			deleteM.delete()
		else:
			content = {
               'status' : 1,
               'message' : 'Does Not Exist/Something Went Wrong',
               }
		getManage = db.Manage.find({"main_user_id":ObjectId(data["main_user_id"])})
		for x in getManage:
			manageArr.append({"main_user_id":str(x.main_user_id),"insta_user_id":str(x.insta_user_id),"name":str(x.name)})
	except Exception as e:
		print e
		content = {
               'status' : 1,
               'message' : 'Does Not Exist/Something Went Wrong',
               }
	content["manageArr"] = manageArr
	return jsonify(content) 

# Return json


# Set the route and accepted methods
@event_link.route('/importManage/', methods=['POST'])
def importManage():
	data = request.json
	content = {
               'status' : 1,
               'message' : 'Update Success',
               }
	try:
		getGrowth=db.Growth.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["from_insta_user_id"]))})
		if getGrowth:
			updateData={
					"max_follow_limit":getGrowth.max_follow_limit,
			        "follow_private":getGrowth.follow_private,
			        "gender":getGrowth.gender,
			        "profile_pic":getGrowth.profile_pic,
			        "business_account":getGrowth.business_account,
			        "min_post":getGrowth.min_post,
			        "max_post":getGrowth.max_post,
			        "min_following":getGrowth.min_following,
			        "max_following":getGrowth.max_following,
			        "min_follower":getGrowth.min_follower,
			        "max_follower":getGrowth.max_follower
			}
			updateGrowth=db.Growth.find_and_modify({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},
												{
													"$set":updateData
													})
		getEngage=db.Engagement.find_one({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["from_insta_user_id"]))})
		if getEngage:
			updateData1={
					"min_like":getEngage.min_like,
			        "max_like":getEngage.max_like
			}
			updateEng=db.Engagement.find_and_modify({"main_user_id":ObjectId(str(data["main_user_id"])),"insta_user_id":ObjectId(str(data["insta_user_id"]))},
												{
													"$set":updateData1
													})
	except Exception as e:
		content = {
               'status' : 0,
               'message' : 'Something Went Wrong',
               'message2': str(e)
               }
	# print data
	return jsonify(content) 

# Return json 

# Login In  API starts
# This will take input insta user id
@event_link.route('/fetchUserMedia/', methods=['POST'])
def fetchUserMedia():
	data = request.json
	res=[]
	result = {}
	content = {"status":1,
				"message":"Success"}
	# res=fetchUserMediaAsync(data)
	p = Process(target=fetchUserMediaAsync, args=(data,))
	p.start()
	content["data"] = res
	return jsonify(content)


def fetchUserMediaAsync(data):
	data = data
	result = {}
	updateData={}
	updateInstaData={}
	instaid = ""
	totalMedia = 0
	content = {"status":1,
				"message":"Success"}
	# data="test"
	# Custom Validation starts #
	try:
		checkuser=db.UserInsta.find_one({"_id":ObjectId(data["insta_user_id"])})
		data["username"]=checkuser.user_name
		data["password"]=checkuser.password
		result = fetchInsta.getMedia((data))
   		if 'search_result' in result:
	   		getMedia = result['search_result']
	   		i=0
			for x in getMedia:
				checkPost=db.MyPost.find_one({"insta_user_id":ObjectId(data["insta_user_id"]),"media_id":str(x['pk'])})
				if checkPost:
					updateData={
						"comment": str(x['comment_count']),
						"like": str(x['like_count'])
					}
					updating=db.MyPost.find_and_modify({"insta_user_id":ObjectId(data["insta_user_id"]),"media_id":str(x['pk'])},
													{
														"$set":updateData
													})
					
				else:
					i+=1
					userObj = db.MyPost()
					userObj.main_user_id = ObjectId(str(data['main_user_id']))
					userObj.insta_user_id = ObjectId(str(data['insta_user_id']))
					userObj.media_id = str(x['pk'])
					if x['media_type']==1:
						if 'image_versions2' in x:
							userObj.image = str(x['image_versions2']['candidates'][1]["url"])
						elif 'carousel_media' in x:
							if 'image_versions2' in x['carousel_media'][0]:
								userObj.image = str(x['carousel_media'][0]['image_versions2']['candidates'][1]["url"])
						else:
							userObj.image = ""
					userObj.comment = str(x['comment_count'])
					userObj.like = str(x['like_count'])
					userObj.type = str(x['media_type'])
					userObj.status = 1
					userObj.date_created = constants.FORMATTED_TIME()
					userObj.date_updated = constants.FORMATTED_TIME()+datetime.timedelta(0, i)
					userObj.save()

		resultFol = fetchInsta.getFollowers((data))
   		if 'search_result' in resultFol:
			fetchFollowers = resultFol['search_result']
			for y in fetchFollowers:
				check = db.Followers.find({"insta_user_id":ObjectId(data["insta_user_id"]),"user_id":str(y['pk'])})
				if check.count() > 0:
					pass
				else:
					userObj = db.Followers()
					userObj.main_user_id = ObjectId(str(data['main_user_id']))
					userObj.insta_user_id = ObjectId(str(data['insta_user_id']))
					userObj.full_name = str(y['username'])
					userObj.profile_pic_url = str(y['profile_pic_url'])
					userObj.user_id = str(y['pk'])
					userObj.user_name = str(y['username'])
					userObj.is_private = str(y['is_private'])
					userObj.is_verified = str(y['is_verified'])
					userObj.status = 3
					userObj.date_created = constants.FORMATTED_TIME()
					userObj.date_updated = constants.FORMATTED_TIME()
					userObj.save()
				
		resultDetail = fetchInsta.getUInstaDetail((data))			
	   	if resultDetail['status']==1:
			updateInstaData["follower_count"]=int(resultDetail['follower_count'])
			updateInstaData["following"]=int(resultDetail['following_count'])
			updateInstaData["posts"]=int(resultDetail['media_count'])
			updateInstaData["user_name"]=str(resultDetail['user_name'])
			updateGrowth=db.UserInsta.find_and_modify({"_id":ObjectId(str(data["insta_user_id"]))},
													{
														"$set":updateInstaData
														})
	except Exception as e:
		print "indiss"
		print e
		result={
		"status":0,
		"message":str(e)
		}
	content["data"] = result
	return content


# Login In  API starts
# This will take input insta user id
@event_link.route('/updateUserMedia/', methods=['POST'])
def updateUserMedia():
	data = request.json
	res=[]
	result = {}
	content = {"status":1,
				"message":"Success"}
	# res=fetchUserMediaAsync(data)
	p = Process(target=updateUserMediaAsync, args=(data,))
	p.start()
	# res=updateUserMediaAsync(data)
	content["data"] = res
	return jsonify(content)


def updateUserMediaAsync(data):
	data = data
	result = {}
	instaid = ""
	totalMedia = 0
	content = {"status":1,
				"message":"Success"}
	# data="test"
	# Custom Validation starts #
	try:
		getUser=db.Users.find_one({"_id":ObjectId(data["main_user_id"]),"$or":[{"status":0},{"status":1},{"status":2},{"status":4}]})
		if getUser:
			if 'insta_user_id' in data:
				checkusers=db.UserInsta.find({"_id":ObjectId(data["insta_user_id"]),"$or":[{"status":0},{"status":1},{"status":2},{"status":4}]})
			else:
				checkusers=db.UserInsta.find({"main_user_id":ObjectId(data["main_user_id"])})
			if checkusers.count():
				for checkuser in checkusers:
					updateInstaData={}				
					data["insta_user_id"]=str(checkuser._id)
					data["username"]=checkuser.user_name
					data["password"]=checkuser.password
					resultDetail = fetchInsta.getUInstaDetail((data))			
					if resultDetail['status']==1:
						updateInstaData["profile_pic_id"]=str(resultDetail['profile_pic_id'])
						updateInstaData["profile_pic_url"]=str(resultDetail['profile_pic_url'])
						updateInstaData["follower_count"]=int(resultDetail['follower_count'])
						updateInstaData["following"]=int(resultDetail['following_count'])
						updateInstaData["posts"]=int(resultDetail['media_count'])
						updateInstaData["user_name"]=str(resultDetail['user_name'])
						print "in"
						updateGrowth=db.UserInsta.find_and_modify({"_id":ObjectId(str(data["insta_user_id"]))},
							{
							"$set":updateInstaData
							})
					result = fetchInsta.getMedia((data))
					if 'search_result' in result:
				   		getMedia = result['search_result']
				   		checkPostlist=list(db.MyPost.find({"insta_user_id":ObjectId(data["insta_user_id"])}))
						#the below list comprehension  removes the already updated posts (list comprehension is very faster than looping)
					   	removelist=[getMedia.remove(y) if y['pk']==x['media_id'] and int(y['like_count'])==int(x['like']) and
								   	 int(y['comment_count'])==int(x['comment']) and y in getMedia else None for x in checkPostlist for y in getMedia ]							   		
						i=0
						#the below list comprehension  finds the new and not updated(we set status 1 for that) post(list comprehension is very faster than looping)
						updatelistData=[x.update(status=1) if x['pk']==y['media_id'] else {} for y in checkPostlist for x in getMedia]
						for x in getMedia:
							if 'status' in x and x['status']==1:
								updateData={
									"comment": str(x['comment_count']),
									"like": str(x['like_count']),
									"date_updated":constants.FORMATTED_TIME()+datetime.timedelta(0, i)
								}
								updating=db.MyPost.find_and_modify({"insta_user_id":ObjectId(data["insta_user_id"]),"media_id":str(x['pk'])},
																{
																	"$set":updateData
																})
							else:
								userObj = db.MyPost()
								userObj.main_user_id = ObjectId(str(data['main_user_id']))
								userObj.insta_user_id = ObjectId(str(data['insta_user_id']))
								userObj.media_id = str(x['pk'])
								if x['media_type']==1:
									if 'image_versions2' in x:
										userObj.image = str(x['image_versions2']['candidates'][1]["url"])
									elif 'carousel_media' in x:
										if 'image_versions2' in x['carousel_media'][0]:
											userObj.image = str(x['carousel_media'][0]['image_versions2']['candidates'][1]["url"])
									else:
										userObj.image = ""
								userObj.comment = str(x['comment_count'])
								userObj.like = str(x['like_count'])
								userObj.type = str(x['media_type'])
								userObj.status = 1
								userObj.date_created = constants.FORMATTED_TIME()
								userObj.date_updated = constants.FORMATTED_TIME()+datetime.timedelta(0, i)
								userObj.save()
							i+=1
					resultFol = fetchInsta.getFollowers((data))
					checkList = list(db.Followers.find({"insta_user_id":ObjectId(data["insta_user_id"])}))
					if 'search_result' in resultFol:
						fetchFollowers = resultFol['search_result']
						remove_follower_list=[checkList.remove(x) if y['pk']==x['user_id'] and y in fetchFollowers else None for y in fetchFollowers for x in checkList ]
						for obj in checkList:
							follower=db.Followers.find_one({"_id":ObjectId(obj._id)})
							if follower:
								if follower.status==1:
									follower.delete()
						for y in fetchFollowers:
							check = db.Followers.find({"insta_user_id":ObjectId(data["insta_user_id"]),"user_id":str(y['pk'])})
							if check.count() > 0:
								# checkList.remove(check[0])
								# print "pass"
								pass
							else:
								# print "inside"
								userObj = db.Followers()
								userObj.main_user_id = ObjectId(str(data['main_user_id']))
								userObj.insta_user_id = ObjectId(str(data['insta_user_id']))
								userObj.full_name = str(y['username'])
								userObj.profile_pic_url = str(y['profile_pic_url'])
								userObj.user_id = str(y['pk'])
								userObj.user_name = str(y['username'])
								userObj.is_private = str(y['is_private'])
								userObj.is_verified = str(y['is_verified'])
								userObj.status = 1
								userObj.date_created = constants.FORMATTED_TIME()
								userObj.date_updated = constants.FORMATTED_TIME()
								userObj.save()
	except Exception as e:
		print "indiss"
		print e
		result={
		"status":0,
		"message":str(e)
		}
	content["data"] = result
	return content

# Set the route and accepted methods
@event_link.route('/checkUsersProxy/', methods=['GET'])
def checkUsersProxy():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getUser = db.UserInsta.find({"$or":[{"status":0},{"status":1},{"status":2},{"status":4}]})
	result=[]
	if getUser.count():
		for user in getUser:
			data={}
			if user.status==1 or user.status==2 or user.status==0 or user.status==4:
				data["username"]=user.user_name
				data["password"]=user.password
				resultResponse = fetchInsta.getUInstaDetail((data))
				time.sleep(randint(1,2))
				if resultResponse['status']==3 or resultResponse['status']==2 or resultResponse['status']==4:
					data["main_user_id"]=user.main_user_id
					data["insta_user_id"]=user._id
					if resultResponse['status']==3:
						data["status"]=int(3)
					if resultResponse['status']==2:
						data["status"]=int(2)
					if resultResponse['status']==4:
						data["status"]=int(4)
					# res=functions.functionChangeInstaStatus(data)
					p = Process(target=functions.functionChangeInstaStatus, args=(data,))
					p.start()
					result.append(resultResponse)
				else:
					content['message']="Working well"
			else:
				content['message']="Already checked"
	content['result']=result
	return jsonify(content) 

# Set the route and accepted methods
@event_link.route('/checkUsersProxyNetwork/', methods=['GET'])
def checkUsersProxyNetwork():
	content = {
               'status' : 1,
               'message' : 'No Content',
               }
	getUser = db.UserInsta.find({})
	result=[]
	print getUser.count()
	if getUser.count():
		p = Process(target=functions.CheckProxyNetwork, args=(getUser,))
		p.start()
	content['result']=result
	content['len']=len(result)
	return jsonify(content)


# Login In  API starts
# This will take input insta user id
@event_link.route('/checkFollowback/', methods=['POST'])
def checkFollowback():
	data = request.json
	result = {}
	instaid = ""
	totalMedia = 0
	content = {"status":1,
				"message":"Success"}
	# data="test"
	# Custom Validation starts #
	try:
		checkuser=db.UserInsta.find_one({"_id":ObjectId(data["insta_user_id"])})
		data["username"]=checkuser.user_name
		data["password"]=checkuser.password
		resultFol = fetchInsta.getFollowers((data))
		fetchFollowers = resultFol['search_result']
		for y in fetchFollowers:
			check=db.Followers.find_one({"insta_user_id":ObjectId(data["insta_user_id"]),"user_id":str(y['pk'])})
			if check:
				pass
			else:
				userObj = db.Followers()
				userObj.main_user_id = ObjectId(str(data['main_user_id']))
				userObj.insta_user_id = ObjectId(str(data['insta_user_id']))
				userObj.full_name = str(y['username'])
				userObj.profile_pic_url = str(y['profile_pic_url'])
				userObj.user_id = str(y['pk'])
				userObj.user_name = str(y['username'])
				userObj.is_private = str(y['is_private'])
				userObj.is_verified = str(y['is_verified'])
				userObj.status = 1
				userObj.sms = 0
				userObj.date_created = constants.FORMATTED_TIME()
				userObj.date_updated = constants.FORMATTED_TIME()
				userObj.save()
		
	
	except Exception as e:
		pass
		result={
		"status":0,
		"message":str(e)
		}
	content["data"] = result
	return jsonify(content)




# Login In  API starts
# This will take input insta user id
@event_link.route('/sendMessage/', methods=['GET'])
def sendMessage():
	result=[]
	content = {"status":1,
				"message":"Success"}
	try:
		# api = InstagramAPI(checkuser.user_name,checkuser.password)
		# api.login()
		getUser = db.UserInsta.find({"status":int(1)})
		for user in getUser:
			getPlan = db.MyPlan.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id,"pla_end_date": {"$gte": constants.FORMATTED_TIME()}})
			if getPlan:
				getPlanDetail = db.Plans.find_one({"_id":getPlan.plan_id})
				if getPlanDetail.engagement=="1":
					getTodayHours=constants.getHour()
					getNewFolllowed=db.Followers.find({"sms":0,"insta_user_id":user._id,"status":1,"date_created": {"$gte": constants.LATER_HOURS(getTodayHours)}})
					if getNewFolllowed.count > 0:
						data = {"username": user.user_name ,"password": user.password}
						for x in getNewFolllowed:
							resMess = []
							myMess = db.Message.find({
										"insta_user_id":ObjectId(x["insta_user_id"]),
										"main_user_id":ObjectId(str(x["main_user_id"])),
										"status":1
										})
							for message in myMess:
								resMess.append({"id":str(message._id),"text":str(message.text)})
							if len(resMess):
								choice = random.choice(resMess)
								string = choice["text"]
								finalStr = ""
								start=0
								end=0
								for i in range(0,len(string)):
									if string[i]=="{":
										start=i+1
									elif string[i]=="}":
										end=i
									else:
										finalStr=finalStr+string[i]
									if start < end:
										handleStr=string[start:end]
										start=0
										end=0
										handleRes = random.choice(handleStr.split("|"))
										finalStr = finalStr.replace(handleStr,handleRes)
										# return jsonify(content)
								finalStr =finalStr.replace('@username',x["user_name"])
								data["user_id"]=x['user_id']
								data["text"]=finalStr
								res = fetchInsta.sendMessage((data))
								res = res['status']
								if res:
									resAddAct = functions.addAction(str(user.main_user_id),str(user._id),x['user_id'],"message",x["user_name"],str(finalStr),"")
									resAddSMS = functions.addSMS(str(user.main_user_id),str(user._id),str(choice["id"]),x['user_id'],str(finalStr))

									updateData={
											"sms":1
									}
									updateNow=db.Followers.find_and_modify({"_id":ObjectId(str(x["_id"]))},
																		{
																			"$set":updateData
																			})


	except Exception as e:
		print e
		result={
		"status":0,
		"message":str(e)
		}
	content["data"] = result
	return jsonify(content)



# Login In  API starts
# This will take input insta user id
@event_link.route('/getGrowthReport/', methods=['POST'])
def getGrowthReport():
	data = request.json
	result = {}
	getRes=[]
	getGraphData=[]
	countData = 0
	content = {"status":1,
				"message":"Success"}
	try:
		FollowersList=list(db.Followers.find({"insta_user_id":ObjectId(data["insta_user_id"]),
												"main_user_id":ObjectId(str(data["main_user_id"])),
												"$or":[{"status":1},{"status":3}]
												}))
		print len(FollowersList)
		if len(FollowersList)>0:
			countData = 1
			daysRange=range(0,int(data["days"]))
			rangeList=daysRange[::-1]
			myCount=0
			print "starts"
			for x in rangeList:				
				Followers=[y for y in FollowersList if y['date_created']<=constants.BEFORE_DAYS(x) ]
				myCount=len(Followers)
				if myCount>0:
					getRes.append({"date":str(constants.getDATE(constants.BEFORE_DAYS(x))),"count":myCount,"diff": 0})
					getGraphData.append(myCount)
			diffList=[{'diff':abs(getRes[getRes.index(y)]['count']-getRes[getRes.index(y)+1]['count'])} for y in getRes if int(getRes.index(y)+1)<len(getRes)]
			diffList.insert(0,{'diff': 0})
			result=[x.update(diff=diffList[getRes.index(x)]['diff']) for x in getRes]
		print "end"
	except Exception as e:
		print e
	content["data"] = getRes[::-1]
	content["countData"] = countData
	content["getGraphData"] = getGraphData
	# [::-1]
	return jsonify(content)




# Login In  API starts
# This will take input insta user id
@event_link.route('/getEngageReport/', methods=['POST'])
def getEngageReport():
	data = request.json
	result = {}
	myCount = 0
	totalLike=0
	totalComment=0
	per=0
	count=0
	perTotal=0
	averageEng=0
	averagePercentage=0
	resPost = []
	content = {"status":1,
				"message":"Success"}
	try:
		myCount=db.Followers.find({"insta_user_id":ObjectId(data["insta_user_id"]),
										"main_user_id":ObjectId(str(data["main_user_id"])),
										"$or":[{"status":1},{"status":3}]
										}).count()
		myPost=db.MyPost.find({"insta_user_id":ObjectId(data["insta_user_id"]),
										"main_user_id":ObjectId(str(data["main_user_id"]))})
		for x in myPost:
			resPost.append({"image":x.image,"like":x.like,"comment":x.comment,"total":str(int(x.like)+int(x.comment))})
			totalLike = totalLike+int(x.like)
			totalComment=totalComment+int(x.comment)
		allTotal=totalLike+totalComment
		for posts in resPost:
			count = count+1
			per= round((float(posts["total"])/float(allTotal))*100,2)
			perTotal = round((perTotal + per),2)
			posts["percentage"]=str(per)
		averagePercentage = str(round(perTotal/float(count),2))
		averageEng = str(int(float(totalLike)/float(count)))
	except Exception as e:
		print e
	content["resPost"] = resPost
	content["followers"] = myCount
	content["averagePercentage"] = averagePercentage
	content["averageEng"] = averageEng
	return jsonify(content)



# Login In  API starts
# This will take input insta user id
@event_link.route('/addMessage/', methods=['POST'])
def addMessage():
	data = request.json
	content = {"status":1,
				"message":"Success"}
	try:
		if data["id"].strip()!="":
			updateData={
					"text":str(data['text']),
			        "status":int(data['status'])
			}
			updateNow=db.Message.find_and_modify({"_id":ObjectId(str(data["id"]))},
												{
													"$set":updateData
													})
		else:
			check=db.Message.find({"insta_user_id":ObjectId(data["insta_user_id"]),
										"main_user_id":ObjectId(str(data["main_user_id"])),
										"status":1
										}).count()
			if check >= 5:
				content["message"] = "Max Limit reached"
			else:
				userObj = db.Message()
				userObj.main_user_id = ObjectId(str(data['main_user_id']))
				userObj.insta_user_id = ObjectId(str(data['insta_user_id']))
				userObj.text = str(data['text'])
				userObj.status = 1
				userObj.date_created = constants.FORMATTED_TIME()
				userObj.date_updated = constants.FORMATTED_TIME()
				userObj.save()
	except Exception as e:
		print e
	return jsonify(content)



# Login In  API starts
# This will take input insta user id
@event_link.route('/getMessage/', methods=['POST'])
def getMessage():
	data = request.json
	resMess=[]
	limit = 0
	content = {"status":1,
				"message":"Success"}
	try:
		myMess = db.Message.find({
						"insta_user_id":ObjectId(data["insta_user_id"]),
						"main_user_id":ObjectId(str(data["main_user_id"])),
						"$or":[{"status":1},{"status":2}]
						})
		limit=myMess.count()
		for x in myMess:
			resMess.append({"id":str(x._id),"text":str(x.text),"status":str(x.status)})
	except Exception as e:
		print e
	content["result"] = resMess
	content["limit"] = limit
	return jsonify(content)



# Login In  API starts
# This will take input insta user id
@event_link.route('/getAccountDash/', methods=['POST'])
def getAccountDash():
	data = request.json
	resMess=[]
	limit = 0
	speedFollow = "Fast"
	speedUnFollow = "Fast"
	currently = "Following"
	speedLike = "Fast"
	instaUser = ""
	content = {"status":1,
				"message":"Success"}
	try:
		checkPlan=functions.checkPlan(data["insta_user_id"])
		if checkPlan["planStatus"]=="Active":
			getGrowth = db.Growth.find_one({"insta_user_id":ObjectId(data["insta_user_id"]),
								"main_user_id":ObjectId(str(data["main_user_id"]))
								})
			getUnfoll = db.Unfollow.find_one({"insta_user_id":ObjectId(data["insta_user_id"]),
								"main_user_id":ObjectId(str(data["main_user_id"]))
								})
			getEng = db.Engagement.find_one({"insta_user_id":ObjectId(data["insta_user_id"]),
								"main_user_id":ObjectId(str(data["main_user_id"]))
								})
			if getGrowth:
				if getGrowth.speed == "2":
					speedFollow="Turbo"
				if getGrowth.speed == "3":
					speedFollow="Extreme"
			
			if getUnfoll:
				if getUnfoll.speed == "2":
					speedUnFollow="Turbo"
				if getUnfoll.speed == "3":
					speedUnFollow="Extreme"
			
			if getEng:
				if getEng.speed == "2":
					speedLike="Turbo"
				if getEng.speed == "3":
					speedLike="Extreme"

		# checkLastType = db.Actions.find({"insta_user_id":ObjectId(data["insta_user_id"]),
		# 									"main_user_id":ObjectId(str(data["main_user_id"])),
		# 										"$or":[{"action_type":"follow"},{"action_type":"unfollow"}]}).limit(1).sort("date_created",-1)
		# if checkLastType.count():
		# 	if checkLastType[0].action_type=="unfollow":
		# 		currently="Unfollowing"
		getInsta=db.UserInsta.find_one({"_id":ObjectId(data["insta_user_id"])})
		if getInsta:
			instaUser=getInsta.user_name
	except Exception as e:
		print e
	content["speedFollow"]=speedFollow
	content["speedUnFollow"]=speedUnFollow
	content["speedLike"]=speedLike
	content["checkPlan"]=checkPlan
	content["instaUser"]=instaUser
	return jsonify(content)



# Login In  API starts
# This will take input insta user id
@event_link.route('/sleepTest/', methods=['GET'])
def sleepTest():
	import time
	content={}
	for x in range(1,10):
		print "1"
		time.sleep(randint(3,10))
		print "2"
	return jsonify(content)

@event_link.route('/countPlus/', methods=['GET'])
def CountPlus():
	countObj=db.TickerCount.find_one({})
	if countObj:
		count=countObj.count+1
		updateData={
					"count":int(count),
			        "status":int(1),
			        "date_updated" : constants.FORMATTED_TIME()
			}
		updateNow=db.TickerCount.find_and_modify({"_id":ObjectId(str(countObj._id))},
											{
												"$set":updateData
												})
	# import time
	content = {	"status":1,
				"message":"success"}
	return jsonify(content)

@event_link.route('/get_count/', methods=['GET'])
def GetCount():
	countObj=db.TickerCount.find_one({})
	if countObj:
		print countObj.count
		count=countObj.count
		
		content = { "count":count,
					"status":1,
					"message":"Success"}
	return jsonify(content)

#Expiry mail API starts here
@event_link.route('/expire_mail/', methods=['GET'])
def Expire_mail():
	responseData={}
	content = {
               'status' : 0,	
               'message' : 'Invalid Call',
               }
	try:
		usersObj=db.Users.find({})
		if usersObj:
			for x in usersObj:
				InstausersObj=db.UserInsta.find({"main_user_id":ObjectId(x._id)})
				for y in InstausersObj:
					if 'proxy' not in y or y.proxy:
						myPlan = db.MyPlan.find_one({
							"insta_user_id":ObjectId(y._id),
							"status":0,
							})
						if myPlan:
							timeDiff=constants.FORMATTED_TIME() - myPlan.pla_end_date
							TotalDiffdays=timeDiff.days 
							ShouldSendMail=0
							if TotalDiffdays==1 and myPlan.email_status!=1:
								ShouldSendMail=1
								sock = urllib.urlopen(os.path.join(APP_STATIC, 'one_day_after_account_expired.html'))
								htmlSource = sock.read()
								sock.close()
								subject = "Kibi- Account Expired"
							if TotalDiffdays==2 and myPlan.email_status!=2:
								ShouldSendMail=2
								sock = urllib.urlopen(os.path.join(APP_STATIC, 'two_days_after_account_expired.html'))
								htmlSource = sock.read()
								sock.close()
								subject = "Get Better IG Engagement"
							if TotalDiffdays==3 and myPlan.email_status!=3:
								ShouldSendMail=3
								sock = urllib.urlopen(os.path.join(APP_STATIC, 'three_days_after_account_expired.html'))
								htmlSource = sock.read()
								sock.close()
								subject = "KIBI Account Discontinuing?"
							if TotalDiffdays==4 and myPlan.email_status!=4:
								ShouldSendMail=4
								sock = urllib.urlopen(os.path.join(APP_STATIC, 'four_days_after_account_expired.html'))
								htmlSource = sock.read()
								sock.close()
								subject = "KIBI Account Deactivation"
							if ShouldSendMail:
								htmlSource = string.replace(htmlSource, "[USERNAME]", str(x.first_name).capitalize())
								htmlSource = string.replace(htmlSource, "[INSTANAME]", str(y.user_name))
								res = sendMail.sendEmail("support@kibisocial.com",str(x.email),subject,htmlSource,"")
								updateData1={
							        "email_status":int(ShouldSendMail),
							        "date_updated" : constants.FORMATTED_TIME()
									}
								updateNow=db.MyPlan.find_and_modify({"_id":ObjectId(str(myPlan._id))},
																	{
																		"$set":updateData1
																		})
							if TotalDiffdays>=5:
								updateData={
								"proxy":None
								}
								updateUserInsta=db.UserInsta.find_and_modify({"_id":ObjectId(y._id)},
															{
																"$set":updateData
																})
					MyPlanObj_1=db.MyPlan.find_one({"main_user_id":ObjectId(x._id),"insta_user_id":ObjectId(y._id),"pla_end_date":{"$lte": constants.FORMATTED_TIME()},"status":int(1)})
					if MyPlanObj_1:
						#send Mail of expired here
						# Email send start
						sock = urllib.urlopen(os.path.join(APP_STATIC, 'account_expired.html'))
						htmlSource = sock.read()
						sock.close()
						subject = "Kibi- Account Expired"
						htmlSource = string.replace(htmlSource, "[USERNAME]", str(x.first_name.capitalize()))
						# htmlSource = string.replace(htmlSource, "[INSTANAME]", str(y.user_name))
						res = sendMail.sendEmail("help@healora.com",str(x.email),subject,htmlSource,"")
						# "expired mail sent to", x.email
						# Email send end
						updateData1={
							        "status":int(0),
							        "email_status":int(0),
							        "date_updated" : constants.FORMATTED_TIME()
									}
						updateNow=db.MyPlan.find_and_modify({"_id":ObjectId(str(MyPlanObj_1._id))},
															{
																"$set":updateData1
																})
						# Add Event Here
						resEvent=functions.addMyEvent(x._id,y._id,"6","Account Stopped","Your plan has expired! Purchase a plan on the billing tab to restart your account.","fal fa-clock fa-started custom-fa fa-clock")
						# Event Added
					AccountEmailsObj=db.AccountEmails.find_one({"main_user_id":ObjectId(x._id),"insta_user_id":ObjectId(y._id)})
					if not AccountEmailsObj:
						AccEmailsNewObj = db.AccountEmails()
						AccEmailsNewObj.main_user_id = ObjectId(x._id)
						AccEmailsNewObj.insta_user_id = ObjectId(y._id)
						AccEmailsNewObj.last_send_date = constants.BEFORE_DAYS(30)
						AccEmailsNewObj.soon_expire_mail_status = 0
						AccEmailsNewObj.status = 1
						AccEmailsNewObj.date_created = constants.FORMATTED_TIME()
						AccEmailsNewObj.date_updated = constants.FORMATTED_TIME()
						AccEmailsNewObj.save()
						AccountEmailsObj=db.AccountEmails.find_one({"main_user_id":ObjectId(x._id),"insta_user_id":ObjectId(y._id)})
					timeDifference=constants.FORMATTED_TIME() - AccountEmailsObj.last_send_date 
					Totaldays=timeDifference.days 
					if Totaldays>=30:
					# if AccountEmailsObj.soon_expire_mail_status==0:
						MyPlanObj=db.MyPlan.find_one({"main_user_id":ObjectId(x._id),"insta_user_id":ObjectId(y._id),"pla_end_date":{"$lte": constants.AFTER_DAYS(2)},"status":int(1)})
						if MyPlanObj:
							# "expired soon mail sent to", x.email
							updateData={
											"soon_expire_mail_status":int(1),
									        "status":int(1),
									        "last_send_date":constants.FORMATTED_TIME(),
									        "date_updated" : constants.FORMATTED_TIME()
											}
							updateNow=db.AccountEmails.find_and_modify({"_id":ObjectId(str(AccountEmailsObj._id))},
																{
																	"$set":updateData
																	})
							#send Mail of expired soon here
							# Email send start
							sock = urllib.urlopen(os.path.join(APP_STATIC, 'trial_expire_Soon.html'))
							htmlSource = sock.read()
							sock.close()
							subject = "Kibi- Account Plan expiry"
							htmlSource = string.replace(htmlSource, "[USERNAME]", str(x.first_name).capitalize())
							htmlSource = string.replace(htmlSource, "[INSTANAME]", str(y.user_name))
							res = sendMail.sendEmail("help@healora.com",str(x.email),subject,htmlSource,"")
							# Email send end
			content = {
				'status' : 1,	
				'message' : 'Success'
			}
	except Exception as e:
		print e
		content["message"] = "Something Went Wrong, Please Try after Some Time"
	content['data'] = responseData
	return jsonify(content)

# createTargetingCsv API starts here
@event_link.route('/createTargetingCsv/', methods=['POST'])
def createTargetingCsv():
	data = request.json['data']
	try:
		fieldnames=['AccPerc','actions','folBackPerc','followbacks','user_name']
		list_data=[]
		requestdata=data
		res=False
		if data:
			for x in range(len(data)):
				dictionary={}
				for key in fieldnames:
					val=requestdata[x][key]
					dictionary[str(key)]=str(val)
				list_data.append(dictionary)
			filename="targeting-"+functions.randStr(8)+'.csv'
			res=functions.createCsvFile(fieldnames,list_data,filename)
			link=functions.tos3(filename)
			return jsonify({'status':1,'link':link})			
		return jsonify({"status":0,	"message":"Invalid data"})
	except Exception as e:
		print e
	return jsonify({"status":0,"message":"Something Went Wrong, Please Try after Some Time"})

# createTargetingCsv API starts here
@event_link.route('/createWhitelistCsv/', methods=['POST'])
def createWhitelistCsv():
	data = request.json['data']
	try:
		fieldnames=['name','date']
		list_data=[]
		requestdata=data
		res=False
		if data:
			for x in range(len(data)):
				dictionary={}
				for key in fieldnames:
					val=requestdata[x][key]
					dictionary[str(key)]=str(val)
				list_data.append(dictionary)
			filename="whitelist-"+functions.randStr(8)+'.csv'
			print filename
			res=functions.createCsvFile(fieldnames,list_data,filename)
			link=functions.tos3(filename)
			return jsonify({'status':1,'link':link})			
		return jsonify({"status":0,	"message":"Invalid data"})
	except Exception as e:
		print e
	return jsonify({"status":0,"message":"Something Went Wrong, Please Try after Some Time"})


# changeInstaStatus API starts here
@event_link.route('/changeInstaStatus/', methods=['POST'])
def ChangeInstaStatus():
	data = request.json
	res=[]
	content = {"status":1,
				"message":"Success"}
	res=functions.functionChangeInstaStatus(data)
	# p = Process(target=functionChangeInstaStatus, args=(data,))
	# p.start()
	content["data"] = res
	return jsonify(content)

# changeInstaStatus API starts here
@event_link.route('/sendemailNowTest/', methods=['GET'])
def sendemailNowTest():
	content={}
	userInsta=db.UserInsta.find({"status":int(5)})
	print userInsta.count()
	print "kunal"
	for x in userInsta:
		user = db.Users.find_one({"_id":x.main_user_id})
		
		sock = urllib.urlopen(os.path.join(APP_STATIC, 'account_reconnect_message.html'))
		html = sock.read()
		sock.close()
		html = string.replace(html, "[USERNAME]", str(str(user.first_name)+" "+str(user.last_name)).strip())
		html = string.replace(html, "[INSTANAME]", str(x.user_name).strip())
		subject = "Kibi - Support: KIBI email is fixed"
		# html= "Your account needs to be Reconnected. Please reconnect your account from dashboard now, so your account doesn't stop running."
		# res = sendMail.sendEmail("support@kibisocial.com",str(user.email),subject,html,"")
		res = sendMail.sendEmail("support@kibisocial.com",str("mongaku@udmercy.edu"),subject,html,"")
	return jsonify(content)

# InstaStatusMail  cron-API starts here
@event_link.route('/InstaStatusMail/', methods=['GET'])
def InstaStatusMail():
	# data = request.json
	content = {"status":1,
				"message":"Success"}
	try:
		instaObj =db.UserInsta.find({"$or":[{"status":2},{"status":3},{"status":4}]
										})
		if instaObj.count():
			for x in instaObj:
				user=db.Users.find_one({"_id":ObjectId(str(x.main_user_id))})
				diff_days=0
				html=""
				if int(x.status)==2:
					diff_time=constants.FORMATTED_TIME()-x.date_updated
					diff_days=diff_time.days
					if diff_days==2:
						#add mail
						subject = "Kibi - Support"
						sock = urllib.urlopen(os.path.join(APP_STATIC, 'account_wait.html'))
						html = sock.read()
						html = string.replace(html, "[DAYS]", str("1-2").strip())
						sock.close()
						#added mail
					
				elif int(x.status)==3:
					diff_time=constants.FORMATTED_TIME()-x.date_updated
					diff_days=diff_time.days
					if diff_days==2:
						#add mail
						subject = "Kibi - Support"
						sock = urllib.urlopen(os.path.join(APP_STATIC, 'account_reconnect.html'))
						html = sock.read()
						sock.close()
						#added mail

				elif int(x.status)==4:
					diff_time=constants.FORMATTED_TIME()-x.date_updated
					diff_days=diff_time.days
					if diff_days==2:
						#add mail
						subject = "Kibi - Support"
						sock = urllib.urlopen(os.path.join(APP_STATIC, 'account_follow_wait.html'))
						html = sock.read()
						sock.close()
						#added mail
				if diff_days==2:
					html = string.replace(html, "[USERNAME]", str(user.first_name+" "+user.last_name).strip())
					html = string.replace(html, "[INSTANAME]", str(x.user_name).strip())
					res = sendMail.sendEmail("support@kibisocial.com",str(user.email),subject,html,"")
					#sent mail
				elif diff_days>=3:
					if int(x.status)==2 or int(x.status)==4:
						updateData ={
										"status":int(1),
										"date_updated":constants.FORMATTED_TIME()
										}
						UserInstaObj=db.UserInsta.find_and_modify({"main_user_id":ObjectId(str(x.main_user_id)),"_id":ObjectId(str(x._id))},
																{
																"$set":updateData
																})
						# Add Event Here
						resEvent=functions.addMyEvent(x.main_user_id,x._id,"1","Account started Following","Your account started following users based on the targets and settings you selected.","fas fa-arrow-up arrowup")
						# Event Added
		else:
			content = {"status":0,
						"message":"None"}
	except Exception as e:
		print e
		content = {"status":1,
					"message":"Something Went Wrong, Please Try after Some Time"}

	return jsonify(content)

#affiliateDashboard API starts here
@event_link.route('/affiliateDashboard/', methods=['POST'])
def AffiliateDashboard():
	data = request.json
	responseData={"status":0,
				"message":"No data"}
	customers=[]
	performance=[]
	perfDict={}
	dictionary={}
	totalCommission=0.0
	content = {"status":1,
				"message":"Success"}
	try:
		AffiliateObj=db.Affiliates.find_one({'_id':ObjectId(data['affiliateId'])})
		if AffiliateObj:
			AffiliatesTokenObj=db.AffiliatesToken.find_one({'affiliate_id':ObjectId(AffiliateObj._id)})
			responseData['token']=str(AffiliatesTokenObj.token)
			CustomersObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id)})

			for c in CustomersObj:
				date=c.date_created.strftime("%d/%m/%Y")
				dictionary['name']=str(c.first_name+' '+c.last_name)
				dictionary['date']=date
				dictionary['_id']=str(c._id)
				# OrderCount=db.Orders.find({"main_user_id":ObjectId(c._id),"status":str(1)})
				# for order in OrderCount:
				# 	print type(AffiliatesTokenObj.percentage)
				# 	Commission=float(order.total_amount)*float(AffiliatesTokenObj.percentage/100)
				# 	totalCommission=totalCommission+float(Commission)
				customers.append(dictionary.copy())
			responseData['customers']=customers
			responseData["status"]=1
			responseData["message"]="Success"
			responseData["affiliate_id"]=str(AffiliateObj._id)
			print "sud"

			week_start=constants.Week_Start_Date(constants.FORMATTED_TIME())
			if week_start!=constants.FORMATTED_TIME():
				CustomersNewObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),
												"$and":[{"date_created":{"$gte": week_start}},{"date_created":{"$lte": constants.FORMATTED_TIME()}}]})
			else:
				Date=constants.FORMATTED_TIME_CREATE(constants.FORMATTED_TIME().strftime("%Y-%m-%d 00:00:00"))
				CustomersNewObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),"date_created":{"$gte": Date}})
			if CustomersNewObj.count():
				perfDict['customers']=CustomersNewObj.count()
				for c in CustomersNewObj:
					OrderObj=db.Orders.find({"main_user_id":ObjectId(c._id),"status":str(1)})
					for order in OrderObj:
						Commission=float(order.total_amount)*float(AffiliatesTokenObj.percentage/100)
						totalCommission=totalCommission+float(Commission)
					customer_id=c._id
				OrderCount=db.Orders.find({"main_user_id":ObjectId(customer_id),"status":str(1)}).count()
				perfDict['orders']=OrderCount
				perfDict['totalCommission']=totalCommission
				perfDict['clicks']=int(AffiliatesTokenObj.clicks)
				performance.append(perfDict.copy())

			responseData['performance']=performance
		else:
			content = {"status":1,
						"message":"Invalid Token"}
	except Exception as e:
		print e
		content = {"status":1,
					"message":"Something Went Wrong, Please Try after Some Time"}
	content['data']=responseData
	return jsonify(content)

#affiliatesCustomers API starts here
@event_link.route('/affiliatesCustomers/', methods=['POST'])
def AffiliatesCustomers():
	data = request.json
	responseData={"status":0,
				"message":"No data"}
	customers=[]
	dictionary={}
	totalCommission=0.0
	try:
		AffiliateObj=db.Affiliates.find_one({'_id':ObjectId(data['affiliateId'])})
		AffiliatesTokenObj=db.AffiliatesToken.find_one({'affiliate_id':ObjectId(AffiliateObj._id)})
		if AffiliateObj:
			CustomersObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id)})
			for c in CustomersObj:
				date=c.date_created.strftime("%d/%m/%Y")
				dictionary['name']=str(c.first_name+' '+c.last_name)
				dictionary['date']=date
				dictionary['_id']=str(c._id)
				OrderObj=db.Orders.find({"main_user_id":ObjectId(c._id),"status":str(1)})
				for order in OrderObj:
					Commission=float(order.total_amount)*float(AffiliatesTokenObj.percentage/100)
					totalCommission=totalCommission+float(Commission)
				myplanObj=db.MyPlan.find_one({"main_user_id":ObjectId(c._id),"status":1})
				active=0
				if myplanObj:
					active=myplanObj.status
				dictionary['active']=active
				dictionary['commision']=totalCommission
				customers.append(dictionary.copy())
			responseData['customers']=customers
			responseData["status"]=1
			responseData["message"]="Success"

	except Exception as e:
		print e
	return jsonify(responseData)

#affiliateCustomerInfo API starts here
@event_link.route('/affiliateCustomerInfo/', methods=['POST'])
def AffiliateCustomerInfo():
	data = request.json
	responseData={"status":0,
				"message":"No data"}
	customer={}
	totalCommission=0.0
	try:
		CustomersObj=db.Users.find_one({"affiliate_status":1,"_id":ObjectId(data['customerId'])})
		AffiliatesTokenObj=db.AffiliatesToken.find_one({'affiliate_id':ObjectId(CustomersObj.affiliate_id)})
		if CustomersObj:
			date=CustomersObj.date_created.strftime("%A %dth %B %Y")
			customer['name']=str(CustomersObj.first_name+' '+CustomersObj.last_name)
			customer['date']=date
			customer['_id']=str(CustomersObj._id)
			myplanObj=db.MyPlan.find_one({"main_user_id":ObjectId(CustomersObj._id),"status":1})
			active=0
			if myplanObj:
				active=myplanObj.status
			customer['active']=active
			OrderObj=db.Orders.find({"main_user_id":ObjectId(CustomersObj._id),"status":str(1)})
			for order in OrderObj:
				Commission=float(order.total_amount)*float(AffiliatesTokenObj.percentage/100)
				totalCommission=totalCommission+float(Commission)
			customer['commision']=totalCommission				
			if totalCommission:
				customer['commision_status']=1
			else:
				customer['commision_status']=1				
			responseData['customer']=customer
			responseData["status"]=1
			responseData["message"]="Success"

	except Exception as e:
		print e
	return jsonify(responseData)

#affiliatesPerformance API starts here
@event_link.route('/affiliatesPerformance/', methods=['POST'])
def AffiliatesPerformance():
	data = request.json
	responseData={"status":0,
				"message":"No data"}
	performance=[]
	perfDict={}
	totalCommission=0.0
	try:
		AffiliateObj=db.Affiliates.find_one({'_id':ObjectId(data['affiliateId'])})
		if AffiliateObj:
			AffiliatesTokenObj=db.AffiliatesToken.find_one({'affiliate_id':ObjectId(AffiliateObj._id)})
			CustomersObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),"date_created":{"$lte": constants.FORMATTED_TIME()}})
			if CustomersObj.count():
				for c in CustomersObj:
					customer_id=c._id
					break
				OrderCount=db.Orders.find({"main_user_id":ObjectId(customer_id),"status":str(1)})
				perfDict['customers']=CustomersObj.count()
				perfDict['orders']=OrderCount.count()
				for order in OrderCount:
					Commission=float(order.total_amount)*float(AffiliatesTokenObj.percentage/100)
					totalCommission=totalCommission+float(Commission)
				perfDict['totalCommission']=totalCommission
				AffiliatesTokenObj=db.AffiliatesToken.find_one({'affiliate_id':ObjectId(AffiliateObj._id)})
				perfDict['clicks']=int(AffiliatesTokenObj.clicks)
				performance.append(perfDict.copy())
			responseData['performance']=performance
			responseData["status"]=1
			responseData["message"]="Success"

	except Exception as e:
		print e
	return jsonify(responseData)

#TokenClick API starts here
@event_link.route('/tokenClick/', methods=['POST'])
def TokenClick():
	data = request.json
	content = {"status":1,
				"message":"Success"}
	clicks=0
	try:
		AffiliatesTokenObj=db.AffiliatesToken.find_one({'token':str(data['token'])})
		if AffiliatesTokenObj:
			clicks=int(AffiliatesTokenObj.clicks)
			clicks+=1
			updateData={
			"clicks":clicks
			}
			updateAffiliatesToken=db.AffiliatesToken.find_and_modify({"_id":ObjectId(AffiliatesTokenObj._id)},
				{
				"$set":updateData
				})
		else:
			content = {"status":1,
						"message":"Invalid Token"}
	except Exception as e:
		print e
		content = {"status":1,
					"message":"Something Went Wrong, Please Try after Some Time"}
	return jsonify(content)

#Tracking API starts here
@event_link.route('/affiliatesPerformanceWithDate/', methods=['POST'])
def AffiliatePerformanceWithDate():
	data = request.json
	responseData={"status":0,
				"message":"No data"}
	performance=[]
	perfDict={}
	dictionary={}
	count=0
	totalCommission=0.0
	content = {"status":1,
				"message":"Success"}
	try:
		AffiliateObj=db.Affiliates.find_one({'_id':ObjectId(data['affiliateId'])})
		if AffiliateObj:
			AffiliatesTokenObj=db.AffiliatesToken.find_one({'affiliate_id':ObjectId(AffiliateObj._id)})
			responseData["status"]=1
			responseData["message"]="Success"
			
			if int(data['date'])==1:	#Today
				Date=constants.FORMATTED_TIME_CREATE(constants.FORMATTED_TIME().strftime("%Y-%m-%d 00:00:00"))
				CustomersNewObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),
												"date_created":{"$gte": Date}})
			elif int(data['date'])==2:	#ThisWeek
				week_start=constants.Week_Start_Date(constants.FORMATTED_TIME())
				if week_start!=constants.FORMATTED_TIME():
					CustomersNewObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),
												"$and":[{"date_created":{"$gte": week_start}},{"date_created":{"$lte": constants.FORMATTED_TIME()}}]})
				else:
					Date=constants.FORMATTED_TIME_CREATE(constants.FORMATTED_TIME().strftime("%Y-%m-%d 00:00:00"))
					CustomersNewObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),"date_created":{"$gte": Date}})
			elif int(data['date'])==3:	#lastweek
				last_week_dates=constants.Last_Week()
				CustomersNewObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),
												"$and":[{"date_created":{"$gte": last_week_dates[0]}},{"date_created":{"$lte": last_week_dates[1]}}]})
			elif int(data['date'])==4:	#ThisMonth
				MonthStartDate=str(constants.FORMATTED_TIME().strftime("%Y-%m-01 %H:%M:%S"))
				MonthStartDate=constants.FORMATTED_TIME_CREATE(MonthStartDate)
				CustomersNewObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),
												"$and":[{"date_created":{"$gte": MonthStartDate}},{"date_created":{"$lte": constants.FORMATTED_TIME()}}]})
			elif int(data['date'])==5:	#lastMonth
				MonthStartDate=constants.LastMonth()[0]
				MonthLastDate =constants.LastMonth()[1]
				CustomersNewObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),
												"$and":[{"date_created":{"$gte": MonthStartDate}},{"date_created":{"$lte": MonthLastDate}}]})
			elif int(data['date'])==6:	#lastNdays
				beforedays=int(data['lastNdays'])
				Date=constants.BEFORE_DAYS(int(beforedays)-1)
				CustomersNewObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),
												"$and":[{"date_created":{"$gte": Date}},{"date_created":{"$lte": constants.FORMATTED_TIME()}}]})
			elif int(data['date'])==7:	#ThisYear
				YearStartDate=constants.FORMATTED_TIME().strftime("%Y-01-01 00:00:00")
				YearStartDate=constants.FORMATTED_TIME_CREATE(YearStartDate)
				CustomersNewObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),
												"$and":[{"date_created":{"$gte": YearStartDate}},{"date_created":{"$lte": constants.FORMATTED_TIME()}}]})
			elif int(data['date'])==8:	#lastyear
				FirstDate=constants.LastYear()[0]
				LastDate=constants.LastYear()[1]
				CustomersNewObj=db.Users.find({"affiliate_status":1,"affiliate_id":ObjectId(AffiliateObj._id),
												"$and":[{"date_created":{"$gte": FirstDate}},{"date_created":{"$lte": LastDate}}]})
			count=CustomersNewObj.count()
			if count:
				for c in CustomersNewObj:
					customer_id=c._id
				OrderCount=db.Orders.find({"main_user_id":ObjectId(customer_id),"status":str(1)})
				perfDict['customers']=count
				perfDict['orders']=OrderCount.count()
				for order in OrderCount:
					Commission=float(order.total_amount)*float(AffiliatesTokenObj.percentage/100)
					totalCommission=totalCommission+float(Commission)
				perfDict['totalCommission']=totalCommission
				perfDict['clicks']=int(AffiliatesTokenObj.clicks)
				performance.append(perfDict.copy())
			responseData['performance']=performance
		else:
			content = {"status":1,
						"message":"Invalid Token"}
	except Exception as e:
		print e
		content = {"status":1,
					"message":"Something Went Wrong, Please Try after Some Time"}
	content['data']=responseData
	return jsonify(content)

# test_mailer In  API starts
@event_link.route('/test_mailer/', methods=['GET'])
def test_mailer():
	data = request.json
	content = {"status":1,
				"message":"Success"}
	try:
		mailbody=['account_follow_wait','account_reconnect','account_stopped1','account_wait','affiliate','invoice','register','ticket','trial_expire_Soon','account_expired','one_day_after_account_expired','two_days_after_account_expired','three_days_after_account_expired','four_days_after_account_expired']
		for body in mailbody:
			subject = "sample: "+str(body)
			sock = urllib.urlopen(os.path.join(APP_STATIC, body+'.html'))
			html = sock.read()
			sock.close()
			#added mail
			if "[USERNAME]" in html:
				html = string.replace(html, "[USERNAME]", (str('Luke').strip()).capitalize())
			if "[INSTANAME]" in html:
				html = string.replace(html, "[INSTANAME]", str('luketheis').strip())
			res = sendMail.sendEmail("support@kibisocial.com",'sdudhe92@gmail.com',subject,html,"")
	except Exception as e:
		print e
		content = {"status":0,
					"message":"Something Went Wrong, Please Try after Some Time"}
	return jsonify(content)

# test_mailer In  API starts
@event_link.route('/checkingFor_Actions/', methods=['GET'])
def checkingFor_Actions():
	# data = request.json
	content = {"status":1,
				"message":"Success"}
	try:
		result=[]
		for user in db.UserInsta.find({"$or":[{"status":1},{"status":2},{"status":0},{"status":4}]}):
			if db.MyPlan.find_one({"main_user_id":user.main_user_id,"insta_user_id":user._id,"status":1}):
				Ac=db.Actions.find({"main_user_id":user.main_user_id,"insta_user_id":user._id,"date_created": {"$gt": constants.LATER_MIN(2880)}}).count()
				if not Ac :
					muser=db.Users.find_one({"_id":ObjectId(str(user.main_user_id))})
					# add mail
					sock = urllib.urlopen(os.path.join(APP_STATIC, 'account_reconnect_message_for_no_actions.html'))
					html = sock.read()
					sock.close()
					html = string.replace(html, "[USERNAME]", str(str(muser.first_name)).strip())
					html = string.replace(html, "[INSTANAME]", str(user.user_name).strip())
					subject = "Kibi - Support:- Urgent: Disconnected KIBI Account."
					# html= "Your account needs to be Reconnected. Please reconnect your account from dashboard now, so your account doesn't stop running."
					res = sendMail.sendEmail("support@kibisocial.com",str(muser.email),subject,html,"")
					#sent mail			
					# Add Event Here
					resEvent=functions.addMyEvent(muser._id,user._id,"8","Account Stopped","Your account needs to be Reconnected. Please reconnect your account from dashboard now, so your account doesn't stop running.","fal fa-pause fa-started custom-fa fa-pause")
					# Event Added
					updateData ={
							"status":int(3),
							"date_updated":constants.FORMATTED_TIME()
							}
					UserInstaObj=db.UserInsta.find_and_modify({"_id":ObjectId(str(user._id))},
															{
															"$set":updateData
															})
	except Exception as e:
		print e
		content = {"status":0,
					"message":"Something Went Wrong, Please Try after Some Time"}
	# content['data']=result
	return jsonify(content)

# test_mailer In  API starts
@event_link.route('/proxy_list/', methods=['GET'])
def proxy_list():
	content = {"status":1,
				"message":"Success"}
	try:
		path=os.path.realpath('')
		print path
		f=open(path+"/proxy.txt", "a+")
		for proxy in db.Proxies.find({}):
			f.write("\n"+proxy.proxy)
		f.close()
		f= open(path+"/proxy.txt","r")
		contents =f.read()
		f.close()
	except Exception as e:
		print e
		content = {"status":0,
					"message":"Something Went Wrong, Please Try after Some Time"}
	return jsonify(content)