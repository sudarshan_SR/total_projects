__author__ = 'Kunal Monga'
client_secret = "feb6d643d6724711957580c1c706947a"
'''
This file will be used across the project for calling functions that are used at various places
'''
from flask import Blueprint, request, jsonify


# Import the database object from the main app module
from app import db, constants, mail, APP_ROOT
import json, os
from app.model import model
from flask_mail import Message
import imageio
# imageio.plugins.ffmpeg.download()
from moviepy.editor import *
from InstagramAPI import InstagramAPI
from bson.objectid import ObjectId

def loginUser(username,password):
	content = {
               'status' : 1,
               'message' : 'Login Success',
               }
	api = InstagramAPI(username, password)
	checkLogin = api.login()
	if (checkLogin):
	    api.getProfileData()
	    content['data']=api.LastJson
	else:
		checkLoginAgain = api.login(True)
		if (checkLoginAgain):
			api.getProfileData()
			content['data']=api.LastJson
		else:
			content['data']=api.LastJson
			content['status']=0
			content['message']="Login Failure"
			if "challenge" in content['data']:
				content['status']=2	
				content['message']="Challenge Required"
				content['url']=content['data']['challenge']['url']
	return content


def search(username,password,text,type):
	content = {
               'status' : 1,
               'message' : 'Search Success',
               }
	api = InstagramAPI(username, password)
	if (api.login()):
		if type=="user":
			api.searchUsers(text)
		elif type=="tags":
			api.searchTags(text)
		elif type=="location":
			api.searchLocation(text)
		else:
			api.searchUsers(text)
		content['data']=api.LastJson
	else:
	    content['status']=0
	    content['message']="Search Failure"
	return content


# Followers
def getTag(username,password,userId):
	api = InstagramAPI(username, password)
	api.login()
	data = api.getUserTags(userId)
	return data

# Followers
def getMyFollowers(username,password):
	api = InstagramAPI(username, password)
	api.login()
	user_id = api.username_id
	followers = getFollowers(user_id,api)
	return followers

def getAnyFollowers(username,password,user_id):
	api = InstagramAPI(username, password)
	api.login()
	followers=getFollowers(user_id,api)
	return followers

def getFollowers(user_id,api):
	followers = []
	next_max_id = True
	while next_max_id:
		if next_max_id is True:
			next_max_id = ''
		api.getUserFollowers(user_id, maxid=next_max_id)
		followers.extend(api.LastJson.get('users', []))
		next_max_id = api.LastJson.get('next_max_id', '')
	return followers

def getHashtagfeeds(name,api):
	users = []
	next_max_id = True
	while next_max_id:
		if next_max_id is True:
			next_max_id = ''
		api.getHashtagFeed(name, maxid=next_max_id)
		users.extend(api.LastJson.get('items', []))
		next_max_id = False
	return users

def getLocationFeeds(id,api):
	users = []
	next_max_id = True
	while next_max_id:
		if next_max_id is True:
			next_max_id = ''
		api.getLocationFeed(id, maxid=next_max_id)
		print api.LastJson
		users.extend(api.LastJson.get('items', []))
		next_max_id = False
	return users

def follow(user_id,api):
	result = api.follow(user_id)
	print result
	return result

def unfollow(user_id,api):
	result = api.unfollow(user_id)
	print result
	return result

def getAnyFollowings(username,password,user_id):
	api = InstagramAPI(username, password)
	api.login()
	data=getFollowings(user_id,api)
	return data

def getFollowings(user_id,api):
	data = []
	next_max_id = True
	while next_max_id:
		if next_max_id is True:
			next_max_id = ''
		api.getUserFollowings(user_id, maxid=next_max_id)
		data.extend(api.LastJson.get('users', []))
		next_max_id = api.LastJson.get('next_max_id', '')
	return data


def getMedia(api):
	feed = []
	next_max_id = True
	while next_max_id:
		if next_max_id is True:
			next_max_id = ''
		api.getSelfUserFeed(maxid=next_max_id)
		feed.extend(api.LastJson.get('items', []))
		next_max_id = False
	return feed

def getMediaOthers(user_id,api):
	feed = []
	next_max_id = True
	while next_max_id:
		if next_max_id is True:
			next_max_id = ''
		api.getUserFeed(user_id,maxid=next_max_id)
		feed.extend(api.LastJson.get('items', []))
		next_max_id = False
	return feed


def like(media_id,api):
	result = api.like(media_id)
	return result

def userUnfollow(user_id,api):
	result = api.unfollow(user_id)
	return result

def countUserFollowings(api):
	result = api.getTotalSelfFollowings()
	return result

def directMessage(text,user_id,api):
	result = api.direct_message(text,user_id)
	return result

# def getMediaSelf(api):
# 	feed = []
# 	feed = api.getSelfSavedMedia()
# 	print feed
# 	return feed
