__author__ = 'Kunal Monga'
client_secret = "feb6d643d6724711957580c1c706947a"
'''
This file will be used across the project for calling functions that are used at various places
'''
from flask import Blueprint, request, jsonify


# Import the database object from the main app module
from app import db, constants, mail, APP_ROOT
import json, os
from app.model import model
from flask_mail import Message
from instagram.client import InstagramAPI
from bson.objectid import ObjectId



def getUserDetail(access_token,user_id):
	api = InstagramAPI(access_token=access_token, client_secret='feb6d643d6724711957580c1c706947a')
	data = api.user(user_id)
	data = json.loads(json.dumps(vars(data)))
	return data

def getUserFollows(access_token,user_id):
	api = InstagramAPI(access_token=access_token, client_secret='feb6d643d6724711957580c1c706947a')
	data = api.user_followed_by(user_id)
	data = json.loads(json.dumps(vars(data)))
	return data